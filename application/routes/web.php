<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('login', 'AuthController@login');
    $router->post('register', 'AuthController@register');
    $router->post('logout', 'AuthController@logout');

    $router->post('google', 'AuthController@google');
    $router->post('facebook', 'AuthController@facebook');
    $router->post('apple', 'AuthController@apple');
    $router->post('deletion', 'AuthController@deletion');
});

$router->group(['prefix' => 'mail'], function () use ($router) {
    $router->get('verification/{id}', 'MailController@validVerification');
    $router->post('reset/password', 'MailController@linkResetPwd');
    $router->get('reset/valid/{id}', 'MailController@validLinkResetPwd');


    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('verification', 'MailController@sendVerification');
        $router->post('otp', 'MailController@sendOTP');
        $router->post('otp/{id}', 'MailController@validOTP');
    });
});

$router->group(['prefix' => 'message'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->group(['prefix' => 'wa'], function () use ($router) {
            $router->post('otp', 'MessageController@sendWaOTP');
            $router->post('otp/{id}', 'MessageController@validWaOTP');
        });
    });
});

$router->group(['prefix' => 'erp'], function () use ($router) {
});

$router->group(['prefix' => 'more', 'namespace' => 'More'], function () use ($router) {


    $router->group(['prefix' => 'setting'], function () use ($router) {
        $router->get('', 'SettingController@index');
        $router->get('import', 'SettingController@import');
        $router->get('wishlist', 'SettingController@wishlist');
        $router->get('addtocart', 'SettingController@addtocart');
        $router->post('reminder', 'SettingController@reminder');
    });

    $router->group(['prefix' => 'event'], function () use($router) {
        $router->post('index', 'EventController@index');
    });

    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('reset', 'UserController@resetPassword');

        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->post('show', 'UserController@show');
            $router->get('check', 'UserController@checkingComplete');
            $router->get('moreversary', 'UserController@moreversary');
            $router->post('store', 'UserController@store');
            $router->put('update', 'UserController@update');
            $router->put('email', 'UserController@email');
            $router->delete('destroy', 'UserController@destroy');
            $router->post('password', 'UserController@password');
            $router->post('have/password', 'UserController@havePassword');
            $router->post('sync/product', 'UserController@synchingProductHistory');
            $router->post('set-location', 'UserController@saveLocation');

            $router->group(['prefix' => 'address'], function () use ($router) {
                $router->post('index', 'UserAddressController@index');
                $router->post('store', 'UserAddressController@store');
                $router->put('update', 'UserAddressController@update');
                $router->post('show', 'UserAddressController@show');
                $router->delete('destroy', 'UserAddressController@destroy');
            });

            $router->group(['prefix' => 'dropshippers'], function () use ($router) {
                $router->post('index', 'UserDropshipperController@index');
                $router->post('store', 'UserDropshipperController@store');
                $router->put('update', 'UserDropshipperController@update');
                $router->post('show', 'UserDropshipperController@show');
                $router->delete('destroy', 'UserDropshipperController@destroy');
            });

            $router->group(['prefix' => 'banks'], function () use ($router) {
                $router->post('index', 'UserBankController@index');
                $router->post('store', 'UserBankController@store');
                $router->post('show', 'UserBankController@show');
                $router->put('update', 'UserBankController@update');
                $router->delete('destroy', 'UserBankController@destroy');
            });

            $router->group(['prefix' => 'wishlist'], function () use ($router) {
                $router->get('', 'UserWishlistController@index');
                $router->post('', 'UserWishlistController@store');
                $router->post('sync-guest', 'UserWishlistController@syncGuest');
                $router->delete('/{id}', 'UserWishlistController@destroy');
            });

            $router->group(['prefix' => 'token'],function() use($router) {
                $router->post('', 'UserTokenController@store');
            });

            $router->group(['prefix' => 'notification'],function() use($router){
                $router->get('','UserNotificationController@index');
                $router->post('update-read','UserNotificationController@updateStatus');
            });

            $router->group(['prefix' => 'giveaway', 'namespace' => 'User'],function() use($router){
                $router->post('index', 'GiveawayController@index');
                $router->post('store', 'GiveawayController@store');
                $router->delete('destroy/{id}', 'GiveawayController@destroy');
            });

            $router->group(['prefix' => 'chat'],function() use($router){
                $router->get('','UserChatController@index');
                $router->post('store','UserChatController@store');
                $router->post('upload','UserChatController@upload');
                $router->get('unread','UserChatController@jmlBelumTerbaca');
                $router->post('read','UserChatController@updateStatus');
            });
        });

    });

    $router->group(['prefix' => 'product', 'namespace' => 'Product'], function () use ($router) {
        $router->group(['prefix' => 'review'], function () use ($router) {
            $router->post('/upload', 'ReviewController@Upload');
            $router->post('/delete-file', 'ReviewController@deleteFile');
            $router->get('/templates', 'ReviewController@textTemplates');
            $router->get('/{id}', 'ReviewController@index');
            $router->post('', 'ReviewController@store');
        });

        $router->group(['prefix' => 'complain'], function () use ($router) {
            $router->get('', 'ComplainController@index');
            $router->post('', 'ComplainController@store');
            $router->post('/upload', 'ComplainController@Upload');
            $router->post('delete-file', 'ComplainController@deleteFile');
            $router->post('show', 'ComplainController@show');
            $router->get('templates', 'ComplainController@textTemplates');

        });
    });

    $router->group(['prefix' => 'transaction', 'namespace' => 'Transaction'], function () use ($router) {
        $router->group(['prefix' => 'cart'], function () use ($router) {
            $router->get('', 'CartController@index');
            $router->get('get-cart', 'CartController@getCart');
            $router->post('', 'CartController@store');
            $router->post('sync', 'CartController@sync');
            $router->post('check-stock', 'CartController@checkStock');
            $router->post('multiple-check-stock', 'CartController@multipleCheckStock');
            $router->post('set-voucher','CartController@useVoucher');
            $router->post('list-voucher','CartController@listVoucher');
            $router->post('pixel', 'CartController@pixel');
            $router->post('check-location', 'CartController@checkLocation');
            $router->post('check-product', 'CartController@checkProduct');
            $router->post('check-product-all', 'CartController@checkProductAll');
        });

        $router->group(['prefix' => 'order'], function () use ($router) {

            $router->get('cc-payment/{encryptedString}', 'OrderController@ccPayment');
            $router->get('list-reason-cancel','OrderController@listReasonCancelOrder');
            $router->get('apps/{ordersn}','OrderController@detailApps');
            $router->get('check-delivery','OrderController@checkDelivery');
            // $router->post('result-cc-payment', 'OrderController@resultCcPayment');

            $router->group(['middleware' => 'auth'], function () use ($router) {
                $router->get('', 'OrderController@index');
                $router->post('', 'OrderController@store');
                $router->post('pre-checkout-summary','OrderController@preCheckoutSummary');
                $router->post('cancel/{ordersn}','OrderController@cancelOrder');
                $router->post('complete/{ordersn}','OrderController@completeOrder');
                $router->post('tracking','OrderController@tracking');

            });
            $router->group(['middleware' => 'auth-apikey'], function () use ($router) {
                $router->post('send-erp/{ordersn}','OrderController@sendOrderToERP');
                $router->post('process/{ordersn}','OrderController@processOrder');
                $router->post('ship/{ordersn}','OrderController@shipOrder');
            });

            $router->get('{ordersn}','OrderController@show');
            $router->post('payment-notif','OrderController@paymentNotif');
            $router->post('payment-notif-cc','OrderController@paymentNotifCreditCard');

            $router->post('doku-payment-notif-va','OrderController@dokuPaymentNotifVA');
            $router->post('doku-payment-notif-cc','OrderController@dokuPaymentNotifCC');
        });
    });

    $router->group(['prefix' => 'promotion', 'namespace' => 'Promotion'], function () use($router) {
        $router->get('flashsale', 'PromotionController@flashsale');
        $router->get('flashsale-group', 'PromotionController@flashsaleGroup');
        $router->get('endorsement', 'PromotionController@endorsement');
        $router->get('free', 'PromotionController@free');
        $router->get('combo', 'PromotionController@combo');
        $router->get('paket', 'PromotionController@paket');
    });

    $router->group(['prefix' => 'voucher'], function () use($router) {
        $router->get('', 'UserVoucherController@index');
        // $router->post('setvoucher','UserVoucherController@useVoucher');
    });

    $router->group(['prefix' => 'project', 'namespace' => 'Project'], function () use($router) {
        $router->post('', 'ProjectController@store');
        $router->get('data/location', 'DataController@getLocation');
        $router->get('data/property', 'DataController@getProperty');
    });

    $router->group(['prefix' => 'event'], function () use($router) {
        $router->post('', 'ProjectController@store');
        $router->get('data/location', 'DataController@getLocation');
        $router->get('data/property', 'DataController@getProperty');
    });

    $router->group(['prefix' => 'update'], function () use ($router) {
        $router->get('discount', 'UpdateController@discount');
        $router->get('price', 'UpdateController@price');
    });
});

$router->group(['prefix' => 'uni', 'namespace' => 'Uni'], function () use ($router) {

    $router->group(['prefix' => 'mitra'], function () use ($router) {
        $router->get('', 'MitraController@index');
        $router->get('apps', 'MitraController@apps');
        $router->get('listkota', 'MitraController@listkota');
        $router->get('kota', 'MitraController@kota');
        $router->get('wilayah', 'MitraController@wilayah');
        $router->get('brand', 'MitraController@brand');
        $router->get('details', 'MitraController@details');
        $router->get('sitemap', 'MitraController@sitemap');
    });

    $router->get('popup', 'PopUpController@index');
    $router->get('banner', 'BannerController@index');
    $router->get('bank', 'BankController@index');

    $router->get('address-active', 'CityController@addressActive');

    $router->group(['prefix' => 'city'], function () use ($router) {
        $router->get('', 'CityController@index');
        $router->get('all', 'CityController@all');
        $router->get('all-city', 'CityController@allCity');
    });

    $router->group(['prefix' => 'district'], function () use ($router) {
        $router->get('', 'DistrictController@index');
        $router->get('all', 'DistrictController@all');
    });

    $router->group(['prefix' => 'province'], function () use ($router) {
        $router->get('', 'ProvinceController@index');
        $router->get('all', 'ProvinceController@all');
    });

    $router->group(['prefix' => 'village'], function () use ($router) {
        $router->get('', 'VillageController@index');
        $router->get('all', 'VillageController@all');
    });

    $router->group(['prefix' => 'category'], function () use ($router) {
        $router->get('', 'CategoryController@index');

        $router->group(['prefix' => 'group'], function () use ($router) {
            $router->get('', 'CategoryGroupController@index');
        });
    });

    $router->group(['prefix' => 'product'], function () use ($router) {
        $router->get('', 'ProductController@index');
        $router->get('brand', 'ProductController@searchBrand');
        $router->get('brand/{slug}', 'ProductController@detailBrand');
        $router->get('variant', 'ProductController@variant');
        $router->get('variant-brand', 'ProductController@variantBrand');
        $router->get('search', 'ProductController@search');
        $router->get('groups', 'ProductController@groups');
        $router->get('check', 'ProductController@checkProduct');
        $router->get('check-item-recive', 'ProductController@itemReciveERP');
        $router->get('group/{slug}', 'ProductController@group');
        $router->get('slugify', 'ProductController@slugify');
        $router->get('top', 'ProductController@top');
        $router->get('filter', 'ProductController@filter');
        $router->post('flashsale', 'ProductController@flashsale');
        $router->post('free', 'ProductController@free');
        $router->post('po', 'ProductController@po');
        $router->get('partner/{id}', 'ProductController@partner');
        $router->get('inspiration', 'ProductController@listInspiration');
        $router->get('inspiration/{id}', 'ProductController@inspiration');
        $router->get('voucher/{id}', 'ProductController@voucher');
        $router->get('combo', 'ProductController@combo');
        $router->get('{slug}', 'ProductController@show');
        $router->get('bundle/{slug}', 'ProductController@showBundle');

    });

    $router->group(['prefix' => 'brand'], function () use ($router) {
        $router->get('', 'GenBrandController@index');
    });

    $router->group(['prefix' => 'material'], function () use ($router) {
        $router->get('', 'GenMaterialController@index');
    });

    $router->group(['prefix' => 'color'], function () use ($router) {
        $router->get('', 'GenColorController@index');
    });

    $router->group(['prefix' => 'inspiration'], function () use ($router) {
        $router->get('', 'InspirationController@index');
        $router->get('highlight', 'InspirationController@highlight');
        $router->get('room', 'InspirationController@room');
        $router->get('{slug}', 'InspirationController@detail');
    });

    $router->group(['prefix' => 'payment'], function () use ($router) {
        $router->get('', 'GenPaymentFaspayController@index');
        $router->get('list', 'GenPaymentFaspayController@list');
    });

    $router->group(['prefix' => 'news'], function () use ($router) {
        $router->get('', 'NewsController@index');
        $router->get('tags', 'NewsController@tags');
        $router->get('tags-top', 'NewsController@tagsTop');
        $router->get('products', 'NewsController@products');
        $router->get('categories', 'NewsController@categories');
        $router->get('selected', 'NewsController@selected');
        $router->get('details', 'NewsController@details');
        $router->get('{slug}', 'NewsController@show');
    });

    $router->group(['middleware' => 'auth-apikey'], function () use ($router) {
        $router->post('upload','GeneralController@upload');
    });

    $router->group(['prefix' => 'page'], function () use ($router) {
        $router->get('', 'PagesController@index');
        $router->get('{slug}', 'PagesController@show');
    });
});

$router->group(['prefix' => 'moreverse', 'namespace' => 'Moreverse'], function () use ($router) {

    $router->get('exhibition/{slug}', 'ExhibitionController@index');
    $router->get('catalogue', 'CatalogueController@index');
    $router->get('video', 'VideoController@index');
    $router->get('activities', 'ActivitiesController@index');
    $router->get('reminder', 'ActivitiesController@reminder');
    $router->get('pixel', 'ActivitiesController@pixel');
    $router->get('notif', 'ActivitiesController@notification');

    $router->group(['prefix' => 'mitra'], function () use ($router) {
        $router->get('', 'MitraController@index');
        $router->get('city', 'MitraController@city');
        $router->get('{slug}', 'MitraController@show');

    });


});


$router->group(['prefix' => 'affiliate', 'namespace' => 'Affiliate'], function () use ($router) {

    $router->get('order', 'CommissionController@order');

});

$router->group(['namespace' => '\Rap2hpoutre\LaravelLogViewer'], function () use ($router) {
    $router->get('logs', 'LogViewerController@index');
});

// $router->group(['prefix' => 'more-v2'],function() use ($router){
//     $baseUrl = env('APP_URL_MOREV2');
//     $router->get('reset/password/{token}', function ($token) use ($baseUrl){
//         return redirect($baseUrl.'/forgotPassword/'.$token);
//     });
// });

