<?php

namespace App\Console\Commands;

use App\Http\Controllers\More\Product\ReviewController;
use Illuminate\Console\Command;
use App\Models\More\Setting;
use App\Models\More\User;
use App\Models\Uni\Product;
use App\Models\More\ProductWishlist;
use App\Models\More\Promotion\Voucher;
use App\Models\More\Transaction\CartHistory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Uni\GenCronJob;
use Carbon\Carbon;

class NotifAddToCart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:addtocart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command notif email add to cart';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = CartHistory::whereNotNull('more_users_id')->where('more_users_id',72)->whereRaw("date(created_at) = CURRENT_DATE - INTERVAL '1 day'")->select('more_users_id')->groupBy('more_users_id')->get();
        $voucher = Voucher::where('status', 1)
                ->where('show_on', 1)
                ->whereDate('duration_start','<=', Carbon::now())
                ->whereDate('duration_end','>=', Carbon::now())
                ->orderByDesc('voucher')
                ->first();

        foreach($data as $value){
            $user = User::find($value->more_users_id);
            $query = "select v.uni_products_id from more_transaction_carts c
            inner join more_transaction_cart_details d on d.more_transaction_carts_id = c.id
            inner join uni_product_variants v on v.id = d.uni_product_variants_id
            where c.more_users_id = $value->more_users_id GROUP BY v.uni_products_id";
            $listProduct =DB::select($query);
            $arrProduct = [];
            $subjectNameProduct = "";
            foreach($listProduct as $valueProduct){
                $product = Product::leftJoin("uni_product_price_view as uppv", "uppv.uni_products_id", "=", "uni_products.id")
                    ->leftJoin("uni_product_stock_view as upsv", "upsv.uni_products_id", "=", "uni_products.id")
                    ->select([
                        "uni_products.*",
                        "upsv.stock",
                        "uppv.min_discount_price",
                        "uppv.max_discount_price"
                    ])
                    ->with([
                        'r_uni_product_variants',
                        'r_uni_product_variants.r_uni_product_variant_prices'
                    ])->find($valueProduct->uni_products_id);

                $subjectNameProduct = $product->name;

                $detailProduct = array();
                $detailProduct['name'] = $product->name;
                $detailProduct['link'] =config("app.web_url")."/product/".$product->slug;
                $detailProduct['price_normal_min'] = $product->price_normal_min;
                $detailProduct['price_discount_min'] = $product->price_discount_min;
                $detailProduct['discount_min'] = $product->discount_min;
                array_push($arrProduct, $detailProduct);
            }
            // dd($arrProduct);
            if(count($listProduct) != 0){
                $result = sendEmail("emails.notif_addtocart", $user->email, $user->name.', cieee lagi ngeliatin '.$subjectNameProduct.'?', ["voucher"=>$voucher,"product" => $arrProduct, "user" => $user]);
            }

        }



    }
}
