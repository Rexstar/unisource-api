<?php

namespace App\Console\Commands;

use App\Http\Controllers\More\Product\ReviewController;
use Illuminate\Console\Command;
use App\Models\More\Setting;
use App\Models\More\User;
use App\Models\Uni\Product;
use App\Models\More\ProductWishlist;
use App\Models\More\Promotion\Voucher;
use App\Models\More\Transaction\CartHistory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Uni\GenCronJob;
use Carbon\Carbon;

class NotifWishlist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:wishlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command notif email wishlist';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = ProductWishlist::whereNull('deleted_at')->where('more_users_id',72)->whereRaw("date(created_at) = CURRENT_DATE - INTERVAL '1 day'")->select('more_users_id')->groupBy('more_users_id')->get();
        $voucher = Voucher::where('status', 1)
                ->where('show_on', 1)
                ->whereDate('duration_start','<=', Carbon::now())
                ->whereDate('duration_end','>=', Carbon::now())
                ->orderByDesc('voucher')
                ->first();

        foreach($data as $value){
            $user = User::find($value->more_users_id);
            $listProduct = ProductWishlist::whereNull('deleted_at')->where('more_users_id',$value->more_users_id)->get();
            $arrProduct = [];
            foreach($listProduct as $valueProduct){
                $product = Product::leftJoin("uni_product_price_view as uppv", "uppv.uni_products_id", "=", "uni_products.id")
                    ->leftJoin("uni_product_stock_view as upsv", "upsv.uni_products_id", "=", "uni_products.id")
                    ->select([
                        "uni_products.*",
                        "upsv.stock",
                        "uppv.min_discount_price",
                        "uppv.max_discount_price"
                    ])
                    ->with([
                        'r_uni_product_variants',
                        'r_uni_product_variants.r_uni_product_variant_prices'
                    ])->find($valueProduct->uni_products_id);

                $detailProduct = array();
                $detailProduct['name'] = $product->name;
                $detailProduct['link'] =config("app.web_url")."/product/".$product->slug;
                $detailProduct['price_normal_min'] = $product->price_normal_min;
                $detailProduct['price_discount_min'] = $product->price_discount_min;
                $detailProduct['discount_min'] = $product->discount_min;
                array_push($arrProduct, $detailProduct);
            }

            $result = sendEmail("emails.notif_wishlist", $user->email, $user->name.', masih kepikiran….?', ["voucher"=>$voucher,"product" => $arrProduct, "user" => $user]);
        }


    }
}
