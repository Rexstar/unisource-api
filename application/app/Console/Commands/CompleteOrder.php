<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\More\Transaction\OrderController;
use App\Models\More\Transaction\Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Uni\GenCronJob;

class CompleteOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'complete:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $completeOrders = Order::where("status", 4)
            ->whereNotNull("shipped_at")
            ->whereRaw("((CURRENT_DATE - DATE (shipped_at)) + 1)  >= 14")
            ->get();

            foreach ($completeOrders as $order) {
                //~ 6 = selesai
                $controler = new OrderController;
                $controler->setStatusOrder($order, 6);
            }
            $cron = GenCronJob::find(2);
            $cron->last_update = date("Y-m-d H:i:s");
            $cron->note = "ok";
            $cron->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            if($e->getMessage()){
                // Log::error("CRONJOB:completeOrder",[$e->getMessage()]);
                $cron = GenCronJob::find(2);
                $cron->last_update = date("Y-m-d H:i:s");
                $cron->note = $e->getMessage();
                $cron->save();
            }
        }

    }
}
