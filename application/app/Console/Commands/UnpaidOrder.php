<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\More\Transaction\OrderController;
use App\Models\More\Transaction\Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Uni\GenCronJob;

class UnpaidOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unpaid:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check unpaid orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $unpaidOrders = Order::where("status",1)
            ->where('faspay_expired_date','<>',null)
            ->where('faspay_expired_date','<',date("Y-m-d H:i:s"))
            ->get();

            foreach ($unpaidOrders as $order) {
                //~ 92 = batal otomatis
                $controler = new OrderController;
                $controler->setStatusOrder($order, 92);
            }

            $cron = GenCronJob::find(1);
            $cron->last_update = date("Y-m-d H:i:s");
            $cron->note = "ok";
            $cron->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            if($e->getMessage()){
                Log::error("CRONJOB:unpaidOrder",[$e->getMessage()]);
                $cron = GenCronJob::find(1);
                $cron->last_update = date("Y-m-d H:i:s");
                $cron->note = $e->getMessage();
                $cron->save();
            }
        }

    }
}
