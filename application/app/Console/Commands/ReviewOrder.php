<?php

namespace App\Console\Commands;

use App\Http\Controllers\More\Product\ReviewController;
use Illuminate\Console\Command;
use App\Models\More\ProductReview;
use App\Models\More\Transaction\Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Uni\GenCronJob;


class ReviewOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'review:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        DB::beginTransaction();
        try {
            $completeOrders = Order::where("status", 6)
            ->whereNotNull("completed_at")
            ->whereRaw("((CURRENT_DATE - DATE (completed_at)) + 1)  > 3")
            ->where("reviewed",0)
            ->get();

            foreach ($completeOrders as $order) {
                foreach ($order->r_more_transaction_order_details[0]->r_more_transaction_order_products_view as $orderProduct) {
                    $data = [];
                    $data['id'] = null;
                    $data['more_transaction_order_products_id'] = $orderProduct->id;
                    $data['uni_products_id'] = $orderProduct->uni_products_id;
                    $data['uni_product_variants_id'] = $orderProduct->uni_product_variants_id;
                    $data['rating'] = 5;
                    $data['description'] = null;
                    $data['more_product_review_images'] = null;

                    $controller = new ReviewController;
                    $controller->doReview((object)$data, $order->more_users_id, false);
                }
            }

            $cron = GenCronJob::find(3);
            $cron->last_update = date("Y-m-d H:i:s");
            $cron->note = "ok";
            $cron->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            if($e->getMessage()){
                Log::error("CRONJOB:reviewOrder",[$e->getMessage()]);
                $cron = GenCronJob::find(3);
                $cron->last_update = date("Y-m-d H:i:s");
                $cron->note = $e->getMessage();
                $cron->save();
            }
        }


    }
}
