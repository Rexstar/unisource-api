<?php

namespace App\Console\Commands;

use App\Http\Controllers\More\UserNotificationController;
use App\Models\More\Transaction\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Uni\GenCronJob;

class PushNotif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:notif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for push notif';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            //~ 1: belum dibayar
            $unnotifiedOrders = Order::where("status", 1)->where("notified",0)->get();
            foreach ($unnotifiedOrders as $order) {
                $controller = new UserNotificationController;
                $controller->pushNotif($order);
            }
            $cron = GenCronJob::find(4);
            $cron->last_update = date("Y-m-d H:i:s");
            $cron->note = "ok";
            $cron->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            if($e->getMessage()){
                // Log::error("CRONJOB:pushNotif",[$e->getMessage()]);
                $cron = GenCronJob::find(4);
                $cron->last_update = date("Y-m-d H:i:s");
                $cron->note = $e->getMessage();
                $cron->save();
            }
        }
    }
}
