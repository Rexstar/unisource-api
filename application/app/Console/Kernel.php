<?php

namespace App\Console;

use App\Console\Commands\ReviewOrder;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UnpaidOrder::class,
        Commands\CompleteOrder::class,
        Commands\ReviewOrder::class,
        Commands\PushNotif::class,
        Commands\NotifWishlist::class,
        Commands\NotifAddToCart::class,
    ];

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'Asia/Jakarta';
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('push:notif')->everyMinute();

        $schedule->command('unpaid:order')->everyMinute();

        $schedule->command('complete:order')->everyMinute();

        $schedule->command('review:order')->everyMinute();

        $schedule->command('notif:addtocart')->dailyAt('08:00');

        $schedule->command('notif:wishlist')->dailyAt('08:00');


    }

}
