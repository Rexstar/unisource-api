<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use App\Models\More\User;
use DB;
use Carbon\Carbon;
use App\Models\More\Chat;
use App\Models\More\Complain;
use App\Models\More\ComplainChat;

class UserChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $paginate = $request->has('per_page') ? $request->per_page : 15;
        $user = Auth::user();
        $complainId = $request->filled("more_complains_id") ? $request->more_complains_id : null;
        $data = $complainId ? ComplainChat::where("more_complains_id",$complainId) : Chat::with('r_uni_products','r_uni_products.r_uni_product_images', 'r_uni_products.r_uni_product_variants');
        $data = $data->where('more_users_id',$user->id)
        ->orderBy('created_at','desc')
        ->paginate($paginate);

        return response_json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'text' => 'required',
            'uni_products_id'=>'numeric',
            'more_complains_id'=>'numeric',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        DB::beginTransaction();
        try {
            $user                   = \Auth::user();
            $data                   = $request->filled("more_complains_id") ? new ComplainChat() : new Chat();
            $data->text             = $request->text;
            $data->file             = $request->file;
            $data->file_type        = $request->file_type;
            $data->more_users_id    = $user->id;
            $data->created_at       = date("Y-m-d H:i:s");
            if($request->filled("uni_products_id")) $data->uni_products_id  = $request->uni_products_id;
            if($request->filled("more_complains_id")) $data->more_complains_id  = $request->more_complains_id;
            if($data->save()){
                DB::commit();
                return response_json($data,201);
            }
            DB::rollback();
            return response_json("Gagal mengirim chat!, coba lagi nanti.",500);
        } catch (Exception $e) {
            DB::rollback();
            return response_json("Gagal mengirim chat!, coba lagi nanti.",500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'file' => 'required|'.config('upload.doc_validator'),
        ]);
        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        try {
            $user = \Auth::user();
            $file = $request->file('file');
            $type = $file->getMimeType();
            $dateTime = Carbon::now()->format('dmYHis');
            $path = config("upload.path.chats");
            $name = generateRandomString(5) . '_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();
            $url  = URL::asset($path.$name);

            if (! File::exists($path)) {
                File::makeDirectory($path,0775, true);
            }

            if($file->move($path, $name)){
                return response_json(compact('url','type'), 200);
            }

        } catch (Exception $e) {
            return response_json("Gagal mengupload file!, coba lagi nanti.",500);
        }
    }

    public function updateStatus(Request $request){
        $user = \Auth::user();
        $complainId = $request->filled("more_complains_id") ? $request->more_complains_id : null;
        $affected =0;
        if($complainId){
            $affected = DB::table("more_complain_chats")->where([
                'more_complains_id' => $complainId,
                'more_users_id' => $user->id,
            ])->update([
                'read_users' => 1,
                'read_user_at' => date("Y-m-d H:i:s")
            ]);
        }else{
            $affected = DB::table("more_chats")->where([
                'more_users_id' => $user->id,
            ])->update([
                'read_users' => 1,
                'read_user_at' => date("Y-m-d H:i:s")
            ]);
        }

        return response_json(compact('affected'),200);
    }

    public function jmlBelumTerbaca(Request $request){
        $user = \Auth::user();
        $complainId = $request->filled("more_complains_id") ? $request->more_complains_id : null;

        $unread = 0;
        if($complainId){
            $unread = ComplainChat::where('more_users_id',$user->id)
            ->where("more_complains_id",$complainId)
            ->where('read_users',0)->whereRaw('admin_users_id is not null')->count();
        }else{
            $unread = Chat::where('more_users_id',$user->id)
            ->where('read_users',0)->whereRaw('admin_users_id is not null')->count();
        }

        return response_json($unread);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
