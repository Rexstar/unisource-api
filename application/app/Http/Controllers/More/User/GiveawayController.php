<?php

namespace App\Http\Controllers\More\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\More\UserEventClaim;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class GiveawayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = UserEventClaim::with([
            'r_more_users',
            'r_more_events'
        ])->orderBy("created_at","desc");

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_giveaway', 10);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->where('more_users_id', auth()->user()->id)->orderBy('created_at', 'DESC')->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
            "total_pages" => ceil($count/$perPage),
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'more_events_id' => 'required',
            'email' => 'required|email',
            'name' => 'required',
            'account_ig' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
            'file_evidence' => 'required|mimes:jpeg,jpg,png|max:'.config("upload.max_size"),
            'file_ktp' => 'required|mimes:jpeg,jpg,png|max:'.config("upload.max_size"),
            'file_npwp' => 'mimes:jpeg,jpg,png|max:'.config("upload.max_size"),
        ], [], [
            'more_events_id' => 'Event',
            'email' => 'Email',
            'name' => 'Nama Lengkap',
            'account_ig' => 'Nama Instagram',
            'phone' => 'No. Telepon',
            'address' => 'Alamat Lengkap',
            'file_ktp' => 'Foto KTP',
            'file_npwp' => 'Foto NPWP',
            'file_evidence' => 'Screenshot DM'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        $user = Auth::user();

        $giveaway = new UserEventClaim();
        $giveaway->more_events_id = $request->more_events_id;
        $giveaway->more_users_id = $user->id;
        $giveaway->email = $request->email;
        $giveaway->name = $request->name;
        $giveaway->account_ig = $request->account_ig;
        $giveaway->phone = $request->phone;
        $giveaway->address = $request->address;

        $dateTime = Carbon::now()->format('dmYHis');
        $path = config("upload.path.giveaway");
        if(!File::exists($path)) File::makeDirectory($path, 0775, true);

        if($request->file('file_evidence')){
            $file = $request->file('file_evidence');
            $name = generateRandomString(5) . '_evidence_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();
            $file->move($path, $name);

            $giveaway->file_evidence = URL::asset($path . $name);
        }

        if($request->file('file_ktp')){
            $file = $request->file('file_ktp');
            $name = generateRandomString(5) . '_ktp_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();
            $file->move($path, $name);

            $giveaway->file_ktp = URL::asset($path . $name);
        }

        if($request->file('file_npwp')){
            $file = $request->file('file_npwp');
            $name = generateRandomString(5) . '_npwp_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();
            $file->move($path, $name);

            $giveaway->file_npwp = URL::asset($path . $name);
        }
        $giveaway->save();


        return response_json($giveaway);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $giveaway = UserEventClaim::where('more_users_id', auth()->user()->id)->where('id', $id)->first();
        if($giveaway) {
            $giveaway->delete();

            return response_json($giveaway);
        }
        return response_json('Data tidak ditemukan.', 403);
    }
}
