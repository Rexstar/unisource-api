<?php

namespace App\Http\Controllers\More\Promotion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\More\Promotion\Flashsale;
use App\Models\More\Promotion\Endorsement;
use App\Models\More\Promotion\ProductFree;
use App\Models\Uni\CategoryGroupDetail;
use App\Models\Uni\ProductCategory;
use App\Models\Uni\Product;
use App\Models\Uni\ProductApplication;
use App\Models\Uni\ProductVariant;

class PromotionController extends Controller
{
    public function endorsement()
    {
        $data = Endorsement::where('status',1)->orderBy('order_data','ASC')->get()->toArray();
        return response_json($data);
    }

    public function flashsale()
    {
        $data = Flashsale::with([
                    'r_more_promotion_flashsale_products',
                    'r_more_promotion_flashsale_products.r_uni_products',
                    'r_more_promotion_flashsale_products.r_uni_products.r_uni_product_variants',
                    'r_more_promotion_flashsale_products.r_uni_product_variants',
                    'r_more_promotion_flashsale_products.r_uni_product_variant_images',
                    'r_more_promotion_flashsale_products.r_uni_product_variant_prices',
                ])
                ->whereRaw("CURRENT_DATE <= date(date_end) and  CURRENT_DATE >= date(date_start)")
                ->where('is_active', 1)
                ->orderBy('date_start','DESC')
                ->limit(1)
                ->get()
                ->toArray();

        return response_json($data);
    }

    public function flashsaleGroup()
    {
        $data = Flashsale::with([
                    'r_more_promotion_flashsale_products',
                    'r_more_promotion_flashsale_products.r_uni_products',
                    'r_more_promotion_flashsale_products.r_uni_products.r_uni_product_variants',
                    'r_more_promotion_flashsale_products.r_uni_products.r_uni_product_suasanas',
                    'r_more_promotion_flashsale_products.r_uni_product_variants',
                    'r_more_promotion_flashsale_products.r_uni_product_variant_images',
                    'r_more_promotion_flashsale_products.r_uni_product_variant_prices',
                ])
                ->where(function($q) {
                    $q->where('date_start', '<=', \DB::raw('NOW()'))
                        ->where('date_end', '>=', \DB::raw('NOW()'));
                })
                ->orWhere(function($q) {
                    $q->where('date_start', '>=', \DB::raw('NOW()'))
                        ->where('date_end', '>=', \DB::raw('NOW()'));
                })
                ->where('is_active', 1)
                ->orderBy('date_start', 'ASC')
                ->get()
                ->take(3)
                ->toArray();

        return response_json($data);
    }

    public function free()
    {
        $data = ProductFree::with([
                    'r_uni_products_main',
                    'r_uni_products_main.r_uni_product_variants',
                    'r_uni_products_main.r_uni_product_images',
                    'r_uni_products_main.r_uni_product_suasanas',
                    'r_uni_product_variants_main',
                    'r_uni_products',
                    'r_uni_products.r_uni_product_variants',
                    'r_uni_products.r_uni_product_images',
                    'r_uni_products.r_uni_product_suasanas',
                ])
                ->whereRaw("CURRENT_DATE <= date_end and CURRENT_DATE >= date_start")
                ->where("remains", ">=", 0)
                ->where("free_type", "FREE")
                ->limit(15)
                ->orderBy('date_start','DESC')
                ->get()
                ->toArray();

        return response_json(collect($data)->groupBy('uni_products_id'));
    }

    public function combo()
    {
        $data = ProductFree::with([
                    'r_uni_products_main',
                    'r_uni_products_main.r_uni_product_variants',
                    'r_uni_products_main.r_uni_product_images',
                    'r_uni_products_main.r_uni_product_suasanas',
                    'r_uni_product_variants_main',
                    'r_uni_products',
                    'r_uni_products.r_uni_product_variants',
                    'r_uni_products.r_uni_product_images',
                    'r_uni_products.r_uni_product_suasanas',
                ])
                ->whereRaw("CURRENT_DATE <= date_end and CURRENT_DATE >= date_start")
                ->where("remains", ">=", 0)
                ->where("free_type", "COMBO")
                // ->limit(15)
                ->orderBy('date_start','DESC')
                ->get()
                ->toArray();

        return response_json(collect($data)->groupBy('uni_products_id'));
    }

    public function paket(Request $request)
    {
        $application = $request->get('application') ?: 1; // id aplikasi (uni_gen_applications)
        $categoryGroup = 123; //ID Kategori Paket

        $uni_categories_id = CategoryGroupDetail::where('uni_category_groups_id', $categoryGroup)->pluck('uni_categories_id');
        $products_id = ProductCategory::whereIn('uni_categories_id', $uni_categories_id)->pluck('uni_products_id');
        $productIds = ProductApplication::where('uni_gen_applications_id', $application)->pluck('uni_products_id');

        $data = Product::with([
            'r_uni_product_images',
            'r_uni_product_variants',
            'r_uni_product_variants.r_uni_product_variant_prices'
        ])
        ->whereIn('id', $products_id)
        ->whereIn('id', $productIds)
        ->orderBy('created_at','DESC')
        ->limit(15)
        ->get();

        return response_json($data);
    }
}
