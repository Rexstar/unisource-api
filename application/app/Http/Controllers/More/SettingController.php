<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\More\Setting;
use App\Models\More\Reminder;
use App\Models\More\User;
use App\Models\Uni\Product;
use App\Models\More\ProductWishlist;
use App\Models\More\Promotion\Voucher;
use App\Models\More\Transaction\CartHistory;

use Carbon\Carbon;
use DB;
class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response_json(Setting::all());
    }

    public function import()
    {

        $data = DB::table('rizal_tbl_1')->get();
        foreach($data as $value){
            $kelebihan = $value->kelebihan;
            $sku = $value->sku;

            $idProduct = DB::table('uni_product_variants')->where('sku_group', $sku)->first();

            if($idProduct){
                // echo $idProduct->uni_products_id."-".$sku."<br>";
                $explode = explode('+',$kelebihan);
                // dd($explode);
                foreach($explode as $detail){
                    DB::table('uni_product_superiorities')->insert(
                        ['uni_products_id' => $idProduct->uni_products_id, 'name' => $detail]
                    );
                    echo $detail."<br>";
                }
            }


        }
    }

    public function wishlist()
    {
        $data = ProductWishlist::whereNull('deleted_at')->whereRaw("date(created_at) = CURRENT_DATE - INTERVAL '1 day'")->select('more_users_id')->groupBy('more_users_id')->get();
        $voucher = Voucher::where('status', 1)
                ->where('show_on','<>',2)
                ->whereDate('duration_start','<=', Carbon::now())
                ->whereDate('duration_end','>=', Carbon::now())
                ->orderByDesc('voucher')
                ->first();

        foreach($data as $value){
            $user = User::find($value->more_users_id);
            $listProduct = ProductWishlist::whereNull('deleted_at')->where('more_users_id',$value->more_users_id)->get();
            $arrProduct = [];
            foreach($listProduct as $valueProduct){
                $product = Product::leftJoin("uni_product_price_view as uppv", "uppv.uni_products_id", "=", "uni_products.id")
                    ->leftJoin("uni_product_stock_view as upsv", "upsv.uni_products_id", "=", "uni_products.id")
                    ->select([
                        "uni_products.*",
                        "upsv.stock",
                        "uppv.min_discount_price",
                        "uppv.max_discount_price"
                    ])
                    ->with([
                        'r_uni_product_variants',
                        'r_uni_product_variants.r_uni_product_variant_prices'
                    ])->find($valueProduct->uni_products_id);

                $detailProduct = array();
                $detailProduct['name'] = $product->name;
                $detailProduct['link'] =config("app.web_url")."/product/".$product->slug;
                $detailProduct['price_normal_min'] = $product->price_normal_min;
                $detailProduct['price_discount_min'] = $product->price_discount_min;
                $detailProduct['discount_min'] = $product->discount_min;
                array_push($arrProduct, $detailProduct);
            }

            $result = sendEmail("emails.notif_wishlist", $user->email, $user->name.', masih kepikiran….?', ["voucher"=>$voucher,"product" => $arrProduct, "user" => $user]);
        }
    }

    public function addtocart()
    {
        $data = CartHistory::whereNotNull('more_users_id')->whereRaw("date(created_at) = CURRENT_DATE - INTERVAL '1 day'")->select('more_users_id')->groupBy('more_users_id')->get();
        $voucher = Voucher::where('status', 1)
                ->where('show_on','<>',2)
                ->whereDate('duration_start','<=', Carbon::now())
                ->whereDate('duration_end','>=', Carbon::now())
                ->orderByDesc('voucher')
                ->first();

        foreach($data as $value){
            $user = User::find($value->more_users_id);
            $query = "select v.uni_products_id from more_transaction_carts c
            inner join more_transaction_cart_details d on d.more_transaction_carts_id = c.id
            inner join uni_product_variants v on v.id = d.uni_product_variants_id
            where c.more_users_id = $value->more_users_id GROUP BY v.uni_products_id";
            $listProduct =DB::select($query);
            $arrProduct = [];
            $subjectNameProduct = "";
            foreach($listProduct as $valueProduct){
                $product = Product::leftJoin("uni_product_price_view as uppv", "uppv.uni_products_id", "=", "uni_products.id")
                    ->leftJoin("uni_product_stock_view as upsv", "upsv.uni_products_id", "=", "uni_products.id")
                    ->select([
                        "uni_products.*",
                        "upsv.stock",
                        "uppv.min_discount_price",
                        "uppv.max_discount_price"
                    ])
                    ->with([
                        'r_uni_product_variants',
                        'r_uni_product_variants.r_uni_product_variant_prices'
                    ])->find($valueProduct->uni_products_id);

                $subjectNameProduct = $product->name;

                $detailProduct = array();
                $detailProduct['name'] = $product->name;
                $detailProduct['link'] =config("app.web_url")."/product/".$product->slug;
                $detailProduct['price_normal_min'] = $product->price_normal_min;
                $detailProduct['price_discount_min'] = $product->price_discount_min;
                $detailProduct['discount_min'] = $product->discount_min;
                array_push($arrProduct, $detailProduct);
            }
            // dd($arrProduct);
            if(count($listProduct) != 0){
                $result = sendEmail("emails.notif_addtocart", $user->email, $user->name.', cieee lagi ngeliatin '.$subjectNameProduct.'?', ["voucher"=>$voucher,"product" => $arrProduct, "user" => $user]);
            }

        }

    }

    public function reminder(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = new Reminder();
            $user->email = $request->email;
            $user->type = 'TIKET POLKI';
            $user->created_at = date('Y-m-d H:i:s');
            $user->save();

            DB::commit();

            return response_json($user, 200);

        } catch (\Exception $e) {
            DB::rollback();
        }

    }

}
