<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Uni\ProductVariantPrice;
use DB;

class UpdateController extends Controller
{

    public function price(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $skip = $request->get('skip');
            $take = $request->get('take');
            $type = $request->get('type');
            $list = ProductVariantPrice::orderBy('uni_product_variants_id', 'ASC')->skip($skip)->take($take)->get();

            foreach($list as $value){
                $data = ProductVariantPrice::where('uni_product_variants_id',$value->uni_product_variants_id)->first();
                $data->more_discount_price = $value->$type;
                $data->save();
            }

            DB::commit();
            return response_json('Success');
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }

    }

    public function discount(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $skip = $request->get('skip');
            $take = $request->get('take');
            $list = ProductVariantPrice::orderBy('uni_product_variants_id', 'ASC')->skip($skip)->take($take)->get();

            foreach($list as $value){
                if($value->more_discount_price != null && $value->ho_normal != 0 && $value->more_discount_price != 0 ){
                    $discount = number_format(round((($value->ho_normal - $value->more_discount_price)*100) / $value->ho_normal));
                    $data = ProductVariantPrice::where('uni_product_variants_id',$value->uni_product_variants_id)->first();
                    $data->more_discount = $discount;
                    $data->save();
                }
            }

            DB::commit();
            return response_json('Success');
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }

    }

}
