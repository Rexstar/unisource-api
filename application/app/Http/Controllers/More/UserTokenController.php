<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\More\UserToken;
use DB;

class UserTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::beginTransaction();
        try {

            $token = UserToken::where(['more_users_id' => \Auth::user()->id,'firebase_token' => $request->firebase_token])->first();
            if(isset($token) == false){
                $token = new UserToken;
            }    
            $token->more_users_id   = \Auth::user()->id;
            $token->device_id       = $request->device_id;
            $token->device_name     = $request->device_name;
            $token->device_os       = $request->device_os;
            $token->access_token    = $request->access_token;
            $token->app_version     = $request->app_version;
            $token->firebase_token  = $request->firebase_token;
            $token->deleted_at      = null;
            $token->deleted_by      = null;
            $token->save();
            DB::commit();
            return response_json($token,201);
        } catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e){
            DB::rollback();
            return response_json('Error',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // DB::beginTransaction();
        // try {
            
        //     $token->save();
        //     DB::commit();
        //     return response_json($token,201);
        // } catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e){
        //     DB::rollback();
        //     return response_json('Error',500);
        // }
    }
}
