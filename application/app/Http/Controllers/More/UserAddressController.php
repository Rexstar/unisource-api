<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use App\Models\More\UserAddress;
use App\Models\MoreView\UserAddressView;
use App\Models\More\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class UserAddressController extends Controller
{

    public function index()
    {
        $data = UserAddressView::where('more_users_id', auth()->user()->id)->whereNull('deleted_at')->orderBy('default','desc')->get();
        return response_json($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'village' => 'required',
            'postal_code' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = new UserAddress();
            $data->more_users_id = auth()->user()->id;
            $data->name = $request->name;
            $data->address = $request->address;
            $data->province = $request->province;
            $data->city = $request->city;
            $data->district = $request->district;
            $data->village = $request->village;
            $data->postal_code = $request->postal_code;
            $data->phone = $request->phone;
            $data->default = $request->default;
            $data->save();

            $addresses = UserAddress::where('more_users_id',auth()->user()->id);
            if($data->default == 1){
                $addresses->where('id','<>', $data->id)->update(['default' => 0]);
            }else{
                if(count($addresses->get()) == 1){
                    $data->default = 1;
                    $addresses->where('id', $data->id)->update(['default' => 1]);
                }
            }

            DB::commit();
            return response_json(UserAddressView::find($data->id));
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }

    }

    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_addresses',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        $data = UserAddressView::where('id', $request->id)->get();
        return response_json($data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_addresses',
            'name' => 'required',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'village' => 'required',
            'postal_code' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = UserAddress::findOrFail($request->id);
            $data->name = $request->name;
            $data->address = $request->address;
            $data->province = $request->province;
            $data->city = $request->city;
            $data->district = $request->district;
            $data->village = $request->village;
            $data->postal_code = $request->postal_code;
            $data->phone = $request->phone;
            $data->default = $request->default;
            $data->save();

            if($data->default == 1){
                UserAddress::where('more_users_id',auth()->user()->id)->where('id','<>', $data->id)->update(['default' => 0]);
            }

            DB::commit();
            return response_json(UserAddressView::find($data->id));

        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_addresses'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $data = UserAddress::findOrFail($request->id);
        if($data->default == 1){
            return response_json(['message' => 'Tidak Dapat Menghapus Alamat Utama'], 403);
        }

        DB::beginTransaction();
        try
        {
            $data->deleted_at = date('Y-m-d H:m:s');
            $data->save();

            DB::commit();
            return response_json($data);
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }
}
