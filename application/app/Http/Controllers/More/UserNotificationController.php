<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\More\UserNotification;
use DB;
use App\Models\More\User;
use App\Models\More\Transaction\Order;
use App\Models\More\UserToken;
use Illuminate\Support\Facades\Log;

class UserNotificationController extends Controller
{
    // ~~ 1 = Belum Dibayar
    // ~~ 2 = Sudah Dibayar
    // ~~ 3 = Proses
    // ~~ 4 = Dikirim
    // ~~ 5 = Komplain
    // ~~ 6 = Selesai
    // ~~ 9 = Batal manual / Batal expired pembayaran
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $paginate = $request->has('per_page') ? $request->per_page : 15;
        $user = \Auth::user();
        $notif = UserNotification::where('more_users_id',$user->id)->orderBy('created_at','desc')->paginate($paginate);
        $unread = UserNotification::where('more_users_id',$user->id)->where('status_read',0)->count();
        return response_json(compact('unread','notif') ,200);
    }

    public function updateStatus(Request $request){
        $user = \Auth::user();
        $affected =0;
        if($request->id == 'all'){
            $affected = DB::table('more_user_notifications')->where([
                'more_users_id' => $user->id
            ])->update(array('status_read' => 1));
        }else{
            $affected = DB::table('more_user_notifications')->where([
                'more_users_id' => $user->id,'id' => $request->id
            ])->update(array('status_read' => 1));
        }
        return response_json(compact('affected'),200);
    }

    public function createNotification($order,$subject){
        try {
            $desc='';
            $desc = UserNotification::getDescription($order)->fcm;
            $firstDetail = $order->r_more_transaction_order_details()->first();
            $firstProductDetail = $firstDetail->r_more_transaction_order_products()->first();
            $product = $firstProductDetail->r_uni_product_variant()->first();
            $productImage = $product->r_uni_product_image()->first();

            $userId = $order->more_users_id;
            $usertoken = UserToken::where('more_users_id',$userId)->whereNull('deleted_at')->pluck('firebase_token');
            // dd($usertoken);
            $notif = new UserNotification();
            $notif->more_users_id =  $userId;
            $notif->title = $subject;
            $notif->text = $desc;
            $notif->link = "/profile/order/".$order->ordersn;
            $notif->image = $productImage && $productImage->image_url ? $productImage->image_url : null;
            $notif->status_read = 0;
            $notif->created_at = date("Y-m-d H:i:s");
            if($notif->save()) {
                // dd(htmlentities($notif->text));
                if(count($usertoken) > 0){
                    sendFCM($usertoken, $notif->title, strip_tags($notif->text),$notif->image,[
                        'image' =>  $notif->image
                    ]);
                }
            }
        } catch (\Exception $e) {
            // Log::info("pushFcm-Catch",[$e->getMessage()]);
        }
    }

    public function pushEmail($order, $subject){
        try {
            $user = User::find($order->more_users_id);
            $result = null;

            if ($order->status == 1) {
                if($order->note == 'POLKI'){
                    $result = sendEmail("emails.belum_bayar_polki", $user->email, "Pembayaran untuk Intimate Event Natsoe & Kamoe by Polki", ["order" => $order, "user" => $user]);
                    // $result = sendEmail("emails.berhasil_bayar_polki", $user->email, "Evoucher untuk Intimate Event Natsoe & Kamoe by Polki", ["order" => $order, "user" => $user]);
                }else{
                    $result = sendEmail("emails.belum_bayar", $user->email, $subject, ["order" => $order, "user" => $user]);
                }

            }
            elseif ($order->status == 2) {
                if($order->note == 'POLKI'){
                    $result = sendEmail("emails.berhasil_bayar_polki", $user->email, "Evoucher untuk Intimate Event Natsoe & Kamoe by Polki", ["order" => $order, "user" => $user]);
                }else{
                    $result = sendEmail("emails.berhasil_bayar", $user->email, $subject, ["order" => $order, "user" => $user]);
                }

                $subject = "Pesanan Baru ".$order->ordersn;

                $result = sendEmail("emails.baru_admin", config("app.email_admin"), $subject, ["order" => $order, "user" => $user]);
            }
            elseif ($order->status == 3) {
                $result = sendEmail("emails.sedang_diproses", $user->email, $subject, ["order" => $order, "user" => $user]);
            }
            elseif ($order->status == 4) {
                $result = sendEmail("emails.sedang_dikirim", $user->email, $subject, ["order" => $order, "user" => $user]);
            }
            elseif ($order->status == 6) {
                $result = sendEmail("emails.telah_selesai", $user->email, $subject, ["order" => $order, "user" => $user]);

                $subject = "Pesanan #".$order->ordersn." Telah Diterima Pelanggan";
                // $result = sendEmail("emails.terima_admin", config("app.email_admin"), $subject, ["order" => $order, "user" => $user]);
            }
            elseif ($order->status == 91) {
                $result = sendEmail("emails.dibatalkan", $user->email, $subject, ["order" => $order, "user" => $user]);
            }
            elseif ($order->status == 92) {
                $result = sendEmail("emails.dibatalkan_otomatis", $user->email, $subject, ["order" => $order, "user" => $user]);
            }
             //code...
        } catch (\Exception $e) {
            // Log::info("pushEmail-Catch",[$e->getMessage()]);
        }
    }


    public function pushNotif($order){
        try {
            if($order && $order->notified == 0){
                $subject = UserNotification::getDescription($order)->subject;

                //~push fcm
                $this->createNotification($order,$subject);

                //~push Email
                $this->pushEmail($order, $subject);

                //~ set notif status
                $order->notified = 1;
                $order->updated_at = date("Y-m-d H:i:s");
                $order->save();
            }else{
                // Log::info("pushNotif-Info",["Order telah dinotif atau Order tidak ditemukan !"]);
            }
        }catch(\Exception $e){
            // Log::info("pushNotif-Catch",[$e->getMessage()]);
        }
    }




}
