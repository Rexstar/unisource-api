<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use App\Models\More\UserDropshipper;
use App\Models\More\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class UserDropshipperController extends Controller
{

    public function index()
    {
        $data = UserDropshipper::where('more_users_id', auth()->user()->id)->whereNull('deleted_at')->get();
        return response_json($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = new UserDropshipper();
            $data->more_users_id = auth()->user()->id;
            $data->name = $request->name;
            $data->phone = $request->phone;
            $data->save();

            $this->apiDropshipper($data->id, $request->name,$request->name,'INSERT');

            DB::commit();
            return response_json(UserDropshipper::find($data->id));
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }

    }

    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_dropshippers',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        $data = UserDropshipper::where('id', $request->id)->get();
        return response_json($data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_dropshippers',
            'name' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = UserDropshipper::findOrFail($request->id);
            $data->name = $request->name;
            $data->phone = $request->phone;
            $data->save();

            $this->apiDropshipper($data->id, $request->name,$request->phone,'UPDATE');

            DB::commit();
            return response_json(UserDropshipper::find($data->id));

        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_dropshippers'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $data = UserDropshipper::findOrFail($request->id);

        DB::beginTransaction();
        try
        {
            $data->deleted_at = date('Y-m-d H:m:s');
            $data->save();

            $this->apiDropshipper($data->id, '','','DELETE');

            DB::commit();
            return response_json($data);
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }

    public function apiDropshipper($id, $name, $phone, $type)
    {
        //Start API Dropshipper
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTQ5LjEyOS4yNDEuODUvbXVsdGljaGFubmVsL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjk2NDk0MzgyLCJuYmYiOjE2OTY0OTQzODIsImp0aSI6Ik1MMGNVTEFxMVZlVXFtS2wiLCJzdWIiOiI4NSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ._2osTBI8-A3CuuU2olKrQKsdmZTeOrrJuImpkYv1lV8";
        $request_headers_efi = array();
        $request_headers_efi[] = 'Content-Type: application/json';
        $request_headers_efi[] = 'Authorization: Bearer ' . $token;

        $curl = curl_init();

        $array_header = array(
            "uni_more_dropshipper_id" => $id,
            "name" => $name,
            "phone" => $phone,
            "status" => 'ACTIVE',
        );
        $post_data_header = json_encode($array_header);

        if($type == "INSERT"){
            $url = "http://149.129.241.85/multichannel/api/v1/dropshipper/create";
        }elseif ($type == "UPDATE") {
            $url = "http://149.129.241.85/multichannel/api/v1/dropshipper/update";
        }elseif ($type == "DELETE") {
            $url = "http://149.129.241.85/multichannel/api/v1/dropshipper/delete";
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => $request_headers_efi,
            CURLOPT_POSTFIELDS => $post_data_header,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //END API Dropshipper
    }
}
