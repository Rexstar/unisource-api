<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use App\Models\More\UserBank;
use App\Models\MoreView\UserBankView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
class UserBankController extends Controller
{

    public function index()
    {
        $data = UserBankView::where('more_users_id', auth()->user()->id)->whereNull('deleted_at')->get();

        return response_json($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'name' => 'required',
            'number' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = new UserBank();
            $data->more_users_id = auth()->user()->id;
            $data->uni_bank_id = $request->bank;
            $data->name = $request->name;
            $data->number = $request->number;
            $data->save();

            DB::commit();
            return response_json(UserBankView::find($data->id));
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }


    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_banks',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        $data = UserBankView::where('id', $request->id)->get();
        return response_json($data);
    }


    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:more_user_banks',
            'bank' => 'required',
            'name' => 'required',
            'number' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = UserBank::findOrFail($request->id);
            $data->uni_bank_id = $request->bank;
            $data->name = $request->name;
            $data->number = $request->number;
            $data->save();

            DB::commit();
            return response_json(UserBankView::find($data->id));

        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try
        {
            $data = UserBank::findOrFail($request->id);
            $data->deleted_at = date('Y-m-d H:m:s');
            $data->save();

            DB::commit();
            return response_json($data);
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }
    }
}
