<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use App\Models\More\User;
use App\Models\More\UserMoreversary;
use App\Models\More\UserProductHistory;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'picture' => 'required|'.config('upload.image_validator'),
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $jwtData = auth()->user();

        $user = User::find($jwtData->id);

        $dateTime = Carbon::now()->format('dmYHis');

        $file = $request->file('picture');

        $path = config("upload.path.user");
        if(!File::exists($path)) File::makeDirectory($path, 0775, true);
        $name = generateRandomString(5) . '_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();

        $file->move($path, $name);
        $url = URL::asset($path . $name);
        $user->picture = $url;
        $user->save();

        return response_json(['picture' => $url, 'message' => 'Picture berhasil disimpan'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return response_json(auth()->user(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function moreversary()
    {
        $data = UserMoreversary::where('more_users_id', auth()->user()->id)->orderBy('created_at','desc')->get();
        return response_json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $jwtData = auth()->user();

        $user = User::find($jwtData->id);
        if ($request->filled('name')) $user->name = $request->input('name');
        if ($request->filled('phone')) $user->phone = $request->input('phone');
        if ($request->filled('gender')) $user->gender = $request->input('gender');
        if ($request->filled('birthday')) $user->birthday = date('Y-m-d', strtotime(str_replace('/', '-', $request->input('birthday'))));
        if ($request->filled('account_fb')) $user->account_fb = $request->input('account_fb');
        if ($request->filled('account_ig')) $user->account_ig = $request->input('account_ig');
        if ($request->filled('account_tiktok')) $user->account_tiktok = $request->input('account_tiktok');
        $user->updated_by = $jwtData->id;
        $user->updated_at = Carbon::now();
        $user->save();

        return response_json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $jwtData = auth()->user();

        $user = User::find($jwtData->id);
        $user->is_active = 0;
        // $user->delete();
        $user->save();

        return response_json($user, 200);
    }

    public function email(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:more_users',
            'password' => 'required',
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $jwtData = auth()->user();

        $user = User::find($jwtData->id);

        if (!Hash::check($request->password, $user->password)) {
            return response_json(['password' => ['Password tidak valid']], 401);
        }

        $validOTP = validateOTP($jwtData->id, $request->otp, 1);

        if (!$validOTP->status) {
            return response_json([
                'otp' => [
                    $validOTP->code == 1 ? "Kode OTP tidak ditemukan" : ($validOTP->code == 2 ? "Kode OTP tidak valid atau sudah digunakan" : "Kode OTP telah expired")
                ]
            ], 403);
        }

        $user->email = $request->email;
        $user->save();

        return response_json($user, 200);
    }

    public function password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'last_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $jwtData = auth()->user();

        $user = User::find($jwtData->id);

        if ($request->filled('last_password') && !Hash::check($request->last_password, $user->password)) {
            return response_json(['password' => ['Password lama tidak valid']], 403);
        }

        if ($request->new_password !== $request->confirm_password) {
            return response_json(['password' => ['Gagal konfirmasi Password baru']], 403);
        }

        if ($request->new_password === $request->last_password) {
            return response_json(['password' => ['Password baru sama seperti password lama']], 403);
        }

        $user = User::find($jwtData->id);
        $user->password = app('hash')->make($request->new_password);
        $user->save();

        return response_json('Berhasil ubah password', 200);
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'new_password' => 'required',
            'confirm_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        try {
            $now = Carbon::now();

            $user = User::where('email', $request->email)->first();
            if (!$user) {
                return response_json(['email' => ['Email Anda tidak terdaftar']], 403);
            }
            if ($request->new_password !== $request->confirm_password) {
                return response_json(['password' => ['Gagal konfirmasi Kata sandi baru']], 403);
            }
            $user->password = app('hash')->make($request->new_password);
            $user->reset_password_at = $now;
            $user->save();

            return response_json('Berhasil memperbarui Kata sandi', 200);
        } catch (Exception $e) {
            return response_json(['password' => [$e->getMessage()]], 500);
        }
    }

    public function havePassword(Request $request)
    {
        $jwtData = auth()->user();
        $user = User::find($jwtData->id);
        return response_json(['status' => (!$user->password) ? false : true], 200);
    }

    public function checkingComplete(Request $request)
    {
        $jwtData = auth()->user();

        $user = User::find($jwtData->id);

        $email = 0;
        $profile = 0;
        $address = 0;

        $score = 0;

        if($score == 100){
            return response_json([
                'email' => 1,
                'profile' => 1,
                'address' => 1,
                'score' => $user->status_complete,
                'message' => 'Skor ' . $user->status_complete . ' point. Data telah lengkap'
            ], 200);
        }

        if ($user->status_verification != 0){
            $score = $score + 30;
            $email = 1;
        }

        if (
            $user->name != null &&
            $user->picture != null &&
            $user->gender != null &&
            $user->phone != null &&
            $user->birthday != null
        ) {
            $score = $score + 30;
            $profile = 1;
        }

        if(count($user->r_more_user_addresses) != 0){
            $score = $score + 40;
            $address = 1;
        }

        $user->status_complete = $score;
        $user->save();

        return response_json([
            'email' => $email,
            'profile' => $profile,
            'address' => $address,
            'score' => $score,
            'message' => 'Skor ' . $score . ' point. ' . ( $score == 100 ? 'Data telah lengkap' : 'Harap lengkapi ' . ( $email == 0 ? 'email,' : null) . ($profile == 0 ? 'profil,' : null) . ($address == 0 ? 'alamat' : null ) )
        ], 200);
    }

    public function synchingProductHistory(Request $request){
        return response_json("ok");
        // $validator = \Validator::make($request->all(), [
        //     'products_id' => 'array',
        // ], [], [
        // ]);
        // if ($validator->fails()) {
        //     return response_json($validator->errors(), 403);
        // }
        // DB::beginTransaction();
        // try {
        //     $user = Auth::user();
        //     foreach ($request->products_id as $product_id) {
                // $existed = UserProductHistory::where("more_users_id",$user->id)
                // ->where("uni_products_id",$product_id)
                // ->whereRaw("DATE(created_at) = CURRENT_DATE")
                // ->first();
                // if(!$existed){
                    // $history = new UserProductHistory();
                    // $history->more_users_id = $user->id;
                    // $history->uni_products_id = $product_id;
                    // $history->created_at = date("Y-m-d H:i:s");
                    // $history->save();
                // }
        //     }
        //     DB::commit();
        //     return response_json("ok");
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return response_json("Gagal menyimpan history produk",500);
        // }

    }

    public function saveLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city_id' => 'required',
            'city_name' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $user = User::find(auth()->user()->id);
        if(!$user) return response_json('User tidak ditemukan.', 500);

        $user->last_city_id = $request->city_id;
        $user->last_city_name = $request->city_name;
        $user->save();
        return response_json($user, 200);
    }
}
