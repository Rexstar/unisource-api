<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\More\Promotion\Voucher;
use Carbon\Carbon;

class UserVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // * parameter device, web | mobile. (default) mobile
        $device = $request->device ?: 'mobile';
        $application = $request->get('application') ?: 1; // id aplikasi (uni_gen_applications)

        $data = Voucher::where('status', 1)->whereNull('more_users_id')
                ->whereDate('duration_start','<=', Carbon::now())
                ->whereDate('duration_end','>=', Carbon::now());

        // ? jika mobile, tampilkan voucher device mobile dan all
        // ? jika web, tampilkan semua
        if($device == "mobile") {
            $data->where('device', 'all')->orWhere('device', $device);
        }

        if($application){

            if($application == 1){ //Website More
                $data->where('show_on','<>',2);
            }

            $data->where(function ($query) use ($application) {
                $query->whereNull('uni_gen_applications_id')->orWhere('uni_gen_applications_id', $application);
            });



        }

        return response_json($data->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function useVoucher(Request $request)
    // {

    //     $user = \Auth::user();

    //     $cart = Cart::where('more_users_id',$user->id)->first();
    //     $data  = Voucher::where('code',$request->code)
    //             ->where('status',1)
    //             ->whereDate('duration_start','<=', Carbon::now())
    //             ->whereDate('duration_end','>=', Carbon::now())->first();
    //     $grandTotalDiskon = 0;

    //     if(!$data){
    //         return response_json('Kode Voucher Tidak Ditemukan!.', 500);
    //     }

    //     $voucherCart = DB::table('more_transaction_orders')
    //                         ->where('voucher_code',$request->code)
    //                         ->where('more_users_id',$user->id)
    //                         ->whereIn('status', [1, 2, 3,4,5,6])->count();
    //     if($voucherCart){
    //         return response_json('Melebihi batas penggunaan voucher!.', 500);
    //     }

    //     if($cart->grand_total <=  $data->min_purchase){
    //         return response_json('Minimal pembelian '.$data->min_purchase, 500);
    //     }

    //     if($data->type_voucher == 2){
    //         $voucherUser = VoucherUser::where('more_promotions_vouchers_id',$data->id)
    //                 ->where('more_users_id',$user->id)->first();
    //         if(!$voucherUser){
    //             return response_json('Kode Voucher Tidak Ditemukan!', 500);
    //         }
    //     }


    //     if($data->area != 0){
    //         $addrss = UserAddressView::where('id',$request->address_code)->first();
    //         if($data->type_voucher == 3){
    //             if($data->area != $addrss->area){
    //                 return response_json('Kode Voucher Tidak dapat digunakan di Lokasi Anda!', 500);
    //             }
    //         }
    //     }

    //     if($data->type_voucher == 4){
    //         $addrss = UserAddressView::where('id',$request->address_code)->first();
    //         $voucherLokasi = DB::table('more_promotion_voucher_cities')
    //                         ->where('more_promotions_vouchers_id',$data->id)
    //                         ->where('uni_city_id',$addrss->city)->count();

    //         if($voucherLokasi == 0){
    //             return response_json('Kode Voucher Tidak dapat digunakan di Lokasi Anda!', 500);
    //         }
    //     }

    //     if($data->type_voucher == 5){
    //         $voucherPayment = DB::table('more_promotion_voucher_payments')
    //                         ->where('more_promotions_vouchers_id',$data->id)
    //                         ->where('payment_code_faspay',$request->payment_code)->count();
    //         if($voucherPayment == 0){
    //             return response_json('Kode Voucher Tidak dapat digunakan untuk Metode Pembayaran Anda!', 500);
    //         }
    //     }

    //     if($data->type_voucher == 6){
    //         $cartDetail = CartDetail::where('more_transaction_carts_id',$cart->id)->pluck('uni_product_variants_id');

    //         $voucherProduct = DB::table('more_promotion_voucher_products')
    //                         ->where('more_promotions_vouchers_id',$data->id)
    //                         ->whereIn('uni_product_variants_id',$cartDetail)->count();
    //         if($voucherProduct == 0){
    //             return response_json('Kode Voucher Tidak dapat digunakan!', 500);
    //         }
    //     }

    //     $potongan = $data->voucher;
    //     if($data->type_discount == 2){
    //         $potongan = $cart->grand_total*($data->voucher/100);
    //         if($potongan > $data->max_discount_price ) $potongan = $data->max_discount_price;
    //     }
    //     $grandTotalDiskon = $cart->grand_total - $potongan;

    //     $data->grandTotalDiskon = $grandTotalDiskon;
    //     $data->potongan = $potongan;
    //     return response_json($data, 200);


    // }
}
