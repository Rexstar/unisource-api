<?php

namespace App\Http\Controllers\More;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\More\ProductWishlist;

class UserWishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = ProductWishlist::with('r_uni_products', 'r_uni_products.r_uni_product_images', 'r_uni_products.r_uni_product_variants')
                ->where('more_users_id', auth()->user()->id);

        $page = $request->get("page") ?: 1;
        $perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
        $offset = ($page - 1) * $perPage;

        $count = $data->count();
        $endCount = $offset + $perPage;
        $morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'product_id' => 'required|exists:uni_products,id'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        \DB::beginTransaction();
        $data = ProductWishlist::where('more_users_id', auth()->user()->id)->where('uni_products_id', $request->product_id)->first();

        if(!$data) {
            $data = new ProductWishlist();
            $data->uni_products_id = $request->product_id;
            if($data->save()) {
                \DB::commit();
                return response_json($data, 201);
            }
        }
        else {
            $data = ProductWishlist::where('more_users_id', auth()->user()->id)
                    ->where('uni_products_id', $request->product_id)
                    ->first();
            if($data) {
                $data->delete();
                \DB::commit();
                return response_json($data, 200);
            }
            return response_json('Delete error.', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $data = ProductWishlist::find($id);
        if($data) {
            $data->delete();
            \DB::commit();
            return response_json($data, 200);
        }

        return response_json('Delete error.', 500);
    }

    public function syncGuest(Request $request)
    {
        \DB::beginTransaction();
        $request = $request->all();
        $data = ProductWishlist::where('more_users_id', auth()->user()->id)->get()->pluck('uni_products_id');
        $diff = collect($request)->diff($data)->flatten();
        if(count($diff) > 0) {
            foreach ($diff as $value) {
                $w = new ProductWishlist();
                $w->uni_products_id = $value;
                $w->save();
            }
            \DB::commit();
            return response_json('Sync success.');
        }
    }
}
