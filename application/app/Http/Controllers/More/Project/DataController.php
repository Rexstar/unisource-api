<?php

namespace App\Http\Controllers\More\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function getLocation()
    {
        $location = ["Surabaya", "Sidoarjo", "Gresik"];
        return response_json($location);
    }

    public function getProperty()
    {
        $property = [
            "Hunian" => [
                "data" => [
                    ["text" => "Rumah", "icon" => "&#xf015;"],
                    ["text" => "Apartment", "icon" => "&#xf1ad;"],
                ],
                "icon" => "fa-user"
            ],
            "Bisnis" => [
                "data" => [
                    ["text" => "Kantor", "icon" => "&#xf108;"],
                    ["text" => "Hotel", "icon" => "&#xf072;"],
                    ["text" => "Restaurant", "icon" => "&#xf0f4;"],
                    ["text" => "Retail", "icon" => "&#xf07a;"],
                ],
                "icon" => "fa-briefcase"
            ]
        ];
        return response_json($property);
    }
}
