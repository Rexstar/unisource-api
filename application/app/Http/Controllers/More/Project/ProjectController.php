<?php

namespace App\Http\Controllers\More\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\More\Project;

class ProjectController extends Controller
{
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'location' => 'required',
            'address' => 'required',
            'property_id' => 'required',
            'property_type' => 'required'
        ], [], [
            'name' => 'Nama Lengkap',
            'email' => 'Email',
            'phone' => 'No. Telepon',
            'location' => 'Lokasi (Kota)',
            'address' => 'Alamat Lengkap',
            'property_id' => 'Jenis Properti',
            'property_type' => 'Tipe Properti'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $project = new Project();
        $project->name = $request->name;
        $project->email = $request->email;
        $project->phone = $request->phone;
        $project->location = $request->location;
        $project->address = $request->address;
        $project->property_id = $request->property_id;
        $project->property_type = $request->property_type;
        $project->save();

        $email1 = "martin.veno@morefurniture.id";
        $email2 = "taki.rizqi@morefurniture.id";
        sendEmail('emails.project_custom', $email1, 'MORE CUSTOM', ['project' => $project]);
        sendEmail('emails.project_custom', $email2, 'MORE CUSTOM', ['project' => $project]);

        return response_json($project);
    }
}
