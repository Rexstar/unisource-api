<?php

namespace App\Http\Controllers\More\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\More\Transaction\OrderController;
use App\Models\More\Complain;
use App\Models\More\ComplainProduct;
use App\Models\More\ComplainProductImage;
use App\Models\More\TemplateText;
use App\Models\More\Transaction\Order;
use App\Models\MoreView\OrderProductView;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use DB;


class ComplainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return response_json($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'more_transaction_orders_id' => 'required|numeric',
            'reason' => 'required',
            'solution' => '',
            'r_more_complain_products' => 'required|array',
            'r_more_complain_products.*.more_transaction_order_products_id' => 'required|numeric',
            'r_more_complain_products.*.note' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $complain = Complain::where('more_users_id', $user->id)
                ->where("more_transaction_orders_id",$request->more_transaction_orders_id)
                ->first();

            if(!$complain) {
                $complain = new Complain();
                $complain->more_transaction_orders_id = $request->more_transaction_orders_id;
                $complain->more_users_id = $user->id;
            }
            $complain->reason = $request->reason;
            $complain->solution = $request->solution;
            $complain->status = 0;
            $complain->save();

            foreach ($request->r_more_complain_products as $product) {
                $existed = ComplainProduct::where("more_complains_id",$complain->id)
                ->where("more_transaction_order_products_id",$product['more_transaction_order_products_id'])
                ->first();
                if(!$existed){
                    $complain->r_more_complain_products()->create([
                        'more_transaction_order_products_id' => $product['more_transaction_order_products_id'],
                        'note' => $product['note'],
                    ]);
                }else{
                    $complain->r_more_complain_products()->where('id', $existed->id)->update([
                        'more_transaction_order_products_id' => $product['more_transaction_order_products_id'],
                        'note' => $product['note'],
                    ]);
                }
            }
            //5 = COMPLAIN
            $controler = new OrderController;
            $controler->setStatusOrder(Order::find($complain->more_transaction_orders_id), 5);

            $complain->load('r_more_complain_products','r_more_complain_products.r_more_complain_product_images');


            DB::commit();
            return response_json($complain);
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            return response_json("Gagal menyimpan komplain.",500);
        }
    }

    public function show(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ],[],[
            'id' => 'Order Id'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $data = Complain::with([
            'r_more_complain_products',
            'r_more_complain_products.r_more_complain_product_images'
        ])->where("more_transaction_orders_id",$request->id)
        ->firstOrFail();
        return response_json($data);
    }

    public function Upload(Request $request){

        $validator = Validator::make($request->all(), [
            'more_complain_products_id' => 'required',
            'file' => 'required|array',
            'file.*' => config('upload.image_validator'),
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $files = $request->file('file');
        $path = config("upload.path.complain");
        if(!File::exists($path)) File::makeDirectory($path, 0775, true);

        $user = Auth::user();
        $images=[];
        $i = 0;
        foreach ($files as $file) {
            $dateTime = Carbon::now()->format('dmYHis');
            $name = generateRandomString(5) . '_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();
            $file->move($path, $name);
            $url = URL::asset($path . $name);

            $model = new ComplainProductImage();
            $model->more_complain_products_id = $request->more_complain_products_id;
            $model->image_url = $url;
            $model->mime_type = $file->getClientMimeType();
            $model->image_size = $_FILES['file']['size'][$i];
            $model->created_at = date("Y-m-d H:i:s");
            $model->created_by = $user->id;
            $model->save();

            $images[] = $model;
            $i++;
        }

        $complains_id = ComplainProduct::find($request->more_complain_products_id)->more_complains_id;
        $data = Complain::with([
            'r_more_complain_products',
            'r_more_complain_products.r_more_complain_product_images'
        ])->where("id",$complains_id)->firstOrFail();
        return response_json($data);
    }

    public function deleteFile(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ],[],[
            'id' => 'Complain Product Image ID'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $image = ComplainProductImage::find($request->id);
        if($image){
            $review_id = $image->more_complain_products_id;
            $filename = $image->name;
            if($image->delete()) unlink(config("upload.path.complain").$filename);
            $data = ComplainProductImage::where("more_complain_products_id",$review_id)->get();
            return response_json($data);
        }
        return response_json("Data tidak ditemukan",403);
    }

    public function textTemplates(){
        $data = TemplateText::where("type", 3)->get();
        return response_json($data);
    }
}
