<?php

namespace App\Http\Controllers\More\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\More\ProductReview;
use App\Models\More\ProductReviewImage;
use App\Models\More\TemplateText;
use App\Models\More\Transaction\Order;
use App\Models\More\Transaction\OrderProduct;
use App\Models\MoreView\OrderProductView;
use App\Models\Uni\Product;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use DB;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $rating = $request->get('rating') ?: null;

        $data = ProductReview::with(
                        'r_more_users',
                        'r_uni_products',
                        'r_uni_product_variants',
                        'r_more_product_review_images',
                        'r_more_product_review_replies'
                    )
                    ->where('uni_products_id', $id);

        if($rating) {
            $data->where('rating', $rating);
        }

        $page = $request->get("page") ?: 1;
        $perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
        $offset = ($page - 1) * $perPage;

        $count = $data->count();
        $endCount = $offset + $perPage;
        $morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->orderBy('created_at', 'desc')->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'more_transaction_order_products_id' => 'required|exists:more_transaction_order_products,id',
            'uni_products_id' => 'required|exists:uni_products,id',
            'uni_product_variants_id' => 'required|exists:uni_product_variants,id',
            'rating' => 'required|integer|min:1|max:5'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        return response_json($this->doReview($request));
    }

    public function doReview($request, $user_id = null, $withReturn = true){
        $user_id = $user_id ? $user_id : auth()->user()->id;

        \DB::beginTransaction();
        try {
            $existed = null;
            if(!$request->id){
                $existed = ProductReview::where('more_users_id', $user_id)
                ->where("more_transaction_order_products_id",$request->more_transaction_order_products_id)
                ->where('uni_products_id', $request->uni_products_id)
                ->where('uni_product_variants_id', $request->uni_product_variants_id)
                ->whereNull("deleted_at")
                ->first();
            }

            if(!$existed) {
                //~ hanya user yang belum memberikan review .
                //~ 1 transaksi hanya boleh 1x review
                //~ TODO: user yang memberikan review harus sudah transaksi.
                $product = Product::with('r_uni_product_variants', 'r_uni_product_variants.r_uni_product_variant_prices')->findOrFail($request->uni_products_id);
                if($product) {
                    $rating = $request->rating;
                    // $ratingCount = $product->{'rating_'.$rating};

                    // ngitung rating ojok lali.
                    $product->{'rating_'.$rating} += 1;
                    $product->total_reviews += 1;

                    // $averageRating = (1*$product->rating_1 + 2*$product->rating_2 + 3*$product->rating_3 + 4*$product->rating_4 + 5*$product->rating_5) / $product->total_reviews;
                    $averageRating = average_rating($product->rating_1 , $product->rating_2 , $product->rating_3 , $product->rating_4 , $product->rating_5);
                    $product->rating = $averageRating;

                    if($product->save()) {
                        $review = !$request->id ? new ProductReview() : ProductReview::find($request->id);
                        $review->more_transaction_order_products_id = $request->more_transaction_order_products_id;
                        $review->uni_products_id = $request->uni_products_id;
                        $review->uni_product_variants_id = $request->uni_product_variants_id;
                        $review->rating = $rating;
                        $review->description = $request->description;
                        $review->more_users_id = $user_id;
                        if(!$request->id){
                            $review->created_by = $user_id;
                            $review->created_at = date("Y-m-d H:i:s");
                        }else{
                            $review->updated_by = $user_id;
                            $review->updated_at = date("Y-m-d H:i:s");
                        }
                        if($review->save()){
                            $oProduct = OrderProduct::find($review->more_transaction_order_products_id);
                            $purchasedProductsPlucked = OrderProduct::where("more_transaction_orders_id",$oProduct->more_transaction_orders_id)->pluck("id");
                            $countReviewed = ProductReview::whereIn("more_transaction_order_products_id",$purchasedProductsPlucked)->count();

                            if(count($purchasedProductsPlucked) == $countReviewed){
                                $order = Order::find($oProduct->more_transaction_orders_id);
                                $order->reviewed = 1;
                                $order->save();
                            }
                        }
                        $result = sendEmail("emails.kasih_ulasan", auth()->user()->email, 'SELAMAT! Klaim Diskon 10 Ribu Belanja Furnitur Sekarang', ["user" =>auth()->user()]);

                        \DB::commit();
                        if($withReturn) return $review;
                    }
                }
            }
            if($withReturn) return $existed;
                //code...
        } catch (\Exception $th) {
            //throw $th;
            \DB::rollback();
            return 'Gagal melakukan review.';
        }
    }

    public function textTemplates(){
        $data = TemplateText::where("type", 1)->get();
        return response_json($data);
    }

    public function Upload(Request $request){

        $validator = Validator::make($request->all(), [
            'more_product_reviews_id' => 'required',
            'file' => 'required|array',
            'file.*' => config('upload.image_validator'),
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $files = $request->file('file');
        $path = config("upload.path.review");
        if(!File::exists($path)) File::makeDirectory($path, 0775, true);
        $user = Auth::user();
        $images=[];
        $i = 0;
        foreach ($files as $file) {
            $dateTime = Carbon::now()->format('dmYHis');
            $name = generateRandomString(5) . '_' . $user->id . $dateTime . '.' . $file->getClientOriginalExtension();
            $file->move($path, $name);
            $url = URL::asset($path . $name);

            $model = new ProductReviewImage();
            $model->more_product_reviews_id = $request->more_product_reviews_id;
            $model->image_url = $url;
            $model->mime_type = $file->getClientMimeType();
            $model->image_size = $_FILES['file']['size'][$i];
            $model->created_at = date("Y-m-d H:i:s");
            $model->created_by = $user->id;
            $model->save();

            $images[] = $model;
            $i++;
        }

        $data = ProductReviewImage::where("more_product_reviews_id",$request->more_product_reviews_id)->get();
        return response_json($data);
    }

    public function deleteFile(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ],[],[
            'id' => 'Product Review Images ID'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        $image = ProductReviewImage::find($request->product_review_images_id);
        if($image){
            $review_id = $image->more_product_reviews_id;
            $filename = $image->name;
            if($image->delete()) unlink(config("upload.path.review").$filename);
            $data = ProductReviewImage::where("more_product_reviews_id",$review_id)->get();
            return response_json($data);
        }
        return response_json("Data tidak ditemukan",403);
    }
}
