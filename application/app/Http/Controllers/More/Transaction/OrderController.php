<?php

namespace App\Http\Controllers\More\Transaction;

use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\More\Affiliate\Order as OrderAffiliate;
use App\Models\More\Transaction\Cart;
use App\Models\More\Transaction\OrderDetail;
use App\Models\More\Transaction\Order;
use App\Models\More\Transaction\OrderHistory;
use App\Models\More\Transaction\OrderProduct;
use App\Models\More\Transaction\OrderProductBundling;
use App\Models\More\User;
use App\Models\More\UserNotification;
use App\Models\MoreView\OrderProductView;
use App\Models\Uni\GenPaymentFaspay;
use App\Models\Erp\ExpeditionPricelist;
use App\Models\Erp\Pack;
use App\Models\Erp\Bundling;
use App\Models\MoreView\UserAddressView;
use App\Http\Controllers\More\UserNotificationController;
use App\Models\More\Promotion\FlashsaleProduct;
use App\Models\More\Promotion\ProductFree;
use App\Models\More\Affiliate\HistoryTransaction;
use App\Models\More\Transaction\CartDetail;
use App\Models\Uni\ProductVariant;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    // ~~ 1 = Belum Dibayar
    // ~~ 2 = Sudah Dibayar
    // ~~ 3 = Proses
    // ~~ 4 = Dikirim
    // ~~ 5 = Komplain
    // ~~ 6 = Selesai
    // ~~ 91 = Batal manual
    // ~~ 92 = Batal expired pembayaran
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $q = $request->filled('q') ? trim($request->q) : null;
        $status = $request->filled('status') ? $request->status : null;
        $sortby = $request->filled('sortby') ? $request->sortby : "terbaru";

        $periode = $request->filled('periode') ? explode(" - ", $request->periode) : [];

        $fromDate = count($periode) > 0 ? date("Y-m-d", strtotime(str_replace("/", "-", $periode[0]))) : null;
        $toDate = count($periode) > 0 && $fromDate ? date("Y-m-d", strtotime(str_replace("/", "-", $periode[1]))) : $fromDate;

        $data = Order::with([
            // 'r_more_transaction_order_histories',
            'r_more_transaction_order_details',
            'r_more_transaction_order_details.r_more_transaction_order_products',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_image',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_variant_prices',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free.r_uni_product_variants',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free.r_uni_product_variants.r_uni_product_variant_prices',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free.r_uni_product_variants.r_uni_product_image',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_get_combo',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_get_combo.r_uni_product_variants',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_get_combo.r_uni_product_variants_main',
            'r_more_transaction_order_details.r_more_transaction_order_products.r_more_transaction_order_products_bundling.r_uni_product_variant',
        ])->where('more_users_id', $user->id);

        if ($q) {
            $details_id = OrderDetail::whereIn("more_transaction_orders_id", $data->pluck("id"))->pluck("id");
            $data->where(function ($query) use ($q, $details_id) {
                // ~~ search product
                $orders_id = OrderProductView::whereIn("more_transaction_order_details_id", $details_id)->whereRaw("LOWER(name_long) like '%" . strtolower($q) . "%' ")->pluck("more_transaction_orders_id");

                $query->whereRaw("LOWER(ordersn) like '%" . strtolower($q) . "%' ");
                if (count($orders_id) > 0) $query->orWhereIn("id", $orders_id);
            });
        }
        if ($status && intval($status) > 0) $data->where("status", $status);

        if ($fromDate) {
            $data->whereRaw("( DATE(created_at) BETWEEN '" . $fromDate . "' AND '" . $toDate . "' )");
        }

        if ($sortby == "terbesar") {
            $data->orderBy("total", "desc");
        } else {
            $data->orderBy("id", "desc");
        };

        $page = $request->filled("page") ? $request->page : 1;
        $perPage = $request->filled('per_page') ? $request->per_page : config('pagination.per_page_order', 5);
        $offset = ($page - 1) * $perPage;

        $count = $data->count();
        $endCount = $offset + $perPage;
        $morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
            "total_pages" => ceil($count / $perPage),
            "pagination" => [
                "more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'address_id'        => 'required',
            'payment_code'      => 'required',
            'city_id'           => '',
            'dropshipper_id'  => '',
            'dropshipper_name'  => '',
            'dropshipper_phone' => '',
            'voucher'           => '',
            'note'              => '',
        ], [], []);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $fee = $request->fee ?: null;
        $tenor = $request->tenor ?: null;

        $user = Auth::user();
        DB::beginTransaction();
        try {
            $address = UserAddressView::find($request->address_id);
            $payment = GenPaymentFaspay::where("code", $request->payment_code)->first();
            if (!$address) return response_json("Alamat tidak ditemukan", 403);
            if (!$payment) return response_json("Metode Pembayaran tidak ditemukan", 403);

            $checkout = $this->preCheckoutSummary($request, false);

            //~ JIKA ADA PRODUK YG TIDAK VALID atau (PRODUK VALID tapi (VOUCHER ADA & VOUCHER TIDAK VALID))
            if (!$checkout->allProductsIsValid || ($checkout->allProductsIsValid && $checkout->voucher && !$checkout->voucher->isValid)){
                if(!$checkout->voucher->isValid){
                    return response_json("Voucher tidak dapat digunakan", 403);
                }else{
                    return response_json("Transaksi tidak dapat dilanjutkan", 403);
                }
                //return response_json($checkout, 403);
            }

            //~ SAVING ORDER
            $order = new Order();
            $order->more_users_id = $user->id;
            $order->ordersn = generateOrderNumber();
            $order->total = $checkout->summary->bill_total;
            $order->status = 1; //~ belum dibayar
            $order->note = $request->note;
            $order->receiver_name = $address->name;
            $order->receiver_phone = $address->phone;
            $order->receiver_email = $user->email;
            $order->dropshipper = 0;
            if ($request->filled("dropshipper_id") && $request->filled("dropshipper_name") && $request->filled("dropshipper_phone")) {
                $order->dropshipper = $request->dropshipper_id;
                $order->dropshipper_name = $request->dropshipper_name;
                $order->dropshipper_phone = $request->dropshipper_phone;
            }
            $order->faspay_code = $payment->code;
            $order->faspay_name = $payment->name;
            $order->voucher_code = $checkout->voucher && $checkout->voucher->isValid ? $checkout->voucher->voucher->code : null;
            $order->voucher_amount = $checkout->summary->discount_amount;
            $order->note_cancel = $request->note_cancel;
            $order->address = $address->address;
            $order->provinces = $address->province_name;
            $order->cities = $address->city_name;
            $order->districts = $address->district_name;
            $order->villages = $address->village_name;
            $order->postal_code = $address->postal_code;
            $order->notified = 0;
            $order->type_device = $request->type_device;
            $order->created_at = date("Y-m-d H:i:s");
            $order->service_fee = $fee;
            $order->tenor = $tenor;

            //Check Kondisi Pembayaran dengan DOKU / Faspay
            if($payment->code == '000'){
                //DOKU CC
                $faspayResult = Order::fetchPaymentDoku($checkout->doku_items, $order,'CC');
                if (!$faspayResult) return response_json("Gagal mengambil data doku!", 403);

                if (isset($faspayResult->bill_expired)) $order->faspay_expired_date = $faspayResult->bill_expired;
            }else{
                //FASPAY
                //~~ REQUEST FASPAY
                $faspayResult = Order::fetchPaymentFaspay($checkout->faspay_items, $order);
                if (!$faspayResult || !$faspayResult->faspay) return response_json("Gagal mengambil data faspay!", 403);

                if (isset($faspayResult->faspay->trx_id)) $order->faspay_trx_id = $faspayResult->faspay->trx_id;
                if (isset($faspayResult->bill_expired)) $order->faspay_expired_date = $faspayResult->bill_expired;
                if (isset($faspayResult->faspay->source_qrcode)) $order->faspay_qr_code = $faspayResult->faspay->source_qrcode;
            }

            $order->save();

            $order_detail_id = $order->r_more_transaction_order_details()->create([
                "more_transaction_orders_id" => $order->id
            ])->id;

            $order->r_more_transaction_order_histories()->create([
                "more_transaction_orders_id" => $order->id,
                "status" => 1,
                "created_at" => date("Y-m-d H:i:s"),
            ]);

            foreach ($checkout->cart_items as $item) {
                $oProduct = new OrderProduct();
                $oProduct->more_transaction_orders_id = $order->id;
                $oProduct->more_transaction_order_details_id = $order_detail_id;
                $oProduct->uni_product_variants_id = $item->uni_product_variants_id;

                // JIKA FREE PRODUCT
                $oProduct->is_free = isset($item->free_description) && $item->free_description ? 1 : 0;
                $oProduct->price = $oProduct->is_free == 1 ? 0 : $item->variant_detail->valid_price;
                $oProduct->free_description = $oProduct->is_free == 1 ? $item->free_description : null;
                $oProduct->more_promotion_product_frees_id = $oProduct->is_free == 1 ? $item->more_promotion_product_frees_id : null;

                //JIKA FLASHSALE
                $oProduct->flashsale = $item->is_flashsale;
                $oProduct->more_promotion_flashsales_id = $oProduct->flashsale == 1 ? $item->variant_detail->pre_checkout_flashsale->flashsale->id : null;
                $oProduct->more_promotion_flashsale_products_id = $oProduct->flashsale == 1 ? $item->variant_detail->pre_checkout_flashsale->productFlashsale->id : null;

                $oProduct->quantity = $item->quantity;
                $oProduct->bundling = $item->is_bundling;
                $oProduct->price_normal = $item->variant_detail->r_uni_product_variant_prices->ho_normal;
                $oProduct->price_delivery = $item->sub_shipping_price;
                $oProduct->sub_total = $item->sub_total;
                $oProduct->save();
                if (count($item->r_more_transaction_cart_bundlings) > 0) {
                    foreach ($item->r_more_transaction_cart_bundlings as $bundleItem) {
                        $oProductBundling = new OrderProductBundling();
                        $oProductBundling->more_transaction_order_details_id = $oProduct->more_transaction_order_details_id;
                        $oProductBundling->more_transaction_order_products_id = $oProduct->id;
                        $oProductBundling->uni_product_variants_id = $bundleItem->uni_product_variants_id;
                        $oProductBundling->quantity = $bundleItem->quantity;
                        $oProductBundling->save();
                    }
                }

                if ($oProduct->flashsale == 1) {
                    $productInFlashsale = FlashsaleProduct::find($item->variant_detail->pre_checkout_flashsale->productFlashsale->id);
                    $productInFlashsale->remains = $productInFlashsale->remains - 1;
                    $productInFlashsale->save();
                }

                if ($oProduct->is_free == 1) {
                    $productFree = ProductFree::find($oProduct->more_promotion_product_frees_id);
                    $productFree->remains = $productFree->remains - 1;
                    $productFree->save();
                }

                //Update Additional Stock
                $productVariant = ProductVariant::find($item->uni_product_variants_id);
                if ($productVariant->additional_stock != 0) {
                    $existedStock = $productVariant->additional_stock;
                    $existedStock = (int)$existedStock-(int)$item->quantity;
                    $productVariant->additional_stock = $existedStock;
                    $productVariant->save();
                }
            }

            // ~~ delete cart
            // $checkout->cart->r_more_transaction_cart_details()->delete();
            $checkout->cart->r_more_transaction_cart_details()->where('is_selected', 1)->delete();

            $user = User::find($order->more_users_id);
            $user->last_payment_code = $order->faspay_code;
            $user->last_address_id = $address->id;
            $user->save();

            // ? SAVE HISTORY TRANSACTION
            if($request->affiliate) {
                foreach ($request->affiliate['utm_content'] as $code_link) {
                    $htr = new HistoryTransaction();
                    $htr->code_link = $code_link;
                    $htr->name_device = $request->affiliate['name_device'] ?: null;
                    $htr->ip_device = $request->affiliate['ip_device'] ?: null;
                    $htr->more_users_id = $user->id;
                    $htr->ordersn = $order->ordersn;
                    $htr->save();
                }
            }

            $order->load(
                'r_more_transaction_order_details',
                'r_more_transaction_order_details.r_more_transaction_order_products',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_transaction_order_products_bundling',
                'r_uni_gen_payment_faspay'
            );

            //~ notif dihandle cronjob, agar proses menyimpan transaksi lebih cepat
            // $this->setStatusOrder($order, 1, false);

            $order = $order->toArray();
            $order['faspay_response'] = $faspayResult->faspay;

            DB::commit();
            return response_json($order);
        } catch (\Exception $e) {
            DB::rollback();
            // dd($e->getMessage());
            \Log::info('ERROR CREATE ORDER', ['message' => $e->getMessage(),
            'line' => $e->getLine(),
            'params' => ($request) ? json_encode(is_object($request) ? $request->all() : $request) : null,
            'stack_trace' => $e->getTraceAsString(),
            'user'=> Auth::user()->id,
            'error_code' => $e->getCode()]);

            return response_json("Gagal menyimpan Transaksi, Coba beberapa saat lagi." . $e->getMessage(), 500);
        }
    }

    public function preCheckoutSummary(Request $request, $returnJson = true)
    {
        $validator = \Validator::make($request->all(), [
            'city_id' => 'required',
            'address_id' => '',
            'payment_code' => '',
            'voucher' => '',
        ], [], []);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $device = $request->device ?: 'mobile';
        $fee = $request->fee ?: 0;

        $products = [];
        $faspayItems = [];
        $dokuItems = [];

        $cart = Cart::with([
            'r_more_transaction_cart_details' => function($q) {
                $q->where("is_selected", 1);
            },
            'r_more_transaction_cart_details.r_uni_product_variants',
            'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product',
            'r_more_transaction_cart_details.r_more_transaction_cart_product_frees',
            'r_more_transaction_cart_details.r_more_transaction_cart_bundlings',
            'r_more_transaction_cart_details.r_more_transaction_cart_bundlings.r_uni_product_variants',
            'r_more_transaction_cart_details.r_uni_product_get_combo',
            'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants',
            'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants_main',
            'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants.r_uni_product_variant_prices',
            'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants.r_uni_product_image',
        ])
            ->where('status', 1)->where('more_users_id', auth()->user()->id)->firstOrFail();

        //~~ Checking Voucher
        $checkVoucher = null;
        if ($request->filled("voucher") && $request->filled("address_id") && $request->filled("payment_code")) {
            $checkVoucher = Order::checkVoucherValidity($request->voucher, $request->address_id, $request->payment_code, $cart, $device);
        }

        //~~ Checking all items in cart
        $totalShippingPrice = 0;
        $allProductsIsValid = true;
        if (count($cart->r_more_transaction_cart_details) > 0) {
            $details = [];
            foreach ($cart->r_more_transaction_cart_details as $key => $detail) {
                $details[] = $detail;
                if ($detail->is_get_free == 1 && $detail->r_more_transaction_cart_product_frees) {
                    $details[] = [
                        "id" => intval($detail->id . $detail->uni_product_variants_id),
                        "more_transaction_carts_id" => $detail->more_transaction_carts_id,
                        "r_more_transaction_cart_bundlings" => [],
                        "uni_product_variants_id" => $detail->r_more_transaction_cart_product_frees->uni_product_variants_id,
                        "quantity" => $detail->quantity,
                        "sub_total" => 0,
                        "sub_total_rp" => "Rp 0",
                        "is_get_free" => $detail->is_get_free,
                        "is_get_combo" => 0,
                        "is_bundling" => 0,
                        "is_flashsale" => 0,
                        "free_description" => $detail->r_more_transaction_cart_product_frees->free_description,
                        "more_promotion_product_frees_id" => $detail->r_more_transaction_cart_product_frees->more_promotion_product_frees_id,
                    ];
                }

                if ($detail->is_get_combo == 1 && $detail->r_more_transaction_cart_product_frees) {
                    // ? COMBO / KOMBO HEMAT
                    $comboPrice = floatval($detail->r_uni_product_get_combo->combo_price);
                    $_subTotal = $detail->quantity * $comboPrice;
                    $details[] = [
                        "id" => intval($detail->id . $detail->uni_product_variants_id),
                        "more_transaction_carts_id" => $detail->more_transaction_carts_id,
                        "r_more_transaction_cart_bundlings" => [],
                        "uni_product_variants_id" => $detail->r_more_transaction_cart_product_frees->uni_product_variants_id,
                        "quantity" => $detail->quantity,
                        "sub_total" => $_subTotal,
                        "sub_total_rp" => toRp($_subTotal),
                        "is_get_free" => 0,
                        "is_get_combo" => $detail->is_get_combo,
                        "is_bundling" => 0,
                        "is_flashsale" => 0,
                        "free_description" => $detail->r_more_transaction_cart_product_frees->free_description,
                        "more_promotion_product_frees_id" => $detail->r_more_transaction_cart_product_frees->more_promotion_product_frees_id,
                    ];
                }
            }


            //Nilai Voucher dihilangkan di produk pertama
            $discount_amount =  $checkVoucher &&  $checkVoucher->isValid ? floatval( $checkVoucher->voucher->potongan) : 0;
            $discount_amount_doku =  $checkVoucher &&  $checkVoucher->isValid ? floatval( $checkVoucher->voucher->potongan) : 0;


            foreach ($details as $key => $item) {

                $item = Order::preCheckoutValidity((object)$item, $request->city_id);

                if (!$item->variant_detail->pre_checkout_status) $allProductsIsValid = false;

                //~ hitung sub ongkir
                $item->sub_shipping_price = 0;
                // if (isset($item->variant_detail->pre_checkout_area->shipping_area->price)) $item->sub_shipping_price = $item->quantity * $item->variant_detail->pre_checkout_area->shipping_area->price;
                // $totalShippingPrice += $item->sub_shipping_price;
                if (isset($item->variant_detail->pre_checkout_area->shipping_area)) $item->sub_shipping_price = $item->variant_detail->pre_checkout_area->shipping_area;
                $totalShippingPrice += $item->sub_shipping_price;

                $products[$key] = $item;

                //~~ FASPAY ITEM
                $fpayItem = [];
                $fpayItem["id"] = $item->variant_detail->id;
                $fpayItem["product"] = $item->variant_detail->name_long;
                $fpayItem["qty"] = $item->quantity;
                $fpayItem["amount"] = strval($item->sub_total+$item->sub_shipping_price-$discount_amount) . "00"; $discount_amount=0;
                $fpayItem["type"] = "Furniture";
                $fpayItem["payment_plan"] = "01";
                $fpayItem["url"] = config("app.web_url");
                if($item->variant_detail->r_uni_product_image){
                    $fpayItem["image_url"] = $item->variant_detail->r_uni_product_image->image_url;
                }else{
                    $fpayItem["image_url"] = "https://cdn.morefurniture.id/images/noimage.png";
                }


                $fpayItem["merchant_id"] = config("faspay.merchant_id");
                $faspayItems[$key] = $fpayItem;

                //~~ DOKU ITEM
                $dokuItem = [];
                $dokuItem["name"] = replaceMultipleChars($item->variant_detail->name_long);
                $dokuItem["price"] = strval($item->sub_total+$item->sub_shipping_price-$discount_amount_doku); $discount_amount_doku=0;
                $dokuItem["quantity"] = 1;
                $dokuItems[$key] = $dokuItem;
            }

            //Check If Voucher //Dihilangkan
            // if ($request->filled("voucher")){

            //     $discount_amount =  $checkVoucher &&  $checkVoucher->isValid ? floatval( $checkVoucher->voucher->potongan) : 0;
            //     //~~ FASPAY ITEM
            //     $fpayItem = [];
            //     $fpayItem["id"] = 999999;
            //     $fpayItem["product"] = 'Voucher';
            //     $fpayItem["qty"] = 1;
            //     $fpayItem["amount"] = "-".$discount_amount. "00";
            //     $fpayItem["type"] = "Furniture";
            //     $fpayItem["payment_plan"] = "01";
            //     $fpayItem["url"] = config("app.web_url");
            //     $fpayItem["image_url"] = "https://cdn.morefurniture.id/images/noimage.png";
            //     $fpayItem["merchant_id"] = config("faspay.merchant_id");
            //     $faspayItems[999999] = $fpayItem;

            // }
        }

        $results = [];
        $results["cart"] = $cart;
        $results["cart_items"] = $products;
        $results["faspay_items"] = $faspayItems;
        $results["doku_items"] = $dokuItems;
        $results["voucher"] = $checkVoucher;

        // if($allProductsIsValid && $request->filled("voucher") && !$checkVoucher->isValid) $allProductsIsValid = false;

        $results["allProductsIsValid"] = $allProductsIsValid;

        $results["summary"] = [];
        $results["summary"]["cart_grand_total"] = round($cart->grand_total, 0, PHP_ROUND_HALF_UP);
        $results["summary"]["shipping_price"] = round($totalShippingPrice, 0, PHP_ROUND_HALF_UP);
        $results["summary"]["discount_amount"] = $results["voucher"] && $results["voucher"]->isValid ? floatval($results["voucher"]->voucher->potongan) : 0;
        $results["summary"]["grand_total"] = ($results["summary"]["cart_grand_total"] + $results["summary"]["shipping_price"]) + $fee;
        $results["summary"]["bill_total"] = ($results["summary"]["cart_grand_total"] + $results["summary"]["shipping_price"]) + $fee - $results["summary"]["discount_amount"];

        $results["summary"]["cart_grand_total_rp"] = formatRupiah($results["summary"]["cart_grand_total"]);
        $results["summary"]["shipping_price_rp"] = formatRupiah($results["summary"]["shipping_price"]);
        $results["summary"]["discount_amount_rp"] = formatRupiah($results["summary"]["discount_amount"]);
        $results["summary"]["grand_total_rp"] = formatRupiah($results["summary"]["grand_total"]);
        $results["summary"]["bill_total_rp"] = formatRupiah($results["summary"]["bill_total"]);
        $results["summary"] = (object)$results["summary"];
        return $returnJson ? response_json($results) : (object)$results;
    }

    public function paymentNotifCreditCard(Request $request)
    {

        $resp = [
            "response" => "Payment Notification",
            "trx_id" => $request->TRANSACTIONID,
            "merchant_id" => $request->MERCHANTID,
            "bill_no" => $request->MERCHANT_TRANID,
            "response_code" => "S",
            "response_desc" => "Sukses",
            "response_date" => date('Y-m-d H:i:s'),
        ];

        $order = Order::where("ordersn", $request->MERCHANT_TRANID)->first();
        try {
            if ($request->TXN_STATUS === "S" || $request->TXN_STATUS === "C") {
                //~ jika order tidak ditemukan
                if (!$order) {
                    $resp["response_code"] = "E";
                    $resp["response_desc"] = "Order tidak ditemukan.";
                    $resp["response_date"] = date('Y-m-d H:i:s');
                }

                if (!$this->setStatusOrder($order, 2)) {
                    $resp["response_desc"] = "Gagal mengubah status transaksi, silahkan coba beberapa saat lagi.";
                    $resp["response_date"] = date('Y-m-d H:i:s');
                }

            } else {
                $this->setStatusOrder($order, 92);
                $resp["response_code"] = $request->TXN_STATUS;
                $resp["response_desc"] = $request->USR_MSG;
                $resp["response_date"] = date('Y-m-d H:i:s');
            }
        } catch (\Exception $e) {
            $resp["response_code"] = "E";
            $resp["response_desc"] = "Gagal mengirim notif pembayaran, coba lagi nanti.";
            $resp["response_catch_desc"] = $e->getMessage();
            $resp["response_date"] = date('Y-m-d H:i:s');
        }

        $order->faspay_response_code = $resp["response_code"];
        $order->faspay_response_desc = $resp["response_desc"];
        $order->save();
        return redirect(config("app.web_url") . "/checkout/thanks?bill_no=" . $resp["bill_no"]);

    }

    public function paymentNotif(Request $request){
        $faspay = (object)config("faspay");
        $resp = [
            "response" => "Payment Notification",
            "trx_id" => $request->trx_id,
            "merchant_id" => $request->merchant_id,
            "merchant" => $request->merchant,
            "bill_no" => $request->bill_no,
        ];

        $order = Order::where("ordersn",$request->bill_no)->first();
        try {

            //~ jika order tidak ditemukan
            if(!$order){
                $resp["response_code"] = "66";
                $resp["response_desc"] = "Order tidak ditemukan.";
                $resp["response_date"] = date('Y-m-d H:i:s');

                $order->faspay_response_code = $resp["response_code"];
                $order->faspay_response_desc = $resp["response_desc"];
                $order->save();
                return response($resp);
            }

            //~ jika signature tidak dikenali
            $ourSignature = sha1(md5($faspay->merchant_user.$faspay->merchant_password.$order->ordersn.$request->payment_status_code));
            if($ourSignature !== $request->signature){
                $resp["oursignature"] = $ourSignature;
                $resp["signature"] = $request->signature;
                $resp["response_code"] = "66";
                $resp["response_desc"] = "Signature tidak dikenali.";
                $resp["response_date"] = date('Y-m-d H:i:s');

                $order->faspay_response_code = $resp["response_code"];
                $order->faspay_response_desc = $resp["response_desc"];
                $order->save();
                return response($resp);
            }

            if($order){
                $resp["outbill"] = $order->total;
                $resp["bill"] = $request->bill_total;
                $resp["response_code"] = "00";
                if($order->total != $request->bill_total) {
                    $resp["response_desc"] = "Total tagihan tidak sama.";
                    $resp["response_date"] = date('Y-m-d H:i:s');

                    $order->faspay_response_code = $resp["response_code"];
                    $order->faspay_response_desc = $resp["response_desc"];
                    $order->save();
                    return response($resp);
                }else if($request->bill_total != $request->payment_total){
                    $resp["response_desc"] = "Jumlah yang dibayar tidak sama dengan jumlah tagihan.";
                    $resp["response_date"] = date('Y-m-d H:i:s');

                    $order->faspay_response_code = $resp["response_code"];
                    $order->faspay_response_desc = $resp["response_desc"];
                    $order->save();
                    return response($resp);
                }
            }

            //~ response api ke faspay
            $resp["response_code"] = "00";
            $resp["response_desc"] = "Sukses";
            $resp["response_date"] = date('Y-m-d H:i:s');

            if(!$this->setStatusOrder($order, 2)){
                $resp["response_desc"] = "Gagal mengubah status transaksi, silahkan coba beberapa saat lagi.";
                $resp["response_date"] = date('Y-m-d H:i:s');
            }

            $order->faspay_response_code = $resp["response_code"];
            $order->faspay_response_desc = $resp["response_desc"];
            $order->save();
            // Log::info("paymentNotif-Success",[$resp]);
            return response($resp);
        } catch (\Exception $e) {
            $resp["response_code"] = "66";
            $resp["response_desc"] = "Gagal mengirim notif pembayaran, coba lagi nanti.";
            $resp["response_date"] = date('Y-m-d H:i:s');
            // $resp["faspay"] = $faspay;
            $order->faspay_response_code = $resp["response_code"];
            $order->faspay_response_desc = $resp["response_desc"];
            $order->save();
            // Log::info("paymentNotif-Error",[$resp]);
            // Log::info("paymentNotif-Catch",[$e->getMessage()]);
            return response($resp);
        }
    }

    public function dokuPaymentNotifVA(Request $request){
        $faspay = (object)config("faspay");
    }

    public function dokuPaymentNotifCC(Request $request){

        $faspay = (object)config("faspay");

        $notificationHeader = getallheaders();
        $notificationBody = file_get_contents('php://input');
        $notificationPath = '/more/transaction/order/doku-payment-notif-cc'; // Adjust according to your notification path
        $secretKey = $faspay->doku_secret_key;

        $digest = base64_encode(hash('sha256', $notificationBody, true));
        $rawSignature = "Client-Id:" . $notificationHeader['Client-Id'] . "\n"
            . "Request-Id:" . $notificationHeader['Request-Id'] . "\n"
            . "Request-Timestamp:" . $notificationHeader['Request-Timestamp'] . "\n"
            . "Request-Target:" . $notificationPath . "\n"
            . "Digest:" . $digest;


        $signature = base64_encode(hash_hmac('sha256', $rawSignature, $secretKey, true));
        $finalSignature = 'HMACSHA256=' . $signature;
        $data = json_decode($notificationBody);

        if ($finalSignature == $notificationHeader['Signature']) {
            // TODO: Process if Signature is Valid
            $ordersn = $data->order->invoice_number;
            $order = Order::where("ordersn", $ordersn)->first();
            if (!$order) {
                return response('Tidak Adda Order SN', 400)->header('Content-Type', 'text/plain');
            }

            $status = $data->transaction->status;
            if($status == 'SUCCESS'){
                $this->setStatusOrder($order, 2);
                return response('OK', 200)->header('Content-Type', 'text/plain');
            }else{
                $this->setStatusOrder($order, 92);
                return response('Status Gagal', 400)->header('Content-Type', 'text/plain');
            }
            // TODO: Do update the transaction status based on the `transaction.status`
        } else {
            // TODO: Response with 400 errors for Invalid Signature
            return response('Invalid Signature', 400)->header('Content-Type', 'text/plain');
        }

    }

    public function plusRemainsOfFlashsaleAndFreeProduct($order_id)
    {
        $fsProducts = OrderProduct::where("more_transaction_orders_id", $order_id)->whereNotNull("more_promotion_flashsale_products_id")->get();
        foreach ($fsProducts as $item) {
            $model = FlashsaleProduct::find($item->more_promotion_flashsale_products_id);
            if ($model) {
                $model->remains = $model->remains + 1;
                $model->save();
            }
        }

        $freeProducts = OrderProduct::where("more_transaction_orders_id", $order_id)->whereNotNull("more_promotion_product_frees_id")->get();
        foreach ($freeProducts as $item) {
            $model = ProductFree::find($item->more_promotion_flashsale_products_id);
            if ($model) {
                $model->remains = $model->remains + 1;
                $model->save();
            }
        }
    }

    public function setStatusOrder($order, $status_id, $withPushNotif = true)
    {
        $success = false;
        $timestamp = date("Y-m-d H:i:s");
        $env = env('APP_ENV', 'production');

        $order = (object)$order;
        if (($order && $status_id > 0) && $order->status != $status_id) {
            $existed = OrderHistory::where("more_transaction_orders_id", $order->id)->where("status", $status_id)->count();
            if ($existed > 0) {
                $order->notified = 1;
                $order->updated_at = $timestamp;
                $order->save();
                return false;
            }

            DB::beginTransaction();
            try {
                if ($status_id == 1) {
                    $order->created_at = $timestamp;
                } elseif ($status_id == 2) {
                    $order->paid_at = $timestamp;

                    if ($env == "production") {
                        $this->sendOrderToERP($order->ordersn);
                    }

                }elseif($status_id == 3){
                    $order->processed_at = $timestamp;
                } elseif ($status_id == 4) {
                    $order->shipped_at = $timestamp;
                } elseif ($status_id == 5) {
                    $order->complained_at = $timestamp;
                } elseif ($status_id == 6) {
                    User::syncingPointsByOrder($order);
                    $order->completed_at = $timestamp;
                } elseif ($status_id == 91) {
                    $order->canceled_at = $timestamp;
                    $this->plusRemainsOfFlashsaleAndFreeProduct($order->id);
                } elseif ($status_id == 92) {
                    $order->autocanceled_at = $timestamp;
                    $this->plusRemainsOfFlashsaleAndFreeProduct($order->id);
                }

                //~ update order (status bisa dinamis!)
                $order->status = $status_id;
                $order->notified = 0;
                $order->updated_at = $timestamp;
                $order->save();

                //~ saving history
                $desc = UserNotification::getDescription($order)->history;
                if ($desc) {
                    $history = new OrderHistory();
                    $history->more_transaction_orders_id = $order->id;
                    $history->status = $order->status;
                    $history->note = $desc;
                    $history->created_at = $timestamp;
                    $history->save();
                }

                DB::commit();
                $success = true;
            } catch (\Exception $e) {
                // Log::error("setStatusOrder-Catch",[$e->getMessage()]);
                DB::rollback();
                $success = false;
            }

            if ($success && $withPushNotif) {
                $notif = new UserNotificationController;
                $notif->pushNotif($order);
            }
        }


        return $success;
    }

    public function show($ordersn)
    {
        try {
            $order = Order::where("ordersn", $ordersn)->first();
            $infoResi = null;
            $jsonResi = null;
            $jsonLastResi = null;

            if (!$order || !auth()->user() || $order->more_users_id != auth()->user()->id) return response_json('Anda tidak memiliki akses atas informasi ini.', 403);

            if ($order->status == 4) {
                $detail = $order->r_more_transaction_order_details[0];

                $shipmentTracking = Order::fetchShipmentTracking($detail->expedition, $detail->receipt_number);
                $infoResi = $shipmentTracking->info;
                $jsonResi = $shipmentTracking->trackings;
                $jsonLastResi = $shipmentTracking->last_tracking;
            }

            $order->load(
                'r_more_transaction_order_details',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free.r_uni_product_variants',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free.r_uni_product_variants.r_uni_product_variant_prices',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_free.r_uni_product_variants.r_uni_product_image',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_combo',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_combo.r_uni_product_variants',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_get_combo.r_uni_product_variants_main',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_image',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_variant_prices',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_transaction_order_products_bundling',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_product_review',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_product_review.r_more_product_review_images',
                'r_more_complain',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_complain_products',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_complain_products.r_more_complain_product_images',
                'r_uni_gen_payment_faspay',
            );

            return response_json(compact('order', 'infoResi', 'jsonResi', 'jsonLastResi'));
        } catch (\Exception $e) {
            // Log::error("OrderController:Show",[$e->getMessage()]);
            return response_json('Gagal mendapatkan data Order', 403);
        }
    }

    public function detailApps($ordersn)
    {
        try {
            $order = Order::where("ordersn", $ordersn)->first();
            $infoResi = null;
            $jsonResi = null;
            $jsonLastResi = null;


            if ($order->status == 4) {
                $detail = $order->r_more_transaction_order_details[0];

                $shipmentTracking = Order::fetchShipmentTracking($detail->expedition, $detail->receipt_number);
                $infoResi = $shipmentTracking->info;
                $jsonResi = $shipmentTracking->trackings;
                $jsonLastResi = $shipmentTracking->last_tracking;
            }

            $order->load(
                'r_more_transaction_order_details',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_image',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_uni_product_variant.r_uni_product_variant_prices',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_transaction_order_products_bundling',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_product_review',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_product_review.r_more_product_review_images',
                'r_more_complain',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_complain_products',
                'r_more_transaction_order_details.r_more_transaction_order_products.r_more_complain_products.r_more_complain_product_images',
                'r_uni_gen_payment_faspay',
            );

            return response_json(compact('order', 'infoResi', 'jsonResi', 'jsonLastResi'));
        } catch (\Exception $e) {
            // Log::error("OrderController:Show",[$e->getMessage()]);
            return response_json('Gagal mendapatkan data Order', 403);
        }
    }

    public function tracking(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'expedition' => 'required',
            'receipt_number' => 'required',
        ], [], []);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $infoResi = null;
        $jsonResi = null;
        $jsonLastResi = null;
        $shipmentTracking = Order::fetchShipmentTracking($request->expedition, $request->receipt_number);
        $infoResi = $shipmentTracking->info;
        $jsonResi = $shipmentTracking->trackings;
        $jsonLastResi = $shipmentTracking->last_tracking;

        return response_json(compact('infoResi', 'jsonResi', 'jsonLastResi'));
    }

    private function sendPixelPurchaseFB(){
        // Generated @ codebeautify.org
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/v2.8/act_381446892652325/customconversions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $post = array(
            'name' => 'PURCHASE',
            'pixel_id' => '519687591908953',
            'pixel_rule' => array(
                'url' => array('i_contains' => 'thank-you.html'),
            ),
            'custom_event_type' => 'PURCHASE',
            'access_token' => 'EAAaqoiZCuw8MBAJcovzOSfSG26YZAGH5M6ZBgAMklm3wU8UUCCDfYVhq9PjQpRT38ZCQYBIlvqsFN1ZBA3WxPoVFHqQEvfTJvZCiSLtA9fly4rwcHlcaiTsvuZCVT0t9POJR9RQj2U1AYalyFV3RxX4g3rt5ZBQIyDqHKbHnfYf4GZB0ZAhrZAVA5jm'
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $result = curl_exec($ch);
        dd($result);
        // if (curl_errno($ch)) {
        //     response_json($ch);
        //     // echo 'Error:' . curl_error($ch);
        // }
        // curl_close($ch);

    }

    public function listReasonCancelOrder()
    {
        $data = [];
        $data[0] = 'Ingin mengubah alamat pengiriman pesanan';
        $data[1] = 'Ingin mengubah detail pesanan (varian, warna, jumlah)';
        $data[2] = 'Ingin mengubah metode pembayaran';
        $data[3] = 'Tidak ingin membeli';
        return response_json($data);
    }

    public function cancelOrder($ordersn, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'note_cancel' => 'required',
        ], [], []);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        DB::beginTransaction();
        try {
            $order = Order::where("ordersn", $ordersn)->first();
            if (!$order) return response_json("Order tidak ditemukan", 403);

            $order->note_cancel = $request->note_cancel;
            $order->save();

            $this->setStatusOrder($order, 91);
            DB::commit();
            return response_json($order);
        } catch (\Exception $e) {
            // Log::error("cancelOrder",$e->getMessage());
            DB::rollback();
            return response_json("Gagal batalkan pesanan!, coba lagi nanti.");
        }
    }

    public function processOrder($ordersn)
    {
        if ($ordersn) {
            DB::beginTransaction();
            try {
                $orderAffiliate = OrderAffiliate::where("ordersn", $ordersn)->first();
                if($orderAffiliate){
                    $orderAffiliate->status = 3;
                    $orderAffiliate->save();

                    DB::commit();
                    return response_json($orderAffiliate);
                }

                $order = Order::where("ordersn", $ordersn)->first();
                if (!$order) return response_json("Order tidak ditemukan", 403);

                $this->setStatusOrder($order, 3);
                DB::commit();
                return response_json($order);
            } catch (\Exception $e) {
                // Log::error("shippingOrder",$e->getMessage());
                DB::rollback();
                return response_json("Gagal proses pesanan!, coba lagi nanti.", 403);
            }
        }
        return response_json("No pesanan wajib dilengkapi", 403);
    }

    public function shipOrder($ordersn, Request $request)
    {

        if ($ordersn) {
            $validator = \Validator::make($request->all(), [
                'expedition' => 'required',
                // 'receipt_number' => 'required',
            ], [], []);

            if ($validator->fails()) {
                return response_json($validator->errors(), 403);
            }

            DB::beginTransaction();
            try {
                $orderAffiliate = OrderAffiliate::where("ordersn", $ordersn)->first();
                if($orderAffiliate){
                    $orderAffiliate->status = 4;
                    $orderAffiliate->save();

                    DB::commit();
                    return response_json($orderAffiliate);
                }

                $order = Order::where("ordersn", $ordersn)->first();
                if (!$order) return response_json("Order tidak ditemukan", 403);

                $detail = OrderDetail::where("more_transaction_orders_id", $order->id)->first();
                $detail->expedition = trim($request->expedition);
                $detail->receipt_number = trim($request->receipt_number);
                $detail->save();

                $this->setStatusOrder($order, 4);
                DB::commit();
                return response_json($order);
            } catch (\Exception $e) {
                // Log::error("shippingOrder",$e->getMessage());
                DB::rollback();
                return response_json("Gagal mengirim pesanan!, coba lagi nanti.", 403);
            }
        }
        return response_json("No pesanan wajib dilengkapi", 403);
    }

    public function completeOrder($ordersn)
    {
        if ($ordersn) {
            DB::beginTransaction();
            try {
                $order = Order::where("ordersn", $ordersn)->first();
                if (!$order) return response_json("Order tidak ditemukan", 403);

                $this->setStatusOrder($order, 6);
                DB::commit();
                return response_json($order);
            } catch (\Exception $e) {
                // Log::error("completeOrder",$e->getMessage());
                DB::rollback();
                return response_json("Gagal selesaikan pesanan!, coba lagi nanti.");
            }
        }
        return response_json("No pesanan wajib dilengkapi", 403);
    }

    public function sendOrderToERP($ordersn)
    {
        //TOKEN EFI ERP (STATIC)

        //START GET & INSERT ORDER
        $data_header = Order::where("ordersn", $ordersn)->first();
        $data_header_potongan = $data_header->voucher_amount ?: 0;
        $data_header_pembelian = (int)$data_header_potongan + $data_header->total;

        //DATA DETAIL TRANSAKSI MORE, UBAH $raw_data['detail'] DENGAN DATA DETAIL MORE
        $query = "select v.sku_item,p.sub_total, p.price_delivery, p.price,p.quantity, o.created_at from more_transaction_orders o
        inner join more_transaction_order_details d on d.more_transaction_orders_id = o.id
        inner join more_transaction_order_products p on p.more_transaction_order_details_id = d.id
        inner join uni_product_variants v on v.id = p.uni_product_variants_id
        where o.ordersn = '$ordersn'";
        $data_detail = DB::select($query);

        //UNTUK TAMPUNG DETAIL TRANSAKSI SAAT TEMBAK KE ERP
        $post_data_detail = [];
        $sub_total_detail = 0;

        foreach ($data_detail as $raw_data2) {
            $kode_sku = $raw_data2->sku_item;
            if ($raw_data2->price_delivery == null) {
                $price = $raw_data2->price;
            } else {
                $hitungPengirimanPerBarang = (int)$raw_data2->price_delivery / $raw_data2->quantity;
                $price = (int)$raw_data2->price + (int)$hitungPengirimanPerBarang;
            }
            $amount = (int)$raw_data2->sub_total + (int)$raw_data2->price_delivery; //Sebelum Diskon
            $komposisi_persen = $amount / $data_header_pembelian;
            $discount = $komposisi_persen * $data_header_potongan; //Kolom diskon
            $netto = $amount - $discount; //Total sub setelah potongan

            array_push(
                $post_data_detail,
                array(
                    "item_id"        => 0, //ISI 0
                    "item"            => trim($kode_sku, " "), //ISI DENGAN SKU BARANG
                    "color"            => "", //KOSONGI SAJA
                    "etd"             => date('Y-m-d', strtotime($raw_data2->created_at)), //ISI DENGAN TGL TRANSAKSI
                    "tipe_item"        => '0', //ISI 0
                    "price_type"    => '0', //ISI 0

                    "qty"            => $raw_data2->quantity, //ISI DENGAN QTY
                    "unit"            => "", //KOSONGI SAJA

                    // "price"            => $raw_data2->price,//ISI PRICE
                    // "price"            => $price, //ISI PRICE //Sebelum Diskon -> Setelah Diskon
                    "price"            => ($netto/$raw_data2->quantity), //ISI PRICE //Sebelum Diskon -> Setelah Diskon
                    "ongkir"        => 0, //ISI 0
                    "discount"        => $discount, //ISI 0 //Kolom diskon di detail
                    "amount"        => $netto, //ISI SUBTOTAL DETAIL //Sebelum Diskon -> Setelah Diskon
                    "netto"            => $netto //ISI SUBTOTAL DETAIL //Setelah Diskon
                )
            );
            $sub_total_detail = $sub_total_detail + $netto;
        }


        //AUTO EXPEDISI DAN VENDOR
        $master_order = $data_header;
        $full_address = $master_order->address . ", DESA " . trim($master_order->villages) . ", KECAMATAN " . trim($master_order->districts) . ", " . trim($master_order->cities) . ", PROVINSI " . trim($master_order->provinces) . " " . $master_order->postal_code;

        $provinsi = $master_order->provinces; //ISI PROVINSI ALAMAT TUJUAN TRANSAKSI
        $kota = $master_order->cities; //ISI KOTA ALAMAT TUJUAN TRANSAKSI

        //BAWAH INI DEFAULT.. NDA USAH DIRUBAH
        //===============================START=============================================

        $arr_taufiqul = array("SURABAYA", "KOTA SURABAYA", "SIDOARJO", "KAB SIDOARJO", "KABUPATEN SIDOARJO", "GRESIK", "KAB GRESIK", "KABUPATEN GRESIK");

        $arr_jatim = array("DKI JAKARTA", "JAWA TENGAH", "JAWA TIMUR");

        $arr_sentral_provinsi = array("DI YOGYAKARTA", "D.I. YOGYAKARTA");
        $arr_sentral_kota = array("BOGOR", "KOTA BOGOR", "KAB BOGOR", "KAB. BOGOR", "KABUPATEN BOGOR", "DEPOK", "KOTA DEPOK", "KAB DEPOK", "KAB. DEPOK", "KABUPATEN DEPOK", "TANGERANG", "KOTA TANGERANG", "KAB TANGERANG", "KAB. TANGERANG", "KABUPATEN TANGERANG", "BEKASI", "KOTA BEKASI", "KAB BEKASI", "KAB. BEKASI", "KABUPATEN BEKASI");
        $arr_indah = array("JAWA BARAT", "BANTEN", "BALI");

        $expedition_id = 0;
        $expedition = "";

        if (in_array($kota, $arr_taufiqul)) {
            $expedition_id = 10;
            $expedition = "TAUFIQUL";
        } else if (in_array($provinsi, $arr_jatim)) {
            $expedition_id = 15;
            $expedition = "CCW CARGO";
        } else if (in_array($provinsi, $arr_sentral_provinsi)) {
            $expedition_id = 9;
            $expedition = "INDAH CARGO";
        } else if (in_array($kota, $arr_sentral_kota)) {
            $expedition_id = 15;
            $expedition = "CCW CARGO";
        } else if (in_array($provinsi, $arr_indah)) {
            $expedition_id = 9;
            $expedition = "INDAH CARGO";
        }
        //=================================FINISH===========================================

        $platform = 'WEB - MORE';

        $total_bayar = $master_order->total; //ISI TOTAL BAYAR CUSTOMER


        $dpp = $total_bayar / 1.1; //HITUNG DPP UNTUK PAJAK
        $status_more = strtoupper($master_order->status); //ISI DENGAN STATUS MORE

        if($data_header->dropshipper != 0){
            $is_dropshipping = true;
            $dropshipper_id = $data_header->dropshipper;
            $dropshipper = $data_header->dropshipper_name;
        }else{
            $is_dropshipping = false;
            $dropshipper_id = null;
            $dropshipper =null;
        }

        $array_header = array(
            "expedition_id"     => $expedition_id, //DARI VARIABEL ATAS
            "expedition"         => $expedition, //DARI VARIABEL ATAS
            "platform_id"         => 45, //BIARKAN SEPERTI ITU
            "platform"             => $platform, //DARI VARIABEL ATAS
            "supplier_id"         => 183, //DARI VARIABEL ATAS
            "supplier"             =>'PT EFORIA FURNITURE INDONESIA', //DARI VARIABEL ATAS
            "date"                 => date('Y-m-d', strtotime($master_order->created_at)), //ISI DATE TRANSAKSI

            "customer"             => strtoupper($master_order->receiver_name), //ISI NAMA PENERIMA
            "customer_hp"         => $master_order->receiver_phone, //ISI HP PENERIMA
            "customer_address"     => trim($full_address), //ISI ALAMAT PENERIMA

            "province"             => trim($master_order->provinces), //ISI PROVINSI PENERIMA
            "city"                 => trim($master_order->cities), //ISI KOTA PENERIMA
            "kecamatan"         => trim($master_order->districts), //ISI KECAMATAN PENERIMA
            "kelurahan"         => trim($master_order->villages), //BIARKAN SEPERTI ITU
            "kode_pos"             => $master_order->postal_code, //ISI KODEPOS PENERIMA
            "free_shipping"     => true, //BIARKAN SEPERTI ITU


            "total_amount"         => (float)$data_header_pembelian, //DARI VARIABEL ATAS //Sebelum Diskon
            "total_discount"     => $data_header_potongan, //BIARKAN SEPERTI ITU
            "total_setelah_discount" => $total_bayar, //DARI VARIABEL ATAS
            "voucher_percent"        => 0, //BIARKAN SEPERTI ITU
            "voucher_nominal"        => 0, //BIARKAN SEPERTI ITU
            "total_setelah_voucher"    => $total_bayar, //DARI VARIABEL ATAS
            "dpp"                    => $dpp, //DARI VARIABEL ATAS
            "tax_percent" => 11, // ?????
            "tax_nominal"            => $dpp * 0.11, //DARI VARIABEL ATAS
            "total_setelah_tax"         => 0, //?????
            "total_ongkir"         => 0, //BIARKAN SEPERTI ITU
            "grand_total"         => (float)$total_bayar, //DARI VARIABEL ATAS //Setelah Diskon
            "komisi_percent"        => 0, //DARI VARIABEL ATAS
            "komisi_nominal"        => 0, //DARI VARIABEL ATAS
            "note"                 => strtoupper($master_order->note), //ISI DENGAN NOTE TRANSAKSI
            "ordersn"             => $master_order->ordersn, //ISI DENGAN ORDERSN TRANSAKSI MORE
            "status_marketplace"    => $status_more, //DARI VARIABEL ATAS
            'is_dropshipping' => $is_dropshipping,
            'dropshipper_id' => $dropshipper_id,
            'dropshipper' => $dropshipper,
            'created_at_marketplace' => date('Y-m-d H:i:s', strtotime($master_order->created_at)), //ISI DATE TRANSAKSI
            "mar_tra_sales_order_detail"    => json_encode($post_data_detail) //INI DARI DATA DETAIL DIATAS, BIARKAN SEPERTI ITU
        );


        $status_shopee = "";
        //UNTUK SET STATUS TRANSAKSI DI EFI ERP
        if ($status_shopee == 'CANCELLED') {
            $array_header['status'] = 'CANCELED';
        } else {
            $array_header['status'] = 'IN PROCESS';
        }

        //TAMPUNG SEMUA DATA TRANSAKSI DAN SESUAKAN FORMAT OBJECT ARRAY NYA
        $post_data_header = json_encode($array_header);

        // return response_json($post_data_header); // For Debug


        //INSERT KE EFI ERP
        $env = env('APP_ENV', 'production');
        if ($env == "development") {
            $url_efi = "http://149.129.241.85/multichannel/api/v1/sales-order/create";
        } else {
            $url_efi = "http://149.129.241.85/multichannel/api/v1/sales-order/create";
        }

        $body = $post_data_header;

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTQ5LjEyOS4yNDEuODUvbXVsdGljaGFubmVsL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjk2NDk0MzgyLCJuYmYiOjE2OTY0OTQzODIsImp0aSI6Ik1MMGNVTEFxMVZlVXFtS2wiLCJzdWIiOiI4NSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ._2osTBI8-A3CuuU2olKrQKsdmZTeOrrJuImpkYv1lV8';
        $request_headers_efi = array();
        $request_headers_efi[] = 'Content-Type: application/json';
        $request_headers_efi[] = 'Authorization: Bearer ' . $token;

        // Performing the HTTP request
        $ch_efi = curl_init();
        curl_setopt($ch_efi, CURLOPT_URL, $url_efi);
        curl_setopt($ch_efi, CURLOPT_HTTPHEADER, $request_headers_efi);
        // curl_setopt($ch_efi, CURLOPT_POST, true);
        curl_setopt($ch_efi, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch_efi, CURLOPT_RETURNTRANSFER, 1);

        // Performs the Request, with specified curl_setopt() options (if any).
        $response_efi = curl_exec($ch_efi);
        // dd($response_efi);
        $response_efi = json_decode($response_efi);

        curl_close($ch_efi);

        //FINISH


        return response()->json($response_efi);
        // END ORDER

    }

    public function ccPayment($encryptedString)
    {
        $decrypted = Crypt::decrypt($encryptedString);
        if ($decrypted) {
            $retrieved = explode(config("app.separator"), $decrypted);
            if ($retrieved && $retrieved[0] && $retrieved[1]) {
                $apikey = $retrieved[0];
                $ordersn = $retrieved[1];
                if ($apikey === config("app.apikey")) {
                    $env = env('APP_ENV', 'production');

                    $order = Order::where("ordersn", $ordersn)->with("r_more_transaction_order_details")->firstOrFail();
                    $user = User::find($order->more_users_id);
                    $shippingCost = OrderProduct::where("more_transaction_order_details_id", $order->r_more_transaction_order_details[0]->id)->sum("price_delivery");

                    //KARTU KREDIT
                    $faspay = (object)config("faspay");
                    $faspayMerchantId = $faspay->cc_merchant_id;
                    $faspayMerchantPassword = $faspay->cc_merchant_password;
                    $signature = sha1('##' . strtoupper($faspayMerchantId) . '##' . strtoupper($faspayMerchantPassword) . '##' . $order->ordersn . '##' . strval($order->total) . '.00##' . '0' . '##');

                    if($order->faspay_code !== "000" && $env == "production") {
                        // TODO: Cicilan 0% => select * from uni_gen_payment_faspay where type = 'Cicilan' and status = 1
                        $tenor = $order->tenor;
                        if($tenor) {
                            $cicilan = \DB::table("uni_gen_payment_faspay")->where("type", "Cicilan")->where("status", 1)->where("code", $order->faspay_code)->first();
                            if($cicilan) {
                                $faspayMerchantId = $cicilan->{"merchant_id_".$tenor};
                                $faspayMerchantPassword = $cicilan->{"merchant_password_".$tenor};
                                $signature = sha1('##' . strtoupper($faspayMerchantId) . '##' . strtoupper($faspayMerchantPassword) . '##' . $order->ordersn . '##' . strval($order->total) . '.00##' . '0' . '##');
                            }
                            else {
                                return response_json("Link tidak Valid", 403);
                            }
                        }
                        else {
                            return response_json("Link tidak Valid", 403);
                        }
                    }

                    $req = [
                        "TRANSACTIONTYPE"               => '1',
                        "RESPONSE_TYPE"                 => '2',
                        "LANG"                          => '',
                        "PAYMENT_METHOD"                => '1',
                        "MERCHANTID"                    => $faspayMerchantId,
                        "TXN_PASSWORD"                  => $faspayMerchantPassword,
                        "MERCHANT_TRANID"               => $order->ordersn,
                        "CURRENCYCODE"                  => 'IDR',
                        "AMOUNT"                        => strval($order->total) . '.00',
                        "CUSTNAME"                      => $order->receiver_name,
                        "CUSTEMAIL"                     => $user->email,
                        "DESCRIPTION"                   => $order->address,
                        "RETURN_URL"                    => config("app.app_url") . '/more/transaction/order/payment-notif-cc',
                        "SIGNATURE"                     => $signature,
                        "BILLING_ADDRESS"               => $order->address,
                        "BILLING_ADDRESS_CITY"          => $order->cities,
                        "BILLING_ADDRESS_REGION"        => $order->provinces,
                        "BILLING_ADDRESS_STATE"         => "Indonesia",
                        "BILLING_ADDRESS_POSCODE"       => $order->postal_code,
                        "BILLING_ADDRESS_COUNTRY_CODE"  => 'ID',
                        "RECEIVER_NAME_FOR_SHIPPING"    => $order->receiver_name,
                        "SHIPPING_ADDRESS"              => $order->address,
                        "SHIPPING_ADDRESS_CITY"         => $order->cities,
                        "SHIPPING_ADDRESS_REGION"       => $order->provinces,
                        "SHIPPING_ADDRESS_STATE"        => "Indonesia",
                        "SHIPPING_ADDRESS_POSCODE"      => $order->postal_code,
                        "SHIPPING_ADDRESS_COUNTRY_CODE" => 'ID',
                        "SHIPPINGCOST"                  => strval($shippingCost) . '.00',
                        "PHONE_NO"                      => $order->receiver_phone,
                        "MPARAM1"                       => '',
                        "MPARAM2"                       => '',
                        "PYMT_IND"                      => '',
                        "PYMT_CRITERIA"                 => '',
                        "PYMT_TOKEN"                    => '',
                        /* ==== customize input card page ===== */
                        "style_merchant_name"         => 'black',
                        "style_order_summary"         => 'black',
                        "style_order_no"              => 'black',
                        "style_order_desc"            => 'black',
                        "style_amount"                => 'black',
                        "style_background_left"       => '#fff',
                        "style_button_cancel"         => 'grey',
                        "style_font_cancel"           => 'white',
                        /* ==== logo directly to your url source ==== */
                        "style_image_url"           => 'http://url_merchant/image.png',
                    ];
                    // return $req;
                    return view("payments/creditCard", ["req" => $req, "link_post" => $faspay->cc_link_post]);
                }
            }
        }
        return response_json("Link tidak Valid", 403);
    }

    function checkDelivery(Request $request){
        $city = $request->get('city');
        $quantity = $request->get('qty');
        $sku_group = $request->get('sku_group'); //SKU GROUP
        $sku_item = $request->get('sku_item'); //SKU VARIANT

        $labelCity = $city;
        //Cek ketersediaan kota
        $expedition = ExpeditionPricelist::where('city_name', $city)->first();
        $maxRate = 0;

        if($expedition){
            $bundling = Bundling::where('sku_bundling', $sku_item)->get();

            if(count($bundling) != 0){
                //Produk Bundling
                foreach($bundling as $value){
                    //Check Product Bund
                    $pack = Pack::where('sku_group', $value->group_item)->first();
                    if($pack->type == 'pack'){ //Pack
                        $volume = $pack->volume/5000;
                        $kg = $pack->kilograms;
                        $rateDetailSKU= $kg;
                        if($volume > $kg){
                            $rateDetailSKU= $volume;
                        }
                        echo $value->group_item.' pack '.$rateDetailSKU.'<br>';
                    }else{
                        //Kelipatan
                        $pack = Pack::where('sku_group', $value->group_item)->where('qty','<=',$value->qty)->orderBy('qty','DESC')->first();
                        $volume = $pack->volume/5000;
                        $rateDetailSKU= $volume;
                        echo $value->group_item.' kelipatan '.$rateDetailSKU.'<br>';
                    }
                    $maxRate = $maxRate+($rateDetailSKU*$quantity); //Dikali jumlah pembelian barang


                }
            }else{

                //Check Product Bund
                $pack = Pack::where('sku_group', $sku_group)->first();

                if($pack->type == 'pack'){ //Pack
                    $volume = $pack->volume/5000;
                    $kg = $pack->kilograms;
                    $maxRate= $kg;
                    if($volume > $kg){
                        $maxRate= $volume;
                    }
                    $maxRate = $maxRate * $quantity;
                }else{
                    //Kelipatan
                    $pack = Pack::where('sku_group', $sku_group)->where('qty','<=',$quantity)->orderBy('qty','DESC')->first();
                    $volume = $pack->volume/5000;
                    $maxRate= $volume;
                }
            }

            if($maxRate < 10){ $maxRate = 10; }
            $rate = $expedition->rate;
            $shippingArea = ($rate-1600)*$maxRate;
            $shippingArea= 12000;
            $pembuatan = pembulatanOngkir($shippingArea);


            echo 'KAB KOTA : '.$labelCity.' <br>';
            echo 'SKU ITEM : '.$sku_item.' <br>';
            echo 'SKU GROUP  : '.$sku_group.' <br><br>';
            echo 'PRICE RATE :'.$rate.' <br>';
            echo 'MAX RATE :'.$maxRate.'<br>';
            echo 'PERHITUNGAN : ('.$rate.'-1600)*'.$maxRate.'<br>';
            echo 'DELIVERY : '.$shippingArea.' <br>';
            echo 'DELIVERY (PEMBULATAN) : '.$pembuatan.'  <br>';

        }
    }

    // public function sendOrderToERP($ordersn)
    // {
    //     //TOKEN EFI ERP (STATIC)
    //     $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJjNzk2Mjc1MzhkMTk4MDNhNjA1NmQyY2ZiYmZiMTBmZDQ4N2MzNmFjZDU3YWJiZDY5NmRhZWM5ODNiMzZkNzhhYmM2MTllNWE3MDUzZTUxIn0.eyJhdWQiOiIxIiwianRpIjoiYmM3OTYyNzUzOGQxOTgwM2E2MDU2ZDJjZmJiZmIxMGZkNDg3YzM2YWNkNTdhYmJkNjk2ZGFlYzk4M2IzNmQ3OGFiYzYxOWU1YTcwNTNlNTEiLCJpYXQiOjE2NTYzMTE1NDMsIm5iZiI6MTY1NjMxMTU0MywiZXhwIjoxNjg3ODQ3NTQzLCJzdWIiOiI0MiIsInNjb3BlcyI6W119.nseyn1Gc6U7Sm5BGNCXix69v-9VRjJF8uLz8lQc2a8spO3gz4-SrisTzTqW7gURBT1sGkHvqJK-v2lmietGtTrPsAA8hL4d8msO-da3t0KIvkG1xmOwOQY6r-05zgNF_MaxQa8Ph3vx0H9qG6RZwl254wMfLpew2ry22uRQoJZMfmIm7e_Xrm9onf-ksW8WS4thHPquVF8Ofp-mg9ALbzno4csN93S3NrntXUnAQVp4a90duZINmlBeGDjNBiHXpAEUa9wxyHQ3hiCmfzrn-k8Rb1DN7E9B3MksjLygQpUdV5blWQA4Q7CSIx-DhFbAYavghAh8bkrLErxBO0eoNIjRW6NewMESVGCvGnHgadLYVlvpmcfyFj8e8EqyvVt5YxFfvnZa62FJFUg_kmXL_NTCZCbCm7ICR7W3GHow_3MFS2ZxcVBIH-mPfh-zEiL15zc9rcC8noZpYpCn6LTZwUK7KY6xsqeI5tLfZ2hG292iT4yK0rpzHZaCkIQmZ0ZunI2XMBYsj08t_2Fl8rfcgW4JU309lfmvLHpi6txRmYVfpLXHerFBrT7bDWC-D-_z66wqIWgQ3aCl2RPmeVrLa3Htke52Rwp0vZq6ESAe9YIbghwpS6Vr9cPyb2OQtWTD2N494faLIfh1Echy0HvvlLOpftaTfZR-EB4bnk6J8kKY';



    //     //CEK JIKA ORDERSN SUDAH ADA DI EFI ERP
    //     $env = env('APP_ENV', 'production');
    //     if ($env == "development") {
    //         $url_efi_count_order_sn = "http://149.129.241.85/efiserver/public/operation/mar_tra_sales_order";
    //     } else {
    //         $url_efi_count_order_sn = "http://149.129.241.85/efiserver/public/operation/mar_tra_sales_order";
    //     }

    //     $request_headers_efi_order_sn = array();
    //     $request_headers_efi_order_sn[] = 'Content-Type: application/json';
    //     $request_headers_efi_order_sn[] = 'Authorization: Bearer ' . $token;

    //     $body_order_sn = http_build_query(array("where" => "ordersn = '" . $ordersn . "'"));

    //     // Performing the HTTP request
    //     $ch_efi_order_sn = curl_init();
    //     curl_setopt($ch_efi_order_sn, CURLOPT_URL, $url_efi_count_order_sn . "?" . $body_order_sn);
    //     curl_setopt($ch_efi_order_sn, CURLOPT_HTTPHEADER, $request_headers_efi_order_sn);
    //     curl_setopt($ch_efi_order_sn, CURLOPT_RETURNTRANSFER, 1);

    //     // Performs the Request, with specified curl_setopt() options (if any).
    //     $response_efi_order_sn = curl_exec($ch_efi_order_sn);
    //     $response_efi_order_sn = json_decode($response_efi_order_sn);

    //     curl_close($ch_efi_order_sn);
    //     if ($response_efi_order_sn == null) {
    //         $ceksn_efi = 0;
    //     } else {
    //         $ceksn_efi = count($response_efi_order_sn->data);
    //     }

    //     //MASUK JIKA ORDERSN MASIH BELUM ADA
    //     if ($ceksn_efi < 1) {
    //     } else {
    //         // return "Your order already exists";
    //     }
    //     //END CEK ORDERSN

    //     //START GET & INSERT ORDER
    //     $data_header = Order::where("ordersn", $ordersn)->first();
    //     $data_header_potongan = $data_header->voucher_amount ?: 0;
    //     $data_header_pembelian = (int)$data_header_potongan + $data_header->total;

    //     //DATA DETAIL TRANSAKSI MORE, UBAH $raw_data['detail'] DENGAN DATA DETAIL MORE
    //     $query = "select v.sku_item,p.sub_total, p.price_delivery, p.price,p.quantity, o.created_at from more_transaction_orders o
    //     inner join more_transaction_order_details d on d.more_transaction_orders_id = o.id
    //     inner join more_transaction_order_products p on p.more_transaction_order_details_id = d.id
    //     inner join uni_product_variants v on v.id = p.uni_product_variants_id
    //     where o.ordersn = '$ordersn'";
    //     $data_detail = DB::select($query);

    //     //UNTUK TAMPUNG DETAIL TRANSAKSI SAAT TEMBAK KE ERP
    //     $post_data_detail = [];
    //     $sub_total_detail = 0;

    //     foreach ($data_detail as $raw_data2) {
    //         $kode_sku = $raw_data2->sku_item;
    //         if ($raw_data2->price_delivery == null) {
    //             $price = $raw_data2->price;
    //         } else {
    //             $hitungPengirimanPerBarang = (int)$raw_data2->price_delivery / $raw_data2->quantity;
    //             $price = (int)$raw_data2->price + (int)$hitungPengirimanPerBarang;
    //         }
    //         $amount = (int)$raw_data2->sub_total + (int)$raw_data2->price_delivery; //Sebelum Diskon
    //         $komposisi_persen = $amount / $data_header_pembelian;
    //         $discount = $komposisi_persen * $data_header_potongan; //Kolom diskon
    //         $netto = $amount - $discount; //Total sub setelah potongan

    //         array_push(
    //             $post_data_detail,
    //             array(
    //                 "item_id"        => 0, //ISI 0
    //                 "item"            => trim($kode_sku, " "), //ISI DENGAN SKU BARANG
    //                 "color"            => "", //KOSONGI SAJA
    //                 "etd"             => date('d/m/Y', strtotime($raw_data2->created_at)), //ISI DENGAN TGL TRANSAKSI
    //                 "tipe_item"        => '0', //ISI 0
    //                 "price_type"    => '0', //ISI 0

    //                 "qty"            => $raw_data2->quantity, //ISI DENGAN QTY
    //                 "unit"            => "", //KOSONGI SAJA

    //                 // "price"            => $raw_data2->price,//ISI PRICE
    //                 "price"            => $price, //ISI PRICE //Sebelum Diskon
    //                 "ongkir"        => 0, //ISI 0
    //                 "discount"        => $discount, //ISI 0 //Kolom diskon di detail
    //                 "amount"        => $amount, //ISI SUBTOTAL DETAIL //Sebelum Diskon
    //                 "netto"            => $netto //ISI SUBTOTAL DETAIL //Setelah Diskon
    //             )
    //         );
    //         $sub_total_detail = $sub_total_detail + $netto;
    //     }


    //     //AUTO EXPEDISI DAN VENDOR
    //     $master_order = $data_header;
    //     $full_address = $master_order->address . ", DESA " . trim($master_order->villages) . ", KECAMATAN " . trim($master_order->districts) . ", " . trim($master_order->cities) . ", PROVINSI " . trim($master_order->provinces) . " " . $master_order->postal_code;

    //     $provinsi = $master_order->provinces; //ISI PROVINSI ALAMAT TUJUAN TRANSAKSI
    //     $kota = $master_order->cities; //ISI KOTA ALAMAT TUJUAN TRANSAKSI

    //     //BAWAH INI DEFAULT.. NDA USAH DIRUBAH
    //     //===============================START=============================================
    //     $arr_sby = array("JAWA TIMUR", "JAWA TENGAH", "JAWA BARAT", "BANTEN", "DKI JAKARTA", "DI YOGYAKARTA", "D.I. YOGYAKARTA", "BALI");
    //     $arr_plm = array("BANGKA BELITUNG", "JAMBI", "KEPULAUAN BANGKA BELITUNG", "LAMPUNG", "RIAU", "KEPULAUAN RIAU", "KEP RIAU", "BENGKULU", "SUMATERA BARAT", "SUMATERA SELATAN", "SUMATERA UTARA", "ACEH");
    //     $arr_bnj = array("KALIMANTAN BARAT", "KALIMANTAN TENGAH", "KALIMANTAN SELATAN", "KALIMANTAN UTARA", "KALIMANTAN TIMUR");
    //     $arr_mks = array("SULAWESI UTARA", "SULAWESI TENGAH", "SULAWESI BARAT", "SULAWESI SELATAN", "GORONTALO");

    //     $supplier_id = 0;
    //     $supplier = "";

    //     if (in_array($provinsi, $arr_sby)) {
    //         $supplier_id = 183;
    //         $supplier = "SURABAYA";
    //     } else if (in_array($provinsi, $arr_plm)) {
    //         $supplier_id = 157;
    //         $supplier = "PALEMBANG";
    //     } else if (in_array($provinsi, $arr_bnj)) {
    //         $supplier_id = 161;
    //         $supplier = "BANJARMASIN";
    //     } else if (in_array($provinsi, $arr_mks)) {
    //         $supplier_id = 164;
    //         $supplier = "MAKASSAR";
    //     }

    //     $arr_taufiqul = array("SURABAYA", "KOTA SURABAYA", "SIDOARJO", "KAB SIDOARJO", "KABUPATEN SIDOARJO", "GRESIK", "KAB GRESIK", "KABUPATEN GRESIK");

    //     $arr_jatim = array("DKI JAKARTA", "JAWA TENGAH", "JAWA TIMUR");

    //     $arr_sentral_provinsi = array("DI YOGYAKARTA", "D.I. YOGYAKARTA");
    //     $arr_sentral_kota = array("BOGOR", "KOTA BOGOR", "KAB BOGOR", "KAB. BOGOR", "KABUPATEN BOGOR", "DEPOK", "KOTA DEPOK", "KAB DEPOK", "KAB. DEPOK", "KABUPATEN DEPOK", "TANGERANG", "KOTA TANGERANG", "KAB TANGERANG", "KAB. TANGERANG", "KABUPATEN TANGERANG", "BEKASI", "KOTA BEKASI", "KAB BEKASI", "KAB. BEKASI", "KABUPATEN BEKASI");
    //     $arr_indah = array("JAWA BARAT", "BANTEN", "BALI");

    //     $expedition_id = 0;
    //     $expedition = "";

    //     if (in_array($kota, $arr_taufiqul)) {
    //         $expedition_id = 10;
    //         $expedition = "TAUFIQUL";
    //     } else if (in_array($provinsi, $arr_jatim)) {
    //         $expedition_id = 15;
    //         $expedition = "CCW CARGO";
    //     } else if (in_array($provinsi, $arr_sentral_provinsi)) {
    //         $expedition_id = 9;
    //         $expedition = "INDAH CARGO";
    //     } else if (in_array($kota, $arr_sentral_kota)) {
    //         $expedition_id = 15;
    //         $expedition = "CCW CARGO";
    //     } else if (in_array($provinsi, $arr_indah)) {
    //         $expedition_id = 9;
    //         $expedition = "INDAH CARGO";
    //     }
    //     //=================================FINISH===========================================

    //     $platform = 'MORE';

    //     $total_bayar = $master_order->total; //ISI TOTAL BAYAR CUSTOMER
    //     $komisi = 0; //ISI 0 SOALNYA PUNYA EFI SENDIRI

    //     $dpp = $total_bayar / 1.1; //HITUNG DPP UNTUK PAJAK
    //     $status_more = strtoupper($master_order->status); //ISI DENGAN STATUS MORE

    //     $array_header = array(
    //         "total_setelah_discount" => $total_bayar, //DARI VARIABEL ATAS
    //         "voucher_percent"        => 0, //BIARKAN SEPERTI ITU
    //         "voucher_nominal"        => 0, //BIARKAN SEPERTI ITU
    //         "total_setelah_voucher"    => $total_bayar, //DARI VARIABEL ATAS
    //         "dpp"                    => $dpp, //DARI VARIABEL ATAS
    //         "tax_nominal"            => $dpp * 0.1, //DARI VARIABEL ATAS
    //         "komisi_nominal"        => $komisi, //DARI VARIABEL ATAS

    //         "no_draft"             => 0, //BIARKAN SEPERTI ITU
    //         "type_so"             => 'Marketplace', //BIARKAN SEPERTI ITU
    //         "perakitan"         => 'KNOCK DOWN', //BIARKAN SEPERTI ITU
    //         "ordersn"             => $master_order->ordersn, //ISI DENGAN ORDERSN TRANSAKSI MORE
    //         "supplier_id"         => $supplier_id, //DARI VARIABEL ATAS
    //         "supplier"             => $supplier, //DARI VARIABEL ATAS
    //         "expedition_id"     => $expedition_id, //DARI VARIABEL ATAS
    //         "expedition"         => $expedition, //DARI VARIABEL ATAS
    //         "platform_id"         => 45, //BIARKAN SEPERTI ITU
    //         "platform"             => $platform, //DARI VARIABEL ATAS
    //         "total_discount"     => $data_header_potongan, //BIARKAN SEPERTI ITU
    //         "total_ongkir"         => 0, //BIARKAN SEPERTI ITU
    //         "total_amount"         => (float)$data_header_pembelian, //DARI VARIABEL ATAS //Sebelum Diskon
    //         "grand_total"         => (float)$total_bayar, //DARI VARIABEL ATAS //Setelah Diskon
    //         "note"                 => strtoupper($master_order->noe), //ISI DENGAN NOTE TRANSAKSI

    //         "customer"             => strtoupper($master_order->receiver_name), //ISI NAMA PENERIMA
    //         "customer_hp"         => $master_order->receiver_phone, //ISI HP PENERIMA
    //         "customer_address"     => trim($full_address), //ISI ALAMAT PENERIMA

    //         "province_id"         => 0, //BIARKAN SEPERTI ITU
    //         "province"             => trim($master_order->provinces), //ISI PROVINSI PENERIMA
    //         "city_id"             => 0, //BIARKAN SEPERTI ITU
    //         "city"                 => trim($master_order->cities), //ISI KOTA PENERIMA
    //         "kecamatan_id"         => 0, //BIARKAN SEPERTI ITU
    //         "kecamatan"         => trim($master_order->districts), //ISI KECAMATAN PENERIMA
    //         "kelurahan_id"         => 0, //BIARKAN SEPERTI ITU
    //         "kelurahan"         => trim($master_order->villages), //BIARKAN SEPERTI ITU

    //         "kode_pos"             => $master_order->postal_code, //ISI KODEPOS PENERIMA
    //         "no_transaction"     => "000", //BIARKAN SEPERTI ITU

    //         "date"                 => date('d/m/Y', strtotime($master_order->created_at)), //ISI DATE TRANSAKSI
    //         "free_shipping"     => true, //BIARKAN SEPERTI ITU

    //         "creator_id"         => 1, //BIARKAN SEPERTI ITU
    //         "editor_id"         => 1, //BIARKAN SEPERTI ITU
    //         "last_approver_id"     => 1, //BIARKAN SEPERTI ITU
    //         "waiting_approver_id"             => 1, //BIARKAN SEPERTI ITU

    //         "mar_tra_sales_order_detail"    => $post_data_detail, //INI DARI DATA DETAIL DIATAS, BIARKAN SEPERTI ITU
    //         "status_marketplace"    => $status_more, //DARI VARIABEL ATAS
    //         'created_at_marketplace' => date('Y-m-d H:i:s', strtotime($master_order->created_at)) //ISI DATE TRANSAKSI
    //     );


    //     $status_shopee = "";
    //     //UNTUK SET STATUS TRANSAKSI DI EFI ERP
    //     if ($status_shopee == 'CANCELLED') {
    //         $array_header['status'] = 'CANCELED';
    //     } else {
    //         $array_header['status'] = 'IN PROCESS';
    //     }

    //     //TAMPUNG SEMUA DATA TRANSAKSI DAN SESUAKAN FORMAT OBJECT ARRAY NYA
    //     $post_data_header = json_encode($array_header);

    //     // return response_json($post_data_header); // For Debug


    //     //INSERT KE EFI ERP
    //     $env = env('APP_ENV', 'production');
    //     if ($env == "development") {
    //         $url_efi = "http://149.129.241.85/efiserver/public/operation/mar_tra_sales_order_api";
    //     } else {
    //         $url_efi = "http://149.129.241.85/efiserver/public/operation/mar_tra_sales_order_api";
    //     }

    //     $body = $post_data_header;

    //     $request_headers_efi = array();
    //     $request_headers_efi[] = 'Content-Type: application/json';
    //     $request_headers_efi[] = 'Authorization: Bearer ' . $token;

    //     // Performing the HTTP request
    //     $ch_efi = curl_init();
    //     curl_setopt($ch_efi, CURLOPT_URL, $url_efi);
    //     curl_setopt($ch_efi, CURLOPT_HTTPHEADER, $request_headers_efi);
    //     // curl_setopt($ch_efi, CURLOPT_POST, true);
    //     curl_setopt($ch_efi, CURLOPT_POSTFIELDS, $body);
    //     curl_setopt($ch_efi, CURLOPT_RETURNTRANSFER, 1);

    //     // Performs the Request, with specified curl_setopt() options (if any).
    //     $response_efi = curl_exec($ch_efi);
    //     // dd($response_efi);
    //     $response_efi = json_decode($response_efi);

    //     curl_close($ch_efi);

    //     //FINISH

    //     //UPDATE DROPSHIPPER
    //     if($data_header->dropshipper != 0){

    //         // $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTQ3LjEzOS4xNzguMTEyL211bHRpY2hhbm5lbC1kZXYvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE2ODE3OTgzNzksIm5iZiI6MTY4MTc5ODM3OSwianRpIjoiWGFEcktMRmVocTNDOFJldCIsInN1YiI6IjgyIiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.YJZJykTP0FCTO2zft9Ku70SoOVopSPL-kv54waVy05E";
    //         $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTQ3LjEzOS4xNzguMTEyL211bHRpY2hhbm5lbC9hcGkvYXV0aC9sb2dpbiIsImlhdCI6MTY4NTY3NzgwOSwibmJmIjoxNjg1Njc3ODA5LCJqdGkiOiJibTduMHVlMGFBUkl6R2xKIiwic3ViIjoiODIiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.f8_yEF7gy-pxWuFbz1GUUlL2fVIiokcmU8n6M1w7obI';
    //         $request_headers_efi = array();
    //         $request_headers_efi[] = 'Content-Type: application/json';
    //         $request_headers_efi[] = 'Authorization: Bearer ' . $token;

    //         $curl = curl_init();

    //         $array_header = array(
    //             "ordersn" => $ordersn,
    //             "uni_more_dropshipper_id" => $data_header->dropshipper,
    //             "name" => $data_header->dropshipper_name,
    //             "phone" => $data_header->dropshipper_phone,
    //         );
    //         $post_data_header = json_encode($array_header);
    //         curl_setopt_array($curl, array(
    //             CURLOPT_URL => 'http://149.129.241.85/multichannel/api/v1/dropshipper/add/ordersn',
    //             CURLOPT_RETURNTRANSFER => true,
    //             CURLOPT_ENCODING => '',
    //             CURLOPT_MAXREDIRS => 10,
    //             CURLOPT_TIMEOUT => 0,
    //             CURLOPT_FOLLOWLOCATION => true,
    //             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //             CURLOPT_CUSTOMREQUEST => 'POST',
    //             CURLOPT_HTTPHEADER => $request_headers_efi,
    //             CURLOPT_POSTFIELDS => $post_data_header,
    //         ));

    //         $response = curl_exec($curl);

    //         curl_close($curl);
    //     }
    //     //END DROPSHIPPER

    //     return response()->json($response_efi);
    //     // END ORDER

    // }
}
