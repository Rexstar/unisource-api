<?php

namespace App\Http\Controllers\More\Transaction;

use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\More\Transaction\Cart;
use App\Models\More\Transaction\CartDetail;
use App\Models\More\Transaction\CartHistory;
use App\Models\More\Transaction\CartHistoryAvailable;
use App\Models\More\Transaction\Order;
use App\Models\More\Promotion\ProductFree;
use App\Models\UniView\ProductVariantStockView;
use App\Models\More\Promotion\Voucher;
use Illuminate\Support\Facades\Auth;
use App\Models\More\User;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['checkStock','checkLocation','multipleCheckStock','pixel']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Cart::with(
                        // 'r_more_users',
                        'r_more_transaction_cart_details',
                        'r_more_transaction_cart_details.r_uni_product_variants',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_po.r_uni_product_variants',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product.r_uni_product_variants',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_image',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_variant_prices',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_flashsale',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants.r_uni_product_variant_prices',
                        'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants.r_uni_product_image',

                        'r_more_transaction_cart_details.r_more_transaction_cart_bundlings',
                        'r_more_transaction_cart_details.r_more_transaction_cart_product_frees',
                        'r_more_transaction_cart_details.r_uni_product_get_combo',
                        'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants',
                        'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants_main',
                        'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants.r_uni_product_variant_prices',
                        'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants.r_uni_product_image',

                    )
                    ->where('status', 1)
                    ->where('more_users_id', auth()->user()->id)
                    ->first();


        return response_json($data);
    }

    public function getCart(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if(!$user){
            return response_json("Email tidak ditemukan", 403);
        }
        $data = Cart::with(
                        // 'r_more_users',
                        'r_more_transaction_cart_details',
                        'r_more_transaction_cart_details.r_uni_product_variants'
                    )
                    ->where('status', 1)
                    ->where('more_users_id', $user->id)
                    ->first();


        return response_json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'r_more_transaction_cart_details'                           => 'array',
            'r_more_transaction_cart_details.*.uni_product_variants_id' => 'required|exists:uni_product_variant_prices,uni_product_variants_id',
            'r_more_transaction_cart_details.*.quantity'                => 'required|integer|min:1'
        ], [], [
            'r_more_transaction_cart_details.*.uni_product_variants_id' => 'Product Variant Id',
            'r_more_transaction_cart_details.*.quantity'                => 'Product Quantity'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        try {

            $grandTotal = 0;

            $cart = Cart::where('more_users_id', auth()->user()->id)
                        ->where('status', 1)
                        ->firstOrNew();

            $cart->save();

            $cart->r_more_transaction_cart_details()->delete();
            if(count($request->r_more_transaction_cart_details) > 0){
                $cart->r_more_transaction_cart_details()->delete();
                foreach ($request->r_more_transaction_cart_details as $detail) {
                    $variant = ProductVariantStockView::with(["r_uni_product_get_free","r_uni_product_get_combo","r_uni_product_flashsale","r_uni_product_variant_prices"])->findOrFail($detail['uni_product_variants_id']);

                    $subTotal = 0;
                    $is_flashsale = $variant->r_uni_product_flashsale ? 1 : 0;
                    $is_get_free = $variant->r_uni_product_get_free ? 1 : 0;
                    $is_get_combo = $detail['is_get_combo'] ? 1 : 0;
                    $id_combo = $detail['id_combo'] ? $detail['id_combo'] : null;
                    $is_bundling = isset($detail['bundle_summary']) && count($detail['bundle_summary']) > 0 ? 1 :0;
                    $quantity = $detail['quantity'];

                    if($is_flashsale == 1 || ($is_get_free == 1 && $variant->r_uni_product_get_free->is_multiple_buy == 0) || ($is_get_combo == 1 && $variant->r_uni_product_get_combo->is_multiple_buy == 0)){
                        $quantity = 1;
                    }
                    $subTotal = $quantity * $variant->valid_price;

                    if($detail['is_selected'] == 1) {
                        $grandTotal += $subTotal;
                    }

                    $cart_detail_id = $cart->r_more_transaction_cart_details()->create([
                        'uni_product_variants_id' => $variant->id,
                        'quantity' => $quantity,
                        'is_flashsale' => $is_flashsale,
                        'is_bundling' => $is_bundling,
                        'is_get_free' => $is_get_free,
                        'is_get_combo' => $is_get_combo,
                        'id_combo' => $id_combo,
                        'sub_total' => $subTotal,
                        'is_selected' => $detail['is_selected']
                    ])->id;

                    if($is_get_free == 1){
                        $cart->r_more_transaction_cart_product_frees()->create([
                            'more_transaction_cart_details_id' => $cart_detail_id,
                            'more_promotion_product_frees_id' => $variant->r_uni_product_get_free->id,
                            'uni_product_variants_id' => $variant->r_uni_product_get_free->free_uni_product_variants_id,
                            'free_description' => "(".$variant->r_uni_product_get_free->free_type.") dari ".$variant->name_short,
                        ]);
                    }

                    if($is_get_combo == 1){
                        $modelCombo = ProductFree::findOrFail($id_combo);
                        $cart->r_more_transaction_cart_product_frees()->create([
                            'more_transaction_cart_details_id' => $cart_detail_id,
                            'more_promotion_product_frees_id' => $modelCombo->id,
                            'uni_product_variants_id' => $modelCombo->free_uni_product_variants_id,
                            'free_description' => "(".$variant->r_uni_product_get_combo->free_type.") dari ".$variant->name_short,
                            'combo_price' => $variant->r_uni_product_get_combo->combo_price,
                        ]);
                        $grandTotal += ($quantity * $modelCombo->combo_price);
                    }

                    if($is_bundling == 1){
                        foreach ($detail['bundle_summary'] as $item) {
                            $cart->r_more_transaction_cart_bundlings()->create([
                                'more_transaction_cart_details_id' => $cart_detail_id,
                                'uni_product_variants_id' => $item['uni_product_variants_id'],
                                'quantity' => $item['quantity'],
                            ]);
                        }
                    }


                }
            }

            $cart->grand_total = $grandTotal;
            $cart->save();

            // $cart->load(
            //     'r_more_transaction_cart_details',
            //     'r_more_transaction_cart_details.r_uni_product_variants',
            //     'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_image',
            //     'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_variant_prices',
            //     'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free',
            //     'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants',
            //     'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants.r_uni_product_variant_prices',
            //     'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants.r_uni_product_image',
            //     'r_more_transaction_cart_details.r_more_transaction_cart_bundlings',
            //     'r_more_transaction_cart_details.r_more_transaction_cart_product_frees',
            // );

            return response_json($cart);
            //code...
        } catch (\Exception $e) {
            // dd($th);
            \Log::info('ERROR CREATE CART', ['message' => $e->getMessage(),
            'line' => $e->getLine(),
            'params' => ($request) ? json_encode(is_object($request) ? $request->all() : $request) : null,
            'stack_trace' => $e->getTraceAsString(),
            'user'=> auth()->user()->id,
            'error_code' => $e->getCode()]);

            return response_json("Gagal menyimpan Keranjang");
        }
    }

    public function sync(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'r_more_transaction_cart_details'                           => 'array',
            'r_more_transaction_cart_details.*.uni_product_variants_id' => 'required|exists:uni_product_variant_prices,uni_product_variants_id',
            'r_more_transaction_cart_details.*.quantity'                => 'required|integer|min:1'
        ], [], [
            'r_more_transaction_cart_details.*.uni_product_variants_id' => 'Product Variant Id',
            'r_more_transaction_cart_details.*.quantity'                => 'Product Quantity'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        try {

            $grandTotal = 0;

            $cart = Cart::where('more_users_id', auth()->user()->id)
                        ->where('status', 1)
                        ->firstOrNew();

            $cart->save();

            $cart->r_more_transaction_cart_details()->delete();
            if(count($request->r_more_transaction_cart_details) > 0){
                $cart->r_more_transaction_cart_details()->delete();
                foreach ($request->r_more_transaction_cart_details as $detail) {
                    $variant = ProductVariantStockView::with(["r_uni_product_get_free","r_uni_product_flashsale","r_uni_product_variant_prices"])->findOrFail($detail['uni_product_variants_id']);

                    $subTotal = 0;
                    $is_flashsale = $variant->r_uni_product_flashsale ? 1 : 0;
                    $is_get_free = $variant->r_uni_product_get_free ? 1 : 0;
                    $is_get_combo = $variant->r_uni_product_get_combo ? 1 : 0;
                    $id_combo = $detail['id_combo'] ? $detail['id_combo'] : null;
                    $is_bundling = isset($detail['bundle_summary']) && count($detail['bundle_summary']) > 0 ? 1 :0;
                    $quantity = $detail['quantity'];

                    if($is_flashsale == 1 || ($is_get_free == 1 && $variant->r_uni_product_get_free->is_multiple_buy == 0) || ($is_get_combo == 1 && $variant->r_uni_product_get_combo->is_multiple_buy == 0)){
                        $quantity = 1;
                    }
                    $subTotal = $quantity * $variant->valid_price;
                    $grandTotal += $subTotal;

                    $cart_detail_id = $cart->r_more_transaction_cart_details()->create([
                        'uni_product_variants_id' => $variant->id,
                        'quantity' => $quantity,
                        'is_flashsale' => $is_flashsale,
                        'is_bundling' => $is_bundling,
                        'is_get_free' => $is_get_free,
                        'is_get_combo' => $is_get_combo,
                        'sub_total' => $subTotal
                    ])->id;

                    if($is_get_free == 1){
                        $cart->r_more_transaction_cart_product_frees()->create([
                            'more_transaction_cart_details_id' => $cart_detail_id,
                            'more_promotion_product_frees_id' => $variant->r_uni_product_get_free->id,
                            'uni_product_variants_id' => $variant->r_uni_product_get_free->free_uni_product_variants_id,
                            'free_description' => "(".$variant->r_uni_product_get_free->free_type.") dari ".$variant->name_short,
                        ]);
                    }

                    if($is_get_combo == 1){
                        $modelCombo = ProductFree::findOrFail($id_combo);
                        $cart->r_more_transaction_cart_product_frees()->create([
                            'more_transaction_cart_details_id' => $cart_detail_id,
                            'more_promotion_product_frees_id' => $modelCombo->id,
                            'uni_product_variants_id' => $modelCombo->free_uni_product_variants_id,
                            'free_description' => "(".$variant->r_uni_product_get_combo->free_type.") dari ".$variant->name_short,
                            'combo_price' => $variant->r_uni_product_get_combo->combo_price,
                        ]);
                        $grandTotal += ($quantity * $modelCombo->combo_price);
                    }

                    if($is_bundling == 1){
                        foreach ($detail['bundle_summary'] as $item) {
                            $cart->r_more_transaction_cart_bundlings()->create([
                                'more_transaction_cart_details_id' => $cart_detail_id,
                                'uni_product_variants_id' => $item['uni_product_variants_id'],
                                'quantity' => $item['quantity'],
                            ]);
                        }
                    }
                }
            }

            $cart->grand_total = $grandTotal;
            $cart->save();

            $data = Cart::with(
                // 'r_more_users',
                'r_more_transaction_cart_details',
                'r_more_transaction_cart_details.r_uni_product_variants',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_image',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_variant_prices',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_flashsale',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants.r_uni_product_variant_prices',
                'r_more_transaction_cart_details.r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants.r_uni_product_image',
                'r_more_transaction_cart_details.r_more_transaction_cart_bundlings',
                'r_more_transaction_cart_details.r_more_transaction_cart_product_frees',
                'r_more_transaction_cart_details.r_uni_product_get_combo',
                'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants',
                'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants_main',
                'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants.r_uni_product_variant_prices',
                'r_more_transaction_cart_details.r_uni_product_get_combo.r_uni_product_variants.r_uni_product_image',

            )
            ->where('status', 1)
            ->where('more_users_id', auth()->user()->id)
            ->first();

            return response_json($data);
            //code...
        } catch (\Exception $e) {

            \Log::info('ERROR SYNC CART', ['message' => $e->getMessage(),
            'line' => $e->getLine(),
            'params' => ($request) ? json_encode(is_object($request) ? $request->all() : $request) : null,
            'stack_trace' => $e->getTraceAsString(),
            'user'=> auth()->user()->id,
            'error_code' => $e->getCode()]);

            return response_json("Gagal menyimpan Keranjang");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkStock(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'uni_product_variants_id'                   => 'required|exists:uni_product_variants_stock_view,id',
            'quantity'                                  => 'required|integer|min:1',
            'bundle_summary'                            => 'array',
            'bundle_summary.*.uni_product_variants_id'  => 'required',
            'bundle_summary.*.quantity'                 => 'required|integer|min:1'
        ], [], [
            'uni_product_variants_id' => 'Product',
            'quantity'                => 'Product Quantity',
            'bundle_summary' => 'Bundle Summary',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }
        $user = auth()->user();

        // ~Jika produk bundle
        if($request->filled("bundle_summary")){
            $bundle = ProductVariantStockView::findOrFail($request->uni_product_variants_id);
            foreach ($request->bundle_summary as $item) {
                //~jika stok habis
                $data = Order::checkStockValidity($item['uni_product_variants_id'],$item['quantity']);
                if(!$data->isValid) return response_json($bundle, 403);
            }
            return response_json($bundle);
        }else{
            // :jika stok ada
            $data = Order::checkStockValidity($request->uni_product_variants_id,$request->quantity);
            if(!$data->isValid) return response_json($data->product,403);

            return response_json($data->product);
        }
    }

    public function multipleCheckStock(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'r_more_transaction_cart_details'                           => 'array',
            'r_more_transaction_cart_details.*.uni_product_variants_id' => 'required',
            'r_more_transaction_cart_details.*.quantity'                => 'required|integer|min:1'
        ], [], [
            'r_more_transaction_cart_details.*.uni_product_variants_id' => 'Product',
            'r_more_transaction_cart_details.*.quantity'                => 'Product Quantity'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $products = [];
        if(count($request->r_more_transaction_cart_details) > 0){
            foreach ($request->r_more_transaction_cart_details as $key => $request) {
                $data = null;
                //~ Jika produk bundle
                if(isset($request['bundle_summary']) && $request['bundle_summary'] != null && count($request['bundle_summary']) > 0){
                    $product = ProductVariantStockView::find($request['uni_product_variants_id']);
                    foreach ($request['bundle_summary'] as $item) {
                        $data = Order::checkStockValidity($item['uni_product_variants_id'],$item['quantity']);
                        $data->product = $product;
                        //~ jika stok habis
                        if(!$data->isValid) return response_json($data->product,403);

                    }

                }else{
                    $data = Order::checkStockValidity($request['uni_product_variants_id'],$request['quantity']);
                    //~ jika stok habis
                    if(!$data->isValid) return response_json($data->product,403);
                }

                $products[$key] = $data->product;
            }
        }
        return response_json($products);
    }

    public function useVoucher(Request $request)
    {
        $device = $request->device ?: 'mobile';
        $data = Order::checkVoucherValidity($request->code, $request->address_code, $request->payment_code, null, $device);

        if($data->isValid) {
            return response_json($data->voucher, 200);
        }
        else {
            return response_json($data->message, 403);
        }
    }

    public function listVoucher(Request $request)
    {
        $device = $request->device ?: 'mobile';
        $address_code = $request->address_code;
        $payment_code = $request->payment_code;

        $activeArray = [];
        $not_activeArray = [];
        $data = Voucher::where('status',1)->where('show_on','<>',2)
                ->whereDate('duration_start','<=', Carbon::now())
                ->whereDate('duration_end','>=', Carbon::now())->get();

        foreach ($data as $value) {
            $checkVoucher = Order::checkVoucherValidity($value->code,$address_code,$payment_code, null, $device);
            if($checkVoucher->isValid) {
                array_push(
                    $activeArray,
                    array(
                        "response"      => 'success',
                        "data"          => $value
                    )
                );
            }
            else {
                array_push(
                    $not_activeArray,
                    array(
                        "response"      => $checkVoucher->message,
                        "data"          => $value
                    )
                );
            }
        }

        $customArray = array(
            "active"        => $activeArray,
            "not_active"      => $not_activeArray,
        );

        return response_json($customArray, 200);
    }

    public function pixel(Request $request)
    {

        $sku = $request->sku;
        $price = $request->price;
        $product = $request->product;
        $ip = $request->ip;
        $browser = $request->browser;

        if($sku != null && $price != null && $product != null){
            pixel_fb('AddToCart',$sku,$price, $product); //FBPIXEL Tambahkan ke Keranjang

            DB::beginTransaction();
            try {
                $user = Auth::user();

                $cart = new CartHistory();
                $cart->more_users_id = $user ? $user->id : null;
                $cart->uni_product_variants_id = $request->variant;
                $cart->price = $price;
                $cart->ip = $ip;
                $cart->browser = $browser;
                $cart->created_at = date('Y-m-d H:i:s');
                $cart->type = $request->type;
                $cart->save();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
            }
        }

    }

    // ? Check sku_group variant berdasarkan lokasi user
    public function checkLocation(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'sku_group' => 'required',
            'uni_product_variants_id' => 'required',
            'city_id' => 'required',
        ], [], [
            'sku_group' => 'Variant',
            'uni_product_variants_id' => 'Variant',
            'city_id' => 'Lokasi',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $data = Order::checkAreaValidity($request->sku_group, $request->city_id);

        if($data->isValid) {
            return response_json($data, 200);
        }
        else {
            // * jika tidak ada simpan ke database
            $history = new CartHistoryAvailable();
            $history->city_id = $request->city_id;
            $history->uni_product_variants_id = $request->uni_product_variants_id;
            $history->sku_group = $request->sku_group;
            $history->more_users_id = auth()->user() ? auth()->user()->id : null;
            $history->save();
            return response_json($data, 403);
        }
    }

    public function checkProduct(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'uni_product_variants_id' => 'required',
            // 'id_combo' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        DB::beginTransaction();
        try {
            $cart = Cart::where('more_users_id', auth()->user()->id)
                        ->where('status', 1)
                        ->first();
            if($cart) {
                $grandTotal = $cart->grand_total;
                $cartDetail = CartDetail::with('r_uni_product_get_combo')->where('more_transaction_carts_id', $cart->id)
                                ->where('uni_product_variants_id', $request->uni_product_variants_id)
                                ->where('id_combo', $request->id_combo)
                                ->first();
                if($cartDetail) {
                    $subTotal = $cartDetail->sub_total;
                    if($cartDetail->is_get_combo != 0){
                        $subTotal += ($cartDetail->quantity * $cartDetail->r_uni_product_get_combo->combo_price);
                    }
                    $cartDetail->is_selected = 1 - $cartDetail->is_selected;
                    $cartDetail->save();

                    $cart->grand_total = $cartDetail->is_selected ? ($grandTotal + $subTotal) : ($grandTotal - $subTotal);
                    $cart->save();
                    DB::commit();

                    return response_json($cart->load('r_more_transaction_cart_details'));
                }
            }
            abort(404);
        }
        catch (\Exception $th) {
            return response_json("Gagal mengubah keranjang", 403);
        }
    }

    public function checkProductAll(Request $request)
    {
        DB::beginTransaction();
        try {
            $cart = Cart::where('more_users_id', auth()->user()->id)
                        ->where('status', 1)
                        ->first();
            if($cart) {
                $is_selected = $request->is_selected;
                $cartDetail = CartDetail::with('r_uni_product_get_combo')->where('more_transaction_carts_id', $cart->id)->get();
                if($cartDetail) {
                    $grandTotal = 0;
                    foreach($cartDetail as $d) {
                        $grandTotal += ($d->quantity * $d->sub_total);
                        if($d->is_get_combo != 0){
                            $grandTotal += ($d->quantity * $d->r_uni_product_get_combo->combo_price);
                        }
                        $d->is_selected = $is_selected;
                        $d->save();
                    }
                    $cart->grand_total = $is_selected ? $grandTotal : 0;
                    $cart->save();
                    DB::commit();

                    return response_json($cartDetail);
                }
            }
            abort(404);
        }
        catch (\Exception $th) {
            return response_json("Gagal mengubah keranjang", 403);
        }
    }
}
