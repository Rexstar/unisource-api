<?php

namespace App\Http\Controllers;

class PaymentController extends Controller
{
    public function checkEnviroment(){
        if(config('app.env') == 'production'){
            return response_json('PRODUCTION', 200);
        }else{
            return response_json('DEVELOPMENT', 200);
        }
    }
}