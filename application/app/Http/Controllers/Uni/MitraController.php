<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Uni\MitraOlymplast;
use DB;

class MitraController extends Controller
{

    public function index(Request $request)
    {
        $application = $request->get('application') ?: 'http://localhost:3000';
        $campaign = $request->get('campaign') ?: 'index' ;
        $city = $request->get('city') ?: '' ;

        $apps = db::connection('postgres_mitra')->table('gen_applications')
                    ->where('link_development', $application)
                    ->orWhere('link_production', $application)
                    // ->where('is_active', 't')
                    ->first();
        if ($campaign && $campaign !== 'index') {
            $cid = db::connection('postgres_mitra')->table('mas_campaigns as mc')
                    ->select('mc.*','ga.googletag_head','ga.googletag_body')
                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.gen_applications_id',$apps->id)
                    ->where('mc.slug','ilike',$campaign)
                    ->first();
        } else {
            $cid = db::connection('postgres_mitra')->table('mas_campaigns as mc')
                    ->select('mc.*','ga.googletag_head','ga.googletag_body')
                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.id',$apps->active_mas_campaigns_id)
                    ->first();
        }

        if ($city == '') {
            $query = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select('mm.*', DB::raw("TO_CHAR(open_hour, 'HH24:MI') AS open_hour"), DB::raw("TO_CHAR(close_hour, 'HH24:MI') AS close_hour"))
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('rma.mas_campaign_id',$cid->id)
                    ->get();
        } else {
            $query = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select('mm.*', DB::raw("TO_CHAR(open_hour, 'HH24:MI') AS open_hour"), DB::raw("TO_CHAR(close_hour, 'HH24:MI') AS close_hour"))
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('rma.mas_campaign_id',$cid->id)
                    ->where('mm.city','ILIKE',$city)
                    ->get();    
        }


        $data = [
            "results" => $query
        ];

        return response_json($data);

    }
    public function apps(Request $request)
    {
        $application = $request->get('application') ?: 'http://localhost:3000';

        $apps = db::connection('postgres_mitra')->table('gen_applications')
                    ->where('link_development', $application)
                    ->orWhere('link_production', $application)
                    // ->where('is_active', 't')
                    ->first();

        $data = [
            "results" => $apps
        ];

        return response_json($data);

    }
    public function listkota(Request $request)
    {
        $application = $request->get('application') ?: 'http://localhost:3000';
        $campaign = $request->get('campaign') ?: 'index' ;

        $apps = DB::connection('postgres_mitra')->table('gen_applications')
            ->where(function($query) use ($application) {
                $query->where('link_development', $application)
                    ->orWhere('link_production', $application);
            })
            ->first();

        if ($campaign && $campaign !== 'index') {
            $cid = DB::connection('postgres_mitra')->table('mas_campaigns as mc')
                ->select('mc.*','ga.googletag_head','ga.googletag_body')
                ->join('gen_applications as ga', 'ga.id', 'mc.gen_applications_id')
                ->where('mc.gen_applications_id', $apps->id)
                ->where('mc.slug', 'ilike', $campaign)
                ->first();
        } else {
            $cid = DB::connection('postgres_mitra')->table('mas_campaigns as mc')
                ->select('mc.*','ga.googletag_head','ga.googletag_body')
                ->join('gen_applications as ga', 'ga.id', 'mc.gen_applications_id')
                ->where('mc.id', $apps->active_mas_campaigns_id)
                ->first();
        }

        $result = DB::connection('postgres_mitra')->table('mas_mitras as mm')
            ->select(DB::raw('UPPER(mm.area) as upper'), 'mm.city as kota')
            ->join('rel_mitras_applications as rma', 'mm.code', 'rma.mas_mitras_id')
            ->where('rma.mas_campaign_id', $cid->id)
            ->where('mm.area', '<>', '')
            ->groupBy('mm.area', 'mm.city')
            ->orderBy('mm.area', 'ASC')
            ->get();
        $wilayah = DB::connection('postgres_mitra')->table('mas_mitras as mm')
            ->select(DB::raw('UPPER(mm.area) as area'))
            ->join('rel_mitras_applications as rma', 'mm.code', 'rma.mas_mitras_id')
            ->where('rma.mas_campaign_id', $cid->id)
            ->where('mm.area', '<>', '')
            ->groupBy('mm.area')
            ->orderBy('mm.area', 'ASC')
            ->get();

        $data = [
            "result" => $result,
            "wilayah" => $wilayah
        ];

        return response_json($data);
    }
    public function kota(Request $request)
    {
        $application = $request->get('application') ?: 'http://localhost:3000';
        $campaign = $request->get('campaign') ?: 'index' ;
        // $city = $request->get('city') ?: '*' ;

        $apps = db::connection('postgres_mitra')->table('gen_applications')
                    ->where('link_development', $application)
                    ->orWhere('link_production', $application)
                    // ->where('is_active', 't')
                    ->first();

        if ($campaign && $campaign !== 'index') {
            $cid = db::connection('postgres_mitra')->table('mas_campaigns as mc')
            ->select('mc.*','ga.googletag_head','ga.googletag_body')

                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.gen_applications_id',$apps->id)
                    ->where('mc.slug','ilike',$campaign)
                    ->first();
        } else {
            $cid = db::connection('postgres_mitra')->table('mas_campaigns as mc')
            ->select('mc.*','ga.googletag_head','ga.googletag_body')

                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.id',$apps->active_mas_campaigns_id)
                    ->first();
        }

        $query = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select(db::raw('upper(mm.area) as upper'), 'mm.city as kota', db::raw('count(*) as total'))
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('rma.mas_campaign_id',$cid->id)
                    // ->where('mm.city','ILIKE',$city)
                    ->groupby('mm.area', 'city')
                    ->orderby('mm.area','ASC')
                    ->get();
        $data = [
            "results" => $query
        ];
        return response_json($data);
    }

    public function wilayah(Request $request)
    {
        $area = $request->get('area') ?: '';
        $application = $request->get('application') ?: 'http://localhost:3000';
        $campaign = $request->get('campaign') ?: 'index' ;


        $apps = db::connection('postgres_mitra')->table('gen_applications')
                    ->where('link_development', $application)
                    ->orWhere('link_production', $application)
                    // ->where('is_active', 't')
                    ->first();

        if ($campaign && $campaign !== 'index') {
                    $cid = db::connection('postgres_mitra')->table('mas_campaigns as mc')
                    ->select('mc.*','ga.googletag_head','ga.googletag_body')

                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.gen_applications_id',$apps->id)
                    ->where('mc.slug','ilike',$campaign)
                    ->first();
        } else {
                    $cid = db::connection('postgres_mitra')->table('mas_campaigns as mc')
                    ->select('mc.*','ga.googletag_head','ga.googletag_body')

                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.id',$apps->active_mas_campaigns_id)
                    ->first();
        }
        if ($area == '') {
            $kota = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select('mm.city',)
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('rma.mas_campaign_id',$cid->id)
                    ->groupby('mm.city')
                    ->get();
            $mitra = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select('mm.*', DB::raw("TO_CHAR(open_hour, 'HH24:MI') AS open_hour"), DB::raw("TO_CHAR(close_hour, 'HH24:MI') AS close_hour"))
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('rma.mas_campaign_id',$cid->id)
                    ->orderby('mm.city')
                    ->get();
        } else {
            $kota = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select('mm.city',)
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('mm.alias_unit', 'ilike', $area)
                    ->where('rma.mas_campaign_id',$cid->id)
                    ->groupby('mm.city')
                    ->get();
            $mitra = db::connection('postgres_mitra')->table('mas_mitras as mm')
                    ->select('mm.*', DB::raw("TO_CHAR(open_hour, 'HH24:MI') AS open_hour"), DB::raw("TO_CHAR(close_hour, 'HH24:MI') AS close_hour"))
                    ->join('rel_mitras_applications as rma','mm.code','rma.mas_mitras_id')
                    ->where('mm.alias_unit', 'ilike', $area)
                    ->where('rma.mas_campaign_id',$cid->id)
                    ->orderby('mm.city')
                    ->get();
        }
        
        $data = [
            "kota" => $kota,
            "mitra"=>$mitra
        ];
        return response_json($data);

        // $unit = $request->get('unit');
        // $application = $request->get('application');



        // $apps = db::connection('postgres_mitra')->table('mas_campaigns as ms')
        //             ->select('ms.*')
        //             ->join('gen_applications as ga','ga.id','ms.gen_applications_id') 
        //             ->where('ga.link_production',$application)
        //             ->where('is_active','t')
        //             ->orwhere('ga.link_development',$application)
        //             ->where('is_active','f')
        //             ->first();

        // $youtube = db::connection('postgres_mitra')->table('gen_campaigns_contents as gcc')
        //             ->join('rel_campaigns_contents as rcc','rcc.id_gen_campaigns_contents','gcc.id') 
        //             ->where('gcc.mas_campaigns_id',$apps->id)
        //             ->where('gcc.section','youtube')
        //             ->where('rcc.page',3)
        //             ->orderby('gcc.sort','ASC')
        //             ->first();
        // $data = [
        //     "apps"=>$apps,
        //     "youtube"=>$youtube
        // ];
    }
    public function brand(Request $request)
    {
        $application = $request->get('application') ?: 'http://localhost:3000';
        $campaign = $request->get('campaign') ?: 'index' ;

        $apps = db::connection('postgres_mitra')->table('gen_applications')
                    ->where('link_development', $application)
                    ->orWhere('link_production', $application)
                    // ->where('is_active', 't')
                    ->first();

        if ($campaign && $campaign !== 'index') {
            $style = db::connection('postgres_mitra')->table('mas_campaigns as mc')
                    ->select('mc.*','ga.googletag_head','ga.googletag_body')
                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.gen_applications_id',$apps->id)
                    ->where('mc.slug','ilike',$campaign)
                    ->first();
        } else {
            $style = db::connection('postgres_mitra')->table('mas_campaigns as mc')
                    ->select('mc.*','ga.googletag_head','ga.googletag_body')
                    ->join('gen_applications as ga','ga.id','mc.gen_applications_id')
                    ->where('mc.id',$apps->active_mas_campaigns_id)
                    ->first();
        }

        
        $sosmed = db::connection('postgres_mitra')->table('gen_campaigns_contents')
                    ->where('mas_campaigns_id',$apps->active_mas_campaigns_id)
                    ->where('section','sosmed')
                    ->orderby('sort','ASC')
                    ->get();
        $data = [
            "apps"=>$style,
            "sosmed"=>$sosmed
        ];
        return response_json($data);
    }
    public function details(Request $request)
    {
        $page = $request->get('page') ?: '';
        $application = $request->get('application') ?: 'http://localhost:3000';
        $campaign = $request->get('campaign') ?: 'index' ;

        $apps = db::connection('postgres_mitra')->table('gen_applications')
                    ->where('link_development', $application)
                    ->orWhere('link_production', $application)
                    // ->where('is_active', 't')
                    ->first();
        if ($campaign && $campaign !== 'index') {
            $cid = db::connection('postgres_mitra')->table('mas_campaigns')
                    ->where('gen_applications_id',$apps->id)
                    ->where('slug','ilike',$campaign)
                    ->first();
        } else {
            $cid = db::connection('postgres_mitra')->table('mas_campaigns')
                    ->where('id',$apps->active_mas_campaigns_id)
                    ->first();
        }
        if ($page == '') {
        $banner = db::connection('postgres_mitra')->table('gen_campaigns_contents as gcs')
                    ->select('gcs.*')
                    ->where('section','banner')
                    ->where('is_active','t')
                    ->where('mas_campaigns_id',$apps->active_mas_campaigns_id)
                    ->get();
        $youtube = db::connection('postgres_mitra')->table('gen_campaigns_contents as gcs')
                    ->select('gcs.*')
                    ->where('section','youtube')
                    ->where('is_active','t')
                    ->where('mas_campaigns_id',$apps->active_mas_campaigns_id)
                    ->first();
        } else {
        $banner = db::connection('postgres_mitra')->table('gen_campaigns_contents as gcs')
                    ->select('gcs.*')
                    ->where('section','banner')
                    ->where('is_active','t')
                    ->where('page',$page)
                    ->where('mas_campaigns_id',$apps->active_mas_campaigns_id)
                    ->get();
        $youtube = db::connection('postgres_mitra')->table('gen_campaigns_contents as gcs')
                    ->select('gcs.*')
                    ->where('section','youtube')
                    ->where('is_active','t')
                    ->where('page',$page)
                    ->where('mas_campaigns_id',$apps->active_mas_campaigns_id)
                    ->first();
        }
        $data = [
            "banner"=>$banner,
            "youtube"=>$youtube
        ];
        return response_json($data);
    }
    public function sitemap(Request $request)
    {
        $application = $request->get('application') ?: '';

        $sitemaps = db::connection('postgres_mitra')->table('sitemap_all')
                    ->where('link_production', $application)
                    ->get();
        $data = [
            "sitemaps"=>$sitemaps
        ];
        return response_json($data);
    }




}
