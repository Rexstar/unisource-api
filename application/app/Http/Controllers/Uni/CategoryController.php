<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Uni\Category;
use App\Models\UniView\CategoryView;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $data = Category::with('r_subs')->where('status', 1)->whereIn('uni_categories_id',
        //             Category::select('id')->whereNull('uni_categories_id')->where('status', 0)->pluck('id')
        //         )->get()->toArray();
        $app = $request->get('app') ?: '1';


        $data = Category::with('r_subs')->where('status', 1)->where('uni_gen_applications_id',$app)->whereNull('uni_categories_id')->get()
                // ->append(['image_url', 'banner_url'])
                ->toArray();

        return response_json($data);
    }
}
