<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\GenBrand;
use Illuminate\Http\Request;

class GenBrandController extends Controller
{
    public function index(){
        return response_json(GenBrand::where('more_active',1)->get());
    }
}
