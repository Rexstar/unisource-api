<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\GenColor;
use App\Models\Uni\GenMaterial;
use Illuminate\Http\Request;

class GenMaterialController extends Controller
{
    public function index(){
        return response_json(GenMaterial::all());
    }
}
