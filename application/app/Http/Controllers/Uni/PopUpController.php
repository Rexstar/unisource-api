<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UniView\PopUpView;

class PopUpController extends Controller
{

    public function index(Request $request)
    {
        $application = $request->get('application') ?: null; // application MORE/OLYMPIC/OLYMPLAST/ERP/ dll

        $data = PopUpView::where('application', $application);

        $data = [
            "results" => $data->get()->toArray()
        ];

        return response_json($data);

    }

}
