<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function show($slug)
    {
        $data = Page::where('slug', trim($slug))->first();
        return response_json($data);
    }

    public function index()
    {
        $data = Page::all();
        return response_json($data);
    }

}
