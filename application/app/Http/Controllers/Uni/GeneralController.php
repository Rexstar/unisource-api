<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class GeneralController extends Controller
{
    public function upload(Request $request){

        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'picture' => 'required|'.config('upload.image_validator'),
            ]);

            if ($validator->fails()) {
                return response_json($validator->errors(), 403);
            }

            $dateTime = Carbon::now()->format('dmYHis');
            $file = $request->file('picture');
            $path = config("upload.path.".$request->type);
            if(!File::exists($path)) File::makeDirectory($path, 0775, true);
            $name = generateRandomString(5) . '_' . $dateTime . '.' . $file->getClientOriginalExtension();

            $file->move($path, $name);
            $url = URL::asset($path . $name);

            return response_json(['picture' => $url, 'message' => 'File berhasil disimpan'], 200);
        } catch (\Exception $e) {
            // Log::error("GeneralController > upload",$e->getMessage());
            DB::rollback();
            return response_json("Gagal proses pesanan!, coba lagi nanti.",403);
        }
    }
}
