<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\City;
use App\Models\Uni\DeliveryArea;
use Illuminate\Http\Request;

use App\Models\Uni\Province;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q') ?: null;

        // $data = DeliveryArea::select('uni_gen_provinces.*')
        //     ->join('uni_gen_cities', 'uni_delivery_areas.city_id', '=', 'uni_gen_cities.city_id')
        //     ->join('uni_gen_provinces', 'uni_gen_cities.province_id', '=', 'uni_gen_provinces.province_id')
        //     ->where('uni_delivery_areas.status', 1)
        //     ->groupBy('uni_gen_provinces.province_id')
        //     ->groupBy('uni_gen_provinces.province_name');
        $data = Province::select('*');

        if ($q) $data->whereRaw('UPPER(province_name) like \'%' . strtoupper($q) . '%\'');

        $page = $request->get("page") ?: 1;
        $perPage = config('pagination.per_page', 10);
        $offset = ($page - 1) * $perPage;

        $count = $data->count();
        $endCount = $offset + $perPage;
        $morePages = $count > $endCount;

        $data = [
            "results" => $data->orderBy('province_name', 'ASC')->skip($offset)->take($perPage)->get()->toArray(),
            "pagination" => [
                "more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function all(Request $request)
    {
        $q = $request->get('q') ?: null;

        $data = DeliveryArea::select('uni_gen_provinces.*')
            ->join('uni_gen_cities', 'uni_delivery_areas.city_id', '=', 'uni_gen_cities.city_id')
            ->join('uni_gen_provinces', 'uni_gen_cities.province_id', '=', 'uni_gen_provinces.province_id')
            ->where('uni_delivery_areas.status', 1)
            ->groupBy('uni_gen_provinces.province_id')
            ->groupBy('uni_gen_provinces.province_name');

        if ($q) $data->whereRaw('UPPER(province_name) like \'%' . strtoupper($q) . '%\'');

        return response_json($data->orderBy('province_name', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
