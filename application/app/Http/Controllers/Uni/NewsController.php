<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\UniView\NewsTagTopView;
use App\Models\Uni\Product;
use App\Models\Uni\NewsCategory;
use App\Models\Uni\NewsCategoryDetail;
use App\Models\UniView\NewsView;
use App\Models\UniView\NewsTagGroupView;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $application = $request->get('application') ?: 1; // application MORE/OLYMPIC/OLYMPLAST/ERP/ dll

        $tags = $request->get('tags') ?: null;

        $q = $request->get('q') ?: null; // nama artikel
        $category = $request->get('category') ?: null; // kategory

        if ($tags != null) {
            $data = NewsView::whereHas('r_uni_news_tags', function ($query) use ($tags) {
                $query->where('name', '=', $tags);
            })->with('r_uni_news_tags','r_uni_news_categories')->where('application', $application);
        }elseif ($category != null) {
            $data = NewsView::with('r_uni_news_tags','r_uni_news_categories')->where('application', $application);

            $uni_news_categories_id = NewsCategory::where('slug',$category)->first();
            $uni_news_categories_id = $uni_news_categories_id->id;

            $detail_category = NewsCategory::where('uni_news_categories_id',$uni_news_categories_id)->pluck('id');

            if(count($detail_category) > 0){

                $news_id = NewsCategoryDetail::whereIn('uni_news_categories_id', $detail_category)->pluck('uni_news_id');
                $data->whereIn('id', $news_id);
            }else{
                $news_id = NewsCategoryDetail::where('uni_news_categories_id', $uni_news_categories_id)->pluck('uni_news_id');
                $data->whereIn('id', $news_id);
            }


        } else {
            $data = NewsView::with('r_uni_news_tags','r_uni_news_categories')->where('application', $application);
        }


        if ($q) $data->whereRaw('LOWER(title) like \'%' . strtolower($q) . '%\'');

        $page = $request->get("page") ?: 1;
        $perPage = $request->get('per_page') ? $request->get('per_page') : config('per_page_artikel', 5);
        $data->orderBy('created_at', 'DESC');
        // $offset = ($page - 1) * $perPage;

        // $count = $data->count();
        // $endCount = $offset + $perPage;
        // $morePages = $count > $endCount;

        // $data = [
        //     "results" => $data->orderBy('created_at', 'DESC')->skip($offset)->take($perPage)->get()->toArray(),
        //     "total" => $count,
        //     "pagination" => [
        //         "more" => $morePages
        //     ],
        // ];

        return response_json($data->paginate($perPage));
    }

    public function selected(Request $request)
    {
        $param = $request->get('param') ?: 'slider_article';
        $application = $request->get('application') ?: 1; // application MORE/OLYMPIC/OLYMPLAST/ERP/ dll

        $data = NewsView::with('r_uni_news_categories')->where('application', $application)->select('slug','title','date','image','description_meta');
        if ($param == 'slider_article') $data->where('slider_article',1);
        if ($param == 'editor_pick') $data->where('editor_pick',1);
        if ($param == 'selected_article') $data->where('selected_article',1);
        $data->orderBy('created_at','desc')->limit(5);
        return response_json($data->get());

    }
    public function categories(Request $request)
    {
        $application = $request->get('application') ?: 1; // application MORE/OLYMPIC/OLYMPLAST/ERP/ dll

        // $data = NewsCategory::where('uni_news_categories_id', null)->where('uni_gen_applications_id', $application);
        $data = NewsCategory::where('uni_gen_applications_id', $application);
        return response_json($data->get());
    }

    public function details(Request $request)
    {
        $application = $request->get('application') ?: 1; // application MORE/OLYMPIC/OLYMPLAST/ERP/ dll
        $slug = $request->get('slug') ?: ''; 
        $data = NewsView::with('r_uni_news_tags','r_uni_news_categories')->where('application',$application)->where('slug', trim($slug))->first();

        $next = NewsView::where('id', '>', $data->id)->where('application', $data->application)->min('id');
        $previous = NewsView::where('id', '<', $data->id)->where('application', $data->application)->max('id');

        $data->setAttribute('next_page', $next != $data->id ? NewsView::find($next) : null);
        $data->setAttribute('prev_page', $previous != $data->id ? NewsView::find($previous) : null);

        return response_json($data);
    }

    public function show($slug)
    {
 
        $data = NewsView::with('r_uni_news_tags','r_uni_news_categories')->where('application',1)->where('slug', trim($slug))->first();

        $next = NewsView::where('id', '>', $data->id)->where('application', $data->application)->min('id');
        $previous = NewsView::where('id', '<', $data->id)->where('application', $data->application)->max('id');

        $data->setAttribute('next_page', $next != $data->id ? NewsView::find($next) : null);
        $data->setAttribute('prev_page', $previous != $data->id ? NewsView::find($previous) : null);

        return response_json($data);
    }



    public function tags(Request $request)
    {
        $application = $request->get('application') ?: 1; // application MORE/OLYMPIC/OLYMPLAST/ERP/ dll

        $data = NewsTagGroupView::select('name')->where('uni_gen_applications_id',$application)->get();

        return response()->json($data);
    }

    public function tagsTop(Request $request)
    {
        $data = NewsTagTopView::get();

        return response()->json($data);
    }


    public function products(Request $request)
    {
        $news_id = $request->get('news_id') ?: null;

        $data = Product::with([
                    'r_uni_product_images',
                    'r_uni_product_suasanas',
                    'r_uni_product_variants',
                    'r_uni_product_variants.r_uni_product_variant_prices'
                ]);

        if($news_id) {
            $data->whereHas('r_uni_news_products', function($q) use($news_id) {
                $q->where('uni_news_id', $news_id);
            });
        }

        return response_json($data->limit(5)->get());
    }
}
