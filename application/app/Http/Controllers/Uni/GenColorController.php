<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\GenColor;
use Illuminate\Http\Request;

class GenColorController extends Controller
{
    public function index(){
        return response_json(GenColor::all());
    }
}
