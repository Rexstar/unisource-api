<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UniView\AddressActiveView;
use App\Models\Uni\City;
use App\Models\Uni\DeliveryArea;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $q = $request->get('q') ?: null;
        $province_id = $request->get('province_id') ?: null;

        // $data = DeliveryArea::select('uni_gen_cities.*')
        // ->join('uni_gen_cities', 'uni_delivery_areas.city_id', '=', 'uni_gen_cities.city_id')
        // ->where('uni_delivery_areas.status', 1)
        // ->groupBy('uni_gen_cities.city_id')
        // ->groupBy('uni_gen_cities.city_name');

        $data = City::select('*');

        if ($province_id) $data->where("province_id",$province_id);
        if ($q) $data->whereRaw('UPPER(city_name) like \'%'. strtoupper($q) .'%\'');

        $page = $request->get("page") ?: 1;
		$perPage = config('pagination.per_page_address', 100);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->orderBy('city_name', 'ASC')->skip($offset)->take($perPage)->get()->toArray(),
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function all(Request $request)
    {
        $q = $request->get('q') ?: null;

        $data = DeliveryArea::select('uni_gen_cities.*')
        ->join('uni_gen_cities', 'uni_delivery_areas.city_id', '=', 'uni_gen_cities.city_id')
        ->where('uni_delivery_areas.status', 1)
        ->groupBy('uni_gen_cities.city_id')
        ->groupBy('uni_gen_cities.city_name');

        if ($q) $data->whereRaw('UPPER(city_name) like \'%'. strtoupper($q) .'%\'');

        return response_json($data->orderBy('city_name', 'ASC')->get());
    }

    public function addressActive(Request $request)
    {
        $q = $request->get('q') ?: null;

        $data = AddressActiveView::select('*');

        return response_json($data->get());
    }

    public function allCity(Request $request)
    {
        $q = $request->get('q') ?: null;

        $data = City::select('*');

        if ($q) $data->whereRaw('UPPER(city_name) like \'%'. strtoupper($q) .'%\'');

        $page = $request->get("page") ?: 1;
		$perPage = config('pagination.per_page', 10);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->orderBy('city_name', 'ASC')->skip($offset)->take($perPage)->get()->toArray(),
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
