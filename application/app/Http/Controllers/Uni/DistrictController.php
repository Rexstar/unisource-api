<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Uni\District;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q') ?: null;
        $city_id = $request->get('city_id') ?: null;

        $data = District::with(['r_city', 'r_villages']);

        // $data = District::query();
        if ($city_id) $data->where("city_id",$city_id);
        if ($q) $data->whereRaw('UPPER(district_name) like \'%'. strtoupper($q) .'%\'');

        $page = $request->get("page") ?: 1;
		$perPage = config('pagination.per_page_address', 100);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->orderBy('district_name', 'ASC')->skip($offset)->take($perPage)->get()->toArray(),
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function all(Request $request)
    {
        $q = $request->get('q') ?: null;

        // $data = District::with(['r_city', 'r_villages']);
        $data = District::query();

        if ($q) $data->whereRaw('UPPER(district_name) like \'%'. strtoupper($q) .'%\'');

        return response_json($data->orderBy('district_name', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
