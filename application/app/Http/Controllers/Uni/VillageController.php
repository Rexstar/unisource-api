<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Uni\Village;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q') ?: null;
        $district_id = $request->get('district_id') ?: null;


        $data = Village::with('r_district');
        // $data = Village::query();

        if ($district_id) $data->where("district_id",$district_id);
        if ($q) $data->whereRaw('UPPER(village_name) like \'%'. strtoupper($q) .'%\'');

        $page = $request->get("page") ?: 1;
		$perPage = config('pagination.per_page_address', 100);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->orderBy('village_name', 'ASC')->skip($offset)->take($perPage)->get()->toArray(),
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function all(Request $request)
    {
        $q = $request->get('q') ?: null;

        // $data = Village::with('r_district');
        $data = Village::query();

        if ($q) $data->whereRaw('UPPER(village_name) like \'%'. strtoupper($q) .'%\'');

        return response_json($data->orderBy('village_name', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
