<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

use App\Models\Uni\Product;
use App\Models\Uni\Room;
use App\Models\Uni\Inspiration;
use App\Models\Uni\InspirationProduct;
use App\Models\Uni\InspirationApplication;
use App\Models\Uni\InspirationRoom;

class InspirationController extends Controller
{

    public function index(Request $request)
    {
        $q = $request->get('q') ?: null; // nama inspirasi

        $data = Inspiration::where('status',1);

        $category = $request->get('category') ?: null; // id ruangan (uni_categorys)

        if($category) {
            $data->where('category', $category);
        }

        if ($q) $data->whereRaw('LOWER(name) like \'%'. strtolower($q) .'%\'')->orWhereRaw('LOWER(keywords) like \'%'. strtolower($q) .'%\'');

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->orderBy('created_at', 'DESC')->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        // pixel_send('PageView'); //FBPIXEL Daftar Inspirasi

        return response_json($data);

    }

    public function detail($slug)
    {
        $data = Inspiration::where('slug', $slug)->first();
        if($data == null){
            abort(404);
        }

        $data = [
            "inspiration" => $data
        ];

        return response_json($data);

    }
    public function highlight()
    {
        $data = Inspiration::with('r_uni_inspiration_product')->where('highlight', 1)->get();
        return response_json($data);
    }

    public function room()
    {
        $data = Room::where('status', 1)->get();
        return response_json($data);
    }
}
