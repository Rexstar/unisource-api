<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\GenBrand;
use App\Models\Uni\GenColor;
use App\Models\Uni\GenMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use App\Models\Uni\Product;
use App\Models\Uni\Category;
use App\Models\Uni\CategoryGroup;
use App\Models\Uni\CategoryGroupDetail;
use App\Models\Uni\ProductApplication;
use App\Models\Uni\ProductCategory;
use App\Models\Uni\ProductPartner;
use App\Models\Uni\ProductVariant;
use App\Models\Uni\ProductVariantColor;
use App\Models\Uni\ProductVariantMaterial;
use App\Models\Uni\ProductVariantPrice;
use App\Models\Uni\ProductSearchRecomendation;
use App\Models\Uni\Inspiration;
use App\Models\Uni\DeliveryArea;
use App\Models\More\ProductGroup;
use App\Models\More\ProductGroupProduct;
use App\Models\Uni\ProductVariantShippingArea;
use App\Models\Erp\Bundling;
use App\Models\More\UserProductHistory;
use App\Models\More\UserSearchHistory;
use App\Models\MoreView\VoucherProductView;
use App\Models\UniView\CategoryView;
use App\Models\UniView\GenBrandView;
use App\Models\UniView\ProductVariantStockView;
use App\Models\Erp\ExpeditionPricelist;
use App\Models\Erp\Pack;
use App\Models\Erp\TransferWarehouse;
use App\Models\Erp\TransferWarehouseDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Moreverse\Product as MoreverseProduct;
use App\Models\Uni\City;
use App\Models\Uni\ProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // ~SAVING HISTORY OF SEARCHING PRODUCT
        $this->saveSearchHistory($request);

        $user = Auth::user();
        $q = $request->get('q') ?: null; // nama product
        $type = $request->get('type') ?: null; // tipe product (terlaris, terpopuler, terbaru)
        $brands = $request->get('brands') ?: null; // filtered brands
        $priceMin = $request->get('price_min') ?: null; // filtered price min
        $priceMax = $request->get('price_max') ?: null; // filtered price max
        $categoryGroup = $request->get('category_group') ?: null; // filtered category group
        $categories = $request->get('categories') ?: null; // filtered categories LEVEL 2
        $materials = $request->get('materials') ?: null; // filtered materials
        $colors = $request->get('colors') ?: null; // filtered colors
        $application = $request->get('application') ?: 1; // id aplikasi (uni_gen_applications)
        $isBundle = $request->get('is_bundle') ?: null; // product bundle
        $notId = $request->get('not_id') ?: null; // not product id
        $roomType = $request->get('room_type') ?: null; // not product id

        $key = "";
        if ($q){
            // $q= $q." HOT";
            $key = str_replace(" "," | ",$q);
        }

        $data = Product::leftJoin("uni_product_price_view as uppv", "uppv.uni_products_id", "=", "uni_products.id")
            ->leftJoin("uni_product_stock_view as upsv", "upsv.uni_products_id", "=", "uni_products.id")
            ->select([
                "uni_products.*",
                "upsv.stock",
                "uppv.min_discount_price",
                "uppv.max_discount_price",
                DB::raw("
                ts_rank(to_tsvector('indonesian', uni_products.name ), plainto_tsquery('indonesian', '$key')) AS rank"
                )
            ])
            ->with([
                'r_uni_product_images',
                'r_uni_product_suasanas',
                'r_uni_product_variants',
                'r_uni_product_variants.r_uni_product_variant_prices'
            ]);

        // if ($isBundle) $data->where("bundling" , 1);
        // if ($isBundle) $data->where("sku_parent" , 'BUNDLING');
        if ($roomType){

            if ($request->get('is_bundle') != null){
                if($request->get('is_bundle') == 0){
                    $data->where("sku_parent" ,'<>', 'BUNDLING');
                }else{
                    $data->where("sku_parent" ,'=', 'BUNDLING');
                }
            }
        }


        if ($notId) $data->where("id","!=", $notId);

        if($type) {
            if($type == 'terlaris') {
                $data->orderBy('total_sales', 'desc');
            } else if($type == 'terbaru') {
                $data->orderBy('created_at', 'desc');
            } else if($type == 'termurah') {
                $data->orderBy('uppv.min_discount_price', 'asc')->orderBy('uppv.max_discount_price', 'asc');
            } else if($type == 'termahal') {
                $data->orderBy('uppv.min_discount_price', 'desc')->orderBy('uppv.max_discount_price', 'desc');
            } else if($type == 'lastseen'){
                if($user){
                    $products_id = UserProductHistory::groupBy("uni_products_id")->where("more_users_id",$user->id)->pluck("uni_products_id");
                    if($products_id) $data->whereIn("id",$products_id)->orderBy("id", "asc");
                }
            } else {
                // product group slug
                $productGroup = ProductGroup::where('slug', $type)->first();
                if($productGroup) {
                    $products_id = ProductGroupProduct::where('more_product_groups_id', $productGroup->id)->pluck('uni_products_id');
                    $data->whereIn('id', $products_id)->inRandomOrder();
                }
            }
        }else{
            if ($q){
                $data->orderBy('stock','desc')->orderBy('rank','desc');
            }else{
                $data->orderBy('stock','desc')->orderBy('total_sales','desc');
            }

        }

        if ($categoryGroup) {
            $uni_categories_id = CategoryGroupDetail::where('uni_category_groups_id', $categoryGroup)->pluck('uni_categories_id');
            $products_id = ProductCategory::whereIn('uni_categories_id', $uni_categories_id)->pluck('uni_products_id');
            $data->whereIn('id', $products_id);
        }

        if (is_array($categories) && count(array_filter($categories)) > 0) {
            $child = Category::whereIn('uni_categories_id', $categories)->pluck('id');
            $products_id = ProductCategory::whereIn('uni_categories_id', $categories)->orWhereIn('uni_categories_id', $child)->pluck('uni_products_id');
            $data->whereIn('id', $products_id);
        }

        if (is_array($materials) && count(array_filter($materials)) > 0) {
            $variants_id = ProductVariantMaterial::whereIn("uni_gen_materials_id", $materials)->pluck("uni_product_variants_id");
            $products_id = ProductVariant::whereIn('id', $variants_id)->pluck('uni_products_id');
            $data->whereIn('id', $products_id);
        }

        if (is_array($colors) && count(array_filter($colors)) > 0) {
            $variants_id = ProductVariantColor::whereIn("uni_gen_colors_id", $colors)->pluck("uni_product_variants_id");
            $products_id = ProductVariant::whereIn('id', $variants_id)->pluck('uni_products_id');
            $data->whereIn('id', $products_id);
        }

        if (is_array($brands) && count(array_filter($brands)) > 0) {
            $data->whereIn("brand",$brands);
        }

        if(intval($priceMin) >= 0 && intval($priceMax) >= 0) {
            $min = intval($priceMin);
            $max = intval($priceMax);
            if($max >= $min) {
                $variantIds = ProductVariantPrice::where('more_discount_price', '>=', $min)->where('more_discount_price', '<', $max)->pluck('uni_product_variants_id');
                if($variantIds->count() > 0) {
                    $data->whereHas('r_uni_product_variants', function(Builder $builder) use($variantIds) {
                        $builder->whereIn('id', $variantIds);
                    });
                }
            }
            else {
                return response_json('Harga yang dimasukkan salah', 403);
            }
        }

        if($application) {
            $productIds = ProductApplication::where('uni_gen_applications_id', $application)->pluck('uni_products_id');
            if($productIds->count() > 0) {
                $data->whereIn('id', $productIds);
            }

            if($application == 1){
                $products_id = ProductVariant::where('more_active', 1)->pluck('uni_products_id');
                $data->whereIn('id', $products_id);
            }
        }

        if($roomType){
            $products_id = MoreverseProduct::where('room_type', $roomType)->pluck('uni_products_id');
            $data->whereIn('id', $products_id);
        }

        if ($q){
            // $q= $q." HOT";
            $key = str_replace(" "," | ",$q);
            // $data->whereRaw('LOWER(name) like \'%'. strtolower($key) .'%\'')->orWhereRaw('LOWER(keyword) like \'%'. strtolower($key) .'%\'');
            $data->whereRaw("to_tsvector('english', search_index) @@ plainto_tsquery('english', '$key')");

            // $data->where(function ($query) use ($key) {
            //     return $query->whereRaw('LOWER(name) like \'%'. strtolower($key) .'%\'')->orWhereRaw('LOWER(keyword) like \''. strtolower($key) .' %\'');
            // });
        }

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->whereHas('r_uni_product_variants')->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        //pixel_send('PageView'); //FBPIXEL Daftar Produk

        return response_json($data);
    }

    public function searchBrand(Request $request)
    {

        $q = $request->get('q') ?: null; // nama product
        $application = $request->get('application') ?: 1; // id aplikasi (uni_gen_applications)
        $categories = $request->get('categories') ?: null; // filtered categories LEVEL 2

        $key = "";
        if ($q){
            // $q= $q." HOT";
            $key = str_replace(" "," | ",$q);
        }

        $data = Product::select([
                "uni_products.*",
                DB::raw("
                ts_rank(to_tsvector('indonesian', uni_products.name ), plainto_tsquery('indonesian', '$key')) AS rank"
                )
            ])
            ->with([
                'r_uni_product_images',
                'r_uni_product_suasanas',
                'r_uni_product_variants'
            ]);

        if (is_array($categories) && count(array_filter($categories)) > 0) {
            $child = Category::whereIn('uni_categories_id', $categories)->pluck('id');
            $products_id = ProductCategory::whereIn('uni_categories_id', $categories)->orWhereIn('uni_categories_id', $child)->pluck('uni_products_id');
            $data->whereIn('id', $products_id);
        }

        if($application) {
            $productIds = ProductApplication::where('uni_gen_applications_id', $application)->pluck('uni_products_id');
            if($productIds->count() > 0) {
                $data->whereIn('id', $productIds);
            }
        }

        if ($q){
            $key = str_replace(" "," | ",$q);
            $data->whereRaw("to_tsvector('english', search_index) @@ plainto_tsquery('english', '$key')");
        }

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        //pixel_send('PageView'); //FBPIXEL Daftar Produk

        return response_json($data);
    }

    function variant(Request $request) {

        $data = ProductVariant::with('r_uni_product_variant_prices','r_uni_product_image','r_uni_product','r_uni_product_get_free_retail','r_uni_product_get_free_retail.r_uni_product_variants')
        ->leftJoin("uni_product_variants_stock_view as upsv", "upsv.id", "=", "uni_product_variants.id")
        ->select('uni_product_variants.*','upsv.stock_available','upsv.stock_erp')->where('uni_product_variants.more_active',1);

        $application = $request->get('application') ?: 1; // id aplikasi (uni_gen_applications)

        if($application) {
            if($application == 14){
                $data->whereRaw('LOWER(uni_product_variants.name_long) like \'%'. strtolower($request->q) .'%\'');
            }

            $productIds = ProductApplication::where('uni_gen_applications_id', $application)->pluck('uni_products_id');
            if($productIds->count() > 0) {
                $data->whereIn('uni_product_variants.uni_products_id', $productIds);
            }
        }

        $data = [
            "results" => $data->orderBy('uni_product_variants.id','ASC')->get()->toArray()
        ];

        return response_json($data);
    }

    function variantBrand(Request $request) {

        $q = $request->get('q') ?: null; // nama product
        $application = $request->get('application') ?: 1; // id aplikasi (uni_gen_applications)
        $categories = $request->get('categories') ?: null; // filtered categories LEVEL 2

        $key = "";
        if ($q){
            // $q= $q." HOT";
            $key = str_replace(" "," | ",$q);
        }

        $data = ProductVariant::with('r_uni_product_image','r_uni_product')->select('uni_product_variants.*',
        DB::raw("
        ts_rank(to_tsvector('indonesian', name_long ), plainto_tsquery('indonesian', '$key')) AS rank"
        ));

        if($application) {
            $productIds = ProductApplication::where('uni_gen_applications_id', $application)->pluck('uni_products_id');
            if($productIds->count() > 0) {
                $data->whereIn('uni_products_id', $productIds);
            }
        }

        if ($q){
            // $data->whereRaw('LOWER(name_long) like \'%'. strtolower($q) .'%\'');
            $key = str_replace(" "," | ",$q);
            $data->whereRaw("to_tsvector('english', name_short) @@ plainto_tsquery('english', '$key')");
        }

        if (is_array($categories) && count(array_filter($categories)) > 0) {
            $child = Category::whereIn('uni_categories_id', $categories)->pluck('id');
            $products_id = ProductCategory::whereIn('uni_categories_id', $categories)->orWhereIn('uni_categories_id', $child)->pluck('uni_products_id');
            $data->whereIn('uni_products_id', $products_id);
        }

        $data->orderBy('uni_product_variants.id','DESC');

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function search(Request $request)
    {
        $q = $request->get('q') ?: null; // nama product

        $data = ProductSearchRecomendation::select([
                "*"
            ])->whereRaw('LOWER(keyword) like \'%'. strtolower($q) .'%\'');

        $data->orderBy('sort','desc');

        $page = $request->get("page") ?: 1;
        $perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
        $offset = ($page - 1) * $perPage;

        $count = $data->count();
        $endCount = $offset + $perPage;
        $morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
            "pagination" => [
                "more" => $morePages
            ],
        ];

        return response_json($data);
    }
    public function saveSearchHistory(Request $request){
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $q = $request->get('q') ?: null; // nama product
            $type = $request->get('type') ?: null; // tipe product (terlaris, terpopuler, terbaru)
            $brands = $request->get('brands') ?: null; // filtered brands
            $priceMin = $request->get('price_min') ?: 0; // filtered price min
            $priceMax = $request->get('price_max') ?: 0; // filtered price max
            $categoryGroup = $request->get('category_group') ?: null; // filtered category group
            $categories = $request->get('categories') ?: null; // filtered categories LEVEL 2
            $materials = $request->get('materials') ?: null; // filtered materials
            $colors = $request->get('colors') ?: null; // filtered colors

            if ($q) {
                $model = new UserSearchHistory();
                $model->type = "KATA KUNCI";
                $model->value_1 = strtolower($q);
                $model->more_users_id = $user ? $user->id : null;
                $model->save();

                pixel_fb('Search', strtolower($q)); //FBPIXEL Detail Produk
            };


            if($type) {
                $model = new UserSearchHistory();
                $model->more_users_id = $user ? $user->id : null;
                $model->type = "URUTAN";
                if($type == 'terlaris') {
                    $model->value_1 = "TERLARIS";
                } else if($type == 'terbaru') {
                    $model->value_1 = "TERBARU";
                } else if($type == 'termurah') {
                    $model->value_1 = "TERMURAH";
                } else if($type == 'termahal') {
                    $model->value_1 = "TERMAHAL";
                } else if($type == 'lastseen') {
                    $model->value_1 = "";
                } else {
                    $model->value_1 = $type;
                }

                if($model->value_1) $model->save();


            }

            if ($categoryGroup) {
                $items = CategoryGroupDetail::with(["r_uni_categories"])->where('uni_category_groups_id', $categoryGroup);

                foreach ($items as $item) {
                    $model = new UserSearchHistory();
                    $model->type = "GROUP KATEGORI";
                    $model->value_1 = $item->r_uni_categories->name;
                    $model->more_users_id = $user ? $user->id : null;
                    $model->save();
                }
            }

            if (is_array($categories) && count(array_filter($categories)) > 0) {
                $items = Category::whereIn('id', $categories)->get();
                foreach ($items as $item) {
                    $model = new UserSearchHistory();
                    $model->type = "KATEGORI";
                    $model->value_1 = $item->name;
                    $model->more_users_id = $user ? $user->id : null;
                    $model->save();
                }


            }

            if (is_array($materials) && count(array_filter($materials)) > 0) {
                $items = GenMaterial::whereIn("id", $materials)->get();
                foreach ($items as $item) {
                    $model = new UserSearchHistory();
                    $model->type = "MATERIAL";
                    $model->value_1 = $item->name;
                    $model->more_users_id = $user ? $user->id : null;
                    $model->save();
                }


            }

            if (is_array($colors) && count(array_filter($colors)) > 0) {
                $items = GenColor::whereIn("id", $colors)->get();
                foreach ($items as $item) {
                    $model = new UserSearchHistory();
                    $model->type = "WARNA";
                    $model->value_1 = $item->name;
                    $model->more_users_id = $user ? $user->id : null;
                    $model->save();
                }


            }

            if (is_array($brands) && count(array_filter($brands)) > 0) {
                foreach ($brands as $item) {
                    $model = new UserSearchHistory();
                    $model->type = "BRAND";
                    $model->value_1 = $item;
                    $model->more_users_id = $user ? $user->id : null;
                    $model->save();
                }

            }

            if(intval($priceMin) >= 0 && intval($priceMax) >= 0) {
                $min = intval($priceMin);
                $max = intval($priceMax);
                if( ($max != 0 && $min != 0) && ($max >= $min) ) {
                    $model = new UserSearchHistory();
                    $model->type = "RENTANG HARGA";
                    $model->value_1 = $min;
                    $model->value_2 = $max;
                    $model->more_users_id = $user ? $user->id : null;
                    $model->save();
                }


            }

            // pixel_send('Search'); //FBPIXEL Pencarian Data
            //pixel_send('PageView'); //FBPIXEL

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
        }

    }

    public function top(Request $request)
    {
        $limit = $request->filled('limit') ?  $request->get('limit') : 3;
        $withs = [ 'r_uni_product_images', 'r_uni_product_variants', 'r_uni_product_variants.r_uni_product_variant_prices' ];
        $result = [];
        $result['terlaris']=Product::with($withs)->orderBy('total_sales', 'desc')->limit($limit)->get();
        $result['terbaru']=Product::with($withs)->orderBy('created_at', 'desc')->limit($limit)->get();

        $products_id = ProductGroupProduct::where('more_product_groups_id', 1)->pluck('uni_products_id');
        $result['terpopuler']=Product::with($withs)->whereIn('id', $products_id)->limit($limit)->get();
        return response_json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug, Request $request)
    {
        $q = $request->get('q') ?: null;

        $data = Product::leftJoin("uni_product_stock_view as upsv", "upsv.uni_products_id", "=", "uni_products.id")->with([
            'r_uni_product_raft_guides',
            'r_uni_product_videos',
            'r_uni_product_superiorities',
            'r_uni_product_images',
            'r_uni_product_infos',
            'r_uni_product_details',
            'r_uni_product_suasanas',
            'r_uni_product_variants',
            'r_uni_product_description_bundling',
            'r_uni_product_image_bundling',
            'r_uni_product_categories',
            'r_uni_product_combo',
            'r_uni_product_combo.r_uni_products',
            'r_uni_product_combo.r_uni_products.r_uni_product_variants',
            'r_uni_product_combo.r_uni_product_variants.r_uni_product_image',
            'r_uni_product_combo.r_uni_products_main',
            'r_uni_product_combo.r_uni_products_main.r_uni_product_variants',
            'r_uni_product_combo.r_uni_product_variants_main',
            'r_uni_product_combo.r_uni_product_variants_main.r_uni_product',
            'r_uni_product_combo.r_uni_product_variants_main.r_uni_product_image',
            'r_uni_product_variants.r_uni_product',
            'r_uni_product_variants.r_uni_product.r_uni_product_variants',
            'r_uni_product_variants.r_uni_product_image',
            'r_uni_product_variants.r_uni_product_variant_prices',
            'r_uni_product_variants.r_uni_product_variant_materials',
            'r_uni_product_variants.r_uni_product_variant_materials.r_uni_gen_material',
            'r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants',
            'r_uni_product_variants.r_uni_product_flashsale.r_uni_product_variants',
            'r_uni_product_variants.r_uni_product_get_po.r_uni_product_variants',
        ])->where('slug', $slug)->first();

        DB::beginTransaction();
        try {
            $user = Auth::user();
            $history = new UserProductHistory();
            $history->more_users_id = $user ? $user->id : null;
            $history->uni_products_id = $data->id;
            $history->created_at = date("Y-m-d H:i:s");
            $history->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
        //pixel_send('ViewContent',$data->name, 'product'); //FBPIXEL Detail Produk
        //pixel_fb('PageView'); //FBPIXEL Detail Produk

        if($data) {
            return response_json($data);
        }
        abort(404);
    }

    public function detailBrand($slug, Request $request)
    {
        $q = $request->get('q') ?: null;

        $data = Product::with([
            'r_uni_product_raft_guides',
            'r_uni_product_videos',
            'r_uni_product_superiorities',
            'r_uni_product_images',
            'r_uni_product_infos',
            'r_uni_product_details',
            'r_uni_product_suasanas',
            'r_uni_product_variants',
            'r_uni_product_categories',
            'r_uni_product_variants.r_uni_product',
            'r_uni_product_variants.r_uni_product.r_uni_product_variants',
            'r_uni_product_variants.r_uni_product_image'
        ])->where('slug', $slug)->first();

        if($data) {
            return response_json($data);
        }
        abort(404);
    }

    public function showBundle($slug, Request $request)
    {
        $data = Product::with([
            'r_uni_product_images', // image produk bundle
            'r_uni_product_variants', // varian bundle
            'r_uni_product_variants.r_uni_product', // varian bundle
            'r_uni_product_variants.r_uni_product_image', // image varian bundle
            'r_uni_product_variants.r_uni_product_variant_bundles', // detail produk bundle
            'r_uni_product_variants.r_uni_product_variant_bundles.r_uni_product_variants', //varian detail sku_group
            'r_uni_product_variants.r_uni_product_variant_bundles.r_uni_product_variants.r_uni_product_image', //varian detail sku_group
            'r_uni_product_variants.r_uni_product_variant_bundles.r_uni_product_variants.r_uni_product_variant_prices', //harga sku_group berdasarkan variant bundle
            'r_uni_product_variants.r_uni_product_get_free.r_uni_product_variants',
            'r_uni_product_variants.r_uni_product_flashsale.r_uni_product_variants',
            ])->where('slug', $slug)->first();

        if($data) {
            //pixel_send('PageView'); //FBPIXEL
            //pixel_send('ViewContent',$data->name, 'product'); //FBPIXEL Detail Produk Bundling

            return response_json($data);
        }
        abort(404);
    }

    public function slugify()
    {
        $products = Product::get();

        foreach ($products as $product) {
            Product::where('id', $product->id)->update(['slug' => Str::slug($product->name)]);
        }
    }

    public function categoryGroups()
    {
        return response_json(CategoryGroup::all());
    }

    public function filter()
    {
        $data = [];
        $data['brands'] = GenBrandView::get();
        $data['materials'] = GenMaterial::all();
        $data['colors'] = GenColor::all();
        $data['categories'] = CategoryView::with(
                                'r_sub',
                                'r_uni_product_categories',
                                'r_sub.r_uni_product_categories'
                            )->where('status', 1)
                            ->whereIn(
                                'uni_categories_id', Category::select('id')->whereNull('uni_categories_id')->where('status', 0)->pluck('id')
                            )
                            // ->whereHas('r_sub.r_uni_product_categories')
                            ->get()
                            ->toArray();

        return response_json($data);
    }

    public function partner($id)
    {
        $partner = ProductPartner::where('uni_products_id', $id)->orWhere('uni_products_id_partner', $id)->get();
        $productIds1 = $partner->pluck('uni_products_id');
        $productIds2 = $partner->pluck('uni_products_id_partner');
        $productIds = $productIds1->concat($productIds2);

        $data = Product::with([
                    'r_uni_product_images',
                    'r_uni_product_variants',
                    'r_uni_product_variants.r_uni_product_variant_prices'
                ])
                ->whereIn('id', $productIds)
                ->where('id', '!=', $id)
                ->inRandomOrder()
                ->get();
        return response_json($data);
    }


    public function listInspiration(Request $request)
    {
        $data = ProductImage::leftJoin("uni_products as upsv", "upsv.id", "=", "uni_product_images.uni_products_id")->select([
            "uni_product_images.*",
            "upsv.slug"])->where('type','SUASANA')->whereNotNull('upsv.slug')->where('status',1)->orderBy('created_at','DESC');

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;

        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);

    }

    public function inspiration($id)
    {
        $data = Inspiration::where('status',1)->where('uni_products_id', $id)->get();
        return response_json($data);
    }

    public function voucher($id)
    {
        $data = VoucherProductView::where('id',$id)->get();
        return response_json($data);
    }

    public function flashsale(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'uni_products_id' => 'required',
            'uni_product_variants_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $variant = ProductVariantStockView::where("uni_products_id",$request->uni_products_id)
        ->where("id",$request->uni_product_variants_id)
        ->with("r_uni_product_flashsale")->first();
        if($variant->r_uni_product_flashsale) {
            return response_json($variant->r_uni_product_flashsale);
        }
        // $flashsaleProduct = FlashsaleProduct::where('uni_products_id', $request->uni_products_id)->where('uni_product_variants_id', $request->uni_product_variants_id)->orderBy("id","desc")->first();

        // if($flashsaleProduct) {
        //     $now = Carbon::now();
        //     $flashsale = Flashsale::where('id', $flashsaleProduct->more_promotion_flashsales_id)
        //                     ->where(function ($q) use ($now) {
        //                         $q->where('date_start', '<=', $now);
        //                         $q->where('date_end', '>=', $now);
        //                     })
        //                     ->first();

        //     if($flashsale) {
        //         return response_json($flashsaleProduct->toArray());
        //     }
        // }
        return response_json('Flashsale tidak ditemukan.', 403);
    }

    public function free(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'uni_products_id' => 'required',
            'uni_product_variants_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $variant = ProductVariantStockView::with(["r_uni_product_get_free.r_uni_product_variants"])
        ->where('id', $request->uni_product_variants_id)
        ->where('uni_products_id', $request->uni_products_id)
        ->first();

        if($variant && $variant->r_uni_product_get_free) return response_json($variant->r_uni_product_get_free);

        return response_json('Produk Gratis tidak ditemukan.', 403);
    }

    public function po(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'uni_products_id' => 'required',
            'uni_product_variants_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $variant = ProductVariantStockView::with(["r_uni_product_get_po.r_uni_product_variants"])
        ->where('id', $request->uni_product_variants_id)
        ->where('uni_products_id', $request->uni_products_id)
        ->first();

        if($variant && $variant->r_uni_product_get_po) return response_json($variant->r_uni_product_get_po);

        return response_json('Produk PO tidak ditemukan.', 403);
    }

    public function combo()
    {
        $now = Carbon::now();
        // $combo = ProductFree::with([
        //                 'r_uni_products.r_uni_product_variants',
        //                 'r_uni_product_variants',
        //                 'r_uni_products_main.r_uni_product_variants',
        //                 'r_uni_products_main.r_uni_product_suasanas',
        //                 'r_uni_products_main.r_uni_product_images',
        //                 'r_uni_product_variants_main'
        //             ])
        //             ->where('free_type', 'COMBO')
        //             ->where('date_start', '<=', $now)
        //             ->where('date_end', '>=', $now)
        //             ->where('remains', '>', 0)
        //             ->get();
        $combo = Product::with([
                    'r_uni_product_variants',
                    'r_uni_product_suasanas',
                    'r_uni_product_images',
                    'r_uni_product_combo' => function ($q) use ($now) {
                        $q->where('date_start', '<=', $now)
                        ->where('date_end', '>=', $now)
                        ->where('remains', '>', 0);
                    },
                    'r_uni_product_combo.r_uni_product_variants',
                    'r_uni_product_combo.r_uni_product_variants_main',
                    ])
                    ->wherehas('r_uni_product_combo')
                    ->limit(9)
                    ->get();

        return response_json($combo);
    }

    public function groups(){
        $data = ProductGroup::with([
            'r_more_product_group_products',
            'r_more_product_group_products.r_uni_product.r_uni_product_images',
            'r_more_product_group_products.r_uni_product.r_uni_product_suasanas',
            'r_more_product_group_products.r_uni_product.r_uni_product_variants',
            'r_more_product_group_products.r_uni_product.r_uni_product_variants.r_uni_product_variant_prices',
        ])->where('display',1)->where('is_active',1)->orderBy("ordered_at","asc")->get();

        //pixel_send('PageView'); //FBPIXEL Halaman Utama MORE

        return response_json($data);
    }

    public function group($slug = null){
        $group = ProductGroup::with(['r_more_product_group_products'])
        ->whereRaw("lower(slug) = lower('".trim($slug)."')")->first();
        if($group) {
            $data = [];
            $data["group"] = $group;
            $data["products"] = Product::leftJoin("uni_product_price_view as uppv", "uppv.uni_products_id", "=", "uni_products.id")
                ->select([
                    "uni_products.*",
                    "uppv.min_discount_price",
                    "uppv.max_discount_price"
                ])
                ->with([
                    'r_uni_product_images',
                    'r_uni_product_suasanas',
                    'r_uni_product_variants',
                    'r_uni_product_variants.r_uni_product_variant_prices'
                ])->whereIn("id",$group->r_more_product_group_products->pluck("uni_products_id"))->get();
            return response_json($data);
        }
        abort(404);

    }

    // public function checkProduct(Request $request){

    //     $city= $request->get('city');
    //    $dataCity = City::where('city_name',$c ity)->first();
    //     if(!$dataCity){
    //         abort(404);
    //     }

    //     $city_id = $dataCity->city_id;
    //     $sku_param= $request->get('sku');
    //     $quantity_param = $request->get('quantity');

    //     $skuArray = explode(',', $sku_param);
    //     $quantityArray = explode(',', $quantity_param);

    //     $list = [];
    //     for ($i=0; $i < count($skuArray); $i++) {
    //         $sku = $skuArray[$i];
    //         $quantity = $quantityArray[$i];

    //         $stockProduct = ProductVariantStockView::where('sku_item', $sku)->first();
    //         if($stockProduct){
    //             $message = "Barang tersedia untuk lokasi anda!";
    //             $isValid = true;
    //             $shippingArea = null;
    //             $priceShipping = 0;

    //             $stock_available = $stockProduct->stock_available;
    //             $sku_group = $stockProduct->sku_group;

    //             // ? Cek Pengecekan Stok
    //             if($quantity > $stock_available){
    //                 $message = "Stok hanya tersisa ".$stock_available;
    //                 $isValid = false;
    //             }

    //             // ? Cek Pengecualian Barang berdasarkan Kab Kota
    //             $exceptedArea = DB::table('uni_delivery_exception')->where('sku_group', $sku_group)->where('city_id', $city_id)->count();
    //             if($exceptedArea >= 1){
    //                 $message = "Barang / lokasi pengirimanmu membutuhkan koordinasi manual dengan Admin.";
    //                 $isValid = false;
    //             }

    //             // ? Cek Cakupan Area Pengiriman
    //             $delivery = DeliveryArea::where(["city_id" => $city_id, "status" => 1])->first();

    //             if(!$delivery){
    //                 $message = "Barang / lokasi pengirimanmu membutuhkan koordinasi manual dengan Admin.";
    //                 $isValid = false;
    //             }else{
    //                 $columnPrice = "area_" . strval($delivery->area) . "_shipping_price";

    //                 $shippingArea = ProductVariantShippingArea::selectRaw(" area_" . strval($delivery->area) . "_status as status, " . $columnPrice . " as price ")->where(["sku_group" => $sku_group])->first();

    //                 if(!$shippingArea || ($shippingArea && $shippingArea->status != 1)){
    //                     $message = "Barang / lokasi pengirimanmu membutuhkan koordinasi manual dengan Admin.";
    //                     $isValid = false;
    //                 }else{
    //                     $priceShipping = (int)$shippingArea->price;
    //                 }
    //             }

    //             $data["delivery_area"] = $delivery;
    //             $data["shipping_area"] = $shippingArea;
    //             $data["stock_available"] = $stockProduct->stock_available;
    //             $data["price_shipping"] = $priceShipping;
    //             $data["quantity"] = (int)$quantity;
    //             $data["sku"] = $sku;
    //             $data["isValid"] = $isValid;
    //             $data["message"] = $message;

    //             array_push($list, $data);
    //         }
    //     }

    //     return response_json($list);
    // }

    public function checkProduct(Request $request){

        $city= $request->get('city');

        $expedition = ExpeditionPricelist::where('city_name', $city)->first();
        if(!$expedition) {
            abort(404);
        }
        $city = City::where('city_name',$city)->first();
        $sku_param= $request->get('sku');
        $quantity_param = $request->get('quantity');

        $skuArray = explode(',', $sku_param);
        $quantityArray = explode(',', $quantity_param);

        $list = [];
        for ($i=0; $i < count($skuArray); $i++) {
            $sku = $skuArray[$i];
            $quantity = $quantityArray[$i];

            $stockProduct = ProductVariantStockView::where('sku_item', $sku)->first();
            if($stockProduct){
                $message = "Barang tersedia untuk lokasi anda!";
                $isValid = true;
                $shippingArea = null;
                $priceShipping = 0;

                $stock_available = $stockProduct->stock_available;
                $additional_stock = $stockProduct->additional_stock;
                $sku_group = $stockProduct->sku_group;
                $totalStock = (int)$stock_available+(int)$additional_stock;
                // ? Cek Pengecekan Stok
                // if($quantity > $totalStock){
                //     $message = "Stok hanya tersisa ".$stock_available;
                //     $isValid = false;
                // }


                $shippingArea = 0;
                if($city->province_id == 31 || $city->province_id == 32 || $city->province_id == 33 || $city->province_id == 34 || $city->province_id == 35 || $city->province_id == 36 || $city->province_id == 51){
                    // 31	DKI JAKARTA
                    // 32	JAWA BARAT
                    // 33	JAWA TENGAH
                    // 34	DI YOGYAKARTA
                    // 35	JAWA TIMUR
                    // 36	BANTEN
                    // 51	BALI

                    $shippingArea = 0;
                }else{
                    $maxRate = 0;
                    $bundling = Bundling::where('sku_bundling', $sku)->get();
                    if(count($bundling) != 0){
                        //Produk Bundling
                        foreach($bundling as $value){
                            //Check Product Bund
                            $pack = Pack::where('sku_group', $value->group_item)->first();
                            if($pack->type == 'pack'){ //Pack
                                $volume = $pack->volume/5000;
                                $kg = $pack->kilograms;
                                $rateDetailSKU= $kg;
                                if($volume > $kg){
                                    $rateDetailSKU= $volume;
                                }
                                $rateDetailSKU = $rateDetailSKU*$value->qty;
                            }else{
                                //Kelipatan
                                $pack = Pack::where('sku_group', $value->group_item)->where('qty','<=',$value->qty)->orderBy('qty','DESC')->first();
                                $volume = $pack->volume/5000;
                                $rateDetailSKU= $volume;
                            }
                            $maxRate = $maxRate+($rateDetailSKU*$quantity); //Dikali jumlah pembelian barang
                        }
                    }else{
                        //Check Product
                        $pack = Pack::where('sku_group', $sku_group)->first();
                        if($pack->type == 'pack'){ //Pack
                            $volume = $pack->volume/5000;
                            $kg = $pack->kilograms;
                            $maxRate= $kg;
                            if($volume > $kg){
                                $maxRate= $volume;
                            }
                            $maxRate = $maxRate * $quantity;
                        }else{
                            //Kelipatan
                            $pack = Pack::where('sku_group', $sku_group)->where('qty','<=',$quantity)->orderBy('qty','DESC')->first();
                            $volume = $pack->volume/5000;
                            $maxRate= $volume;
                        }
                    }

                    if($maxRate < 10){ $maxRate = 10; }
                    $rate = $expedition->rate;
                    $shippingArea = ($rate-1600)*$maxRate;

                    $shippingArea = (int)$shippingArea;
                }

                // ? Cek Barang berdasarkan Kab Kota
                $checkAreaProduct = DB::table('uni_product_delivery_view')->where('sku_group', $sku_group)->count();
                if($checkAreaProduct >= 1){
                    $checkArea = DB::table('uni_product_delivery_view')->where('sku_group', $sku_group)->where('city_id',$city->city_id)->count();
                    if($checkArea == 0){
                        $message = "Produk tidak tersedia di lokasi tersebut";
                        $isValid = false;
                    }
                }

                $data["shipping_area"] = $shippingArea;
                $data["stock_available"] = $stockProduct->stock_available;
                $data["price_shipping"] = $priceShipping;
                $data["quantity"] = (int)$quantity;
                $data["sku"] = $sku;
                $data["isValid"] = $isValid;
                $data["message"] = $message;

                array_push($list, $data);
            }
        }

        return response_json($list);
    }


    public function itemReciveERP(){
        $data = TransferWarehouse::with('r_detail','r_detail.r_item')->where('status','POSTED')->whereIn('warehouse_to_id',[1,2]);
        $data = [
            "results" => $data->orderBy('created_at','DESC')->get()->toArray()
        ];

        return response_json($data);
    }

}
