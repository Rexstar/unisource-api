<?php

namespace App\Http\Controllers\Uni;

use App\Http\Controllers\Controller;
use App\Models\Uni\GenPaymentFaspay;
use Illuminate\Http\Request;

class GenPaymentFaspayController extends Controller
{
    public function index(Request $request) {
        // type: Virtual Account, Cicilan, Cicilan Tanpa Kartu, E-wallet, Minimarket
        $type = $request->get('type') ?: null;

        // status: aktif
        $status = $request->get('status');
        if(!$status || $status == 1) $status = 1;
        else if($status == 'all') $status = 'all';
        else $status = 0;

        $data = GenPaymentFaspay::query();

        if($type) {
            $data->where('type', $type);
        }

        if($status !== 'all') {
            $data->where('status', $status);
        }

        return response_json($data->get());
    }

    public function list()
    {
        $data['Virtual Account'] = GenPaymentFaspay::where('status', 1)->where('type','Virtual Account')->get();
        $data['E-wallet'] = GenPaymentFaspay::where('status', 1)->where('type','E-wallet')->get();
        $data['Kartu Kredit'] = GenPaymentFaspay::where('status', 1)->where('type','Kartu Kredit')->get();
        $data['Cicilan Tanpa Kartu'] = GenPaymentFaspay::where('status', 1)->where('type','Cicilan Tanpa Kartu')->get();
        $data['Minimarket'] = GenPaymentFaspay::where('status', 1)->where('type','Minimarket')->get();
        $data['Cicilan'] = GenPaymentFaspay::where('status', 1)->where('type','Cicilan')->get();
        // $cicilan = GenPaymentFaspay::where('status', 1)->where('type','Cicilan')->get();
        // $data['Cicilan'] = [];
        // foreach ($cicilan as $item) {
        //     foreach([3, 6, 12] as $key => $tenor) {
        //         $persentase_tenor = (float)$item->{$tenor."_month"};
        //         $cicilan_per_bulan = $total_pembelian + ($total_pembelian * ($persentase_tenor / 100));
        //         $data['Cicilan'][$item->name][$key]["code"] = $item->code;
        //         $data['Cicilan'][$item->name][$key]["name"] = $tenor . "x @" . $persentase_tenor ."%";
        //         $data['Cicilan'][$item->name][$key]["icon"] = $item->icon;
        //         $data['Cicilan'][$item->name][$key]["status"] = $item->status;
        //         $data['Cicilan'][$item->name][$key]["type"] = $item->type;
        //         $data['Cicilan'][$item->name][$key]["merchant_id_".$tenor] = $item->{"merchant_id_".$tenor};
        //         $data['Cicilan'][$item->name][$key]["merchant_password_".$tenor] = $item->{"merchant_password_".$tenor};
        //         $data['Cicilan'][$item->name][$key]["sort"] = $item->sort;
        //         $data['Cicilan'][$item->name][$key]["fee_is_free"] = $item->fee_is_free;
        //         $data['Cicilan'][$item->name][$key]["icon_url"] = $item->icon_url;
        //         $data['Cicilan'][$item->name][$key]["description"] = toRp($cicilan_per_bulan) . " / bulan";
        //     }
        // }
        return response_json($data);
    }
}
