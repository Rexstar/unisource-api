<?php

namespace App\Http\Controllers;

use App\Models\More\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    function sendVerification(Request $request)
    {
        $jwtData = auth()->user();

        $user = User::find($jwtData->id);

        $now = Carbon::now();
        $expiredTime = 60;

        if ($user->status_verification == 1) {
            return response_json([
                'verification_send' => false,
                'message' => 'Anda telah melakukan verifikasi email pada ' . tglWaktuIndo($user->verified_at) . ' WIB'
            ], 200);
        }

        if ($user->emailed_at != null && Carbon::parse($user->emailed_at)->addMinute($expiredTime)->isAfter($now)) {
            return response_json([
                'verification_send' => false,
                'message' => 'Anda telah melakukan request verifikasi pada ' . tglWaktuIndo($user->emailed_at) . ' WIB dan dapat melakukan request kembali pada ' . tglWaktuIndo(Carbon::parse($user->emailed_at)->addMinute($expiredTime)) . ' WIB'
            ], 200);
        }

        $token = [
            'email' => $user->email,
            'expired_date' => $now->addMinute($expiredTime),
            'expired_time' => $expiredTime,
        ];

        $url = config("app.web_url") . '/verify-email/' . Crypt::encrypt(json_encode($token));

        $result = sendEmail('emails.emailVerification', $user->email, 'Email Verifikasi', ['token' => $token, 'url' => $url]);

        if ($result) {
            $user->emailed_at = $now;
            $user->save();

            return response_json([
                'verification_send' => true,
                'message' => 'Email berhasil dikirim ke ' . $user->email
            ], 200);
        } else {
            return response_json([
                'verification_send' => false,
                'message' => 'Email gagal dikirim ke ' . $user->email
            ], 500);
        }
    }

    public function validVerification(Request $request)
    {
        try {
            $decrypt = Crypt::decrypt($request->id);
            $data = json_decode($decrypt);

            $email = User::where('email', $data->email)->first();

            $now = Carbon::now();
            $expiredTime = Carbon::parse($data->expired_date);

            if ($email->status_verification == 1) {
                //Redirect Page Done
                return response_json('Anda telah melakukan verifikasi email pada ' . tglWaktuIndo($email->verified_at) . ' WIB', 403);
            }

            if ($now->isAfter($expiredTime)) {
                //Redirect Page Expired
                return response_json('Token telah expired pada ' . tglIndoAngka($expiredTime), 403);
            }

            $email->status_verification = 1;
            $email->verified_at = $now;
            $email->save();

            //Redirect Page Sukses Verifikasi
            return response_json($email, 200);
        } catch (Exception $e) {
            //Redirect Link Page Gagal
            return response_json($e->getMessage(), 500);
        }
    }

    public function sendOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:more_users',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $jwtData = auth()->user();

        $validity = 60;

        $generateOtp = generateOTP($jwtData->id, 1, $validity);

        if (!$generateOtp->status) {
            return response_json([
                'otp_send' => false,
                'message' => 'Anda telah melakukan request OTP pada ' . tglWaktuIndo($generateOtp->param->created_at) . ' WIB dan dapat melakukan request kembali pada ' . tglWaktuIndo(Carbon::parse($generateOtp->param->expired_at)) . ' WIB'
            ], 403);
        }

        $token = [
            'name' => $jwtData->name != null ? $jwtData->name : $request->email,
            'email' => $request->email,
            'otp' =>  $generateOtp->param->token,
            'expired_date' => $generateOtp->param->expired_at,
            'expired_time' => $validity,
        ];

        $result = sendEmail('emails.emailOtp', $request->email, 'Email OTP', ['token' => $token]);

        if ($result) {
            return response_json([
                'otp_send' => true,
                'message' => 'Email kode OTP berhasil dikirim ke ' . $request->email
            ], 200);
        } else {
            return response_json([
                'otp_send' => false,
                'message' => 'Email kode OTP gagal dikirim ke ' . $request->email
            ], 500);
        }
    }

    public function validOTP(Request $request)
    {
        $jwtData = auth()->user();
        $token = $request->id;

        $validOTP = validateOTP($jwtData->id, $token, 1);

        if (!$validOTP->status) {
            return response_json([
                'status_verification' => $validOTP->status,
                'message' => $validOTP->code == 1 ? "Kode OTP tidak ditemukan" : ($validOTP->code == 2 ? "Kode OTP tidak valid atau sudah digunakan" : "Kode OTP telah expired")
            ], 403);
        }

        return response_json([
            'status_verification' => $validOTP->status,
            'message' => "Kode OTP valid"
        ], 200);
    }

    public function linkResetPwd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $user = User::where('email', $request->email)->first();

        if(!$user){
            return response_json(['email' => ['Email Anda tidak terdaftar']], 403);
        }

        $now = Carbon::now();
        $expiredTime = 60;

        $token = [
            'email' => $user->email,
            'name' => $user->name,
            'expired_date' => $now->addMinute($expiredTime),
            'expired_time' => $expiredTime,
        ];

        // dd(Carbon::parse($user->emailed_link_at));

        if ($user->emailed_link_at && Carbon::parse($user->emailed_link_at)->addMinute($expiredTime)->isAfter($now)) {
            return response_json(['email' => [
                "Anda baru saja melakukan request link, dapat request kembali pada " . tglWaktuIndo(Carbon::parse($user->emailed_link_at)->addMinute($expiredTime))
            ]], 403);
        }

        $url = config("app.web_url").'/forgotPassword/' . Crypt::encrypt(json_encode($token));
        $result = sendEmail('emails.emailLinkResetPwd', $user->email, 'Permintaan Atur Ulang Kata Sandi', ['token' => $token, 'url' => $url]);

        if ($result) {
            $user->emailed_link_at = $now;
            $user->reset_password_at = null;
            $user->save();
            return response_json([
                'sent' => true,
                'message' => 'Link berhasil dikirim ke ' . $user->email
            ], 200);
        } else {
            return response_json([
                'sent' => false,
                'message' => 'Link gagal dikirim ke ' . $user->email
            ], 500);
        }

    }
    public function validLinkResetPwd(Request $request)
    {
        try {
            $decrypt = Crypt::decrypt($request->id);
            $data = json_decode($decrypt);

            $now = Carbon::now();
            $expiredTime = Carbon::parse($data->expired_date);
            $user = User::where('email', $data->email)->first();
            if ($now->isAfter($expiredTime)) {
                return response_json(['password' => ['Link tidak berlaku sejak ' . tglIndoAngka($expiredTime)]], 403);
            }
            if ($user->reset_password_at != null) {
                return response_json(['password' => ['Anda baru saja mengganti kata sandi, silahkan melakukan permintaan kembali']], 403);
            }
            return response_json($data, 200);
        } catch (Exception $e) {
            return response_json($e->getMessage(), 500);
        }
    }
}
