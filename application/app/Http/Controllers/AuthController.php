<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\More\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'google', 'facebook','apple', 'deletion']]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:more_users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        try {

            $user = new User;
            $user->email = $request->input('email');
            $user->password = app('hash')->make($request->input('password'));
            $user->is_subscribe = $request->is_subscribe;
            $user->last_login =  date('Y-m-d H:i:s');
            $user->newsletter = $request->input('newsletter');
            $now = Carbon::now();
            $expiredTime = 60;

            $token = [
                'email' => $user->email,
                'expired_date' => $now->addMinute($expiredTime),
                'expired_time' => $expiredTime,
            ];

            $url = config("app.web_url") . '/verify-email/' . Crypt::encrypt(json_encode($token));

            $result = sendEmail('emails.emailVerification', $user->email, 'Email Verifikasi', ['token' => $token, 'url' => $url]);

            if ($result) {
                $user->emailed_at = $now;
            }

            $user->save();

            pixel_send('CompleteRegistration'); //FBPIXEL Daftar Produk

            $result = [
                'user' => $user,
                'token' => Auth::fromUser($user),
                'verification_send' => $result ? true : false
            ];

            return response_json($result, 200);
        } catch (\Exception $e) {
            \Log::info('ERROR REGISTER', ['message' => $e->getMessage(),
            'line' => $e->getLine(),
            // 'params' => ($request) ? json_encode(is_object($request) ? $request->all() : $request) : null,
            'stack_trace' => $e->getTraceAsString(),
            'error_code' => $e->getCode()]);

            return response_json($e->getMessage(), 409);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|exists:more_users',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $user = User::where('email',$request->email)->first();

        if($user->is_active== 0){
            return response_json(['message' => 'Account anda tersuspend / tidak aktif'], 401);
        }
        $user->last_login =  date('Y-m-d H:i:s');
        $user->save();
        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response_json(['message' => 'Password anda salah'], 401);
        }

        return $this->respondWithToken($token, $credentials['email']);
    }

    public function logout()
    {
        Auth::logout();
        return response_json("Success", 200);
    }

    public function google(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'uuid' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $user = User::where('email', $request->email)->orwhere('uuid_google', $request->uuid)->first();

        if($user != null){
            if($user->is_active == 0){
                return response_json(['message' => 'Account anda tersuspend / tidak aktif'], 401);
            }

            if($user->uuid_google == null){
                $user->uuid_google = $request->uuid;
                $user->save();
            }

            $token = Auth::fromUser($user);
            return $this->respondWithToken($token, $request->email);
        }

        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->picture = $request->picture;
        $user->uuid_google = $request->uuid;
        $user->status_verification = 1;
        $user->last_login =  date('Y-m-d H:i:s');
        $user->save();

        pixel_send('CompleteRegistration'); //FBPIXEL Daftar Produk

        $token = Auth::fromUser($user);

        $result = [
            'user' => $user,
            'token' => $token
        ];

        return response_json($result, 200);
    }

    public function facebook(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email',
            'uuid' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $user = User::where('email', $request->email)->orwhere('uuid_fb', $request->uuid)->first();

        if($user != null){
            if($user->is_active == 0){
                return response_json(['message' => 'Account anda tersuspend / tidak aktif'], 401);
            }

            if($user->uuid_fb == null){
                $user->uuid_fb = $request->uuid;
                $user->save();
            }

            $token = Auth::fromUser($user);
            return $this->respondWithToken($token, $request->email);
        }

        $user = new User();
        $user->email = $request->email;
        $user->status_verification = $request->email == null ? null : 1;

        $user->name = $request->name;
        $user->picture = $request->picture;
        $user->uuid_fb = $request->uuid;

        $user->save();

        pixel_send('CompleteRegistration'); //FBPIXEL Daftar Produk

        $token = Auth::fromUser($user);

        $result = [
            'user' => $user,
            'token' => $token
        ];

        return response_json($result, 200);
    }

    public function apple(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email',
            'uuid' => 'required'
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $user = User::where('email', $request->email)->orwhere('uuid_apple', $request->uuid)->first();

        if($user != null){
            if($user->is_active == 0){
                return response_json(['message' => 'Account anda tersuspend / tidak aktif'], 401);
            }

            if($user->uuid_apple == null){
                $user->uuid_apple = $request->uuid;
                $user->save();
            }

            $token = Auth::fromUser($user);
            return $this->respondWithToken($token, $request->email);
        }

        $user = new User();
        $user->email = $request->email;
        $user->status_verification = $request->email == null ? null : 1;

        $user->name = $request->name;
        $user->picture = $request->picture;
        $user->uuid_apple = $request->uuid;

        $user->save();

        pixel_send('CompleteRegistration'); //FBPIXEL Daftar Produk

        $token = Auth::fromUser($user);

        $result = [
            'user' => $user,
            'token' => $token
        ];

        return response_json($result, 200);
    }

    protected function respondWithToken($token, $user)
    {
        return response_json([
            'email' => $user,
            'token' => $token,
            'sso' => config('app.env') !== 'production' ? false : true,
            'expires_in' => Auth::factory()->getTTL()
        ], 200);
    }

    public function deletion(Request $request)
    {
        #Function For Facebook
        $signed_request = $request->signed_request;
        $data = $this->parse_signed_request($signed_request);
        $user_id = $data['user_id'];

        // Start data deletion
        $status_url = 'https://www.morefurniture.id/auth/deletion?id=abc123'; // URL to track the deletion
        $confirmation_code = 'abc123'; // unique code for the deletion request

        $data = array(
            'url' => $status_url,
            'confirmation_code' => $confirmation_code
        );
        echo json_encode($data);
    }

    protected function parse_signed_request($signed_request) {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        $secret = "43d4fbb50b87a950fb4aba67f74751e1"; // Use your app secret here

        // decode the data
        $sig = $this->base64_url_decode($encoded_sig);
        $data = json_decode($this->base64_url_decode($payload), true);

        // confirm the signature
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    protected function base64_url_decode($input) {
        return base64_decode(strtr($input, '-_', '+/'));
    }
}
