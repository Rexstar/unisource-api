<?php

namespace App\Http\Controllers;

use App\Models\More\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function sendWaOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric|unique:more_users',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $jwtData = auth()->user();

        $validity = 60;

        $generateOtp = generateOTP($jwtData->id, 2, $validity);

        if (!$generateOtp->status) {
            return response_json([
                'otp_send' => false,
                'message' => 'Anda telah melakukan request OTP pada ' . tglWaktuIndo($generateOtp->param->created_at) . ' WIB dan dapat melakukan request kembali pada ' . tglWaktuIndo(Carbon::parse($generateOtp->param->expired_at)) . ' WIB'
            ], 200);
        }

        $message = "Kode Verifikasi MORE : *".$generateOtp->param->token."* JANGAN MEMBERITAHU KODE RAHASIA INI KE SIAPAPUN termasuk pihak MORE\n\nAkan expired pada *".tglWaktuIndo(Carbon::parse($generateOtp->param->expired_at))."*";

        $result = sendWa($request->phone, $message);
        // dd($result);
        if ($result->getStatusCode() == 200 || $result->getStatusCode() == 201) {
            return response_json([
                'otp_send' => true,
                'message' => 'Whatsapp kode OTP berhasil dikirim ke ' . $request->phone
            ], 200);
        } else {
            return response_json([
                'otp_send' => false,
                'message' => 'Whatsapp kode OTP gagal dikirim ke ' . $request->email
            ], 500);
        }
    }

    public function validWaOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric|unique:more_users',
        ]);

        if ($validator->fails()) {
            return response_json($validator->errors(), 403);
        }

        $jwtData = auth()->user();
        $token = $request->id;

        $validOTP = validateOTP($jwtData->id, $token, 2);

        if (!$validOTP->status) {
            return response_json([
                'status_verification' => $validOTP->status,
                'message' => $validOTP->code == 1 ? "Kode OTP tidak ditemukan" : ($validOTP->code == 2 ? "Kode OTP tidak valid atau sudah digunakan" : "Kode OTP telah expired")
            ], 403);
        }

        $user = User::find($jwtData->id);
        $user->phone = $request->phone;
        $user->save();

        return response_json([
            'status_verification' => $validOTP->status,
            'message' => "Kode OTP valid"
        ], 200);
    }
}
