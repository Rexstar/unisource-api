<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class CommissionController extends Controller
{

    public function order()
    {

        $query = DB::select("select * from more_affiliate_order_view
        where status_link = 1 and status_order = 4");

        foreach($query as $value){
            $ordersn = $value->ordersn;
            $code_link = $value->code_link;
            $created_order = $value->created_order;
            $product_commision = $value->product_commision;
            $quantity_order = $value->quantity_order;

            $commission = (int)$product_commision*(int)$quantity_order;

            DB::table('more_affiliate_history_transactions')
              ->where('ordersn', $ordersn)
              ->where('code_link', $code_link)
              ->where('created_at', $created_order)
              ->update(['commission' => $commission,'status' => 2]);
        }
        return response_json("Total Update : ".count($query));
    }

}
