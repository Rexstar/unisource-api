<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Product as MoreverseProduct;
use App\Models\Moreverse\ProductCoohom as MoreverseProductCoohoom;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $data = MoreverseProduct::with('r_uni_products');
        return response_json($data->get());
    }

    public function coohom(Request $request)
    {
        $data = MoreverseProductCoohoom::with('r_uni_product_variants');
        return response_json($data->get());
    }
}
