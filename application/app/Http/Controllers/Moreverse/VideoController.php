<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Video as MoreverseVideo;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        return response_json(MoreverseVideo::where('status',1)->orderBy('sort', 'ASC')->get());
    }
}
