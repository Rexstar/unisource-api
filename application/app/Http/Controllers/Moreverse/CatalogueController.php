<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Catalogue as MoreverseCatalogue;

class CatalogueController extends Controller
{
    public function index(Request $request)
    {
        return response_json(MoreverseCatalogue::where('status',1)->orderBy('sort', 'ASC')->get());
    }
}
