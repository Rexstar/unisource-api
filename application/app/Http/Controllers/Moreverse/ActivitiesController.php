<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Activities as MoreverseActivities;
use App\Models\Moreverse\UserReminder as MoreverseUserReminder;
use App\Models\Moreverse\UserHistory as MoreverseUserHistory;
use App\Models\Moreverse\UserNotification as MoreverseUserNotification;

use DB;

class ActivitiesController extends Controller
{
    public function index(Request $request)
    {
        $duration = $request->get('duration') ?: null;
        $category = $request->get('category') ?: null;

        $data = MoreverseActivities::where('status',1);
        if($category){
            $data->where('type', $category);
        }

        if($duration){
            if($duration == 1){
                //Now
                $data->where('end_date','>=',date('Y-m-d'));
                $data->where('end_date','<=',date('Y-m-d'));
            }else{
                //Soon
                $data->where('start_date','>',date('Y-m-d'));
            }
        }

        return response_json($data->orderBy('start_date','ASC')->get());
    }


    public function reminder(Request $request)
    {

        $activities = $request->get('activities') ?: null;
        if($activities){

            DB::beginTransaction();
            try
            {
                $data = new MoreverseUserReminder();
                if(auth()->user()) {
                    $data->more_users_id =auth()->user()->id;
                }
                $data->moreverse_activities_id =$activities;
                $data->save();

                DB::commit();
                return response_json($data);
            }
            catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
            {
                DB::rollback();
                return response_json('Error',500);
            }


        }
    }

    public function pixel(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $button = $request->get('button') ?: null;

            $data = new MoreverseUserHistory();
            if(auth()->user()) {
                $data->more_users_id =auth()->user()->id;
            }
            $data->button = $button;
            $data->save();

            pixel_send('ViewContent',$button, 'product'); //FBPIXEL Detail Produk

            DB::commit();
            return response_json($data);
        }
        catch(\Yajra\Pdo\Oci8\Exceptions\Oci8Exception $e)
        {
            DB::rollback();
            return response_json('Error',500);
        }


    }

    public function notification(Request $request)
    {

        $data = MoreverseUserNotification::orderBy('created_at','DESC')->limit('10')->get();
        return response_json($data);
    }
}
