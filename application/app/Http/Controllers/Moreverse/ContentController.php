<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Content as MoreverseContent;

class ContentController extends Controller
{
    public function index(Request $request)
    {
        return response_json(MoreverseContent::get());
    }
}
