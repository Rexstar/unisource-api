<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Mitra as MoreverseMitra;

class MitraController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('q') ?: null; // nama mitra atau alamat
        $p = $request->get('p') ?: null; // nama Provinsi
        $k = $request->get('k') ?: null; // nama Provinsi

        $data = MoreverseMitra::with('r_uni_company')->where('status',1);

        if ($q) $data->whereRaw('LOWER(name) like \'%'. strtolower($q) .'%\'');
        if ($p) $data->whereRaw('LOWER(province) = \''. strtolower($p) .'\'');
        if ($k) $data->whereRaw('LOWER(city) like \'%'. strtolower($k) .'%\'');

        $data->orderBy('sort', 'asc');

        $page = $request->get("page") ?: 1;
		$perPage = $request->get('per_page') ? $request->get('per_page') : config('pagination.per_page_product', 24);
		$offset = ($page - 1) * $perPage;

		$count = $data->count();
		$endCount = $offset + $perPage;
		$morePages = $count > $endCount;


        $dataRayon = MoreverseMitra::join("uni_company as c", "c.id", "=", "moreverse_mitras.uni_company_id")
        ->select([
            "c.rayon_number"
        ]);
        if ($q) $dataRayon->whereRaw('LOWER(name) like \'%'. strtolower($q) .'%\'');
        if ($p) $dataRayon->whereRaw('LOWER(province) = \''. strtolower($p) .'\'');
        if ($k) $dataRayon->whereRaw('LOWER(city) like \'%'. strtolower($k) .'%\'');
        $rayon = $dataRayon->groupBy('c.rayon_number')->limit(1)->first();
        $numberRayon = 1;
        if($rayon != null){
            $numberRayon = $rayon->rayon_number;
        }
        $data = [
            "results" => $data->skip($offset)->take($perPage)->get()->toArray(),
            "total" => $count,
            "rayon" => $numberRayon,
			"pagination" => [
				"more" => $morePages
            ],
        ];

        return response_json($data);
    }

    public function show($slug)
    {
        $data = MoreverseMitra::findOrFail($slug);
        return response_json($data);
    }

    public function city(Request $request)
    {
        $q = $request->get('q') ?: null; // Province

        $data = MoreverseMitra::where('status',1)->where('province', $q)->groupBy('city')->select('city')->get();
        // $data->get();
        // $data->where('province', $q)->groupBy('city')->select('city')->get();
        return response_json($data);
    }


}
