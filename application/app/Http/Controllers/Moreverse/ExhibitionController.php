<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Exhibition as MoreverseExhibition;

class ExhibitionController extends Controller
{
    public function index($slug)
    {
        $data = MoreverseExhibition::where('room_type',$slug)->first();
        return response_json($data);
    }
}
