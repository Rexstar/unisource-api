<?php

namespace App\Http\Controllers\Moreverse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Moreverse\Map as MoreverseMap;

class MapController extends Controller
{
    public function index(Request $request)
    {
        return response_json(MoreverseMap::get());
    }
}
