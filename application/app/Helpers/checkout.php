<?php
use Carbon\Carbon;

if (!function_exists('generateOrderNumber')) {
    function generateOrderNumber(){
        $year  = Carbon::now()->format('y');
        $month = Carbon::now()->format('m');
        $date  = Carbon::now()->format('d');

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $char6 = substr(str_shuffle($permitted_chars), 0, 6);
        $sequence = mt_rand(100000,999999);
        return strtoupper($year.$month.$date.$char6.$sequence);
    }
}
if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 128){
        // Generate random bytes
        $random_bytes = random_bytes($length);

        // Create a unique string by concatenating timestamp and random bytes
        $timestamp = microtime(true) * 10000; // Multiply by 10000 to get more precision
        $unique_string = $timestamp . bin2hex($random_bytes);

        // Hash the unique string to ensure a fixed length and uniqueness
        $hashed_string = hash('sha256', $unique_string);

        // Trim the string to the desired length
        return substr($hashed_string, 0, $length);
    }
}
if (!function_exists('generateUTCTimestamp')) {
    function generateUTCTimestamp() {
        $timezone_offset = -7;
        // Get the current time in UTC
        $current_time = new DateTime('now');

        // Add the timezone offset in hours (in this case, UTC+7)
        $current_time->modify('+' . $timezone_offset . ' hours');

        // Format the time to ISO8601 UTC+0 format
        return $current_time->format('Y-m-d\TH:i:s\Z');
    }
}
