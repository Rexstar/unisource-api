<?php

// https://github.com/fightbulc/moment.php
use \Moment\Moment;

$locale = config('app.locale');
$momentLocale = $locale === 'id' ? 'id_ID' : 'en_US';

Moment::setLocale($momentLocale);

if (! function_exists('moment')) 
{
    function moment($date = null)
    {
        if(!$date) return new Moment();
        return new Moment($date);
    }
}
