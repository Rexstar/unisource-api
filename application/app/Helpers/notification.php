<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

if (! function_exists('sendFCM'))
{
    function sendFCM($recipients, $title, $body,$icon,$data = null)
    {
        $client = new \GuzzleHttp\Client(["base_uri" => "https://fcm.googleapis.com"]);

        $serverKey = 'key='.env('FCM_SERVER_KEY', '');

        $options = [
            'headers' => [
                'Authorization' => $serverKey,
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'priority' => 'high',
                'registration_ids' => $recipients,
                'notification' => [
                    'body' => $body,
                    'title' => $title,
                    'icon' => $icon
                ],
                'data' => $data
            ]
        ];

        // dd($options);

        $response = $client->post("/fcm/send", $options);

        return $response;
    }
}

if (! function_exists('sendEmail'))
{
    function sendEmail($view, $to, $subject, $item = [])
    {
        try{
            Mail::send($view, $item, function($message) use ($to,$subject) {
                $message->to($to)->subject($subject);
            });

            return true;
        }catch(Exception $e){
            // dd($e);
            // Log::info("sendEmail-Catch",[$e->getMessage()]);
            return false;
        }
    }
}

if (! function_exists('sendWa'))
{
    function sendWa($telp, $message){
        $userkey = 'eb79e2c1f47d';
        $passkey = 'df03af9b337cc0fc759c3bd1';

        $client = new \GuzzleHttp\Client(["base_uri" => "https://console.zenziva.net"]);

        $options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'userkey' => $userkey,
                'passkey' => $passkey,
                'to' => $telp,
                 'message' => $message
            ]
        ];

        $response = $client->post("wareguler/api/sendWA/", $options);

        return $response;
    }
}
