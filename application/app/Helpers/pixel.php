<?php
if (!function_exists('pixel_fb')) {
    function pixel_fb($event,$param1 = null, $param2 = null, $param3 = null, $param4 = null) {
        $env = env('APP_ENV', 'production');

        if ($env == "production") {
            //Start FB Pixel
            // if($event == 'PageView'){
            //     \LaravelFacebookPixel::createEvent('ViewContent', [
            //         'content_type'=>'product_group',
            //         'content_ids'=>$param1, //SKU
            //         'content_name'=>$param2, //Name Product & Name Group Product
            //     ]);
            // }elseif ($event == 'Search') {
            //     \LaravelFacebookPixel::createEvent('Search', ['search_string'=>$param1]);
            // }elseif ($event == 'CompleteRegistration') {
            //     \LaravelFacebookPixel::createEvent('CompleteRegistration');
            // }elseif ($event == 'AddToCart') {
            //     \LaravelFacebookPixel::createEvent('AddToCart', [
            //         'content_type'=>'product',
            //         'content_ids'=>"['".$param1."']", //SKU
            //         'value'=>$param2, //Price
            //         'num_items'=>'1',
            //         'content_name'=>$param3, //Name Product
            //         'currency'=>'IDR',
            //     ]);
            // }elseif ($event == 'InitiateCheckout') {
            //     \LaravelFacebookPixel::createEvent('InitiateCheckout', [
            //         'content_type'=>'product',
            //         'content_ids'=>$param1, //SKU
            //         'value'=>$param2, //Total
            //         'num_items'=>$param3, //QTY
            //         'currency'=>"IDR",
            //     ]);
            // }
            //End FB Pixel
        }
    }
}
if (!function_exists('pixel_send')) {
    function pixel_send($event,$content_name = null, $content_type = null, $price = null, $ipAddress = null, $typeBrowser = null, $email = null) {
        $env = env('APP_ENV', 'production');

        if ($env == "production") {
            $data=array();
            $data["event_name"] = $event;
            $data["event_time"] = time();


            if(auth()->user()) {
                $data["user_data"]["em"]=hash("sha256", strtolower(auth()->user()->email));
                $data["user_data"]["fn"]=hash("sha256", strtolower(auth()->user()->name));

                // if(auth()->user()->birthday != null){
                //     $data["user_data"]["db"]=hash("sha256", date('Ymd', strtotime(auth()->user()->birthday)));
                // }
                if(auth()->user()->gender != null){
                    if(auth()->user()->gender =='L'){
                        $gender = 'm';
                    }else{
                        $gender = 'f';
                    }
                    $data["user_data"]["ph"]=hash("sha256", $gender);
                }

                // TODO: name => auth()->user()->name
                // TODO: phone => auth()->user()->phone
            }
            if($email!= null){
                $data["user_data"]["em"]=hash("sha256", strtolower($email));
            }

            if($ipAddress!= null){
                $data["user_data"]["client_ip_address"]=$ipAddress;
                $data["user_data"]["client_user_agent"]=$typeBrowser;
            }
            if($event == 'AddToCart' || $event == 'Purchase' || $event == 'InitiateCheckout'){
                $data["custom_data"]["currency"]="IDR";
                $data["custom_data"]["num_items"]="1";
                $data["custom_data"]["value"]=$price;
            }
            if($content_name!= null){
                $data["custom_data"]["content_name"] = $content_name;
            }
            if($content_type!= null){
                $data["custom_data"]["content_type"] = $content_type;
            }
            $access_token = 'EAAaqoiZCuw8MBAFpyUnXyiGDwLZB1DdVxcZAGieutpC1oNCVIJ2oZBDxTTWCKMG8gHJAvKkmGZCw5015ZADYAfgSRDcvBr2BtzaXaLkFTHTYmZCiZA5jKWGwORlIcdYyb7KINg9fHRqHG0iKl9vetZAr48LLEuUxZCLgToZBfg2fwIWP7SnE8o4nBtnMu5nUdE7cpYZD';

            // PURCHASE DATA
            $data_json = json_encode(array($data));

            $fields = array();
            $fields['data'] = $data_json;

            // dd($fields);

            $ch = curl_init();
            curl_setopt_array($ch, array(
                // Replace with your offline_event_set_id
                CURLOPT_URL => "https://graph.facebook.com/v13.0/519687591908953/events?access_token=".$access_token,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>  http_build_query($fields),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    // "content-type: multipart/form-data",
                    "Accept: application/json"  ),
            ));

            $result = curl_exec($ch);
            // echo "\nResult encode";
            // echo ($result);
            // if (curl_errno($ch)) {
            //     echo 'Error:' . curl_error($ch);
            // }
            curl_close ($ch);
        }


    }
}
