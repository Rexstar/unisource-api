<?php

use App\Models\More\UserOtp;
use Carbon\Carbon;


if (!function_exists('pembulatanOngkir')) {
    function pembulatanOngkir($ongkir) {
        $ongkir = (int)$ongkir/5000;
        $pembualatan = ceil($ongkir);
        return ($pembualatan*5000);
    }
}

if (!function_exists('replaceMultipleChars')) {
    function replaceMultipleChars($string) {
        // Tentukan karakter-karakter yang ingin digantikan
        $charsToReplace = array('(', ')', '!', '@');

        // Tentukan karakter pengganti untuk setiap karakter yang akan digantikan
        $replacementChars = array(' ', ' ', ' ', ' ');

        // Lakukan penggantian karakter
        $result = str_replace($charsToReplace, $replacementChars, $string);

        return $result;
    }
}

if (!function_exists('getDays')) {
	function getDays()
	{
		return [
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu',
			'Sun' => 'Minggu'
		];
	}
}

if (!function_exists('getMonths')) {
	function getMonths()
	{
		return [
			1	=> "Januari",
			2	=> "Februari",
			3	=> "Maret",
			4	=> "April",
			5	=> "Mei",
			6	=> "Juni",
			7	=> "Juli",
			8	=> "Agustus",
			9	=> "September",
			10	=> "Oktober",
			11	=> "November",
			12	=> "Desember"
		];
	}
}

if (!function_exists('toRp')) {
	function toRp($parm, $with2digit = true)
	{
		$str = 'Rp.' . number_format(floatval($parm), 0, '', '.');
        if($with2digit) $str .',00';
        return $str;
	}
}

if (!function_exists('toDecimal')) {
	function toDecimal($parm)
	{
		return number_format(floatval($parm), 0, '', '.');
	}
}

if (!function_exists('tglIndo')) {
	function tglIndo($parm)
	{
		$array_bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$dataBulan = date('n', strtotime($parm));
		return date('d', strtotime($parm)) . " " . $array_bulan[$dataBulan] . " " . date('Y', strtotime($parm));
	}
}

if (!function_exists('tglIndoAngka')) {
	function tglIndoAngka($parm)
	{
		return date('d/m/Y', strtotime($parm));
	}
}

if (!function_exists('waktuIndo')) {
	function waktuIndo($parm)
	{
		return date('H:i', strtotime($parm));
	}
}

if (!function_exists('tglWaktuIndo')) {
	function tglWaktuIndo($parm)
	{
		if ($parm == '0000-00-00 00:00:00') {
			return "-";
		}
		$array_bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$dataBulan = date('n', strtotime($parm));
		$dataWaktu = date('H:i', strtotime($parm));
		return date('d', strtotime($parm)) . " " . $array_bulan[$dataBulan] . " " . date('Y', strtotime($parm)) . " " . $dataWaktu;
	}
}

if (!function_exists('hariTglWaktuIndo')) {
	function hariTglWaktuIndo($parm)
	{
		if ($parm == '0000-00-00 00:00:00') {
			return "-";
		}
		$array_bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$dataBulan = date('n', strtotime($parm));

		$array_hari = array(1 => "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu");
		$dataHari = date('N', strtotime($parm));

		$dataWaktu = date('H:i', strtotime($parm));

		return $array_hari[$dataHari] . ", " . date('d', strtotime($parm)) . " " . $array_bulan[$dataBulan] . " " . date('Y', strtotime($parm)) . " " . $dataWaktu;
	}
}

if (!function_exists('timeInterval')) {
	function timeInterval($start, $end)
	{
		$date1 = date_create($start);
		$date2 = date_create($end);
		$diff = date_diff($date1, $date2);
		$jam = $diff->format('%h') + ($diff->format('%a') * 24);
		$menit = $diff->format('%i') + ($diff->format('%a') * 24);
		$detik = $diff->format('%s') + ($diff->format('%a') * 24);
		$time = ['jam' => $jam, 'menit' => $menit, 'detik' => $detik];
		return $time;
	}
}

if (!function_exists('timeInterval24')) {
	function timeInterval24($start, $end)
	{
		$date1 = date_create($start);
		$date2 = date_create($end);
		$diff = date_diff($date1, $date2);
		$jam = $diff->format('%h') + ($diff->format('%a') * 24);
		$menit = $diff->format('%i') + ($diff->format('%a') * 24);
		$detik = $diff->format('%s') + ($diff->format('%a') * 24);
		$time = Date('H:i:s', strtotime($jam . ':' . $menit . ':' . $detik));
		return $time;
	}
}

if (!function_exists('dateDiff')) {
	function dateDiff($parm)
	{
		$datetime1 = new DateTime();
		$datetime2 = new DateTime($parm);
		$interval = $datetime1->diff($datetime2);

		$year = $interval->format('%y');
		$month = $interval->format('%m');
		$day = $interval->format('%a');
		$hour = $interval->format('%h');
		$min = $interval->format('%i');

		$words = "";
		if ($year > 0) {
			$words .= $year;
			if ($year == 1) {
				$words .= " year ";
			} else {
				$words .= " years ";
			}
		}
		if ($month > 0) {
			$words .= $month;
			if ($month == 1) {
				$words .= " month ";
			} else {
				$words .= " months ";
			}
		}
		if ($day > 0) {
			$words .= $day;
			if ($day == 1) {
				$words .= " day ";
			} else {
				$words .= " days ";
			}
		}
		if ($hour > 0) {
			$words .= $hour;
			if ($hour == 1) {
				$words .= " hour ";
			} else {
				$words .= " hours ";
			}
		}
		if ($min > 0) {
			$words .= $min;
			if ($min == 1) {
				$words .= " min ";
			} else {
				$words .= " mins ";
			}
		}

		$con = $datetime1 > $datetime2 ? " ago" : " later";

		return $words . $con;
	}
}

if (!function_exists('collect_count')) {
	function collect_count($array, $count)
	{
		$return = [];
		for ($i = 0; $i <= $count - 1; $i++) {
			if (count($array) >= $count)
				array_push($return, $array[$i]);
		}
		return collect($return);
	}
}

if (!function_exists('same_collection')) {
	function same_collection($collection1, $collection2)
	{
		if (count($collection1->diff($collection2)) == 0)
			return true;
		return false;
	}
}

if (!function_exists('formatRupiah')) {
	function formatRupiah($number)
	{
		return 'Rp' . number_format($number, 0, '.', '.');
	}
}

if (!function_exists('formatDate')) {
	function formatDate($date, $format = 'd/m/Y H:i:s')
	{
		$dateFormat = Carbon::parse($date);
		return $dateFormat->format($format);
	}
}

if (!function_exists('generateRandomString')) {
	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}

if (!function_exists('generatePin')) {
	function generatePin($digits = 4)
	{
		$i = 0;
		$pin = "";

		while ($i < $digits) {
			$pin .= mt_rand(0, 9);
			$i++;
		}

		return $pin;
	}
}

if (!function_exists('generateOTP')) {
	function generateOTP($identifier, $type, $validity = 10, $digits = 4)
	{
		$now = Carbon::now();
		$expired = Carbon::now();

		$otp = UserOtp::where('more_user_id', $identifier)->where('type', $type)->where('valid', 1)->first();

		if($otp != null){
			$lastOtp = Carbon::parse($otp->expired_at);

			if($now->isBefore($lastOtp)){
				return (object)[
					'status' => false,
					'param' => $otp
				];
			}

			$otp->valid = 0;
			$otp->save();
		}

		$token = str_pad(generatePin(), 4, '0', STR_PAD_LEFT);

		if ($digits == 5)
			$token = str_pad(generatePin(5), 5, '0', STR_PAD_LEFT);

		if ($digits == 6)
			$token = str_pad(generatePin(6), 6, '0', STR_PAD_LEFT);

		$param = UserOtp::create([
			'more_user_id' => $identifier,
			'token' => $token,
			'valid' => 1,
			'created_by' => $identifier,
			'created_at' => $now,
			'expired_at' => $expired->addMinutes($validity),
			'type' => $type
		]);

		return (object)[
			'status' => true,
			'param' => $param
		];
	}
}

if (!function_exists('validateOTP')) {
	function validateOTP($identifier, $token, $type)
	{
		$otp = UserOTP::where('more_user_id', $identifier)->where('type', $type)->where('token', $token)->first();

		if ($otp == null) {
			return (object)[
				'status' => false,
				'code' => 1
			];
		} else {
			if ($otp->valid == 1) {
				$now = Carbon::now();
				$expired = Carbon::parse($otp->expired_at);

				if ($now->isAfter($expired)) {
					return (object)[
						'status' => false,
						'code' => 3
					];
				} else {
					$otp->valid = 0;
					$otp->updated_by = $identifier;
					$otp->updated_at = $now;
					$otp->save();

					return (object)[
						'status' => true,
						'code' => 4
					];
				}
			} else {
				return (object)[
					'status' => false,
					'code' => 2
				];
			}
		}
	}
}

if (!function_exists('average_rating')) {
	function average_rating($rating1, $rating2, $rating3, $rating4, $rating5)
	{
		$averageRating = 0;

		if(is_int($rating1) && is_int($rating2) && is_int($rating3) && is_int($rating4) && is_int($rating5)) {
			$averageRating = round((1*$rating1 + 2*$rating2 + 3*$rating3 + 4*$rating4 + 5*$rating5) / ($rating1 + $rating2 + $rating3 + $rating4 + $rating5), 1);
		}

		return $averageRating;
	}
}
if (!function_exists('toNumberOnly')) {
	function toNumberOnly($text)
	{
		// NUMBER ONLY
		return preg_replace("/[^0-9]+/", "", $text);
	}
}

