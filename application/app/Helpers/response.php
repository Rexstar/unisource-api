<?php

if (! function_exists('response_json'))
{
	function response_json($data, $statusCode = 200)
	{
        $statusMessage = trans()->has('http_status_message.'.$statusCode) ? trans('http_status_message.'.$statusCode) : '';

		return response()->json([
            'success' =>  in_array($statusCode,[200,201,202,203]) ? true : false,
            'statusCode' => $statusCode,
            'message' => $statusMessage,
            'data' => $data
        ], $statusCode);
	}
}
