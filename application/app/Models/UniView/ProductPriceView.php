<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class ProductPriceView extends Model
{
    protected $table = 'uni_product_price_view';
    protected $primaryKey = 'uni_products_id';
    public $timestamps = false;

    protected $appends = [
        'price_normal',
        'price_range'
    ];

    public function getPriceNormalAttribute()
    {
        $priceNormal = $this->max_price_normal;
        $priceNormal = formatRupiah(floatval($priceNormal));
        return $priceNormal;
    }

    public function getPriceRangeAttribute()
    {
        if($this->max_discount_price != $this->min_discount_price){
            return formatRupiah(floatval($this->min_discount_price))."-".formatRupiah(floatval($this->max_discount_price));
        }else{
            return formatRupiah(floatval($this->max_discount_price));
        }
    }

}
