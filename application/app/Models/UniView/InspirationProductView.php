<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class InspirationProductView extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'uni_inspiration_products_view';

    public $timestamps = false;

    protected $appends = [
        'image_url'
    ];

    public function getImageUrlAttribute()
    {
        $imagePath = null;
        $filename = $this->path.'/'.$this->filename_webp;
        if($this->filename_webp == null){
            return null;
        }
        if($filename) {
            $imagePath = config('app.cdn') . $filename;
        }
        return $imagePath;

    }

}
