<?php

namespace App\Models\UniView;

use App\Models\More\Promotion\Flashsale;
use App\Models\More\Promotion\ProductFree;
use App\Models\More\Promotion\ProductPreOrder;
use App\Models\MoreView\FlashsaleProductView;
use Illuminate\Database\Eloquent\Model;

use App\Models\Uni\{PlainProduct, ProductImage, ProductVariantPrice, ProductVariantColor, ProductVariantMaterial,ProductVariantBundling};
use Carbon\Carbon;

class ProductVariantStockView extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'uni_product_variants_stock_view';

    public $timestamps = false;

    protected $appends = [
        "uni_product_variants_id",
        "valid_price",
        "valid_price_rp",
        "valid_discount",
        "valid_stock",
    ];

    public function getUniProductVariantsIdAttribute()
    {
        return $this->id;
    }

    public function getValidPriceAttribute()
    {
        $flashsale = $this->r_uni_product_flashsale;
        if($flashsale) {
            return intval($flashsale->price);
        }

        $defaultPrice = $this->r_uni_product_variant_prices;
        return intval($defaultPrice->more_discount_price);
    }

    public function getValidPriceRpAttribute()
    {
        $value = 0;
        if($this->valid_price) $value = $this->valid_price;
        return formatRupiah($value);
    }

    public function getValidDiscountAttribute()
    {
        $flashsale = $this->r_uni_product_flashsale;
        if($flashsale) {
            return $flashsale->discount;
        }

        $defaultPrice = $this->r_uni_product_variant_prices;
        return $defaultPrice->more_discount;
    }

    public function getValidStockAttribute()
    {
        $flashsale = $this->r_uni_product_flashsale;
        if($flashsale) {
            return $flashsale->remains;
        }

        $getFree = $this->r_uni_product_get_free;
        if($getFree) {
            return $getFree->remains;
        }

        return $this->stock_available;
    }

    public function r_uni_product_variant_prices()
    {
        return $this->hasOne(ProductVariantPrice::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_materials()
    {
        return $this->hasMany(ProductVariantMaterial::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_color()
    {
        return $this->hasOne(ProductVariantColor::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_bundles()
    {
        return $this->hasMany(ProductVariantBundling::class, 'uni_product_variants_id', 'id');
    }

    public function r_uni_product_image()
    {
        return $this->hasOne(ProductImage::class, 'uni_products_variants_id', 'id')->where(['status'=> 1,'type'=>'PRODUK']);
    }

    public function r_uni_product()
    {
        return $this->hasOne(PlainProduct::class, 'id', 'uni_products_id');
    }

    public function r_uni_product_get_free()
    {
        $now = Carbon::now()->format('Y-m-d');

        return $this->hasOne(ProductFree::class, 'uni_product_variants_id','id')->where(function ($q) use ($now) {
            $q->where('date_start','<=', $now);
            $q->where('date_end','>=', $now);
        })
        ->where('free_type', 'FREE')
        ->where(function ($query) {
            $query->whereNull('uni_gen_applications_id')
                  ->orWhere('uni_gen_applications_id', '=', 1);
        })
        ->orderBy("date_start","DESC");
    }

    public function r_uni_product_get_free_retail()
    {
        $now = Carbon::now()->format('Y-m-d');

        return $this->hasOne(ProductFree::class, 'uni_product_variants_id','id')->where(function ($q) use ($now) {
            $q->where('date_start','<=', $now);
            $q->where('date_end','>=', $now);
        })
        ->where('free_type', 'FREE')
        ->where(function ($query) {
            $query->whereNull('uni_gen_applications_id')
                  ->orWhere('uni_gen_applications_id', '=', 14);
        })
        ->orderBy("date_start","DESC");
    }

    public function r_uni_product_get_combo()
    {
        $now = Carbon::now()->format('Y-m-d');

        return $this->hasOne(ProductFree::class, 'uni_product_variants_id','id')->where(function ($q) use ($now) {
            $q->where('date_start','<=', $now);
            $q->where('date_end','>=', $now);
        })
        ->where('free_type', 'COMBO')
        ->where('remains', '>', 0)
        ->orderBy('combo_price', 'asc')
        ->limit(1);
    }

    public function r_uni_product_get_po()
    {
        $now = Carbon::now()->format('Y-m-d');

        return $this->hasOne(ProductPreOrder::class, 'uni_product_variants_id','id')->where(function ($q) use ($now) {
            $q->where('date_start','<=', $now);
            $q->where('date_end','>=', $now);
        })
        ->orderBy("date_start","DESC");
    }

    public function r_uni_product_flashsale()
    {
        $now = Carbon::now();
        return $this->hasOne(FlashsaleProductView::class, 'uni_product_variants_id','id')->where(function ($q) use ($now) {
            $q->where('date_start','<=', $now);
            $q->where('date_end','>=', $now);
        })->orderBy("date_start","DESC");
    }


}
