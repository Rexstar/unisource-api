<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class PopUpView extends Model
{
    protected $table = 'uni_popups_view';
    public $timestamps = false;


    protected $appends = [
        'image_url', 'link_popup'
    ];

    public function getImageUrlAttribute()
    {
        $path = 'POPUP/';

        $imagePath = null;
        $filename = $this->image;

        $imagePath = config('app.cdn') . $path . $filename;

        return $imagePath;

    }

    public function getLinkPopUpAttribute()
    {
        if($this->link) {
            return config('app.web_url') . '/' . $this->link;
        }
        return null;
    }

}
