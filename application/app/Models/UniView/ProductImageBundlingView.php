<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class ProductImageBundlingView extends Model
{
    protected $table = 'more_list_product_image_bundling_view';
    protected $primaryKey = 'uni_products_id';
    public $timestamps = false;

    protected $appends = [
        'image_url'
    ];

    public function getImageUrlAttribute()
    {
        $path = $this->path.'/';

        $imagePath = null;
        $filename = $this->filename_webp;

        if(!$filename) {
            $filename = $this->filename_non_webp;
        }

        if(!$filename){
            $path = "images/";
            $filename = "noimage.png";
        }

        if($filename) {
            $imagePath = config('app.cdn') . $path . $filename;
        }


        return $imagePath;

    }
}
