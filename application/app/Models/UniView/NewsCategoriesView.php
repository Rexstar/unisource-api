<?php

namespace App\Models\UniView;

use App\Models\Uni\NewsTag;
use Illuminate\Database\Eloquent\Model;

class NewsCategoriesView extends Model
{
    protected $table = 'uni_news_categories_view';
    protected $primaryKey = 'uni_news_id';
    public $timestamps = false;

}
