<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class ProductDescriptionBundlingView extends Model
{
    protected $table = 'more_list_product_description_bundling_view';
    protected $primaryKey = 'uni_products_id';
    public $timestamps = false;
}
