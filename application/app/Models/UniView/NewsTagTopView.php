<?php

namespace App\Models\UniView;

use App\Models\Uni\NewsTag;
use Illuminate\Database\Eloquent\Model;

class NewsTagTopView extends Model
{
    protected $table = 'uni_news_tags_top_view';
    public $timestamps = false;

}
