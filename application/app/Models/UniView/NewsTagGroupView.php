<?php

namespace App\Models\UniView;

use App\Models\Uni\NewsTag;
use Illuminate\Database\Eloquent\Model;

class NewsTagGroupView extends Model
{
    protected $table = 'uni_news_tags_group_view';
    public $timestamps = false;

}
