<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class AddressActiveView extends Model
{
    protected $table = 'uni_address_active_view';
    public $timestamps = false;

}
