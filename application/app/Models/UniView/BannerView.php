<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class BannerView extends Model
{
    protected $table = 'uni_banners_view';
    public $timestamps = false;


    protected $appends = [
        'image_desktop_url','image_mobile_url', 'link_banner'
    ];

    public function getImageDesktopUrlAttribute()
    {
        $path = 'BANNER/';

        $imagePath = null;
        $filename = $this->image_desktop;

        $imagePath = config('app.cdn_asset') . $path . $filename;

        return $imagePath;

    }

    public function getImageMobileUrlAttribute()
    {
        $path = 'BANNER/';

        $imagePath = null;
        $filename = $this->image_mobile;

        $imagePath = config('app.cdn_asset') . $path . $filename;

        return $imagePath;

    }

    public function getLinkBannerAttribute()
    {
        if($this->link) {
            if($this->type_link == 1) return config('app.web_url') . '/' . $this->link;
            return $this->link;
        }
        return null;
    }
}
