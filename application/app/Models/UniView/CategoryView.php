<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

use App\Models\Uni\Product;
use App\Models\Uni\ProductCategory;
use App\Models\Uni\CategoryGroupDetail;

class CategoryView extends Model
{
    protected $table = 'uni_categories_view';
    public $timestamps = false;

    protected $appends = [
        'image_url',
        'banner_url'
    ];

    public function getImageUrlAttribute()
    {
        if($this->image)
            return config('app.cdn') . 'MORE/' . $this->image;
        return null;
    }

    public function getBannerUrlAttribute()
    {
        if($this->banner)
            return config('app.cdn') . 'MORE/' . $this->banner;
        return null;
    }

    public function r_uni_products()
    {
        return $this->belongsToMany(Product::class, 'uni_product_categories', 'uni_products_id', 'uni_categories_id');
    }

    public function r_uni_product_categories()
    {
        return $this->hasMany(ProductCategory::class, 'uni_categories_id');
    }

    public function r_uni_category_group_details()
    {
        return $this->hasMany(CategoryGroupDetail::class, 'uni_categories_id');
    }

    public function r_sub()
    {
        return $this->hasMany($this, 'uni_categories_id')->where('status', 1);
    }

    public function r_subs()
    {
        return $this->r_sub()->with('r_subs');
    }
}
