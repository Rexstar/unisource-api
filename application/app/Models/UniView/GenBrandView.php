<?php

namespace App\Models\UniView;

use Illuminate\Database\Eloquent\Model;

class GenBrandView extends Model
{
    protected $table = 'uni_gen_brands_view';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    public function r_uni_products()
    {
        return $this->hasMany(Product::class, 'brand');
    }
}
