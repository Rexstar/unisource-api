<?php

namespace App\Models\UniView;

use App\Models\Uni\NewsTag;
use Illuminate\Database\Eloquent\Model;

class NewsView extends Model
{
    protected $table = 'uni_news_view';
    public $timestamps = false;

    protected $appends = [
        'image_url','date_indo'
    ];

    public function getDateIndoAttribute()
    {
        return tglIndo($this->date);
    }
    public function getImageUrlAttribute()
    {
        $path = 'NEWS/';

        $imagePath = null;
        $filename = $this->image;

        $imagePath = config('app.cdn_asset') . $path . $filename;

        return $imagePath;
    }


    public function r_uni_news_tags()
    {
        return $this->hasMany(NewsTag::class, 'uni_news_id', 'id');
    }

    public function r_uni_news_categories()
    {
        return $this->hasMany(NewsCategoriesView::class, 'uni_news_id', 'id');
    }
}
