<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class ComplainProduct extends Model
{
    protected $table = 'more_complain_products';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_order_products_id',
        'note',
    ];

    public function r_more_complain_product_images()
    {
        return $this->hasMany(ComplainProductImage::class, 'more_complain_products_id');
    }
}
