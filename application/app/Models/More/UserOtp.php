<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class UserOtp extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_user_otps';

    public $timestamps = false;

    protected $fillable = [
        'more_user_id','token','valid', 'expired_at','created_by','updated_by','created_at','updated_at','type'
    ];
}
