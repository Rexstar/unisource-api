<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'more_settings';
    public $timestamps = false;

}
