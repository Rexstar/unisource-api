<?php

namespace App\Models\More\Affiliate;

use Illuminate\Database\Eloquent\Model;
use App\Models\More\User;

class Order extends Model
{
    protected $table = 'more_affiliate_orders';

    public function r_more_user()
    {
        return $this->belongsTo(User::class, 'more_users_id', 'id');
    }
}
