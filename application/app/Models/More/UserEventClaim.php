<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class UserEventClaim extends Model
{
    protected $table = 'more_user_event_claims';

    protected $appends = [
        'created_at_indonesia',
        'created_date_indonesia',
        'status_description',
    ];

    public function getCreatedAtIndonesiaAttribute()
    {
        if($this->created_at) return hariTglWaktuIndo($this->created_at);
        return null;
    }
    public function getCreatedDateIndonesiaAttribute()
    {
        if($this->created_at) return tglIndo($this->created_at);
        return null;
    }

    public function getStatusDescriptionAttribute()
    {
        if($this->status == 1) return "Pengajuan";
        if($this->status == 2) return "Diterima";
        if($this->status == 3) return "Dikirim";
        if($this->status == 91) return "Ditolak";
        return null;
    }

    public function r_more_users()
    {
        return $this->hasOne(User::class, 'id', 'more_users_id');
    }

    public function r_more_events()
    {
        return $this->hasOne(Event::class, 'id', 'more_events_id');
    }
}
