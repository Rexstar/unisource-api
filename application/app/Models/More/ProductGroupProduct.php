<?php

namespace App\Models\More;

use App\Models\Uni\Product;
use Illuminate\Database\Eloquent\Model;

class ProductGroupProduct extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_product_group_products';

    public $timestamps = false;

    public function r_uni_product()
    {
        return $this->hasOne(Product::class, 'id','uni_products_id');
    }

}
