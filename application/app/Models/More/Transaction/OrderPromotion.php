<?php

namespace App\Models\More\Transaction;

use Illuminate\Database\Eloquent\Model;

class OrderPromotion extends Model
{
    protected $table = 'more_transaction_order_promotions';
    public $timestamps = false;
}
