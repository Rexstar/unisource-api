<?php

namespace App\Models\More\Transaction;

use App\Models\More\Promotion\ProductFree;
use App\Models\More\ComplainProduct;
use App\Models\More\ComplainProductImage;
use App\Models\More\ProductReview;
use Illuminate\Database\Eloquent\Model;
use App\Models\Uni\ProductVariant;
use App\Models\UniView\ProductVariantStockView;

class OrderProduct extends Model
{
    protected $table = 'more_transaction_order_products';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_orders_id',
        'more_transaction_order_details_id',
        'uni_product_variants_id',
        'price',
        'quantity',
        'flashsale',
        'bundling',
        'price_normal',
        'price_delivery',
        'is_free',
        'free_description',
        'more_promotion_product_frees_id',
        'more_promotion_flashsale_products_id',
    ];

    protected $appends = [
        'price_rp',
        'price_normal_rp',
        'sub_total_rp'
    ];

    public function getPriceRpAttribute()
    {
        $value = 0;
        if($this->price) $value = $this->price;
        return formatRupiah($value);
    }

    public function getPriceNormalRpAttribute()
    {
        $value = 0;
        if($this->price_normal) $value = $this->price_normal;
        return formatRupiah($value);
    }

    public function getSubTotalRpAttribute()
    {
        $value = 0;
        if($this->sub_total) $value = $this->sub_total;
        return formatRupiah($value);
    }


    public function r_uni_product_variant()
    {
        return $this->hasOne(ProductVariantStockView::class, 'id', 'uni_product_variants_id');
    }

    public function r_more_transaction_order_products_bundling()
    {
        return $this->hasMany(OrderProductBundling::class, 'more_transaction_order_products_id', 'id')->orderBy("id","asc");
    }

    public function r_more_product_review()
    {
        return $this->hasOne(ProductReview::class,"more_transaction_order_products_id","id");
    }

    public function r_more_complain_products()
    {
        return $this->hasOne(ComplainProduct::class,"more_transaction_order_products_id","id");
    }

    public function r_uni_product_get_combo()
    {
        return $this->belongsTo(ProductFree::class, 'more_promotion_product_frees_id','id');
    }
}
