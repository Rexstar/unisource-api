<?php

namespace App\Models\More\Transaction;
use Illuminate\Support\Facades\Http;
use App\Models\More\Complain;
use App\Models\More\ProductReview;
use Carbon\Carbon;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\More\Promotion\FlashsaleProduct;
use App\Models\More\Transaction\Cart;
use App\Models\More\Transaction\CartDetail;
use App\Models\More\Promotion\Voucher;
use App\Models\MoreView\OrderProductView;
use App\Models\Uni\City;
use App\Models\Uni\DeliveryArea;
use App\Models\Uni\ProductVariantShippingArea;
use App\Models\MoreView\UserAddressView;
use App\Models\Uni\GenPaymentFaspay;
use App\Models\UniView\ProductVariantStockView;
use App\Models\Erp\ExpeditionPricelist;
use App\Models\Erp\Pack;
use App\Models\Erp\Bundling;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;

class Order extends Model
{
    protected $table = 'more_transaction_orders';
    public $timestamps = false;

    protected $appends = [
        'total_rp',
        'faspay_expired_date_indonesia',
        'created_at_indonesia',
        'updated_at_indonesia',
        'paid_at_indonesia',
        'processed_at_indonesia',
        'shipped_at_indonesia',
        'complained_at_indonesia',
        'completed_at_indonesia',
        'canceled_at_indonesia',
        'autocanceled_at_indonesia',
        'processed_latest_at_indonesia',
        'shipped_latest_at_indonesia',
        'full_address',
    ];

    public function getTotalRpAttribute()
    {
        $value = 0;
        if ($this->total) $value = $this->total;
        return formatRupiah($value);
    }

    public function getFaspayExpiredDateIndonesiaAttribute()
    {
        return hariTglWaktuIndo($this->faspay_expired_date);
    }

    public function getCreatedAtIndonesiaAttribute()
    {
        if ($this->created_at) return hariTglWaktuIndo($this->created_at);
        return null;
    }

    public function getUpdatedAtIndonesiaAttribute()
    {
        if ($this->paid_at) return hariTglWaktuIndo($this->updated_at);
        return null;
    }

    public function getPaidAtIndonesiaAttribute()
    {
        if ($this->paid_at) return hariTglWaktuIndo($this->paid_at);
        return null;
    }

    public function getProcessedAtIndonesiaAttribute()
    {
        if ($this->processed_at) return hariTglWaktuIndo($this->processed_at);
        return null;
    }

    public function getShippedAtIndonesiaAttribute()
    {
        if ($this->shipped_at) return hariTglWaktuIndo($this->shipped_at);
        return null;
    }

    public function getComplainedAtIndonesiaAttribute()
    {
        if ($this->complained_at) return hariTglWaktuIndo($this->complained_at);
        return null;
    }

    public function getCompletedAtIndonesiaAttribute()
    {
        if ($this->completed_at) return hariTglWaktuIndo($this->completed_at);
        return null;
    }

    public function getCanceledAtIndonesiaAttribute()
    {
        if ($this->canceled_at) return hariTglWaktuIndo($this->canceled_at);
        return null;
    }

    public function getAutocanceledAtIndonesiaAttribute()
    {
        if ($this->autocanceled_at) return hariTglWaktuIndo($this->autocanceled_at);
        return null;
    }

    public function getProcessedLatestAtIndonesiaAttribute()
    {
        if ($this->paid_at && $this->status == 2) return hariTglWaktuIndo(date('Y-m-d H:i:s', strtotime($this->paid_at . ' +2 days')));
        return null;
    }

    public function getShippedLatestAtIndonesiaAttribute()
    {
        if ($this->processed_at && $this->status == 3) return hariTglWaktuIndo(date('Y-m-d H:i:s', strtotime($this->processed_at . ' +2 days')));
        return null;
    }

    public function getFullAddressAttribute()
    {
        return trim($this->address) . ", " .  trim($this->villages) . ", Kec. " .  trim($this->districts) . ", " .  trim($this->cities) . ", " .  trim($this->provinces) . ", " .  trim($this->postal_code);
    }

    public function r_more_transaction_order_details()
    {
        return $this->hasMany(OrderDetail::class, 'more_transaction_orders_id')->orderBy("id", "asc");
    }

    public function r_more_transaction_order_histories()
    {
        return $this->hasMany(OrderHistory::class, 'more_transaction_orders_id')->orderBy("id", "asc");
    }

    public function r_uni_gen_payment_faspay()
    {
        return $this->hasOne(GenPaymentFaspay::class, 'code', 'faspay_code');
    }

    public function r_more_transaction_order_history_created()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 1)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_paid()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 2)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_processed()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 3)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_shipped()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 4)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_complained()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 5)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_completed()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 6)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_canceled()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 91)->whereNotNull("note");
    }

    public function r_more_transaction_order_history_autocancel()
    {
        return $this->hasOne(OrderHistory::class, 'more_transaction_orders_id')->where("status", 92)->whereNotNull("note");
    }

    public function r_more_complain()
    {
        return $this->hasOne(Complain::class, 'more_transaction_orders_id');
    }

    public static function checkStockValidity($uni_product_variants_id, $quantity, $stockAvailableByParam = null)
    {
        $quantity = $quantity ? intval($quantity) : 1;

        $data['product'] = null;
        $data['isValid'] = false;
        $data['message'] = "Barang yang kamu pilih sedang habis. Silahkan pilih produk serupa lainnya.";

        if ($stockAvailableByParam != null) {
            if ($quantity <= intval($stockAvailableByParam)) {
                $data['isValid'] = true;
                $data['message'] = "Stok tersedia.";
                return (object)$data;
            }
        }
        else {
            $variant = ProductVariantStockView::find($uni_product_variants_id);

            if(!$variant) return (object)$data;

            if($variant->valid_price == 0) {
                $data['isValid'] = false;
                $data['message'] = "Produk tidak dapat dibeli.";
                return (object)$data;
            }

            $data['product'] = $variant;
            if ($quantity <= $variant->stock_available) {
                $data['isValid'] = true;
                $data['message'] = "Stok tersedia.";
            }
            return (object)$data;
        }
    }

    // public static function checkAreaValidity($sku_group, $city_id, $is_flashsale = 0)
    // {
    //     $data["isValid"] = false;
    //     $data["message"] = "Barang / lokasi pengirimanmu membutuhkan koordinasi manual dengan Admin. Untuk memproses lebih lanjut, segera hubungi Admin.";

    //     // ? Cek Pengecualian Barang berdasarkan Kab Kota
    //     $exceptedArea = DB::table('uni_delivery_exception')->where('sku_group', $sku_group)->where('city_id', $city_id)->count();

    //     if($exceptedArea >= 1) return (object)$data;

    //     // ? Cek Cakupan Area Pengiriman
    //     $delivery = DeliveryArea::where(["city_id" => $city_id, "status" => 1])->first();

    //     if(!$delivery) return (object)$data;

    //     $columnPrice = "area_" . strval($delivery->area) . "_shipping_price";
    //     $columnPrice = $is_flashsale == 1 ? $columnPrice . " + default_shipping_price" : $columnPrice;

    //     $shippingArea = ProductVariantShippingArea::selectRaw(" area_" . strval($delivery->area) . "_status as status, " . $columnPrice . " as price ")
    //         ->where(["sku_group" => $sku_group])->first();

    //     if(!$shippingArea || ($shippingArea && $shippingArea->status != 1)) return (object)$data;

    //     $data["delivery_area"] = $delivery;
    //     $data["shipping_area"] = $shippingArea;
    //     $data["isValid"] = true;
    //     $data["message"] = "Barang tersedia untuk lokasi anda!";

    //     return (object)$data;
    // }

    public static function checkAreaValidity($sku_item, $sku_group, $city_id, $quantity, $is_flashsale = 0)
    {
        $data["isValid"] = false;
        $data["message"] = "Barang / lokasi pengirimanmu membutuhkan koordinasi manual dengan Admin. Untuk memproses lebih lanjut, segera hubungi Admin.";


        //Cek ketersediaan kota
        $city = City::where('city_id', $city_id)->first();

        // ? Cek Barang berdasarkan Kab Kota
        $checkAreaProduct = DB::table('uni_product_delivery_view')->where('sku_group', $sku_group)->count();
        if($checkAreaProduct >= 1){
            $checkArea = DB::table('uni_product_delivery_view')->where('sku_group', $sku_group)->where('city_id',$city->city_id)->count();
            if($checkArea == 0){
                return (object)$data;
            }
        }


        if($city->province_id == 31 || $city->province_id == 32 || $city->province_id == 33 || $city->province_id == 34 || $city->province_id == 35 || $city->province_id == 36 || $city->province_id == 51){
            // 31	DKI JAKARTA
            // 32	JAWA BARAT
            // 33	JAWA TENGAH
            // 34	DI YOGYAKARTA
            // 35	JAWA TIMUR
            // 36	BANTEN
            // 51	BALI

            $data["shipping_area"] = 0;
            $data["isValid"] = true;
            $data["message"] = "Barang tersedia untuk lokasi anda!";
            return (object)$data;
        }
        $expedition = ExpeditionPricelist::where('city_name', $city->city_name)->first();
        if(!$expedition) return (object)$data;

        $maxRate = 0;
        $bundling = Bundling::where('sku_bundling', $sku_item)->get();
        if(count($bundling) != 0){
            //Produk Bundling
            foreach($bundling as $value){
                //Check Product Bund
                $pack = Pack::where('sku_group', $value->group_item)->first();

                if($pack == null) return (object)$data;

                if($pack->type == 'pack'){ //Pack
                    $volume = $pack->volume/5000;
                    $kg = $pack->kilograms;
                    $rateDetailSKU= $kg;
                    if($volume > $kg){
                        $rateDetailSKU= $volume;
                    }
                    $rateDetailSKU = $rateDetailSKU*$value->qty;
                }else{
                    //Kelipatan
                    $pack = Pack::where('sku_group', $value->group_item)->where('qty','<=',$value->qty)->orderBy('qty','DESC')->first();
                    $volume = $pack->volume/5000;
                    $rateDetailSKU= $volume;
                }
                $maxRate = $maxRate+($rateDetailSKU*$quantity); //Dikali jumlah pembelian barang
            }
        }else{

            //Check Product
            $pack = Pack::where('sku_group', $sku_group)->first();

            if($pack == null) return (object)$data;

            if($pack->type == 'pack'){ //Pack
                $volume = $pack->volume/5000;
                $kg = $pack->kilograms;
                $maxRate= $kg;
                if($volume > $kg){
                    $maxRate= $volume;
                }
                $maxRate = $maxRate * $quantity;
            }else{
                //Kelipatan
                $pack = Pack::where('sku_group', $sku_group)->where('qty','<=',$quantity)->orderBy('qty','DESC')->first();
                $volume = $pack->volume/5000;
                $maxRate= $volume;
            }
        }

        if($maxRate < 10){ $maxRate = 10; }
        $rate = $expedition->rate;
        $shippingArea = ($rate-1600)*$maxRate;
        $shippingArea = pembulatanOngkir($shippingArea);

        if($shippingArea < 0){
            return (object)$data;
        }
        $data["shipping_area"] = $shippingArea;
        $data["isValid"] = true;
        $data["message"] = "Barang tersedia untuk lokasi anda!";

        return (object)$data;
    }

    public static function checkFlashSaleValidity($uni_product_variants_id, $productInCartDetail = null)
    {
        $data = [];
        $user = auth()->user();

        $productInFlashsale = FlashsaleProduct::where(["uni_product_variants_id" => $uni_product_variants_id])->orderBy("id", "desc")->first();
        if ($productInFlashsale) {
            $now = Carbon::now();
            $flashsaleActive = DB::table("more_promotion_flashsales")->where("id", $productInFlashsale->more_promotion_flashsales_id)
                ->where(function ($q) use ($now) {
                    $q->where('date_start', '<=', $now);
                    $q->where('date_end', '>=', $now);
                })->first();

            if ($flashsaleActive) {
                $data["flashsale"] = $flashsaleActive;
                $data["productFlashsale"] = $productInFlashsale;

                if (!$productInCartDetail) {
                    $cart = Cart::where(['status' => 1, 'more_users_id' => $user->id])->first();
                    $productInCartDetail = CartDetail::where([
                        "more_transaction_carts_id" => $cart->id,
                        "uni_product_variants_id" => $uni_product_variants_id,
                        "is_flashsale" => 1
                    ])->first();
                }


                $orderId = Order::where("more_users_id", $user->id)->whereIn('status', [1, 2, 3, 4, 5, 6])->pluck("id");
                $orderProduct = OrderProductView::whereIn("more_transaction_orders_id", $orderId)
                    ->where("uni_product_variants_id", $productInCartDetail->uni_product_variants_id)
                    ->where("more_promotion_flashsales_id", $flashsaleActive->id)->first();

                if ($orderProduct) {
                    $data["isValid"] = false;
                    $data["message"] = "Anda telah membeli produk flashsale yang sama!";
                } else {
                    if ($productInFlashsale && $productInCartDetail && ($productInCartDetail->quantity <= $productInFlashsale->limit) && ($productInCartDetail->quantity <= $productInFlashsale->remains)) {
                        $data["isValid"] = true;
                        $data["message"] = "Stok Tersedia";
                    } else {
                        $data["isValid"] = false;
                        $data["message"] = "Stok Produk Flashsale telah habis!";
                    }
                }
            } else {
                $data["isValid"] = false;
                $data["message"] = "Flashsale telah habis atau tidak tersedia!";
            }
        } else {
            $data["isValid"] = false;
            $data["message"] = "Produk Flashsale tidak ditemukan!";
        }

        return (object)$data;
    }

    public static function checkVoucherValidity($code, $address_code, $payment_code, $cart = null, $device = 'mobile')
    {
        $user = \Auth::user();
        $data = [];
        $code = strtoupper($code);
        $cart = $cart ? $cart : Cart::where('more_users_id', $user->id)->first();

        $voucher  = Voucher::where('code', $code)
            ->where('status', 1)
            ->whereDate('duration_start', '<=', Carbon::now())
            ->whereDate('duration_end', '>=', Carbon::now())
            ->first();

        // ! default return data isValid = false
        $data["isValid"] = false;

        if (!$voucher) {
            // ? voucher tidak ditemukan
            $data["message"] = trans('voucher.error_not_found');
            return (object)$data;
        }

        if (!$cart) {
            // ? cart kosong
            $data["message"] = trans('voucher.error_cart');
            return (object)$data;
        }

        // if ($cart) {
        //     //Check cart dengan promo
        //     $checkCartPromo = DB::table('more_transaction_cart_details')
        //     ->where('more_transaction_carts_id', $cart->id)
        //     ->where(function ($query) {
        //         $query->where('is_get_free', '=', 1)->orWhere('is_flashsale', '=', 1);
        //     })->count();

        //     if($checkCartPromo >= 1){
        //         $data["message"] = trans('voucher.error_promotion');
        //         return (object)$data;
        //     }
        // }

        // ? lakukan pengecekan jika voucher bukan untuk semua device
        if($voucher->device !== "all") {
            if($voucher->device !== $device) {
                // ? voucher yg akan digunakan tidak sesuai dengan device user
                $paramDevice = $voucher->device === "mobile" ? "Mobile" : "Website";
                $data["message"] = trans('voucher.error_device', ['device' => $paramDevice]);
                return (object)$data;
            }
        }

        $data["voucher"] = $voucher;
        $data["request_code"] = $code;

        $useVoucher = DB::table('more_transaction_orders')
            ->where('voucher_code', $code)
            ->whereIn('status', [1, 2, 3, 4, 5, 6])
            ->count();

        if ($useVoucher > $voucher->limit_use) {
            // ? voucher habis
            $data["message"] = trans('voucher.error_limit_use');
            return (object)$data;
        }

        if ($voucher->use == 0) {
            $voucherCart = DB::table('more_transaction_orders')
                ->where('voucher_code', $code)
                ->where('more_users_id', $user->id)
                ->whereIn('status', [1, 2, 3, 4, 5, 6])->count();

            if ($voucherCart) {
                // ? voucher melebihi batas
                $data["message"] = trans('voucher.error_over_use');
                return (object)$data;
            }
        }

        if ($cart->grand_total <= $voucher->min_purchase) {
            // ? voucher harus dengan pembelian minimal $voucher->min_purchase
            $data["message"] = trans('voucher.error_min_purchase', ['min' => toRp($voucher->min_purchase)]);
            return (object)$data;
        }

        if ($voucher->type_voucher == 2) {
            $voucherUser = VoucherUser::where('more_promotions_vouchers_id', $voucher->id)
                ->where('more_users_id', $user->id)->first();

            if (!$voucherUser) {
                $data["message"] = trans('voucher.error_not_found');
                return (object)$data;
            }
        }

        if ($voucher->area != 0) {
            $addrss = UserAddressView::where('id', $address_code)->first();

            if ($voucher->type_voucher == 3) {
                if ($voucher->area != $addrss->area) {
                    // ? voucher tidak dapat digunakan pada lokasi user
                    $data["message"] = trans('voucher.error_location');
                    return (object)$data;
                }
            }
        }

        if ($voucher->type_voucher == 4) {
            $addrss = UserAddressView::where('id', $address_code)->first();
            $voucherLokasi = DB::table('more_promotion_voucher_cities')
                ->where('more_promotions_vouchers_id', $voucher->id)
                ->where('uni_city_id', $addrss->city)->count();

            if ($voucherLokasi == 0) {
                // ? voucher tidak dapat digunakan pada lokasi user
                $data["message"] = trans('voucher.error_location');
                return (object)$data;
            }
        }

        if ($voucher->type_voucher == 5) {
            $voucherPayment = DB::table('more_promotion_voucher_payments')
                ->where('more_promotions_vouchers_id', $voucher->id)
                ->where('payment_code_faspay', $payment_code)->count();

            if ($voucherPayment == 0) {
                // ? voucher tidak dapat digunakan untuk metode pembayaran yang dipilih
                $data["message"] = trans('voucher.error_payment_method');
                return (object)$data;
            }
        }

        if ($voucher->type_voucher == 6) {
            $cartDetail = CartDetail::where('more_transaction_carts_id', $cart->id)->pluck('uni_product_variants_id');

            $voucherProduct = DB::table('more_promotion_voucher_products')
                ->where('more_promotions_vouchers_id', $voucher->id)
                ->whereIn('uni_product_variants_id', $cartDetail)->count();

            if ($voucherProduct == 0) {
                $data["message"] = trans('voucher.error');
                return (object)$data;
            }
        }

        $potongan = $voucher->voucher;
        if ($voucher->type_discount == 2) {
            $potongan = $cart->grand_total * ($voucher->percentage / 100);
            if ($potongan > $voucher->max_discount_price) $potongan = $voucher->max_discount_price;
        }

        $voucher->potongan = floatval($potongan);

        // * warning : perhitungan tidak termasuk ongkir
        $voucher->grandTotalDiskon = $cart->grand_total - $voucher->potongan;

        $data["voucher"] = $voucher;
        $data["isValid"] = true;
        return (object)$data;
    }

    public static function checkProductFree($uni_product_variants_id, $quantity)
    {
        $data = [];
        $data['productFree'] = null;


        $variant = ProductVariantStockView::with(["r_uni_product_get_free", "r_uni_product_get_combo", "r_uni_product_get_combo.r_uni_product_variants", "r_uni_product_get_combo.r_uni_product_variants_main"])->find($uni_product_variants_id);
        if ($variant->r_uni_product_get_free) {

            $data['productFree'] = $variant->r_uni_product_get_free;

            if ($quantity <= $variant->r_uni_product_get_free->remains) {
                if ($variant->r_uni_product_get_free->is_multiple_buy == 0) {
                    $orderId = Order::where("more_users_id", Auth::user()->id)->whereIn('status', [1, 2, 3, 4, 5, 6])->pluck("id");
                    $orderProduct = OrderProduct::whereIn("more_transaction_orders_id", $orderId)->where("more_promotion_product_frees_id", $variant->r_uni_product_get_free->id)->first();
                    if ($orderProduct) {
                        $data['isValid'] = false;
                        $data['message'] = "Produk ini hanya bisa dibeli 1 kali, Anda telah membeli produk ini.";
                    } else {
                        $data['isValid'] = true;
                        $data['message'] = "Stok tersedia!";
                    }
                } else {
                    $data['isValid'] = true;
                    $data['message'] = "Stok tersedia!";
                }
            } else {
                $data['isValid'] = false;
                $data['message'] = "Stok tidak tersedia!";
            }
        } else if ($variant->r_uni_product_get_combo) {
            $data['productCombo'] = $variant->r_uni_product_get_combo;
            if ($quantity <= $variant->r_uni_product_get_combo->remains) {
                if ($variant->r_uni_product_get_combo->is_multiple_buy == 0) {
                    $orderId = Order::where("more_users_id", Auth::user()->id)->whereIn('status', [1, 2, 3, 4, 5, 6])->pluck("id");
                    $orderProduct = OrderProduct::whereIn("more_transaction_orders_id", $orderId)->where("more_promotion_product_frees_id", $variant->r_uni_product_get_combo->id)->first();
                    if ($orderProduct) {
                        $data['isValid'] = false;
                        $data['message'] = "Produk ini hanya bisa dibeli 1 kali, Anda telah membeli produk ini.";
                    } else {
                        $data['isValid'] = true;
                        $data['message'] = "Stok tersedia!";
                    }
                } else {
                    $data['isValid'] = true;
                    $data['message'] = "Stok tersedia!";
                }
            } else {
                $data['isValid'] = false;
                $data['message'] = "Stok tidak tersedia!";
            }
        } else {

            $data['isValid'] = false;
            $data['message'] = "Promo sudah tidak tersedia!";
        }
        return (object)$data;
    }

    public static function preCheckoutValidity($cartDetail, $city_id)
    {
        $variant = ProductVariantStockView::with(["r_uni_product_image", "r_uni_product_variant_prices"])->find($cartDetail->uni_product_variants_id);

        $variant->pre_checkout_status = true;
        $variant->pre_checkout_message = "success";
        if ($variant) {

            // ~checking flashsale
            if ($cartDetail->is_flashsale == 1) {
                $checkFlashsale = self::checkFlashSaleValidity($variant->id, $cartDetail);
                $variant->pre_checkout_flashsale = $checkFlashsale;
                if (!$checkFlashsale->isValid) {
                    $variant->pre_checkout_status = false;
                    $variant->pre_checkout_message = $checkFlashsale->message;
                    $cartDetail->variant_detail = $variant;
                    return $cartDetail;
                }
            }

            // ~checking Free Product
            if ($cartDetail->is_get_free == 1 && !$cartDetail->free_description) {
                $checkProductFree = self::checkProductFree($variant->id, $cartDetail->quantity);
                $variant->pre_checkout_product_free = $checkProductFree;
                if (!$checkProductFree->isValid) {
                    $variant->pre_checkout_status = false;
                    $variant->pre_checkout_message = $checkProductFree->message;
                    $cartDetail->variant_detail = $variant;
                    return $cartDetail;
                }
            }

            // * Checking COMBO Product
            if ($cartDetail->is_get_combo == 1 && !$cartDetail->free_description) {
                $checkProductCombo = self::checkProductFree($variant->id, $cartDetail->quantity);
                $variant->pre_checkout_product_free = $checkProductCombo;
                if (!$checkProductCombo->isValid) {
                    $variant->pre_checkout_status = false;
                    $variant->pre_checkout_message = $checkProductCombo->message;
                    $cartDetail->variant_detail = $variant;
                    return $cartDetail;
                }
            }

            // ~checking area
            // $checkArea = self::checkAreaValidity($variant->sku_group, $city_id, $cartDetail->is_flashsale);
            // $variant->pre_checkout_area = $checkArea;
            // if (!$checkArea->isValid) {
            //     $variant->pre_checkout_status = false;
            //     $variant->pre_checkout_message = $checkArea->message;
            //     $cartDetail->variant_detail = $variant;
            //     return $cartDetail;
            // }

            // ~checking area
            $checkArea = self::checkAreaValidity($variant->sku_item,$variant->sku_group, $city_id,$cartDetail->quantity, $cartDetail->is_flashsale);
            $variant->pre_checkout_area = $checkArea;
            if (!$checkArea->isValid) {
                $variant->pre_checkout_status = false;
                $variant->pre_checkout_message = $checkArea->message;
                $cartDetail->variant_detail = $variant;
                return $cartDetail;
            }

            // ~checking stock
            if (isset($cartDetail->r_more_transaction_cart_bundlings) && count($cartDetail->r_more_transaction_cart_bundlings) > 0) {
                foreach ($cartDetail->r_more_transaction_cart_bundlings as $bundleItem) {
                    $checkStock = self::checkStockValidity($bundleItem->uni_product_variants_id, $bundleItem->quantity);
                    $variant->pre_checkout_stock = $checkStock;
                    if (!$checkStock->isValid) {
                        $variant->pre_checkout_status = false;
                        $variant->pre_checkout_message = $checkStock->message;
                        $cartDetail->variant_detail = $variant;
                        return $cartDetail;
                    }
                }
            } else {
                $checkStock = self::checkStockValidity($cartDetail->uni_product_variants_id, $cartDetail->quantity, $variant->stock_available);
                $variant->pre_checkout_stock = $checkStock;
                if (!$checkStock->isValid) {
                    $variant->pre_checkout_status = false;
                    $variant->pre_checkout_message = $checkStock->message;
                    $cartDetail->variant_detail = $variant;
                    return $cartDetail;
                }
            }


            $cartDetail->variant_detail = $variant;
            return $cartDetail;
        }
        $cartDetail->variant_detail = $variant;
        return $cartDetail;
    }

    public static function fetchPaymentDoku($items, $order, $typePayment)
    {
        $faspay = (object)config("faspay");
        $web_url = (object)config("app");
        $user = Auth::user();

        $requestID = generateRandomString();
        $timeStamp = generateUTCTimestamp();

        if($typePayment == 'CC'){
            //Kartu Kredit
            $arr = [
                "order" => [
                    "invoice_number"=>$order->ordersn,
                    "line_items"=>$items,
                    "amount"=>strval($order->total),
                    "callback_url"=>$web_url->web_url.'/profile/order/'.$order->ordersn,
                    "auto_redirect"=>false
                ],
                "customer" => [
                    "id" => $user->id,
                    "name" => $order->receiver_name,
                    "email" => $user->email,
                    "phone" => $order->receiver_phone,
                    "address" => $order->address,
                    "country" => "ID"
                ],
                "override_configuration" => [
                    "themes" => [
                        "language"=> "ID",
                        "background_color"=> "F5F8FB",
                        "font_color"=> "1A1A1A",
                        "button_background_color"=> "E1251B",
                        "button_font_color"=> "FFFFFF"
                    ]
                ]
            ];
        }
        // dd($arr);
        $jsonBody = json_encode($arr);
        $digestSHA256 = hash('sha256', $jsonBody, true);
        $digestBase64 = base64_encode($digestSHA256);

        // Menggantikan script untuk menghitung signature HMAC-SHA256 dan Base64 di sisi JavaScript
        $signatureComponents = "Client-Id:".$faspay->doku_client_id."\n" .
            "Request-Id:" . $requestID . "\n" .
            "Request-Timestamp:" . $timeStamp . "\n" .
            "Request-Target:/credit-card/v1/payment-page" . "\n" .
            "Digest:" . $digestBase64;

        $secretKey = $faspay->doku_secret_key;
        $signatureHmacSha256 = hash_hmac('sha256', $signatureComponents, $secretKey, true);
        $signatureBase64 = 'HMACSHA256='.base64_encode($signatureHmacSha256);
        //   dd($signatureBase64);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $faspay->doku_end_point_cc,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>$jsonBody,
        CURLOPT_HTTPHEADER => array(
            'Client-Id:'.$faspay->doku_client_id,
            'Request-Id:'.$requestID.'',
            'Request-Timestamp:'.$timeStamp,
            'Signature:'.$signatureBase64,
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // dd($response);
        curl_close($curl);

        if($httpcode == 200){
            if($typePayment == 'CC'){
                //Kartu Kredit
                $bill_expired = date("Y-m-d H:i:s", strtotime('+1 days'));

                $response = json_decode($response);
                $result["bill_expired"] = $bill_expired;
                $result["faspay"]['redirect_url'] = $response->credit_card_payment_page->url;
                return (object)$result;
            }else{
                //Virtual Account
                $result["faspay"]['redirect_url'] = '';
                return (object)$result;
            }
        }
        return null;


    }
    public static function fetchPaymentFaspay($items, $order)
    {
        $faspay = (object)config("faspay");
        $signature = sha1(md5($faspay->merchant_user . $faspay->merchant_password . $order->ordersn));
        $user = Auth::user();

        $bill_expired = null;
        if ($order->faspay_code == "812") {
            $bill_expired = date("Y-m-d H:i:s", strtotime('+1 Minutes'));
        } else if ($order->faspay_code == "706") {
            $bill_expired = date("Y-m-d H:i:s", strtotime('+2 days'));
        } else if ($order->faspay_code == "707") {
            $bill_expired = date("Y-m-d H:i:s", strtotime('+2 days'));
        } else {
            $bill_expired = date("Y-m-d H:i:s", strtotime('+3 hours'));
        }

        if ($order->faspay_code == "709") {
            $req = [
                "json" => [
                    "request" => "Transmisi Info Detil Pembelian",
                    "merchant_id" => $faspay->merchant_id,
                    "merchant" => $faspay->merchant_name,
                    "bill_no" => $order->ordersn,
                    "bill_date" => $order->created_at,
                    "bill_expired" => $bill_expired,
                    "bill_desc" => "Payment Online Via Faspay",
                    "bill_currency" => "IDR",
                    "bill_gross" => strval($order->total) . "00",
                    "bill_miscfee" => "0",
                    "bill_total" => strval($order->total) . "00",
                    "cust_no" => $user->id,
                    "cust_name" => $order->receiver_name,
                    "payment_channel" => $order->faspay_code,
                    "pay_type" => "1",
                    "msisdn" => $order->receiver_phone,
                    "email" => $user->email,
                    "terminal" => "10",
                    "billing_name" => $order->receiver_name,
                    "billing_lastname" => $order->receiver_name,
                    "billing_address" => $order->address,
                    "billing_address_city" => $order->cities,
                    "billing_address_region" => $order->provinces,
                    "billing_address_state" => "Indonesia",
                    "billing_address_poscode" => $order->postal_code,
                    "billing_address_country_code" => "ID",
                    "receiver_name_for_shipping" => $order->receiver_name,
                    "shipping_address" => $order->address,
                    "shipping_address_city" => $order->cities,
                    "shipping_address_region" => $order->provinces,
                    "shipping_address_state" => "Indonesia",
                    "shipping_address_poscode" => $order->postal_code,
                    "shipping_msisdn" => $order->receiver_phone,
                    "shipping_address_country_code" => "ID",
                    "item" => $items,
                    "reserve1" => "",
                    "reserve2" => "30_days",
                    "signature" => $signature,
                ]
            ];
        } else {
            $req = [
                "json" => [
                    "request" => "Transmisi Info Detil Pembelian",
                    "merchant_id" => $faspay->merchant_id,
                    "merchant" => $faspay->merchant_name,
                    "bill_no" => $order->ordersn,
                    "bill_date" => $order->created_at,
                    "bill_expired" => $bill_expired,
                    "bill_desc" => "Pembayaran Tagihan",
                    "bill_currency" => "IDR",
                    "bill_total" => strval($order->total) . "00",
                    "cust_no" => $user->id,
                    "cust_name" => $order->receiver_name,
                    "payment_channel" => $order->faspay_code,
                    "pay_type" => "1",
                    "msisdn" => $order->receiver_phone,
                    "email" => $user->email,
                    "terminal" => "10",
                    "receiver_name_for_shipping" =>  $order->receiver_name,
                    "shipping_address" => $order->address,
                    "shipping_address_city" => $order->cities,
                    "shipping_address_region" => $order->provinces,
                    "shipping_address_state" => "Indonesia",
                    "shipping_address_poscode" => $order->postal_code,
                    "shipping_msisdn" => $order->receiver_phone,
                    "shipping_address_country_code" => "ID",
                    "item" => $items,
                    "signature" => $signature,
                ]
            ];
        }

        try {
            $result = [];

            $cicilanCode = \DB::table("uni_gen_payment_faspay")->where("type", "Cicilan")->where("status", 1)->pluck("code")->toArray();

            if($order->faspay_code == "000" || in_array($order->faspay_code, $cicilanCode)){
                $result["faspay"]=[];
                $result["faspay"]['redirect_url'] = config("app.app_url") . '/more/transaction/order/cc-payment/'.Crypt::encrypt(config("app.apikey").config("app.separator").$order->ordersn);
                $result["bill_expired"] = $bill_expired;
                return (object)$result;

            }else{
                // dd($req); //Debug Req Faspay
                $client = new GuzzleClient();
                $url = $faspay->link_post . "cvr/300011/10";
                $post = $client->post($url, $req);

                if (in_array($post->getStatusCode(), [200, 201])) {
                    $result["faspay"] = json_decode($post->getBody()->getContents(), false);

                    // dd($result); //Debug Hasil Faspay
                    if ($result["faspay"] && $result["faspay"]->response_code == "00") {

                        if (!in_array($order->faspay_code, ["807", "302", "814", "812", "819", "713", "709"])) {
                            $result["faspay"]->redirect_url = null;
                        }

                        $result["faspay"]->source_qrcode = null;

                        if ($order->faspay_code == "711") {
                            $qrcode = $result["faspay"]->web_url;
                            $imageData = base64_encode(file_get_contents($qrcode));
                            $src_qrcode = 'data:image/png;base64,' . $imageData;
                            $result["faspay"]->source_qrcode = $src_qrcode;
                        }
                        $result["bill_expired"] = $bill_expired;
                        $result["faspay"]->faspay_url = $faspay->link_post;
                        // $result["faspay"]->signature = $signature;
                        return (object)$result;
                    }
                    return null;
                }
            }
            return null;
        } catch (\Exception $e) {
            Log::info("REQUEST FASPAY ERROR:", [$e->getMessage()]);
            return null;
        }
    }

    public static function fetchShipmentTracking($expedition = "", $receipt_number = "")
    {
        $result = [];
        $result['expedition'] = "";
        $result['info'] = "";
        $result['trackings'] = null;
        $result['last_tracking'] = null;

        if ($expedition && $receipt_number) {
            $jsonResi = [];
            $namaEkspedisi = trim($expedition);
            $noResi = trim($receipt_number);

            if (strtoupper($namaEkspedisi) == 'TAUFIQUL') {
                $namaEkspedisi = 'MORE EKSPRESS';
            } else if (strtoupper($namaEkspedisi) == 'INDAH CARGO') {
                try {
                    $username = 'olympicsurabaya';
                    $password = '8e09ebfe226416904251f4cbbdd7862b';
                    $URL = 'https://api.indahonline.com/partner_api.php?action=cek_resi';

                    $data = array(
                        'no_resi' => $noResi
                    );
                    $body = json_encode($data);

                    $ch = curl_init();
                    $headers = array(
                        'Content-Type:application/json',
                        'Authorization: Basic ' . base64_encode("$username:$password")
                    );
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_URL, $URL);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    $response = curl_exec($ch);

                    $json = json_decode($response, true);

                    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                    // Log::info("fetchShipmentTracking:INDAH CARGO",$json);

                    if ($status_code == '200') {
                        $jsonResi = $json['result'];
                        $items = [];
                        foreach (collect($jsonResi)->sortBy("tanggal") as $key => $item) {
                            $item = (object)$item;
                            $map = [];
                            $map['datetime'] = hariTglWaktuIndo($item->tanggal);
                            $map['description'] = $item->nama_status;
                            // $map['status'] = $item->PODStat;
                            // $map['recipient'] = $item->RecipientName;
                            $items[] = $map;
                        }
                        $jsonResi = $items;
                    }
                } catch (\Exception $e) {
                    // Log::error("fetchShipmentTracking:INDAH CARGO",[$e->getMessage()]);
                    return (object)$result;
                }
            } else if (strtoupper($namaEkspedisi) == 'SENTRAL CARGO') {
                try {
                    $jsonResi = json_decode(file_get_contents('https://api.sentralcargo.co.id/api/pod/' . $noResi), true);
                    // Log::info("fetchShipmentTracking:SENTRAL CARGO",$jsonResi);
                    // $jsonLastResi = json_decode(file_get_contents('https://api.sentralcargo.co.id/api/podLastStatus/'.$noResi), true);
                    $items = [];
                    foreach (collect($jsonResi)->sortBy("PODDt") as $key => $item) {
                        $item = (object)$item;
                        $map = [];
                        $map['datetime'] = hariTglWaktuIndo($item->PODDt);
                        $map['description'] = $item->PODDesc;
                        // $map['status'] = $item->PODStat;
                        // $map['recipient'] = $item->RecipientName;
                        $items[] = $map;
                    }
                    $jsonResi = $items;
                } catch (\Exception $e) {
                    // Log::error("fetchShipmentTracking:SENTRAL CARGO",[$e->getMessage()]);
                    return (object)$result;
                }
            }

            $infoResi = "Pesanan sedang dalam proses pengiriman oleh <b>" . $namaEkspedisi . "</b> dengan No Resi <b>" . $noResi . "</b>.";

            $result['expedition'] = $namaEkspedisi;
            $result['info'] = $infoResi;
            $result['trackings'] = $jsonResi;
            $result['last_tracking'] = last($jsonResi);
        }
        return (object)$result;
    }
}
