<?php

namespace App\Models\More\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Uni\ProductVariant;
use App\Models\UniView\ProductVariantStockView;

class OrderProductBundling extends Model
{
    protected $table = 'more_transaction_order_products_bundling';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_order_details_id',
        'more_transaction_order_products_id',
        'uni_product_variants_id',
        'quantity',
    ];

    public function r_uni_product_variant()
    {
        return $this->hasOne(ProductVariantStockView::class, 'id', 'uni_product_variants_id');
    }
}
