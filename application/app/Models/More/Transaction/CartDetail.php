<?php

namespace App\Models\More\Transaction;

use App\Models\More\Promotion\ProductFree;
use App\Models\Uni\ProductVariant;
use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class CartDetail extends Model
{
    protected $table = 'more_transaction_cart_details';
    public $timestamps = false;

    protected $fillable = [
        'uni_product_variants_id', 'quantity', 'sub_total','is_flashsale','is_bundling','is_get_free','is_selected','is_get_combo','id_combo'
    ];

    protected $appends = [
        'sub_total_rp',
    ];

    public function getSubTotalRpAttribute()
    {
        $value = 0;
        if($this->sub_total) $value = $this->sub_total;
        return formatRupiah($value);
    }

    public function r_more_transaction_carts()
    {
        return $this->belongsTo(Cart::class);
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id');
    }

    public function r_more_transaction_cart_bundlings()
    {
        return $this->hasMany(CartBundling::class, 'more_transaction_cart_details_id','id');
    }

    public function r_more_transaction_cart_product_frees()
    {
        return $this->hasOne(CartProductFree::class, 'more_transaction_cart_details_id','id');
    }

    public function r_uni_product_get_combo()
    {
        return $this->belongsTo(ProductFree::class, 'id_combo','id');
    }
}
