<?php

namespace App\Models\More\Transaction;

use Illuminate\Database\Eloquent\Model;

use App\Models\More\User;

class Cart extends Model
{
    protected $table = 'more_transaction_carts';
    public $timestamps = false;

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->more_users_id = auth()->user()->id;
            // $model->created_by = auth()->user()->id;
        });

        // static::updating(function ($model) {
        //     $model->updated_by = auth()->user()->id;
        // });
    }

    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id');
    }

    public function r_more_transaction_cart_details()
    {
        return $this->hasMany(CartDetail::class, 'more_transaction_carts_id')->orderBy("id","asc");
    }

    public function r_more_transaction_cart_bundlings()
    {
        return $this->hasMany(CartBundling::class, 'more_transaction_carts_id');
    }

    public function r_more_transaction_cart_product_frees()
    {
        return $this->hasMany(CartProductFree::class, 'more_transaction_carts_id');
    }
}
