<?php

namespace App\Models\More\Transaction;

use App\Models\MoreView\OrderProductView;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'more_transaction_order_details';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_orders_id',
        'expedition',
        'receipt_number',
    ];

    public function r_more_transaction_order_products()
    {
        return $this->hasMany(OrderProduct::class, 'more_transaction_order_details_id','id')->orderBy("id","asc");
    }

    public function r_more_transaction_order_products_view()
    {
        return $this->hasMany(OrderProductView::class, 'more_transaction_order_details_id','id')->orderBy("id","asc");
    }

    public function r_more_transaction_order_products_bundling()
    {
        return $this->hasOne(OrderProductBundling::class, 'more_transaction_order_details_id','id')->orderBy("id","asc");
    }
}
