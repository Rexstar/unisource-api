<?php

namespace App\Models\More\Transaction;

use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class CartBundling extends Model
{
    protected $table = 'more_transaction_cart_bundlings';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_cart_details_id','uni_product_variants_id', 'quantity'
    ];

    public function r_more_transaction_carts()
    {
        return $this->belongsTo(Cart::class);
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id');
    }

}
