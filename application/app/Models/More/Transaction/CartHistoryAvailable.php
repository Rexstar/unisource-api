<?php

namespace App\Models\More\Transaction;

use Illuminate\Database\Eloquent\Model;

class CartHistoryAvailable extends Model
{
    protected $table = 'more_transaction_cart_history_available';
    // public $timestamps = false;
}
