<?php

namespace App\Models\More\Transaction;

use Illuminate\Database\Eloquent\Model;

class CartHistory extends Model
{
    protected $table = 'more_transaction_cart_histories';
    public $timestamps = false;


}
