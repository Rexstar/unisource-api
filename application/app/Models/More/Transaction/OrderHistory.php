<?php

namespace App\Models\More\Transaction;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table = 'more_transaction_order_histories';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_orders_id',
        'status',
        'note',
        'created_at',
    ];


    protected $appends = [
        'created_at_indonesia',
    ];

    public function getCreatedAtIndonesiaAttribute()
    {
        return hariTglWaktuIndo($this->created_at);
    }
}
