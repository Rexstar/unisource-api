<?php

namespace App\Models\More\Transaction;

use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class CartProductFree extends Model
{
    protected $table = 'more_transaction_cart_product_frees';
    public $timestamps = false;

    protected $fillable = [
        'more_transaction_cart_details_id','more_promotion_product_frees_id','uni_product_variants_id', 'free_description', 'combo_price'
    ];

    public function r_more_transaction_carts()
    {
        return $this->belongsTo(Cart::class);
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id');
    }

}
