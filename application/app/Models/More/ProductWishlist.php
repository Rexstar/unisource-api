<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Uni\Product;

class ProductWishlist extends Model
{
    use SoftDeletes;
    protected $table = 'more_product_wishlists';

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->more_users_id = auth()->user()->id;
            $model->created_by = auth()->user()->id;
        });

        static::updating(function ($model) {
            $model->updated_by = auth()->user()->id;
        });
    }

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class, 'uni_products_id', 'id');
    }

    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id', 'id');
    }
}
