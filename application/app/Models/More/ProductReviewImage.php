<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;


class ProductReviewImage extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_product_review_images';

    protected $appends = [
        "name",
    ];

    public function getNameAttribute()
    {
        $split = explode("/",$this->image_url);
        return last($split);
    }

}
