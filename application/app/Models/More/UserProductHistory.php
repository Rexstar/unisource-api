<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class UserProductHistory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_user_product_histories';

    public $timestamps = false;
}
