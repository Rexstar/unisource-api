<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Uni\Product;

class Chat extends Model
{
    //
    protected $table = 'more_chats';
    protected $dates = ['created_at', 'updated_at'];

    public function getReadAdminAtAttribute($value)
    {
        Carbon::setLocale('id');
        return $value ? Carbon::parse($value)->diffForhumans() : $value;
    }

    public function getCreatedAtAttribute($value)
    {
        // $created = new Carbon($value);
        // $now = Carbon::now();
        // if($created->diff($now)->days < 1){
        //     return waktuIndo($value).', '.'Today';
        // }
        // return waktuIndo($value).', '.tglIndo($value);
        return Carbon::parse($value)->diffForhumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return tglWaktuIndo($value);
    }

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class, 'uni_products_id', 'id');
    }
}
