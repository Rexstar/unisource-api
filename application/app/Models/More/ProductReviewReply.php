<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductReviewReply extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'more_product_review_replies';
}
