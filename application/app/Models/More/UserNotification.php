<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserNotification extends Model
{
    protected $table = 'more_user_notifications';
    public $timestamps = false;

    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id');
    }

    public function getCreatedAtAttribute($value)
    {
        if($value) return hariTglWaktuIndo($value) . ' WIB';
        return null;
    }

    public static function getDescription($order){
        $user = User::find($order->more_users_id);

        $result = [];
        if($order->status == 1){
            $result['subject'] = "Pesanan #".$order->ordersn." belum dibayar";
            $result['history'] = "Pesanan kamu #".$order->ordersn." sebesar ".$order->total_rp." belum dibayar. Segera lakukan pembayaran sebelum hari ".$order->faspay_expired_date_indonesia." WIB. ";
            $result['fcm'] = "Hai <strong>".$user->name.", </strong><br><br>Segera lakukan pembayaran untuk pesananmu <strong>#".$order->ordersn."</strong> paling lambat <strong>".$order->faspay_expired_date_indonesia." WIB</strong>. Jika melebihi waktu tersebut maka pesanan kamu akan otomatis dibatalkan.";
        }else if($order->status == 2){
            $result['subject'] = "Pesanan #".$order->ordersn." berhasil dibayar";
            $result['history'] = "Pembayaran pesanan kamu #".$order->ordersn." telah berhasil. Mohon menunggu produk untuk diproses.";
            $result['fcm'] = "Hai <strong>".$user->name.", </strong><br><br>Pembayaran pesananmu <strong>#".$order->ordersn."</strong> telah berhasil. Pesanan akan diproses paling lambat pada <strong> ".$order->processed_latest_at_indonesia. " WIB</strong>.";
        }else if($order->status == 3){
            $result['subject'] = "Pesanan #".$order->ordersn." sedang diproses";
            $result['history'] = "Proses pesanan kamu #".$order->ordersn." telah berhasil. Mohon menunggu produk untuk dikirim.";
            $result['fcm'] = "Hai <strong>". $user->name .", </strong><br><br>Pesananmu <strong>#".$order->ordersn."</strong> sedang di proses. Mohon menunggu produk selesai diproses dan siap untuk dikirimkan. Pesanan akan dikirim paling lambat <strong>".$order->shipped_latest_at_indonesia." WIB</strong>.";
        }else if($order->status == 4){
            $result['subject'] = "Pesanan #".$order->ordersn." telah dikirim";
            $result['history'] = "Pesanan kamu #". $order->ordersn ." telah dikirim. Mohon menunggu produk untuk diterima.";
            $result['fcm'] = "Hai <strong>". $user->name .", </strong><br><br>Pesanan kamu <strong>#".$order->ordersn."</strong> telah dikirim pada <strong>".$order->shipped_at_indonesia." WIB</strong>. Jika sudah menerima pesananmu, mohon periksa semua produk dan lakukan konfirmasi terima pesanan pada MORE untuk menyelesaikan transaksi.";
        }else if($order->status == 5){
            $result['subject'] = "Pesanan #".$order->ordersn." mangajukan komplain";
            $result['history'] = "Pesanan kamu #". $order->ordersn ." telah mangajukan komplain. Mohon menunggu, kami akan segera menindak lanjuti komplain Anda.";
            $result['fcm'] = "Hai <strong>". $user->name .", </strong><br><br>Pesanan kamu <strong>#".$order->ordersn."</strong> telah mangajukan komplain pada <strong>".$order->complained_at_indonesia." WIB</strong>. Mohon menunggu, kami akan segera menindak lanjuti komplain Anda.";
        }else if($order->status == 6){
            $result['subject'] = "Pesanan #". $order->ordersn ." telah selesai";
            $result['history'] = "Pesanan kamu #". $order->ordersn ." telah selesai. Terima kasih sudah berbelanja di MORE.";
            $result['fcm'] = "Hai <strong>". $user->name .", </strong><br><br>Pesananmu <strong>#".$order->ordersn."</strong> telah selesai pada <strong>".$order->completed_at_indonesia." WIB</strong>. Terima kasih sudah berbelanja di MORE.";
        }else if($order->status == 91){
            $result['subject'] = "Pesanan #". $order->ordersn ." telah dibatalkan";
            $result['history'] = "Pesanan kamu #". $order->ordersn ." telah dibatalkan. karena ".$order->note_cancel.".";
            $result['fcm'] = "Pesanan kamu #". $order->ordersn ." telah dibatalkan. karena ".$order->note_cancel.".";

        }else if($order->status == 92){
            $result['subject'] = "Pesanan #". $order->ordersn. " telah dibatalkan otomatis";
            $result['history'] = "Pesanan kamu #". $order->ordersn. " telah dibatalkan secara otomatis. pada ".$order->autocanceled_at_indonesia.", karena telah melewati batas waktu pembayaran. Silahkan belanja lagi untuk tetap mendapatkan produk yang kamu inginkan.";
            $result['fcm'] = "Hai <strong>". $user->name .", </strong><br><br>Pesananmu <strong>#".$order->ordersn."</strong> telah dibatalkan secara otomatis pada <strong>".$order->autocanceled_at_indonesia ." WIB</strong> karena telah melewati batas waktu pembayaran. Silakan belanja lagi untuk tetap mendapatkan produk yang kamu inginkan.";
        }
        return (object)$result;
    }


}
