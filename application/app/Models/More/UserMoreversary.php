<?php

namespace App\Models\More;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMoreversary extends Model
{

    protected $table = 'more_moreversary';
    protected $primaryKey = 'id';

}
