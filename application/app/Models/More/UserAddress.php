<?php

namespace App\Models\More;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    use SoftDeletes;

    protected $table = 'more_user_addresses';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'address',
        'province',
        'city',
        'subdistrict',
        'postal_code',
        'phone',
        'default',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id');
    }
}
