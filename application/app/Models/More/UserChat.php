<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class UserChat extends Model
{
    protected $table = 'more_user_chats';

    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id');
    }
    
}
