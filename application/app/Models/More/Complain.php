<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $table = 'more_complains';
    public $timestamps = false;

    public function r_more_complain_products()
    {
        return $this->hasMany(ComplainProduct::class, 'more_complains_id');
    }
}
