<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class ComplainChat extends Model
{
    protected $table = 'more_complain_chats';
    public $timestamps = false;

    protected $dates = ['created_at', 'updated_at'];

    public function getReadAdminAtAttribute($value)
    {
        Carbon::setLocale('id');
        return $value ? Carbon::parse($value)->diffForhumans() : $value;
    }

    public function getCreatedAtAttribute($value)
    {
        // $created = new Carbon($value);
        // $now = Carbon::now();
        // if($created->diff($now)->days < 1){
        //     return waktuIndo($value).', '.'Today';
        // }
        // return waktuIndo($value).', '.tglIndo($value);
        return Carbon::parse($value)->diffForhumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return tglWaktuIndo($value);
    }

    public function r_more_complains()
    {
        return $this->hasOne(Complain::class, 'more_complains_id', 'id');
    }
}
