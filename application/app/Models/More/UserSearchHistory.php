<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class UserSearchHistory extends Model
{
    protected $table = 'more_user_search_histories';
    public $timestamps = false;

    protected $fillable = [
        'more_users_id',
        'type',
        'value_1',
        'value_2',
    ];
}
