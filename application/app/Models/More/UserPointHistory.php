<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserPointHistory extends Model
{
    protected $table = 'more_user_point_histories';
    public $timestamps = false;


    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id');
    }

}
