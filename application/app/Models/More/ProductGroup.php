<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_product_groups';

    public $timestamps = false;

    protected $appends = [
        'image_url',
        'image_desktop_url',
        'image_mobile_url',
    ];

    public function getImageUrlAttribute()
    {
        $imagePath = "SIDE-BANNER/";
        $filename = $this->image;

        if(!$filename) return config('app.cdn') . "images/noimage.png";

        return config('app.cdn_asset') . $imagePath . $this->image;
    }

    public function getImageDesktopUrlAttribute()
    {
        $imagePath = "SIDE-BANNER/";
        $filename = $this->image_desktop;

        if(!$filename) return config('app.cdn') . "images/noimage.png";

        return config('app.cdn_asset') . $imagePath . $this->image_desktop;

    }

    public function getImageMobileUrlAttribute()
    {
        $imagePath = "SIDE-BANNER/";
        $filename = $this->image_mobile;

        if(!$filename) return config('app.cdn') . "images/noimage.png";

        return config('app.cdn_asset') . $imagePath . $this->image_mobile;
    }

    public function r_more_product_group_products()
    {
        return $this->hasMany(ProductGroupProduct::class, 'more_product_groups_id')->orderBy("id","asc");
    }
}
