<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class ComplainProductImage extends Model
{
    protected $table = 'more_complain_product_images';
    public $timestamps = false;

    protected $appends = [
        "name",
    ];

    public function getNameAttribute()
    {
        $split = explode("/",$this->image_url);
        return last($split);
    }
}
