<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'more_events';
}
