<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

use App\Models\More\User;
use App\Models\MoreView\ProductActiveView;
use App\Models\Uni\Product;
use App\Models\Uni\ProductVariant;
use App\Models\UniView\ProductVariantStockView;
use DateTimeInterface;


class ProductReview extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_product_reviews';

    protected $appends = [
        'created_at_indonesia',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getCreatedAtIndonesiaAttribute()
    {
        return tglWaktuIndo($this->created_at);
    }

    public function r_more_users()
    {
        return $this->belongsTo(User::class, 'more_users_id');
    }

    public function r_uni_products()
    {
        return $this->belongsTo(ProductActiveView::class, 'uni_products_id');
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id');
    }

    public function r_more_product_review_images()
    {
        return $this->hasMany(ProductReviewImage::class, 'more_product_reviews_id');
    }

    public function r_more_product_review_replies()
    {
        return $this->hasMany(ProductReviewReply::class, 'more_product_reviews_id');
    }
}
