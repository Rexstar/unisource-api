<?php

namespace App\Models\More\Promotion;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_promotion_vouchers';

    protected $appends = [
        'expired','min_purchase_rp','voucher_rp','max_discount_price_rp'
    ];

    public function getMinPurchaseRpAttribute()
    {
        return formatRupiah($this->min_purchase);
    }

    public function getMaxDiscountPriceRpAttribute()
    {
        return formatRupiah($this->max_discount_price);
    }

    public function getVoucherRpAttribute()
    {
        return formatRupiah($this->voucher);
    }


    public function getExpiredAttribute($value)
    {
        $value = \Carbon\Carbon::parse($this->duration_end)->format('d M Y');
        return $value;
    }

    public function getImageAttribute($value)
    {

        if($value) {
            $value = config('app.cdn_asset') .'VOUCHER/'. $value;
        }
        return $value;

    }
}
