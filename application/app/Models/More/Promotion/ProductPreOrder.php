<?php

namespace App\Models\More\Promotion;

use App\Models\Uni\Product;
use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class ProductPreOrder extends Model
{
    protected $table = 'more_promotion_pre_orders';

    public $timestamps = false;


    public function r_uni_products()
    {
        return $this->hasOne(Product::class, 'id', 'uni_products_id');
    }


    public function r_uni_product_variants()
    {
        return $this->hasOne(ProductVariantStockView::class, 'id', 'uni_product_variants_id');
    }


}
