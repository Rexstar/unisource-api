<?php

namespace App\Models\More\Promotion;

use Illuminate\Database\Eloquent\Model;

class Endorsement extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_endorsements';

    public $timestamps = false;

}
