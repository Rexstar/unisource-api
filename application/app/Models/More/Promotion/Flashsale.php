<?php

namespace App\Models\More\Promotion;

use Illuminate\Database\Eloquent\Model;

class Flashsale extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_promotion_flashsales';

    public $timestamps = false;

    protected $appends = [
        'start','end','discount_max'
    ];

    public function getStartAttribute($value)
    {
        $value = strtotime($this->date_start);
        return $value;
    }

    public function getEndAttribute($value)
    {
        $value = strtotime($this->date_end);
        return $value;
    }

    public function getDiscountMaxAttribute()
    {
        $discountMax = '0%';
        if($this->getRelation('r_more_promotion_flashsale_products')) {
            $products = $this->getRelation('r_more_promotion_flashsale_products');
            if(collect($products)->count() > 0) return $products->max('discount');
        }
        return $discountMax;
    }

    public function r_more_promotion_flashsale_products()
    {
        return $this->hasMany(FlashsaleProduct::class, 'more_promotion_flashsales_id');
    }
}
