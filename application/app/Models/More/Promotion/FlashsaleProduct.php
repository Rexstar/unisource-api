<?php

namespace App\Models\More\Promotion;

use Illuminate\Database\Eloquent\Model;

use App\Models\Uni\Product;
use App\Models\Uni\ProductVariant;
use App\Models\Uni\ProductImage;
use App\Models\Uni\ProductVariantPrice;
use App\Models\UniView\ProductVariantStockView;

class FlashsaleProduct extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'more_promotion_flashsale_products';

    public $timestamps = false;

    protected $appends = [
        'price_rp'
    ];

    public function getPriceRpAttribute()
    {
        $value = 0;
        if($this->price) $value = $this->price;
        return formatRupiah($value);
    }

    public function getDiscountAttribute($value)
    {
        if(!$value) $value = 0;
        return $value . '%';
    }

    public function r_more_promotion_flashsales()
    {
        return $this->belongsTo(Flashsale::class, 'more_promotions_flashsales_id');
    }

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class, 'uni_products_id');
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_images()
    {
        return $this->hasMany(ProductImage::class, 'uni_products_variants_id', 'uni_product_variants_id')->where(['status'=> 1,'type'=>'PRODUK'])->orderBy('no', 'ASC');
    }

    public function r_uni_product_variant_prices()
    {
        return $this->belongsTo(ProductVariantPrice::class, 'uni_product_variants_id', 'uni_product_variants_id');
    }
}
