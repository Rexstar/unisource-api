<?php

namespace App\Models\More\Promotion;

use App\Models\Uni\Product;
use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class ProductFree extends Model
{
    protected $table = 'more_promotion_product_frees';

    public $timestamps = false;

    protected $appends = [
        'combo_price_rp',
        'combo_price_hemat',
        'combo_price_hemat_rp',
        'combo_price_total',
        'combo_price_total_rp',
    ];

    public function getComboPriceRpAttribute()
    {
        return toRp($this->combo_price);
    }

    public function getComboPriceHematAttribute()
    {
        if($this->free_type == 'COMBO') {
            if($this->getRelation('r_uni_product_variants')) {
                $variant = $this->getRelation('r_uni_product_variants');
                if($variant->valid_price >= $this->combo_price) return ($variant->valid_price - $this->combo_price);
            }
        }
        return 0;
    }

    public function getComboPriceHematRpAttribute()
    {
        return toRp($this->combo_price_hemat);
    }

    public function getComboPriceTotalAttribute()
    {
        if($this->free_type == 'COMBO') {
            if($this->getRelation('r_uni_product_variants_main')) {
                $variant = $this->getRelation('r_uni_product_variants_main');
                return ($variant->valid_price + $this->combo_price);
            }
        }
        return 0;
    }

    public function getComboPriceTotalRpAttribute()
    {
        return toRp($this->combo_price_total);
    }

    public function r_uni_products_main()
    {
        return $this->hasOne(Product::class, 'id', 'uni_products_id');
    }

    public function r_uni_products()
    {
        return $this->hasOne(Product::class, 'id', 'free_uni_products_id');
    }

    public function r_uni_product_variants_main()
    {
        return $this->hasOne(ProductVariantStockView::class, 'id', 'uni_product_variants_id');
    }

    public function r_uni_product_variants()
    {
        return $this->hasOne(ProductVariantStockView::class, 'id', 'free_uni_product_variants_id');
    }

}
