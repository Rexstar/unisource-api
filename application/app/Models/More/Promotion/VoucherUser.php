<?php

namespace App\Models\More\Promotion;

use Illuminate\Database\Eloquent\Model;

class VoucherUser extends Model
{
    protected $table = 'more_promotion_voucher_users';
}
