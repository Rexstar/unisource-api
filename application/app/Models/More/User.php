<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\More\GenPointSale;
use Illuminate\Support\Facades\Log;
use DB;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    protected $table = 'more_users';

    use Authenticatable, Authorizable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'picture',
        'gender',
        'phone',
        'birthday',
        'account_id',
        'account_fb',
        'account_tiktok',
        'status_verfication',
        'status_complete',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'status_verification',
        'status_complete',
        'emailed_at',
        'uuid_fb',
        'uuid_google',
        'is_active',
        'is_subscribe',
        'last_payment_code',
        'last_address_id',
        'last_login',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getDefaultaddresses()
    {
        return $this->hasMany(UserAddress::class, 'more_users_id', 'id')->where('default', 1);
    }

    public function r_more_user_addresses()
    {
        return $this->hasMany(UserAddress::class, 'more_users_id', 'id')->orderBy('default','desc')->orderBy('id', 'asc');
    }

    public function r_more_user_token()
    {
        return $this->hasMany(UserToken::class, 'more_users_id', 'id')->orderBy('id', 'desc');
    }

    public static function syncingPointsByOrder($order){
        DB::beginTransaction();
        try {
            $pointsEarned = GenPointSale::whereRaw($order->total." >= start_range AND ".$order->total." <= end_range")->orderBy("point","desc")->first();
            if($pointsEarned){
                $history = new UserPointHistory();
                $history->more_users_id = $order->more_users_id;
                $history->point = $pointsEarned->point;
                $history->note = "Kamu mendapat ".$pointsEarned->point." point dari pesanan kamu #".$order->ordersn;
                $history->created_at = date("Y-m-d H:i:s");
                if($history->save()){
                    $user = User::find($order->more_users_id);
                    $user->points = UserPointHistory::where("more_users_id",$order->more_users_id)->sum("point");
                    $user->save();
                }
            }else{
                // Log::info("syncingPointsByOrder",["Tidak mendapatkan data point."]);
            }
            DB::commit();
        } catch (\Exception $e) {
            // Log::error("syncingPointsByOrder-Catch",[$e->getMessage()]);
            DB::rollback();
        }
    }

}
