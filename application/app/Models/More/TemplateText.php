<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TemplateText extends Model
{
    protected $table = 'more_template_texts';
    public $timestamps = false;


}
