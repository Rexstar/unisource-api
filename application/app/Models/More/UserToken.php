<?php

namespace App\Models\More;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserToken extends Model
{
    use SoftDeletes;

    protected $table = 'more_user_tokens';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'id',
        'more_users_id',
        'device_id',
        'device_name',
        'device_os',
        'app_version',
        'access_token',
        'firebase_token',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by'
    ];

    public function r_more_users(){
        return $this->belongsTo(User::class, 'id', 'more_users_id');
    }
}
