<?php

namespace App\Models\MoreView;

use Illuminate\Database\Eloquent\Model;

class OrderProductView extends Model
{
    protected $table = 'more_transaction_order_products_view';
    protected $primaryKey = 'id';

    protected $appends = [
        'price_rp',
        'price_normal_rp',
        'sub_total_rp'
    ];

    public function getPriceRpAttribute()
    {
        $value = 0;
        if($this->price) $value = $this->price;
        return formatRupiah($value);
    }

    public function getPriceNormalRpAttribute()
    {
        $value = 0;
        if($this->price_normal) $value = $this->price_normal;
        return formatRupiah($value);
    }

    public function getSubTotalRpAttribute()
    {
        $value = 0;
        if($this->sub_total) $value = $this->sub_total;
        return formatRupiah($value);
    }
    
    public function r_more_complain_products()
    {
        return $this->hasOne(ComplainProduct::class,"more_transaction_order_products_id","id");
    }
}
