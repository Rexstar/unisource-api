<?php

namespace App\Models\MoreView;

use Illuminate\Database\Eloquent\Model;

class ProductActiveView extends Model
{
    protected $table = 'more_list_product_active_view';
    protected $primaryKey = 'id';


}
