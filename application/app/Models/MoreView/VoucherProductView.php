<?php

namespace App\Models\MoreView;

use Illuminate\Database\Eloquent\Model;

class VoucherProductView extends Model
{
    protected $table = 'more_promotion_voucher_products_view';

    protected $appends = [
        'expired'
    ];

    public function getExpiredAttribute($value)
    {
        $value = \Carbon\Carbon::parse($this->duration_end)->format('d M Y');
        return $value;
    }

    public function getImageAttribute($value)
    {

        if($value) {
            $value = config('app.cdn_asset') .'VOUCHER/'. $value;
        }
        return $value;

    }
}
