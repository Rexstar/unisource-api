<?php

namespace App\Models\MoreView;

use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class FlashsaleProductView extends Model
{
    protected $table = 'more_promotion_flashsale_products_view';
    protected $primaryKey = 'id';

    protected $appends = [
        'price_rp'
    ];

    public function getPriceRpAttribute()
    {
        $value = 0;
        if($this->price) $value = $this->price;
        return formatRupiah($value);
    }

    public function getDiscountAttribute($value)
    {
        if(!$value) $value = 0;
        return $value . '%';
    }

    public function r_more_promotion_flashsales()
    {
        return $this->belongsTo(Flashsale::class, 'more_promotions_flashsales_id');
    }

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class, 'uni_products_id');
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_images()
    {
        return $this->hasMany(ProductImage::class, 'uni_products_id', 'uni_products_id');
    }

    public function r_uni_product_variant_prices()
    {
        return $this->belongsTo(ProductVariantPrice::class, 'uni_product_variants_id', 'uni_product_variants_id');
    }
}
