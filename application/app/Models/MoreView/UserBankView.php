<?php

namespace App\Models\MoreView;

use Illuminate\Database\Eloquent\Model;

class UserBankView extends Model
{
    protected $table = 'more_user_banks_view';
    
    protected $appends = [
        'icon_url',
    ];

    public function getIconUrlAttribute()
    {
        $imagePath = "MORE/payment/";
        $filename = $this->icon;

        if(!$filename) return config('app.cdn') . "images/noimage.png";

        return config('app.cdn') . $imagePath . $this->icon;
    }
}
