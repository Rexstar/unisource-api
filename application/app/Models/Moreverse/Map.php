<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_maps';
}
