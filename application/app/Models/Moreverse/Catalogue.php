<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

class Catalogue extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_catalogue';

    protected $appends = [
        'image_url',
    ];

    public function getImageUrlAttribute()
    {
        $path = 'MORE/moreverse/catalog/';

        $imagePath = null;
        $filename = $this->image;
        if($this->image != null){
            $imagePath = config('app.cdn') . $path . $filename;
        }else{
            $imagePath = config('app.cdn') . 'images/noimage.png';
        }


        return $imagePath;

    }

}
