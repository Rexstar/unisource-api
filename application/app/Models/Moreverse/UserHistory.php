<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;


class UserHistory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_user_histories';
    public $timestamps = false;

}
