<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_videos';

}
