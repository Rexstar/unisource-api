<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

use App\Models\Uni\ProductVariant as UniProductVariant;

class ProductCoohom extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_product_coohoms';

    public function r_uni_product_variants()
    {
        return $this->belongsTo(UniProductVariant::class, 'sku_group', 'sku_group');
    }
}
