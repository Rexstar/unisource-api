<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;


class UserNotification extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_user_notifications';
    public $timestamps = false;


    protected $appends = [
        'date'
    ];

    public function getDateAttribute()
    {
        return tglWaktuIndo($this->created_at);

    }

}
