<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_contents';
}
