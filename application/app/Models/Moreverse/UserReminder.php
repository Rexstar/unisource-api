<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;


class UserReminder extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_user_reminders';
    public $timestamps = false;

}
