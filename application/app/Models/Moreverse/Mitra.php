<?php

namespace App\Models\Moreverse;
use App\Models\Uni\GenCompany;
use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_mitras';

    protected $appends = [
        'image_url','address_mitra'
    ];

    public function r_uni_company()
    {
        return $this->hasOne(GenCompany::class, 'id','uni_company_id');
    }

    public function getAddressMitraAttribute()
    {
        return $this->address. ','.$this->city;
    }

    public function getImageUrlAttribute()
    {
        $path = 'MITRA/JLB/';

        $imagePath = null;
        $filename = $this->image;
        if($this->image != null){
            $imagePath = config('app.cdn') . $path . $filename;
        }else{
            $imagePath = config('app.cdn') . 'images/noimage.png';
        }


        return $imagePath;

    }
}
