<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

use App\Models\Uni\Product as UniProduct;

class Product extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_products';

}
