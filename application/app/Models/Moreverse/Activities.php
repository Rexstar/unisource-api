<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_activities';

    protected $appends = [
        'image_url','date'
    ];

    public function getImageUrlAttribute()
    {
        $path = 'MORE/moreverse/activities/';

        $imagePath = null;
        $filename = $this->image;
        if($this->image != null){
            $imagePath = config('app.cdn') . $path . $filename;
        }else{
            $imagePath = null;
        }


        return $imagePath;

    }

    public function getDateAttribute()
    {
        return tglIndo($this->start_date);

    }
}
