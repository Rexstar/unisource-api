<?php

namespace App\Models\Moreverse;

use Illuminate\Database\Eloquent\Model;

class Exhibition extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'moreverse_exhibitions';

    protected $fillable = [
        'room_type',
        'link',
    ];
}
