<?php

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class TransferWarehouse extends Model
{
    protected $table = 'inv_tra_transfer_warehouse';
    protected $connection= 'postgres_erp';

    public function r_detail()
    {
        return $this->hasMany(TransferWarehouseDetail::class, 'inv_tra_transfer_warehouse_id', 'id');
    }

}
