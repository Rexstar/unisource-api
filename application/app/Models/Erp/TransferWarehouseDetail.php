<?php

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class TransferWarehouseDetail extends Model
{
    protected $table = 'inv_tra_transfer_warehouse_detail';
    protected $connection= 'postgres_erp';



    public function r_item()
    {
        return $this->hasOne(MarMasItem::class, 'id','item_id');
    }
}
