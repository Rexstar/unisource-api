<?php

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class ExpeditionPricelist extends Model
{
    protected $table = 'erp_mar_mas_expedition_pricelist';
}
