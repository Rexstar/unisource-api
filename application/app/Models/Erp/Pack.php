<?php

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $table = 'erp_mar_mas_item_d_pack';
}
