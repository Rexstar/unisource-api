<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductVideo extends Model
{
    protected $table = 'uni_product_videos';
    public $timestamps = false;

    protected $appends = [
        'youtube_url'
    ];

    public function getYoutubeUrlAttribute()
    {
        if($this->link) {
            return 'https://www.youtube.com/embed/'.$this->link.'?rel=0';
        }
        return null;
    }
}
