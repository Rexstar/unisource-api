<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductStockHistory extends Model
{
    protected $table = 'uni_product_stock_histories';
    public $timestamps = false;
}
