<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenCompany extends Model
{
    protected $table = 'uni_company';
    public $timestamps = false;
}
