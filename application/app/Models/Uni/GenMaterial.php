<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenMaterial extends Model
{
    protected $table = 'uni_gen_materials';
    public $timestamps = false;
}
