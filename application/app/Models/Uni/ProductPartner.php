<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductPartner extends Model
{
    protected $table = 'uni_product_partners';
    public $timestamps = false;

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class);
    }
}
