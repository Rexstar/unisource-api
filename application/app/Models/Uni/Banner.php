<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'uni_banners';
    public $timestamps = false;
}
