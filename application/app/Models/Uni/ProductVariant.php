<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;
use App\Models\More\Promotion\ProductFree;
use Carbon\Carbon;

class ProductVariant extends Model
{
    protected $table = 'uni_product_variants';
    public $timestamps = false;

    protected $appends = [
        "uni_product_variants_id",
    ];

    public function getUniProductVariantsIdAttribute()
    {
        return $this->id;
    }

    public function r_uni_product_variant_prices()
    {
        return $this->hasOne(ProductVariantPrice::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_color()
    {
        return $this->hasOne(ProductVariantColor::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_materials()
    {
        return $this->hasMany(ProductVariantMaterial::class, 'uni_product_variants_id');
    }

    public function r_uni_product_variant_bundles()
    {
        return $this->hasMany(ProductVariantBundling::class, 'uni_product_variants_id', 'id');
    }

    public function r_uni_product_image()
    {
        return $this->hasOne(ProductImage::class, 'uni_products_variants_id', 'id')->where(['status'=> 1,'type'=>'PRODUK'])->orderBy("id","asc");
    }

    public function r_uni_product()
    {
        return $this->hasOne(PlainProduct::class, 'id', 'uni_products_id');
    }

    public function r_uni_product_variant_prices_asc()
    {
        return $this->hasOne(ProductVariantPrice::class, 'uni_product_variants_id')->orderBy('ho_normal', 'ASC');
    }

    public function r_uni_product_get_free_retail()
    {
        $now = Carbon::now()->format('Y-m-d');

        return $this->hasOne(ProductFree::class, 'uni_product_variants_id','id')->where(function ($q) use ($now) {
            $q->where('date_start','<=', $now);
            $q->where('date_end','>=', $now);
        })
        ->where('free_type', 'FREE')
        ->where(function ($query) {
            $query->whereNull('uni_gen_applications_id')
                  ->orWhere('uni_gen_applications_id', '=', 14);
        })
        ->orderBy("date_start","DESC");
    }

}
