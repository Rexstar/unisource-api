<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenLanguage extends Model
{
    protected $table = 'uni_gen_languages';
    public $timestamps = false;
}
