<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class NewsProduct extends Model
{
    protected $table = 'uni_news_products';
    public $timestamps = false;

    public function r_uni_news()
    {
        return $this->belongsTo(News::class, 'id', 'uni_news_id');
    }
}
