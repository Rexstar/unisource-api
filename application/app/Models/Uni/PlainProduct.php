<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;
use App\Models\UniView\ProductVariantStockView;

class PlainProduct extends Model
{
    protected $table = 'uni_products';
    public $timestamps = false;

    protected $appends = [
        'brand_url',
    ];


    public function getBrandUrlAttribute()
    {
        if($this->brand) {
            return config('app.cdn') .'MORE/brand/'.$this->brand.'.png';
        }
        return null;
    }

    public function r_uni_product_variants()
    {
        return $this->hasMany(ProductVariantStockView::class, 'uni_products_id', 'id')->where(['more_active'=> 1])/*->orderBy('id', 'ASC')*/; // hanya untuk MORE
    }
}
