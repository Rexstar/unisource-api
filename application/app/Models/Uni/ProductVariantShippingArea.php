<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductVariantShippingArea extends Model
{
    protected $table = 'uni_product_variant_shippingarea';
    public $timestamps = false;

}
