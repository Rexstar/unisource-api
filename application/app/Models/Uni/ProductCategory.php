<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'uni_product_categories';
    public $timestamps = false;

    public function r_uni_categories()
    {
        return $this->belongsTo(Category::class, 'uni_categories_id');
    }
}
