<?php

namespace App\Models\Uni;

use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class ProductVariantBundling extends Model
{
    protected $table = 'uni_product_variant_bundling';
    public $timestamps = false;
    protected $appends = [
        'r_uni_product_images',
        'product_name',
        'product_slug',
        'price_normal',
        'price_range',
        'discount_min',
        'discount_max',
        'brand_url',
    ];


    public function getPriceRangeAttribute()
    {
        $priceRange = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $min = $variants->min('r_uni_product_variant_prices.more_discount_price');
            $max = $variants->max('r_uni_product_variant_prices.more_discount_price');
            $priceRange = formatRupiah(floatval($min));

            if($variants->count() > 1 && ($min < $max)) {
                $priceRange .= ' - ' . formatRupiah($max);
            }
        }
        return $priceRange;
    }

    public function getPriceNormalAttribute()
    {
        $priceNormal = '';

        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $max = $variants->max('r_uni_product_variant_prices.ho_more');
            if($max) $priceNormal = formatRupiah(floatval($max));
        }
        return $priceNormal;
    }

    public function getDiscountMaxAttribute()
    {
        $discountMax = '0%';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if(collect($variants)->count() > 0)
                return $variants->max('r_uni_product_variant_prices.more_discount');
        }
        return $discountMax;
    }

    public function getDiscountMinAttribute()
    {
        $discountMin = '0%';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if(collect($variants)->count() > 0)
                return $variants->min('r_uni_product_variant_prices.more_discount');
        }
        return $discountMin;
    }

    public function getRUniProductImagesAttribute()
    {
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if($variants->count() > 0){
                return ProductImage::where(['uni_products_id'=>$variants[0]->uni_products_id,'status'=> 1,'type'=>'PRODUK'])
                ->whereIn("uni_products_variants_id",$variants->pluck("id"))
                ->orderBy('id', 'ASC')->get();
            }
        }
        return [];
    }

    public function getBrandUrlAttribute()
    {
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if($variants->count() > 0){
                return Product::find($variants[0]->uni_products_id)->brand_url;
            }
        }
        return null;
    }

    public function getProductNameAttribute()
    {
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if($variants->count() > 0){
                return Product::find($variants[0]->uni_products_id)->name;
            }
        }
        return null;
    }

    public function getProductSlugAttribute()
    {
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if($variants->count() > 0){
                return Product::find($variants[0]->uni_products_id)->slug;
            }
        }
        return null;
    }

    public function r_uni_product()
    {
        return $this->hasOne(Product::class, 'id', 'uni_product_id');
    }


    public function r_uni_product_variants()
    {
        return $this->hasMany(ProductVariantStockView::class, 'sku_group', 'sku_group')->orderBy('id', 'ASC');
    }

    // public function r_uni_product_images()
    // {
    //     dd($this->getRelation('r_uni_product_variants'));
    //     $product_id = $this->getRelation('r_uni_product_variants')[0]->uni_products_id;
    //     return $this->hasMany(ProductImage::class, 'uni_products_id', $product_id)->where(['status'=> 1,'type'=>'PRODUK'])->orderBy('id', 'ASC');
    // }
}
