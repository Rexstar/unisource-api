<?php

namespace App\Models\Uni;

use App\Models\UniView\ProductVariantStockView;
use Illuminate\Database\Eloquent\Model;

class ProductVariantPrice extends Model
{
    protected $table = 'uni_product_variant_prices';
    protected $primaryKey = 'uni_product_variants_id';
    public $timestamps = false;

    protected $appends = [
        'ho_normal_rp',
        'ho_more_rp',
        'more_discount_price_rp'
    ];

    public function getMoreDiscountAttribute($value)
    {
        if(!$value) $value = 0;
        return $value . '%';
    }

    public function getHoNormalRpAttribute()
    {
        $value = 0;
        if($this->ho_normal) $value = $this->ho_normal;
        return formatRupiah($value);
    }

    public function getHoMoreRpAttribute()
    {
        $value = 0;
        if($this->ho_more) $value = $this->ho_more;
        return formatRupiah($value);
    }

    public function getMoreDiscountPriceRpAttribute()
    {
        $value = 0;
        if($this->more_discount_price) $value = $this->more_discount_price;
        return formatRupiah($value);
    }

    public function r_uni_product_variants()
    {
        return $this->belongsTo(ProductVariantStockView::class, 'uni_product_variants_id', 'id');
    }
}
