<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenBrand extends Model
{
    protected $table = 'uni_gen_brands';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $appends = [
        'product_count'
    ];

    public function getProductCountAttribute()
    {
        // if(collect($this->getRelations())->contains('r_uni_products')) {
        //     return $this->getRelation('r_uni_products')->count();
        // }
        // return 0;
        return Product::where('brand', $this->id)->count();
    }

    public function r_uni_products()
    {
        return $this->hasMany(Product::class, 'brand');
    }
}
