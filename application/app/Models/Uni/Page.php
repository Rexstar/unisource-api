<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'uni_pages';
    public $timestamps = false;
}
