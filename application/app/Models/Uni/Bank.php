<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'uni_gen_banks';
    public $timestamps = false;

    protected $appends = [
        'icon_url',
    ];

    public function getIconUrlAttribute()
    {
        $imagePath = "MORE/payment/";
        $filename = $this->icon;

        if(!$filename) return config('app.cdn') . "images/noimage.png";

        return config('app.cdn') . $imagePath . $this->icon;
    }
}
