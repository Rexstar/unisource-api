<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $table = 'uni_product_stocks';
    public $timestamps = false;
}
