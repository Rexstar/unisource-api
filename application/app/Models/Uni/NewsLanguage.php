<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class NewsLanguage extends Model
{
    protected $table = 'uni_news_languages';
    public $timestamps = false;
}
