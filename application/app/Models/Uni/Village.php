<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'uni_gen_villages';
    protected $primaryKey = 'village_id';
    protected $keyType = 'string';
    public $timestamps = false;

    public function r_district()
    {
        return $this->hasOne(District::class, 'district_id', 'district_id');
    }
}
