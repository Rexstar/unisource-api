<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductSearchRecomendation extends Model
{
    protected $table = 'uni_product_search_recomendations';
    public $timestamps = false;
}
