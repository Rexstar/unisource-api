<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

use App\Models\More\ProductReview;
use App\Models\UniView\ProductVariantStockView;
use App\Models\UniView\ProductDescriptionBundlingView;
use App\Models\UniView\ProductImageBundlingView;
use App\Models\MoreView\ProductFreeView as ProductFree;
use Carbon\Carbon;

class Product extends Model
{
    protected $table = 'uni_products';
    public $timestamps = false;

    protected $appends = [
        'price_minimum',
        'price_normal',
        'price_normal_min',
        'price_discount_min',
        'price_range',
        'price_range_raft',
        'discount_min',
        'discount_max',
        'brand_url',
        'is_multiple_variant',
        'r_multiple_variant'
    ];

    public function getPriceMinimumAttribute()
    {
        $price = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $price = $variants->min('r_uni_product_variant_prices.more_discount_price');
        }
        return $price;
    }

    public function getPriceRangeAttribute()
    {
        $priceRange = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $min = $variants->min('r_uni_product_variant_prices.more_discount_price');
            $max = $variants->max('r_uni_product_variant_prices.more_discount_price');
            $priceRange = formatRupiah(floatval($min));

            if($variants->count() > 1 && ($min < $max)) {
                $priceRange .= ' - ' . formatRupiah($max);
            }
        }
        return $priceRange;
    }

    public function getPriceRangeRaftAttribute()
    {
        $priceRange = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $min = $variants->min('r_uni_product_variant_prices.assembly_costs');
            $max = $variants->max('r_uni_product_variant_prices.assembly_costs');
            $priceRange = formatRupiah(floatval($min));

            if($variants->count() > 1 && ($min < $max)) {
                $priceRange .= ' - ' . formatRupiah($max);
            }
        }
        return $priceRange;
    }

    public function getPriceNormalAttribute()
    {
        $priceNormal = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $max = $variants->max('r_uni_product_variant_prices.ho_normal');

            if($max) $priceNormal = formatRupiah(floatval($max));
        }
        return $priceNormal;
    }

    public function getPriceNormalMinAttribute()
    {
        $priceNormal = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $min = $variants->min('r_uni_product_variant_prices.ho_normal');

            if($min) $priceNormal = formatRupiah(floatval($min));
        }
        return $priceNormal;
    }

    public function getPriceDiscountMinAttribute()
    {
        $priceNormal = '';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            $min = $variants->min('r_uni_product_variant_prices.more_discount_price');

            if($min) $priceNormal = formatRupiah(floatval($min));
        }
        return $priceNormal;
    }

    public function getDiscountMaxAttribute()
    {
        $discountMax = '0%';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if(collect($variants)->count() > 0)
                return $variants->max('r_uni_product_variant_prices.more_discount');
        }
        return $discountMax;
    }

    public function getDiscountMinAttribute()
    {
        $discountMin = '0%';
        if($this->getRelation('r_uni_product_variants')) {
            $variants = $this->getRelation('r_uni_product_variants');
            if(collect($variants)->count() > 0)
                return $variants->min('r_uni_product_variant_prices.more_discount');
        }
        return $discountMin;
    }

    public function getBrandUrlAttribute()
    {
        if($this->brand) {
            return config('app.cdn') .'MORE/brand/'.$this->brand.'.png';
        }
        return null;
    }

    public function getIsMultipleVariantAttribute()
    {
        if($this->variant_1_name && $this->variant_2_name) return true;
        return false;
    }

    public function getRMultipleVariantAttribute()
    {
        if($this->is_multiple_variant && count($this->getRelation('r_uni_product_variants')) > 0) {
            $variants = $this->getRelation('r_uni_product_variants');
            $variant1 = $variants->pluck('variant', 'variant')->groupBy('variant');
            $variant2 = $variants->pluck('color', 'color')->groupBy('color');
            return [
                [
                    'text' => $this->variant_1_name,
                    'column' => 'variant_1_name',
                    'detail' => $variant1['']
                ],
                [
                    'text' => $this->variant_2_name,
                    'column' => 'variant_2_name',
                    'detail' => $variant2['']
                ]
            ];
        }
        return [];
    }

    public function r_uni_categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function r_uni_product_applications()
    {
        return $this->hasOne(ProductApplication::class, 'uni_products_id');
    }

    public function r_uni_product_images()
    {
        return $this->hasMany(ProductImage::class, 'uni_products_id', 'id')->where(['status'=> 1,'type'=>'PRODUK'])->orderBy('no', 'ASC');
    }

    public function r_uni_product_infos()
    {
        return $this->hasMany(ProductImage::class, 'uni_products_id', 'id')->where(['status'=> 1,'type'=>'PRODUK-INFO'])->orderBy('no', 'ASC');
    }

    public function r_uni_product_details()
    {
        return $this->hasMany(ProductImage::class, 'uni_products_id', 'id')->where(['status'=> 1,'type'=>'PRODUK-DETAIL'])->orderBy('no', 'ASC');
    }

    public function r_uni_product_suasanas()
    {
        return $this->hasMany(ProductImage::class, 'uni_products_id', 'id')->where(['status'=> 1,'type'=>'SUASANA'])->orderBy('no', 'ASC');
    }

    public function r_uni_product_variants()
    {
        return $this->hasMany(ProductVariantStockView::class, 'uni_products_id', 'id')->where(['more_active'=> 1])/*->orderBy('id', 'ASC')*/; // hanya untuk MORE
    }

    public function r_uni_product_videos()
    {
        return $this->hasMany(ProductVideo::class, 'uni_products_id', 'id')->where(['status'=> 1])/*->orderBy('id', 'ASC')*/;
    }

    public function r_uni_product_raft_guides()
    {
        return $this->hasMany(ProductRaftGuide::class, 'uni_products_id', 'id')->where(['status'=> 1]);
    }

    public function r_uni_product_description_bundling()
    {
        return $this->hasMany(ProductDescriptionBundlingView::class, 'uni_products_id', 'id');
    }

    public function r_uni_product_image_bundling()
    {
        return $this->hasMany(ProductImageBundlingView::class, 'uni_products_id', 'id');
    }

    public function r_uni_product_categories()
    {
        return $this->hasMany(ProductCategory::class, 'uni_products_id', 'id');
    }

    public function r_more_product_reviews()
    {
        return $this->hasMany(ProductReview::class, 'uni_products_id');
    }

    public function r_uni_product_superiorities()
    {
        return $this->hasMany(ProductSuperiorities::class, 'uni_products_id', 'id');
    }

    public function r_uni_news_products()
    {
        return $this->hasMany(NewsProduct::class, 'uni_products_id');
    }

    public function r_uni_product_combo()
    {
        $now = Carbon::now();
        return $this->hasMany(ProductFree::class, 'uni_products_id')
            ->where('date_start', '<=', $now)
            ->where('date_end', '>=', $now)
            ->where('free_type', 'COMBO')
            ->where('remains', '>', 0)
            ->orderBy('combo_price', 'asc');
    }
}
