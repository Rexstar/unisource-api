<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'uni_gen_districts';
    protected $primaryKey = 'district_id';
    protected $keyType = 'string';
    public $timestamps = false;

    public function r_city()
    {
        return $this->hasOne(City::class, 'city_id', 'city_id');
    }

    public function r_villages()
    {
        return $this->hasMany(Village::class, 'district_id');
    }
}
