<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'uni_news';
    public $timestamps = false;

    public function r_uni_news_products()
    {
        return $this->hasMany(NewsProduct::class, 'uni_news_id');
    }
}
