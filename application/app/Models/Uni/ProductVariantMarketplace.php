<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductVariantMarketplace extends Model
{
    protected $table = 'uni_product_variant_marketplaces';
    public $timestamps = false;
}
