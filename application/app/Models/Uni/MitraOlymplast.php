<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class MitraOlymplast extends Model
{
    protected $table = 'mitra_olymplast';
    public $timestamps = false;



    public function r_kota()
    {
        $instance =$this->hasMany(MitraOlymplast::class,'wilayah','wilayah')->groupBy('wilayah','kota')->select('wilayah','kota');
        return $instance;

    }
}
