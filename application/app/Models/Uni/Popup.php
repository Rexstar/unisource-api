<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{
    protected $table = 'uni_popups';
    public $timestamps = false;
}
