<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class NewsTag extends Model
{
    protected $table = 'uni_news_tags';
    public $timestamps = false;

    public function r_uni_news()
    {
        return $this->hasOne(News::class, 'id','uni_news_id');
    }
}
