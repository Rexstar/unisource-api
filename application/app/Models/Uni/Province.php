<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'uni_gen_provinces';
    protected $primaryKey = 'province_id';
    protected $keyType = 'string';
    public $timestamps = false;

    public function r_cities()
    {
        return $this->hasMany(City::class, 'province_id');
    }
}
