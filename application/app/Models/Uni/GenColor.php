<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenColor extends Model
{
    protected $table = 'uni_gen_colors';
    public $timestamps = false;
}
