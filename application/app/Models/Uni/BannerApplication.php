<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class BannerApplication extends Model
{
    protected $table = 'uni_banner_applications';
    public $timestamps = false;
}
