<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductVariantMaterial extends Model
{
    protected $table = 'uni_product_variant_materials';
    public $timestamps = false;

    public function r_uni_gen_material()
    {
        return $this->hasOne(GenMaterial::class, 'id','uni_gen_materials_id');
    }
}
