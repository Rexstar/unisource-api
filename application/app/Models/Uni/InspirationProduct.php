<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class InspirationProduct extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'uni_inspiration_products';

    public $timestamps = false;

}
