<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'uni_categories';
    public $timestamps = false;

    protected $appends = [
        'image_url',
        'banner_url',
        'product_count'
    ];

    public function getImageUrlAttribute()
    {
        if($this->image)
            return config('app.cdn') . 'MORE/' . $this->image;
        return null;
    }

    public function getBannerUrlAttribute()
    {
        if($this->banner)
            return config('app.cdn') . 'MORE/' . $this->banner;
        return null;
    }

    public function getProductCountAttribute()
    {
        // if(collect($this->getRelations())->contains('r_uni_product_categories')) {
        //     return $this->getRelation('r_uni_product_categories')->count();
        // }
        // return 0;
        return ProductCategory::where('uni_categories_id', $this->id)->count();
    }

    public function r_uni_products()
    {
        return $this->belongsToMany(Product::class, 'uni_product_categories', 'uni_products_id', 'uni_categories_id');
    }

    public function r_uni_product_categories()
    {
        return $this->hasMany(ProductCategory::class, 'uni_categories_id');
    }

    public function r_uni_category_group_details()
    {
        return $this->hasMany(CategoryGroupDetail::class, 'uni_categories_id');
    }

    public function r_sub()
    {
        return $this->hasMany($this, 'uni_categories_id')->where('status', 1)->orderBy('sort','ASC');
    }

    public function r_subs()
    {
        return $this->r_sub()->with('r_subs');
    }

}
