<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class CategoryGroupDetail extends Model
{
    protected $table = 'uni_category_group_details';
    public $timestamps = false;

    public function r_uni_categories()
    {
        return $this->belongsTo(Category::class, 'uni_categories_id');
    }
}
