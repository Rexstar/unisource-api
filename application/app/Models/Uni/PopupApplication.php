<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class PopupApplication extends Model
{
    protected $table = 'uni_popup_applications';
    public $timestamps = false;
}
