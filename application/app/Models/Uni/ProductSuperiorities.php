<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductSuperiorities extends Model
{
    protected $table = 'uni_product_superiorities';
    public $timestamps = false;
}
