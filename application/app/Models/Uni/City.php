<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'uni_gen_cities';
    protected $primaryKey = 'city_id';
    protected $keyType = 'string';
    public $timestamps = false;

    public function r_province()
    {
        return $this->hasOne(Province::class, 'province_id', 'province_id');
    }

    public function r_districts()
    {
        return $this->hasMany(District::class, 'city_id');
    }
}
