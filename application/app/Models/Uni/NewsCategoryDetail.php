<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class NewsCategoryDetail extends Model
{
    protected $table = 'uni_news_categories_details';
    public $timestamps = false;

}
