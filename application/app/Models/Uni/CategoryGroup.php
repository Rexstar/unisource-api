<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class CategoryGroup extends Model
{
    protected $table = 'uni_category_groups';
    public $timestamps = false;

    protected $appends = [
        'image_url',
        'banner_url'
    ];

    public function getImageUrlAttribute()
    {
        if($this->image)
            return config('app.cdn') . 'MORE/' . $this->image;
        return null;
    }

    public function getBannerUrlAttribute()
    {
        if($this->banner)
            return config('app.cdn') . 'MORE/' . $this->banner;
        return null;
    }

    public function r_uni_category_group_details()
    {
        return $this->hasMany(CategoryGroupDetail::class, 'uni_category_group_id', 'id');
    }
}
