<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class InspirationRoom extends Model
{
    protected $table = 'uni_inspiration_rooms';
    public $timestamps = false;

    public function r_uni_inspirations()
    {
        return $this->belongsTo(Inspiration::class, 'uni_inspirations_id', 'id');
    }
}
