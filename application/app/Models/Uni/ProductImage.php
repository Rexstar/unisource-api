<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'uni_product_images';
    public $timestamps = false;

    protected $with = [
        'r_uni_products'
    ];

    protected $hidden = [
        'r_uni_products'
    ];

    protected $appends = [
        'image_url'
    ];

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class, 'uni_products_id', 'id');
    }

    public function getImageUrlAttribute()
    {
        $path = '';
        if($this->getRelation('r_uni_products')) {
            $path = $this->getRelation('r_uni_products')->path . "/";
        }

        $imagePath = null;
        $filename = $this->filename_webp;

        if(!$filename) {
            $filename = $this->filename_non_webp;
        }

        if(!$filename){
            $path = "images/";
            $filename = "noimage.png";
        }

        if($filename) {
            $imagePath = config('app.cdn') . $path . $filename;
        }


        return $imagePath;

    }
}
