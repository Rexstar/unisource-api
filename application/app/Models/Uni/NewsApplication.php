<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class NewsApplication extends Model
{
    protected $table = 'uni_news_applications';
    public $timestamps = false;
}
