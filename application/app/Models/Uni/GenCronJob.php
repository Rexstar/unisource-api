<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenCronJob extends Model
{
    protected $table = 'uni_gen_cronjob';
    public $timestamps = false;
}
