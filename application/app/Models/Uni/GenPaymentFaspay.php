<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenPaymentFaspay extends Model
{
    protected $table = 'uni_gen_payment_faspay';
    public $timestamps = false;

    protected $appends = [
        "icon_url",
    ];

    public function getIconUrlAttribute()
    {
        if($this->icon) {
            return config('app.cdn') .'MORE/payment/'.$this->icon;
        }
        return null;
    }
}
