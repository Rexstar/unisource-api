<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenMarketplace extends Model
{
    protected $table = 'uni_gen_marketplaces';
    public $timestamps = false;
}
