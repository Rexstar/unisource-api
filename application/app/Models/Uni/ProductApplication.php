<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductApplication extends Model
{
    protected $table = 'uni_product_applications';
    public $timestamps = false;

    public function r_uni_products()
    {
        return $this->belongsTo(Product::class, 'uni_products_id', 'id');
    }
}
