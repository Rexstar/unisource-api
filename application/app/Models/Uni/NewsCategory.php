<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $table = 'uni_news_categories';
    public $timestamps = false;

    protected $appends = [
        'image_url'
    ];

    public function getImageUrlAttribute()
    {
        $path = 'MORE/category_news/';

        $imagePath = null;
        $filename = $this->image;

        $imagePath = config('app.cdn') . $path . $filename;

        return $imagePath;

    }
}
