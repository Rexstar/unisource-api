<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'uni_rooms';
    public $timestamps = false;

    protected $appends = [
        'image_url'
    ];

    public function getImageUrlAttribute()
    {
        $path = 'MORE/asset/';

        $imagePath = null;
        $filename = $this->image;

        $imagePath = config('app.cdn') . $path . $filename;

        return $imagePath;

    }
}
