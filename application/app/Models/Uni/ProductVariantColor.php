<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class ProductVariantColor extends Model
{
    protected $table = 'uni_product_variant_colors';
    public $timestamps = false;
}
