<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;
use App\Models\UniView\InspirationProductView;
use App\Models\UniView\ProductPriceView;

class Inspiration extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'uni_inspirations';

    public $timestamps = false;

    protected $appends = [
        'image_url'
    ];

    public function r_uni_inspiration_product()
    {
        return $this->hasMany(InspirationProductView::class, 'uni_inspirations_id', 'id');
    }

    public function r_uni_inspiration_applications()
    {
        return $this->hasOne(InspirationApplication::class, 'uni_inspirations_id');
    }

    public function r_uni_inspiration_price()
    {
        return $this->hasOne(ProductPriceView::class, 'uni_products_id','uni_products_id');
    }

    public function getImageUrlAttribute()
    {
        $path = 'INSPIRASI/';

        $imagePath = null;
        $filename = $this->image;

        $imagePath = config('app.cdn') . $path . $filename;

        return $imagePath;

    }

}
