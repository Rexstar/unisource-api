<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class GenApplication extends Model
{
    protected $table = 'uni_gen_applications';
    public $timestamps = false;
}
