<?php

namespace App\Models\Uni;

use Illuminate\Database\Eloquent\Model;

class DeliveryArea extends Model
{
    protected $table = 'uni_delivery_areas';
    public $timestamps = false;
}
