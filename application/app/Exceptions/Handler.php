<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        // Bugsnag::notifyException($exception);
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        // dd($exception);
        $render = parent::render($request, $exception);

        $statusCode = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;
        $stackTrace = method_exists($exception, 'getTrace') ? $exception->getTrace() : '';
        $message  = method_exists($exception, 'getMessage') ? $exception->getMessage() : '';

        $env = config('app.env');

        // $data = $env !== 'production' ? [
        //     'message' => $message,
        //     'stackTrace' => collect($stackTrace)->take(2)
        // ] : (object)[];
        // return response_json($data, $statusCode);
        return response_json([
            'message' => $message,
            'stackTrace' => collect($stackTrace)->take(2)
        ], $statusCode);
    }
}
