<form method="POST" name="form" action="{{$link_post}}">
    @foreach ($req as $name => $value)
        <input type="hidden" name="{{$name}}" value="{{$value}}">
    @endforeach
</form>

<script>
    document.form.submit();
</script>
