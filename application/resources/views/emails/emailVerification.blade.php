<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5" style="table-layout:fixed;">
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    <tr>
        <td>
            <table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                <tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
                <tr>
                    <td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="{{config("app.web_url")}}"><img width="125" height="50" border="0" alt="MORE" src="{{ config("app.cdn") }}/MORE/asset/morelogo.png"></a>
                    </td>
                </tr>
                <tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#ffffff">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                                        <tr>
                                            <td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                                Hai, <strong>{{ $token['email'] }}</strong><br><br>kamu mendapatkan email ini karena kamu melakukan pendaftaran pada aplikasi MORE.<br><br> Lakukan verifikasi email dengan cara menekan tombol dibawah ini :
                                        </tr>
                                        <tr>
                                            <td height="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" width="100%" style="backline-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                    <a href="{{url($url)}}" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;border-radius: 8px;background:#364b7b;">VERIFIKASI EMAIL</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="30" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding-bottom: 20px;text-align: center">
                                    Link akan expired dalam <strong>{{ $token['expired_time'] }} MENIT</strong> pada <strong>{{ tglWaktuIndo($token['expired_date']) }} WIB</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#364b7b">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                &copy; {{ date('Y') }} MORE. All rights reserved
                                </td>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    </table>
