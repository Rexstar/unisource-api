<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5" style="table-layout:fixed;">
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    <tr>
        <td>
            <table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                <tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
                <tr>
                    <td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="#"><img width="125" height="50" border="0" alt="MORE" src="{{ config("app.cdn") }}/MORE/asset/morecustom.png"></a>
                    </td>
                </tr>
                <tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#ffffff">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                                        <tr>
                                            <td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                                Hai terdapat pelanggan yang menghubungi MORE CUSTOM.
                                        </tr>
                                        <tr>
                                            <td height="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            Nama Lengkap
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            <strong>{{ $project->name }}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            No Telepon
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            {{ $project->phone }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            Email
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            {{ $project->email }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            Lokasi
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            {{ $project->location }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            Alamat Lengkap
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            {{ $project->address }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            Jenis Properti
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            <strong>{{ $project->property_id }}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            Tipe Properti
                                                        </td>
                                                        <td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                            :
                                                        </td>
                                                        <td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                            <strong>{{ $project->property_type }}</strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="100%" style="line-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                <a href="https://wa.me/{{ $project->phone }}" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;border-radius: 8px;background:#364b7b;">Hubungi Pelanggan</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#364b7b">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                &copy; {{date("Y")}} MORE CUSTOM. All rights reserved
                                </td>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    </table>
