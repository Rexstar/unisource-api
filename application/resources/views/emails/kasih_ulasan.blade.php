<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5" style="table-layout:fixed;">
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    <tr>
        <td>
            <table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                <tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
                <tr>
                    <td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="{{config("app.web_url")}}"><img width="125" height="50" border="0" alt="MORE" src="{{ config("app.cdn") }}/MORE/asset/morelogo.png"></a>
                    </td>
                </tr>
                <tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#ffffff">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                                        <tr>
                                            <td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                                Hai <strong>{{ $user->name }}</strong>
                                                <br><br>
                                                Terima kasih sudah berbelanja dan memberikan ulasanmu mengenai produk Olympic Group.
                                                <br><br>
                                                Sebagai tanda apresiasi dan terima kasih, ada potongan
                                                <strong>Rp 10,000
                                                *tanpa minimum pembelian spesial untukmu</strong>. Kamu bisa menggunakan promo
                                                ini di <a href="https://morefurniture.id/" target="_blank">www.morefurniture.id</a> dengan memasukan kode promo "<strong>MOREREVIEW</strong>".
                                                <br><br>
                                                Saatnya kamu belanja furniture di More Furniture. Nikmati gratis ongkir 100% tanpa minimum pembelian, garansi produk 2x24 jam, hingga jaminan produk original.
                                                <br><br>
                                                Salam hangat,<br>
                                                <strong> More Furniture</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="100%" style="line-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                <a href="{{ config("app.web_url") }}/products" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;border-radius: 8px;background:#c13b37;margin-right:30px">Belanja Lagi</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                                Untuk informasi lebih lanjut, kamu bisa menghubungi team admin via Whatsapp <a href="https://api.whatsapp.com/send/?phone=+62812-3024-2365&text=Hai+MORE%2C+saya+ingin+bertanya:+&app_absent=0" target="_blank">di sini</a>.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#364b7b">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                &copy; {{ date('Y') }} MORE. All rights reserved
                                </td>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    </table>
