<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5" style="table-layout:fixed;">
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
<tr>
	<td>
		<table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
			<tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
			<tr>
				<td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="#"><img width="125" height="50" border="0" alt="MORE" src="{{ config("app.cdn") }}/MORE/asset/morelogo.png"></a>
				</td>
			</tr>
			<tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#ffffff">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
							<td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
									<tr>
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Hai <strong>{{ $user->name }},</strong><br><br>Segera lakukan pembayaran untuk pesananmu <strong>#{{ $order->ordersn }}</strong> paling lambat <strong>{{ $order->faspay_expired_date_indonesia }} WIB</strong>. Jika melebihi waktu tersebut maka pesanan kamu akan otomatis dibatalkan.
									</tr>
									<tr>
										<td height="24" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Nomor Pesanan
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{ $order->ordersn }}</strong>
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Tanggal Pemesanan
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{ $order->created_at_indonesia }} WIB
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Batas Pembayaran
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{ $order->faspay_expired_date_indonesia }}WIB
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Metode Pembayaran
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{ $order->faspay_name }}
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Nomor Virtual Account
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{ $order->faspay_trx_id }}
													</td>
												</tr>
                                                @php
                                                    $totalOngkir = 0;
                                                    foreach ($order->r_more_transaction_order_details[0]->r_more_transaction_order_products as $item)
                                                    {
                                                        $totalOngkir = $totalOngkir + $item->price_delivery;
                                                    }

                                                    if($totalOngkir == 0){
                                                        $labelOngkir = "Gratis Ongkir";
                                                    }else{
                                                        $labelOngkir = formatRupiah($totalOngkir);
                                                    }
                                                @endphp
                                                <tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Total Ongkir
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{ $labelOngkir }}</strong>
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Total Pembayaran
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{ $order->total_rp }}</strong>
													</td>
												</tr>
                                                @if ($order->dropshipper != 0)
                                                <tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Dropshipper
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">

													</td>
													<td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">

													</td>
												</tr>
                                                <tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Nama
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{ $order->dropshipper_name }}</strong>
													</td>
												</tr>
                                                <tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														No HP
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#c13b37;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{ $order->dropshipper_phone }}</strong>
													</td>
												</tr>
                                                @endif
											</table>
										</td>
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
											Rincian Produk :
										</td>
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
												<tr bgcolor="#364b7b">
													<th height="40" width="35%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
														Produk
													</th>
													<th width="20%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
														Harga Satuan
													</th>
													<th width="20%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
														Jumlah
													</th>
													<th width="20%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
														Sub Total
													</th>
												</tr>
												@foreach ($order->r_more_transaction_order_details[0]->r_more_transaction_order_products as $item)
													<tr>
														<td height="35" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;border-bottom:1px solid #cccccc;">
															{{ $item->r_uni_product_variant->name_long }}
														</td>
														<td height="35" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;border-bottom:1px solid #cccccc;text-align:right;">
															@if ($item->price_normal)
																<span style="display:block;color:#AAA;text-decoration:line-through;font-size:12px;line-height:12px;">{{ $item->price_normal_rp }}</span>
															@endif
															{{ $item->price_rp }}
														</td>
														<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;text-align:center;border-bottom:1px solid #cccccc;">
															{{ $item->quantity }}
														</td>
														<td height="35" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;border-bottom:1px solid #cccccc;text-align:right;">
															{{ formatRupiah($item->quantity * $item->price) }}
														</td>
													</tr>
												@endforeach
											</table>
										</td>
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td width="100%" style="line-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
											<a href="{{ config("app.web_url") }}/profile/order/{{ $order->ordersn }}" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;border-radius: 8px;background:#c13b37;">Bayar Sekarang</a>
											<a href="{{ config("app.web_url") }}/profile/order/{{ $order->ordersn }}" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;border-radius: 8px;background:#364b7b;">Lihat Transaksi</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#364b7b">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
							<td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
							&copy; {{date("Y")}} MORE. All rights reserved
							</td>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
</table>
