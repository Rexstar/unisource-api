<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5" style="table-layout:fixed;">
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    <tr>
        <td>
            <table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                <tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
                <tr>
                    <td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="#"><img width="125" height="50" border="0" alt="MORE" src="{{ config("app.cdn") }}/MORE/asset/morelogo.png"></a>
                    </td>
                </tr>
                <tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#ffffff">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                                        <tr>
                                            <td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                                Hai <strong>{{ $user->name }},</strong><br><br>
                                                MORE mau ingetin kamu nih, jangan sampai nyesel kehabisan.
                                                Yuk segera selesaikan transaksimu dan dapatkan gratis ongkir 100% tanpa minimum pembelian.
                                                Eitsss tidak hanya itu, khusus untukmu ada potongan langsung <strong>{{ formatRupiah($voucher->voucher) }}</strong> dengan kode promo <strong>{{ $voucher->code }}</strong>.

                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
                                                Rincian Produk :
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="24" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                                                    <tr bgcolor="#364b7b">
                                                        <th height="60%" width="35%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
                                                            Produk
                                                        </th>
                                                        <th width="20%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
                                                            Harga Normal
                                                        </th>
                                                        <th width="20%" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;vertical-align:middle;color:#ffffff;font-weight:bold;">
                                                            Harga Diskon
                                                        </th>
                                                    </tr>
                                                    @foreach ($product as $value)
                                                        <tr>
                                                            <td height="35" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;border-bottom:1px solid #cccccc;">
                                                                {{ $value['name'] }}
                                                            </td>
                                                            <td height="35" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;border-bottom:1px solid #cccccc;text-align:center;">
                                                                <span style="display:block;color:#AAA;text-decoration:line-through;font-size:12px;line-height:12px;">{{ $value['price_normal_min'] }}</span>
                                                            </td>
                                                            <td height="35" style="line-height:23px;font-size:15px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;vertical-align:middle;border-bottom:1px solid #cccccc;text-align:center;">
                                                                {{ $value['price_discount_min'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="100%" style="line-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
                                                <a href="{{ config("app.web_url") }}/wishlist" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;border-radius: 8px;background:#c13b37;">Belanja Sekarang</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                                Bingung? Yuk hubungi team MORE aja! <a href="https://api.whatsapp.com/send/?phone=+62812-3024-2365&text=Hai+MORE%2C+saya+ingin+bertanya:+&app_absent=0" target="_blank">di sini</a>.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
                <tr>
                    <td width="100%" bgcolor="#364b7b">
                        <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                            <tr>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                                <td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
                                &copy; {{date("Y")}} MORE. All rights reserved
                                </td>
                                <td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
    </table>
