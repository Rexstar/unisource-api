<?php

return [
    // ! ERROR STRING PADA VOUCHER
    'error' => `You can't use this voucher.`,
    'error_not_found' => 'Voucher not found.',
    'error_device' => 'This voucher is only can be used on :device Apps.',
    'error_limit_use' => 'Sorry, this voucher is on limit.',
    'error_over_use' => 'Voucher melebihi batas penggunaan.',
    'error_min_purchase' => 'Voucher harus dengan pembelian minimal :min.',
    'error_location' => 'Voucher tidak dapat digunakan pada lokasi Anda.',
    'error_payment_method' => 'Voucher tidak dapat digunakan untuk Metode Pembayaran Anda.',
    'error_cart' => 'Harap tambahkan dulu produk ke dalam cart.',
];
