<?php

return [
    // ! ERROR STRING PADA VOUCHER
    'error' => 'Voucher tidak dapat digunakan.',
    'error_not_found' => 'Voucher tidak ditemukan.',
    'error_device' => 'Voucher hanya dapat digunakan pada Aplikasi :device.',
    'error_limit_use' => 'Maaf, voucher sudah habis.',
    'error_over_use' => 'Voucher melebihi batas penggunaan.',
    'error_min_purchase' => 'Voucher harus dengan pembelian minimal :min.',
    'error_location' => 'Voucher tidak dapat digunakan pada lokasi Anda.',
    'error_payment_method' => 'Voucher tidak dapat digunakan untuk Metode Pembayaran Anda.',
    'error_cart' => 'Harap tambahkan dulu produk ke dalam cart.',
    'error_promotion' => 'Voucher tidak dapat digabungkan dengan Promo Lain.',
];
