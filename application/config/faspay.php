<?php

$env = env('APP_ENV', 'production');

return [
    'merchant_id' => $env == 'production' ? '33126' : env('FASPAY_MERCHANT_ID', '33126'),
    'merchant_user' => $env == 'production' ? 'bot33126' : env('FASPAY_MERCHANT_USER', 'bot33126'),
    'merchant_name' =>  $env == 'production' ? 'Eforia Furnitur' : env('FASPAY_MERCHANT_NAME', 'Eforia Furnitur'),
    'merchant_password' => $env == 'production' ? 'iP5Jdp2w' : 'p@ssw0rd',
    // 'link_post' => $env == 'production' ? 'https://web.faspay.co.id/' : 'https://dev.faspay.co.id/',
    'link_post' => $env == 'production' ? 'https://web.faspay.co.id/' : 'https://debit-sandbox.faspay.co.id/',


    // KARTU KREDIT
    'cc_merchant_id' => $env == 'production' ? 'morefurniture' : env('FASPAY_CC_MERCHANT_ID', 'tes_auto'),
    'cc_merchant_password' => $env == 'production' ? 'bojtb' : env('FASPAY_CC_MERCHANT_ID', 'abcde'),
    'cc_link_post' => $env == 'production' ? 'https://fpg.faspay.co.id/payment/' : 'https://fpg-sandbox.faspay.co.id/payment/',


    //DOKU
    'doku_client_id'=>$env == 'production' ? 'BRN-0231-1688719397704' : 'BRN-0225-1691046948132',
    'doku_secret_key'=>$env == 'production' ? 'SK-LBm1I7B8BU1yp1FmoGfj' : 'SK-QaMYTEDiL6oFivlhLzv1',
    'doku_end_point_va'=>$env == 'production' ? 'https://api.doku.com/mandiri-virtual-account/v2/merchant-payment-code' : 'https://api-sandbox.doku.com/mandiri-virtual-account/v2/merchant-payment-code',
    'doku_end_point_cc'=>$env == 'production' ? 'https://api.doku.com/credit-card/v1/payment-page' : 'https://api-sandbox.doku.com/credit-card/v1/payment-page',
];
