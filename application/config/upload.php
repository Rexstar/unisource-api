<?php
    $self = [];
    $self['path'] = [
        'user' => 'public/user/picture/',
        'review' => 'public/user/review/',
        'giveaway' => 'public/user/giveaway/',
        'complain' => 'public/user/complain/',
        'chats' => 'public/user/chats/',
        'group-product' => 'public/group-product/'
    ];
    $self['max_size']  = 5000;
    $self['image_types']  = 'jpeg,jpg,png,webp';
    $self['media_types']  = 'jpeg,jpg,png,webp,mp4,mkv,3gp,mp3';
    $self['doc_types']  = 'pdf,xls,xlsx,doc,docx,ppt,pptx,rar,zip,'.$self["image_types"];
    $self['image_validator'] ='mimes:'.$self['image_types'].'|max:'.$self["max_size"];
    $self['media_validator'] ='mimes:'.$self["media_types"].'|max:'.$self["max_size"];
    $self['doc_validator'] ='mimes:'.$self["doc_types"].'|max:'.$self["max_size"];
    return $self;
