<?php

return [
    'default' => 'postgres',
    'migrations' => 'migrations',
    'connections' => [
        'postgres' => [
            'driver'   => 'pgsql',
            'host'      => env('DB_HOST'),
            'database'  => env('DB_DATABASE'),
            'port'      => env('DB_PORT'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public'
        ],
        'postgres_erp' => [
            'driver'   => 'pgsql',
            'host'      => env('DB_HOST'),
            'database'  => 'efiserver',
            'port'      => env('DB_PORT'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public'
        ],
        'postgres_mitra' => [
            'driver'   => 'pgsql',
            'host'      => env('DB_HOST'),
            'database'  => 'og_dnamis',
            'port'      => env('DB_PORT'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public'
        ]
    ]
];
