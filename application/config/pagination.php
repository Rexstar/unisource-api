<?php

return [
    'per_page' => 10,

    'per_page_product' => 25,

    'per_page_order' => 5,

    'per_page_artikel' => 5,

    'per_page_giveaway' => 10,

    'per_page_address' => 100,
];
