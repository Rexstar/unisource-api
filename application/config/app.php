<?php

$env = env('APP_ENV', 'production');

return [
    'name' => 'More Api',

    'version' => '0.0.1' . ($env === 'production' ? '' : '-rc'),

    'env' => $env,

    'locale' => env('APP_LOCALE', 'id'),

    'timezone' => env('APP_TIMEZONE', 'Asia/Jakarta'),

    'fallback_locale' => 'en',

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    'cdn' => 'https://asset.morefurniture.id/',
    // 'cdn' => 'https://cdn.morefurniture.id/',

    'cdn_asset' => 'https://asset.morefurniture.id/',

    'web_url' => $env === 'production' ? 'https://morefurniture.id' : 'https://dev.morefurniture.id',
    // 'web_url' => ($env === 'production' ? 'https://morefurniture.id' : $env === 'development') ? 'https://dev.morefurniture.id' : 'http://localhost:8080',

    'app_url' => $env === 'production' ? 'https://api.morefurniture.id' : 'https://devapi.morefurniture.id',
    // 'app_url' => ($env === 'production' ? 'https://api.morefurniture.id' : $env === 'development' ) ? 'https://devapi.morefurniture.id' : 'http://more-v2-api.test',

    'email_admin' => 'more.officialcs@gmail.com',

    'apikey' => 'ErOm$2y$10$cQB7Nv1qTTX6A0oEY94u2oCTclJggQ5vSPg7KHIpCT4S4Eycm2',

    'separator' => 'MoreFurnitureId',

    'upload_path' =>[
        'review' => "public/reviews",
    ]
];
