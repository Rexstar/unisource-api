<?php

return [
    'api_key' => env('BUGSNAG_API_KEY', 'f8254e076d6fdfee4955e519c16661be'),

    'app_type' => 'HTTP',

    'release_stage' => env('APP_ENV', 'local'),

    'notify_release_stages' => [/* 'local', 'development',  */'production'],
];
