// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.6.8/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.6.8/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyCD41NdbE-gyNsyvFVF3n6RYKpziilxj-0",
    authDomain: "test-ussage-only.firebaseapp.com",
    projectId: "test-ussage-only",
    storageBucket: "test-ussage-only.appspot.com",
    messagingSenderId: "797029608883",
    appId: "1:797029608883:web:b164786d8e244786c76fc6",
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.onMessage(function(payload) {
    console.log("TEST")
    console.log("on Message", payload);
});